<?php

/*
 * This file is part of the Shepherd project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

defined('APPLICATION_UNIQUE_KEY') or define('APPLICATION_UNIQUE_KEY', '6d6a9ecbd0ea8898de500a0f480d7d44032b0408');
defined('LICENSE_KEY') or define('LICENSE_KEY', 'nup6z94j2gzo52o96ak1ultgs79xu9h5aassr2km/169461');
defined('APPLICATION_MODE') or define('APPLICATION_MODE', 'production'); // set to 'development' if you need to debug installer
defined('CONFIG_PATH') or define('CONFIG_PATH', __DIR__);
defined('ROOT') or define('ROOT', dirname(CONFIG_PATH) . '/activecollab');
defined('ROOT_URL') or define('ROOT_URL', 'http://activecollab.dev/public');
defined('FORCE_ROOT_URL') or define('FORCE_ROOT_URL', false);
require_once CONFIG_PATH . '/version.php';
require_once CONFIG_PATH . '/defaults.php';
