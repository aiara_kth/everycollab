<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Default configuration values.
 *
 * @package ActiveCollab.resources
 */
const APPLICATION_NAME = 'ActiveCollab';
const APPLICATION_BUILD = '%APPLICATION-BUILD%';

define('APPLICATION_PATH', ROOT . '/' . APPLICATION_VERSION); // If we are using unpacked file, make sure that value is well set

defined('APPLICATION_MODE') or define('APPLICATION_MODE', 'production');
defined('ANGIE_PATH') or define('ANGIE_PATH', APPLICATION_PATH . '/angie');
defined('APPLICATION_UNIQUE_KEY') or define('APPLICATION_UNIQUE_KEY', LICENSE_KEY);
if (!defined('WAREHOUSE_URL')) {
    define(
        'WAREHOUSE_URL',
        APPLICATION_MODE === 'production' ? 'https://warehouse.activecollab.com' : 'http://warehouse.dev:8080'
    );
}
defined('SHEPHERD_URL') or define('SHEPHERD_URL', 'http://localhost:8888');
defined('SHEPHERD_ACCESS_TOKEN') or define('SHEPHERD_ACCESS_TOKEN', 'access_token');
defined('SHEPHERD_IDP_ENDPOINT') or define('SHEPHERD_IDP_ENDPOINT', SHEPHERD_URL.'/api/v2/idp-authenticate');
defined('SHEPHERD_SAML_CRT') or define('SHEPHERD_SAML_CRT', CONFIG_PATH.'/saml.crt');
defined('SHEPHERD_SAML_KEY') or define('SHEPHERD_SAML_KEY', CONFIG_PATH.'/saml.key');
defined('IDP_NEW_SHEPHERD') or define('IDP_NEW_SHEPHERD', true);

if (defined('IS_ON_DEMAND') && IS_ON_DEMAND) {
    if (empty(getenv('PASSWORD_CRYPT_HASH'))
        && !(defined('PASSWORD_CRYPT_HASH') && PASSWORD_CRYPT_HASH)) {
        throw new InvalidArgumentException('Env PASSWORD_CRYPT_HASH is missing.');
    }

    if (!defined('PASSWORD_CRYPT_HASH')) {
        define('PASSWORD_CRYPT_HASH', getenv('PASSWORD_CRYPT_HASH'));
    }
}

// ---------------------------------------------------
//  Defaults MVC mapping
// ---------------------------------------------------

const DEFAULT_CONTROLLER = 'backend';

// ---------------------------------------------------
//  Frontend defaults
// ---------------------------------------------------

defined('FRONTEND_PATH') or define('FRONTEND_PATH', APPLICATION_PATH . '/frontend');

// ---------------------------------------------------
//  Load framework default configuration
// ---------------------------------------------------

require_once ANGIE_PATH . '/defaults.php';
