<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Introduce mention tag.
 *
 * @package angie.migrations
 */
class MigrateIntroduceMentionTag extends AngieModelMigration
{
    /**
     * Migrate up.
     */
    public function up()
    {
        $config_option_name = 'whitelisted_tags';
        $whitelisted_tags = $this->getConfigOptionValue($config_option_name);
        $whitelisted_tags['visual_editor']['span'][] = 'object-id';
        $this->setConfigOptionValue($config_option_name, $whitelisted_tags);
    }
}
