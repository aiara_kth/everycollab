<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Routing context implementation.
 *
 * @package angie.frameworks.environment
 * @subpackage models
 */
trait IRoutingContextImplementation
{
    /**
     * Return URL from cache, or assemble it, cache it and than return it.
     *
     * @param  string       $route_extension
     * @param  Closure|null $url_modifier
     * @param  bool         $url_modififer_condition
     * @return string
     */
    protected function getSubrouteUrlFromCache($route_extension, $url_modifier = null, $url_modififer_condition = true)
    {
        $url = AngieApplication::cache()->getByObject($this, ['urls', $route_extension], function () use ($route_extension) {
            return Router::assemble($this->getRoutingContext() . '_' . $route_extension, $this->getRoutingContextParams(), [
                'cache' => false, // Don't cache globally @TODO
            ]);
        });

        if ($url_modififer_condition && $url_modifier instanceof Closure) {
            $modified_url = $url_modifier->__invoke($url);

            if ($modified_url) {
                return $modified_url;
            }
        }

        return $url;
    }

    // ---------------------------------------------------
    //  We need routing context
    // ---------------------------------------------------

    /**
     * Return routing context name.
     *
     * @return string
     */
    abstract public function getRoutingContext();

    /**
     * Returing routing context params.
     *
     * @return array
     */
    abstract public function getRoutingContextParams();
}
