<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

use Angie\Error;

/**
 * Route assemble error.
 *
 * This error is thrown when we fail to assembe URL based on default values
 * and provided data
 *
 * @package angie.library.router
 * @subpackage errors
 */
class AssembleURLError extends Error
{
    /**
     * @param string $route_string
     * @param array  $assembly_data
     * @param array  $default_data
     * @param string $part
     * @param string $message
     */
    public function __construct($route_string, $assembly_data, $default_data = null, $part = null, $message = null)
    {
        if (empty($message)) {
            $message = "Failed to assemble '$route_string' based on provided data";
        }

        parent::__construct($message, ['route_string' => $route_string, 'assembly_data' => $assembly_data, 'default_data' => $default_data, 'part' => $part]);
    }
}
