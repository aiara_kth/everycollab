<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

use Angie\Error;

/**
 * Route not defined error.
 *
 * @package angie.library.router
 * @subpackage errors
 */
class RouteNotDefinedError extends Error
{
    /**
     * Construct route not defined error instance.
     *
     * @param  string               $name
     * @param  string               $message
     * @return RouteNotDefinedError
     */
    public function __construct($name, $message = null)
    {
        if ($message === null) {
            $message = "Route '$name' is not defined";
        }

        parent::__construct($message, [
            'name' => $name,
        ]);
    }
}
