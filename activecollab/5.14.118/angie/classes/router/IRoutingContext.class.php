<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Interface that implement all objects that are subrouting context.
 *
 * @package angie.library.routing
 */
interface IRoutingContext
{
    /**
     * Return routing context name.
     *
     * @return string
     */
    public function getRoutingContext();

    /**
     * Return routing context parameters.
     *
     * @return mixed
     */
    public function getRoutingContextParams();
}
