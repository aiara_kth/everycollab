<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

use Angie\Error;

/**
 * Router.
 *
 * Router provides support for canonical, pretty URL-s out of box. Reuqest is
 * matched with set of routes mapped by the user; when router finds first match
 * it will use data collected from it and match process will be stoped. Routes
 * are matched in reveresed order so make sure that general routes are on top
 * of the map list
 *
 * @package angie.library.router
 */
final class Router
{
    // Config
    const REGEX_DELIMITER = '#';
    const URL_VARIABLE = ':';

    // Common matches
    const MATCH_ID = '\d+';
    const MATCH_WORD = '\w+';
    const MATCH_SLUG = '[a-z0-9\-\._]+';
    const MATCH_HASH = '[a-z0-9]{40}';
    const MATCH_DATE = '([0-9]{2,4})-([0-1][0-9])-([0-3][0-9])';

    // Name of the compiled
    const COMPILED_FILE_PREFIX = '_router';

    /**
     * Hash that's used to test whether we have new modules.
     *
     * @var string
     */
    private static $control_hash;

    /**
     * Enable or desiable assembled routes caching.
     *
     * @var bool
     */
    private static $cache_assembled_routes = true;

    /**
     * Name of the compiled match function.
     *
     * @var string
     */
    private static $compiled_match_function;

    /**
     * Name of the compiled assemble function.
     *
     * @var string
     */
    private static $compiled_assemble_function;

    /**
     * Array of mapped routes.
     *
     * @var array
     */
    private static $routes = [];

    /**
     * Current module.
     *
     * Used by loadByModules function to remember the name of current module.
     * When this value is present, by no 'module' is defined in route
     * definition, map() method will use this value
     *
     * @var string
     */
    private static $current_module = null;

    /**
     * Parsed params from current url.
     *
     * @var array
     */
    private static $url_params = [];

    /**
     * Initialize route.
     *
     * @param AngieFramework[] $frameworks
     * @param AngieModule[]    $modules
     * @param bool             $validate_caches
     */
    public static function init(&$frameworks, &$modules, $validate_caches)
    {
        if ($validate_caches) {
            self::validateCaches(); // Make sure that caches are valid in environment where ROOT_URL can change
        }

        self::$control_hash = [AngieApplication::getVersion()]; // Make sure that version is taken into account when compiled caches are used

        $max_mtime = 0;

        foreach ($frameworks as $framework) {
            self::$control_hash[] = $framework->getName();

            $definition_mtime_file = filemtime($framework->getPath() . '/' . get_class($framework) . '.php');

            if ($definition_mtime_file > $max_mtime) {
                $max_mtime = $definition_mtime_file;
            }
        }

        foreach ($modules as $module) {
            self::$control_hash[] = $module->getName();

            $definition_mtime_file = filemtime($module->getPath() . '/' . get_class($module) . '.php');

            if ($definition_mtime_file > $max_mtime) {
                $max_mtime = $definition_mtime_file;
            }
        }

        self::$control_hash = sha1(implode(',', self::$control_hash));

        // ---------------------------------------------------
        //  Check if we need to recompile
        // ---------------------------------------------------

        $match_file = self::getCompiledMatchFilePath();
        $assemble_file = self::getCompiledAssembleFilePath();

        if (is_file($match_file) && is_file($assemble_file)) {
            $match_file_mtime = filemtime($match_file);
            $assemble_file_mtime = filemtime($assemble_file);

            $recompile = $match_file_mtime < $max_mtime || $assemble_file_mtime < $max_mtime;
        } else {
            $recompile = true;
        }

        // ---------------------------------------------------
        //  Load definitions and recompile, if needed
        // ---------------------------------------------------

        if ($recompile) {
            foreach ($frameworks as $framework) {
                self::$current_module = $framework->getName();
                $framework->defineRoutes();
            }

            foreach ($modules as $module) {
                self::$current_module = $module->getName();
                $module->defineRoutes();
            }

            self::$current_module = null;

            self::recompile();
        }
    }

    /**
     * Make sure that caches are valid.
     */
    private static function validateCaches()
    {
        $root_url_from_cache = AngieApplication::cache()->get('root_url');

        // If ROOT_URL changed, clean up cached routes
        if ($root_url_from_cache && $root_url_from_cache != ROOT_URL) {
            AngieApplication::cache()->clear();
            self::cleanUpCache(true);
        }

        AngieApplication::cache()->set('root_url', ROOT_URL);
    }

    /**
     * Clean up routing cache.
     *
     * @param bool $full
     */
    public static function cleanUpCache($full = false)
    {
        if ($full) {
            DB::execute('DELETE FROM routing_cache');
        } else {
            DB::execute('DELETE FROM routing_cache WHERE last_accessed_on < ?', DateTimeValue::makeFromString('-30 days'));
        }
    }

    /**
     * Return compiled match file path.
     *
     * @return string
     */
    private static function getCompiledMatchFilePath()
    {
        return COMPILE_PATH . '/' . self::COMPILED_FILE_PREFIX . '_match_' . self::$control_hash . '.php';
    }

    /**
     * Return compiled assemble file path.
     *
     * @return string
     */
    private static function getCompiledAssembleFilePath()
    {
        return COMPILE_PATH . '/' . self::COMPILED_FILE_PREFIX . '_assemble_' . self::$control_hash . '.php';
    }

    /**
     * Recompile match and and assemble routes.
     */
    public static function recompile()
    {
        self::recompileMatch();
        self::recompileAssemble();

        self::$compiled_match_function = null;
        self::$compiled_assemble_function = null;
    }

    /**
     * Recompile match file.
     */
    private static function recompileMatch()
    {
        $match_file_path = self::getCompiledMatchFilePath();

        $handle = fopen($match_file_path, 'w');
        if ($handle) {
            fwrite($handle, "<?php\n  \n");

            fwrite($handle, '  return function ($path, $query_string) {' . "\n");
            fwrite($handle, '    $matches = null;' . "\n");

            /** @var Route[] $routes */
            $routes = array_reverse(self::$routes);

            $counter = 0;
            foreach ($routes as $route_name => $route) {
                ++$counter;

                if ($counter == 1) {
                    fwrite($handle, '    if (preg_match(' . var_export($route->getRegularExpression(), true) . ', $path, $matches)) {' . "\n");
                } else {
                    fwrite($handle, '    } elseif (preg_match(' . var_export($route->getRegularExpression(), true) . ', $path, $matches)) {' . "\n");
                }

                $name = var_export($route->getName(), true);
                $route_string = var_export($route->getRouteString(), true);

                $defaults = var_export($route->getDefaults(), true);
                if (strpos($defaults, "\n") !== false) {
                    $defaults = explode("\n", $defaults);
                    foreach ($defaults as $k => $v) {
                        $defaults[$k] = trim($v);
                    }

                    $defaults = implode(' ', $defaults);
                }

                $parameters = $route->getNamedParameters();

                if (count($parameters)) {
                    $parameters = var_export($parameters, true);
                } else {
                    $parameters = 'array()';
                }

                fwrite($handle, '      return Router::doMatch($path, ' . $name . ', ' . $route_string . ', ' . $defaults . ', ' . $parameters . ', $matches, $query_string);' . "\n");
            }

            fwrite($handle, "    }\n\n");
            fwrite($handle, "    return false;\n");
            fwrite($handle, '  };');

            fclose($handle);

            self::applyDirGroupAndOwner($match_file_path);
        } else {
            throw new FileCreateError($match_file_path);
        }
    }

    /**
     * Recompile assemble file.
     */
    private static function recompileAssemble()
    {
        $assemble_file_path = self::getCompiledAssembleFilePath();

        $handle = fopen($assemble_file_path, 'w');
        if ($handle) {
            fwrite($handle, "<?php\n  \n");

            fwrite($handle, '  return function ($name, $data, $url_base, $query_arg_separator, $anchor) {' . "\n");
            fwrite($handle, '    switch ($name) {' . "\n");

            foreach (self::$routes as $route_name => $route) {
                fwrite($handle, "      case '$route_name':\n");

                $name = var_export($route->getName(), true);
                $route_string = var_export(trim($route->getRouteString(), '/'), true);

                $defaults = var_export($route->getDefaults(), true);
                if (strpos($defaults, "\n") !== false) {
                    $defaults = explode("\n", $defaults);
                    foreach ($defaults as $k => $v) {
                        $defaults[$k] = trim($v);
                    }

                    $defaults = implode(' ', $defaults);
                }

                $requiremetns = is_foreachable($route->getRequirements()) ? var_export($route->getRequirements(), true) : 'array()';

                fwrite($handle, '        return Router::doAssemble(' . $name . ', ' . $route_string . ', ' . $defaults . ', $data, $url_base, $query_arg_separator, $anchor);' . "\n");
            }

            fwrite($handle, "      default:\n");
            fwrite($handle, "        return '';\n");
            fwrite($handle, "    }\n");
            fwrite($handle, "  };\n");

            fclose($handle);

            self::applyDirGroupAndOwner($assemble_file_path);
        } else {
            throw new FileCreateError($assemble_file_path);
        }
    }

    /**
     * Make sure that file that's on $file_path inherits group and owner from parent directory.
     *
     * @param  string       $file_path
     * @throws FileDnxError
     */
    private static function applyDirGroupAndOwner($file_path)
    {
        if (empty($file_path) || !file_exists($file_path)) {
            throw new FileDnxError($file_path);
        }

        if (DIRECTORY_SEPARATOR != '\\') {
            $file_stats = stat($file_path);
            $dir_stats = stat(dirname($file_path));

            if ($file_stats['gid'] != $dir_stats['gid']) {
                chgrp($file_path, $dir_stats['gid']);
            }

            if ($file_stats['uid'] != $dir_stats['uid']) {
                chown($file_path, $dir_stats['uid']);
            }
        }
    }

    /**
     * Map resource.
     *
     * Settings:
     *
     * - path - route path, converts underscors to - (team_members -> team-members)
     * - controller_name - name of the controller, defaults to resource name (team_members)
     * - id - name of the ID field, defaults to singular of resource name + '_id' (team_member_id)
     * - id_format - pattern that ID value needs to match, default is number
     *
     * @param string       $resource_name
     * @param array        $settings
     * @param Closure|null $extend
     */
    public static function mapResource($resource_name, $settings = [], $extend = null)
    {
        $id = empty($settings['id']) ? Angie\Inflector::singularize($resource_name) . '_id' : $settings['id'];
        $module = empty($settings['module']) ? self::$current_module : $settings['module'];
        $controller = empty($settings['controller']) ? $resource_name : $settings['controller'];
        $id_format = empty($settings['id_format']) ? self::MATCH_ID : $settings['id_format'];

        $c = [
            'name' => $resource_name,
            'path' => empty($settings['collection_path']) ? str_replace('_', '-', $resource_name) : $settings['collection_path'],
            'module' => $module,
            'controller' => $controller,
            'actions' => empty($settings['collection_actions']) ? self::collection() : $settings['collection_actions'],
            'requirements' => empty($settings['collection_requirements']) || !is_array($settings['collection_requirements']) ? null : $settings['collection_requirements'],
        ];

        $s = [
            'name' => Angie\Inflector::singularize($c['name']),
            'path' => empty($settings['single_path']) ? "$c[path]/:$id" : $settings['single_path'],
            'module' => $module,
            'controller' => $controller,
            'actions' => empty($settings['single_actions']) ? self::single() : $settings['single_actions'],
            'requirements' => empty($c['requirements']) ? [$id => $id_format] : array_merge($c['requirements'], [$id => $id_format]),
        ];

        self::map($c['name'], $c['path'], ['module' => $c['module'], 'controller' => $c['controller'], 'action' => $c['actions']], $c['requirements']);
        self::map($s['name'], $s['path'], ['module' => $s['module'], 'controller' => $s['controller'], 'action' => $s['actions']], $s['requirements']);

        if ($extend instanceof Closure) {
            call_user_func($extend, $c, $s);
        }
    }

    /**
     * Return actions for a collection.
     *
     * @return array
     */
    public static function collection()
    {
        return ['GET' => 'index', 'POST' => 'add'];
    }

    /**
     * Return actions that work with resource instances.
     *
     * Router::single(); // Returns all
     * Router::single('GET', 'DELETE'); // Returns only actions for specified methods
     *
     * @return array
     */
    public static function single()
    {
        $default = ['GET' => 'view', 'PUT' => 'edit', 'DELETE' => 'delete'];

        if (func_num_args()) {
            $result = [];

            foreach (func_get_args() as $arg) {
                if (isset($default[$arg])) {
                    $result[$arg] = $default[$arg];
                }
            }

            return $result;
        }

        return $default;
    }

    /**
     * Regiter a new route.
     *
     * This function will create a new route based on route string, default
     * values and additional requirements and save  it under specific name. Name
     * is used so you can access the route when assembling URL based on a given
     * route. Name needs to be unique (if route with a given name is already
     * registered it will be overwriten).
     *
     * @param  string $name
     * @param  string $route
     * @param  array  $defaults
     * @param  array  $requirements
     * @return Route
     */
    public static function map($name, $route, $defaults = null, $requirements = null)
    {
        if ($defaults) {
            if (!isset($defaults['module'])) {
                $defaults['module'] = self::$current_module;
            }
        } else {
            $defaults = ['module' => self::$current_module];
        }

        return self::$routes[$name] = new Route($name, $route, $defaults, $requirements);
    }

    /**
     * Does same thing as match, but as input parameter it uses $url which is parsed.
     *
     * @param  string       $url
     * @return bool
     * @throws RoutingError
     */
    public static function matchUrl($url)
    {
        $parsed_url = self::parseUrl($url);

        return self::match($parsed_url[0], $parsed_url[1]);
    }

    // ---------------------------------------------------
    //  Compiler
    // ---------------------------------------------------

    /**
     * Extracts path_info & query_string from some url.
     *
     * @param  string $url
     * @return array
     * @throws Error
     */
    public static function parseUrl($url)
    {
        $query_string = '';

        if (strpos($url, 'index.php/') !== false) {
            // http://afiveone.activecollab.net/public/index.php/reports/assignments?pero=sara
            $path_info_keyword = 'index.php/';
            $question_mark_pos = strpos($url, '?');

            $path_info_start = strpos($url, $path_info_keyword) + strlen($path_info_keyword);

            if ($question_mark_pos !== false) {
                $path_info = substr($url, $path_info_start, $question_mark_pos - $path_info_start);
            } else {
                $path_info = substr($url, $path_info_start);
            }

            if ($question_mark_pos !== false) {
                $query_string = substr($url, $question_mark_pos + 1);
            }
        } elseif (strpos($url, 'index.php?') !== false) {
            // http://afiveone.activecollab.net/public/index.php?path_info=reports/assignments&pero=sara
            parse_str(parse_url($url, PHP_URL_QUERY), $parsed_query_string);
            $path_info = isset($parsed_query_string['path_info']) ? $parsed_query_string['path_info'] : null;

            $question_mark_pos = strpos($url, '?');
            if ($question_mark_pos !== false) {
                $query_string = substr($url, $question_mark_pos + 1);
            }
        } else {
            // http://afiveone.activecollab.net/reports/assignments?pero=sara
            if (strpos($url, ROOT_URL) !== 0) {
                throw new Error('Url is invalid');
            }

            $useful_part = substr($url, strlen(ROOT_URL));
            if ($useful_part[0] == '/') {
                $useful_part = substr($useful_part, 1);
            }

            $question_mark_pos = strpos($useful_part, '?');

            if ($question_mark_pos !== false) {
                $path_info = substr($useful_part, 0, $question_mark_pos);
            } else {
                $path_info = $useful_part;
            }

            if ($path_info[strlen($path_info) - 1] == '/') {
                $path_info = substr($path_info, 0, -1);
            }

            if ($question_mark_pos !== false) {
                $query_string = substr($useful_part, $question_mark_pos + 1);
            }
        }

        return [$path_info, $query_string];
    }

    /**
     * Match request string agains array of mapped routes.
     *
     * This function will loop request string agains array of mapped routes. As
     * soon as request string is matched looping is stopped and result of route
     * match method is returned (array of name => value pairs). In case that
     * none of the mapped routes does not match request string RoutingError will
     * be thrown
     *
     * @param  string               $str
     * @param  string               $query_string
     * @param  bool                 $use_cache
     * @return bool
     * @throws RoutingError
     * @throws InvalidInstanceError
     */
    public static function match($str, $query_string, $use_cache = true)
    {
        $str = trim($str, '/');

        $used_request_from_cache = $use_cache ? self::getFromCache($str, $query_string) : null;

        if (empty($used_request_from_cache)) {
            self::requireMatchFunction();

            if (self::$compiled_match_function instanceof Closure) {
                $route_matched = call_user_func(self::$compiled_match_function, $str, $query_string);

                if ($route_matched === false) {
                    throw new RoutingError($str);
                }
            } else {
                throw new InvalidInstanceError('compiled_match_function', self::$compiled_match_function, 'Closure');
            }
        }

        return true;
    }

    /**
     * Return from cache.
     *
     * @param  string $str
     * @param  string $query_string
     * @return bool
     */
    private static function getFromCache($str, $query_string)
    {
        $row = DB::executeFirstRow('SELECT id, name, content FROM routing_cache WHERE path_info = ?', $str);

        if ($row) {
            DB::execute('UPDATE routing_cache SET last_accessed_on = UTC_TIMESTAMP() WHERE id = ?', $row['id']);

            $values = $row['content'] ? unserialize($row['content']) : [];

            if ($query_string) {
                self::doProcessQueryString($values, $query_string);
            }

            self::$url_params = $values;

            return true;
        }

        return false;
    }

    /**
     * Process query string.
     *
     * @param array  $values
     * @param string $query_string
     */
    private static function doProcessQueryString(&$values, $query_string)
    {
        $reserved = ['module', 'controller', 'action'];

        $query_string_parameters = [];
        parse_str($query_string, $query_string_parameters);

        if (is_foreachable($query_string_parameters)) {
            foreach ($query_string_parameters as $parameter_name => $parameter_value) {
                if (isset($values[$parameter_name]) && in_array($values[$parameter_name], $reserved)) {
                    continue;
                }

                $values[$parameter_name] = $parameter_value;
            }
        }
    }

    /**
     * Require compiled match function.
     */
    private static function requireMatchFunction()
    {
        if (empty(self::$compiled_match_function)) {
            $compiled_match_file = self::getCompiledMatchFilePath();

            if (!is_file($compiled_match_file)) {
                self::recompileMatch();
            }

            self::$compiled_match_function = include $compiled_match_file;
        }
    }

    /**
     * Assemble route from query string.
     *
     * @param  string               $string
     * @return string
     * @throws RouteNotDefinedError
     */
    public static function assembleFromString($string)
    {
        $params = parse_string(substr($string, 1));

        $route = isset($params['route']) ? $params['route'] : null;
        if (empty($route)) {
            throw new RouteNotDefinedError($route);
        }
        unset($params['route']);

        return self::assemble($route, $params);
    }

    /**
     * Assemble URL.
     *
     * Supported options:
     *
     * - url_base (string): base for URL-s, default is an empty string
     * - query_arg_separator (string): what to use to separate query string
     *   arguments, default is '&'
     * - anchor (string): name of the URL anchor
     *
     * @param  string               $name
     * @param  array                $data
     * @param  array                $options
     * @throws AssembleURLError
     * @throws InvalidInstanceError
     * @return string
     */
    public static function assemble($name, $data = null, $options = null)
    {
        if (!is_array($data)) {
            $data = empty($data) ? [] : ['id' => $data];
        }

        // Performance note: If we cache rotes with two parameters, we increase
        // memory usage without any significant speed gain on top level pages, but
        // pages that display a lot of project level data get small execution time
        // increase (around 0.01 seconds) and significant memory usage reduction
        // (down for 3-5MB)

        if (self::$cache_assembled_routes) {
            switch (count($data)) {
                case 0:
                    $cache_name = 'simple_routes';
                    $cache_key = $name;
                    break;
                case 1:
                    $cache_name = $name . '_routes';
                    $cache_key = first($data);
                    break;
                case 2:
                    $cache_name = $name . '_routes';

                    ksort($data);

                    $first = true;
                    foreach ($data as $k => $v) {
                        if ($first) {
                            $cache_name .= "_{$k}_{$v}";
                        } else {
                            $cache_key = "{$k}_{$v}";
                        }

                        $first = false;
                    }

                    break;
            }
        }

        if (isset($cache_name) && isset($cache_key)) {
            $cached_values = AngieApplication::cache()->get($cache_name);

            if (is_array($cached_values)) {
                if (isset($cached_values[$cache_key])) {
                    return $cached_values[$cache_key];
                }
            } else {
                $cached_values = [];
            }
        }

        $url_base = $options && isset($options['url_base']) && $options['url_base'] ? $options['url_base'] : URL_BASE;
        $query_arg_separator = $options && isset($options['query_arg_separator']) && $options['query_arg_separator'] ? $options['query_arg_separator'] : '&';
        $anchor = $options && isset($options['anchor']) && $options['anchor'] ? $options['anchor'] : '';

        self::requireAssembleFunction();

        if (self::$compiled_assemble_function instanceof Closure) {
            $result = call_user_func(self::$compiled_assemble_function, $name, $data, $url_base, $query_arg_separator, $anchor);

            if (empty($result)) {
                throw new AssembleURLError($name, $data);
            }

            if (isset($cache_name) && isset($cache_key)) {
                $cached_values[$cache_key] = $result;
                AngieApplication::cache()->set($cache_name, $cached_values);
            }

            return $result;
        } else {
            throw new InvalidInstanceError('compiled_assemble_function', self::$compiled_assemble_function, 'Closure');
        }
    }

    /**
     * Require assemble function.
     */
    private static function requireAssembleFunction()
    {
        if (empty(self::$compiled_assemble_function)) {
            $compiled_assemble_file = self::getCompiledAssembleFilePath();

            if (!is_file($compiled_assemble_file)) {
                self::recompileAssemble();
            }

            self::$compiled_assemble_function = include $compiled_assemble_file;
        }
    }

    /**
     * Clean router, mostly used in tests.
     */
    public static function cleanUp()
    {
        self::$routes = [];
    }

    /**
     * Do match route.
     *
     * Method used by compiled match script to extract the data when URL is
     * matched
     *
     * @param string $path_info
     * @param string $name
     * @param string $route
     * @param array  $defaults
     * @param array  $parameters
     * @param array  $matches
     * @param string $query_string
     */
    public static function doMatch($path_info, $name, $route, $defaults, $parameters, $matches, $query_string)
    {
        $values = $defaults;

        // Match variables from path
        $index = 0;
        foreach ($parameters as $parameter_name) {
            ++$index;

            if ($parameter_name == 'id' || str_ends_with($parameter_name, '_id')) {
                $values[$parameter_name] = (int) $matches[$index];
            } else {
                $values[$parameter_name] = $matches[$index];
            }
        }

        DB::execute('REPLACE INTO routing_cache (path_info, name, content, last_accessed_on) VALUES (?, ?, ?, UTC_TIMESTAMP())', $path_info, $name, serialize($values));

        // Match variables from query string
        if ($query_string) {
            self::doProcessQueryString($values, $query_string);
        }

        self::$url_params = $values;
    }

    /**
     * Do assemble route based on given parameters.
     *
     * This function is called by compiled assemble script
     *
     * @param  string           $name
     * @param  string           $route
     * @param  array            $defaults
     * @param  array            $data
     * @param  string           $url_base
     * @param  string           $query_arg_separator
     * @param  string           $anchor
     * @return string
     * @throws AssembleURLError
     */
    public static function doAssemble($name, $route, $defaults, $data, $url_base, $query_arg_separator, $anchor)
    {
        $path_parts = $query_parts = $part_names = [];

        // Prepare path param
        foreach (explode('/', $route) as $key => $part) {
            if (substr($part, 0, 1) == ':') {
                $part_name = substr($part, 1);
                $part_names[] = $part_name;

                if (isset($data[$part_name])) {
                    $path_parts[$key] = $data[$part_name] === false ? 0 : $data[$part_name];
                } elseif (isset($defaults[$part_name])) {
                    $path_parts[$key] = $defaults[$part_name] === false ? 0 : $defaults[$part_name];
                } else {
                    throw new AssembleURLError($route, $data, $defaults, $part_name);
                }
            } else {
                $path_parts[$key] = $part;
            }
        }

        // Query string params
        foreach ($data as $k => $v) {
            if (!in_array($k, $part_names)) {
                $query_parts[$k] = $v === false ? 0 : $v;
            }
        }

        $url = with_slash($url_base) . trim(implode('/', $path_parts), '/');

        // Query string
        if (count($query_parts)) {
            $url .= version_compare(PHP_VERSION, '5.1.2', '>=') ? ('?' . http_build_query($query_parts, '', $query_arg_separator)) : ('?' . http_build_query($query_parts, ''));
        }

        // Anchor
        return $anchor ? "$url#$anchor" : $url;
    }

    // ---------------------------------------------------
    //  Getters and setters
    // ---------------------------------------------------

    /**
     * Returns array of mapped routes.
     *
     * @return Route[]
     */
    public static function getRoutes()
    {
        return self::$routes;
    }

    /**
     * Return route by name.
     *
     * @param  string $name
     * @return Route
     */
    public static function getRoute($name)
    {
        return isset(self::$routes[$name]) ? self::$routes[$name] : null;
    }

    /**
     * Return parsed params from current url.
     *
     * @return array
     */
    public static function getUrlParams()
    {
        return self::$url_params;
    }
}
