<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Composite collection.
 *
 * @package angie.library.database
 */
abstract class CompositeCollection extends DataObjectCollection
{
}
