<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

use ActiveCollab\Memories\Adapter\MySQL;
use ActiveCollab\Memories\Memories;

/**
 * Angie memories delegate.
 *
 * @package angie.library.application
 * @subpackage delegates
 */
class AngieMemoriesDelegate extends AngieDelegate
{
    /**
     * @var Memories
     */
    private $memories;

    /**
     * Construct a delegate instance.
     */
    public function __construct()
    {
        $this->memories = new Memories(
            new MySQL(DB::getConnection()->getLink(), false)
        );
    }

    /**
     * Return Memories instance.
     *
     * @return Memories
     */
    public function &getInstance()
    {
        return $this->memories;
    }

    /**
     * Return value that is stored under the key. If value is not found, $if_not_found_return should be returned.
     *
     * Set $use_cache to false if you want this method to ignore cached values
     *
     * @param  string     $key
     * @param  mixed|null $if_not_found_return
     * @param  bool       $use_cache
     * @return mixed
     */
    public function get($key, $if_not_found_return = null, $use_cache = true)
    {
        return $this->memories->get($key, $if_not_found_return, $use_cache);
    }

    /**
     * Set a value for the given key.
     *
     * @param  string                   $key
     * @param  mixed                    $value
     * @param  bool                     $bulk
     * @return array
     * @throws InvalidArgumentException
     */
    public function set($key, $value = null, $bulk = false)
    {
        return $this->memories->set($key, $value, $bulk);
    }

    /**
     * Forget a value that we have stored under the $key.
     *
     * @param string     $key
     * @param bool|false $bulk
     */
    public function forget($key, $bulk = false)
    {
        $this->memories->forget($key, $bulk);
    }
}
