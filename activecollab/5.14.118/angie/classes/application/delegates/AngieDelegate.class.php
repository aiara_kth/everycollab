<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Base angie application delegate.
 *
 * @package angie.library.application
 * @subpackage delegates
 */
abstract class AngieDelegate
{
}
