<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Foundation for all model definitions used by modules.
 *
 * @package angie.library.application
 */
class AngieModuleModel extends AngieFrameworkModel
{
}
