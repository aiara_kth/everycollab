<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

use Angie\Error;

/**
 * Migration environment error.
 *
 * @package angie.library.errors
 */
class MigrationEnvError extends Error
{
    /**
     * Construct error object.
     *
     * @param string $message
     */
    public function __construct($message = null)
    {
        if (empty($message)) {
            $message = 'Migration Environment Error';
        }

        parent::__construct($message);
    }
}
