<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

use Sabre\VObject\Component\VEvent;

/**
 * Basic calendar feed implementation.
 *
 * @package angie.frameworks.calendars
 * @subpackage models
 */
trait ICalendarFeedImplementation
{
    /**
     * Export calendar to iCalendar file.
     *
     * @param  User            $user
     * @return string
     * @throws FileCreateError
     */
    public function exportCalendarToFile(User $user)
    {
        $filename = $this->getCalendarExportFilename($user);

        if ($handle = fopen($filename, 'w')) {
            try {
                $vcalendar = $this->getVCalendarObject();

                $summary_prefix = $this->getCalendarElementSummaryPrefix();
                $summary_sufix = $this->getCalendarElementSummarySufix();

                foreach ($this->getCalendarFeedElements($user) as $calendar_feed_element) {
                    if (!($calendar_feed_element instanceof ICalendarFeedElement) || $calendar_feed_element->skipCalendarFeed()) {
                        continue;
                    }

                    $uid = $calendar_feed_element->getCalendarFeedUID();

                    $vevent = $vcalendar->add('VEVENT');

                    if ($calendar_feed_element instanceof RecurringTask) {
                        $today = DateValue::now()->beginningOfDay();
                        $ghost_tasks = RecurringTasks::getRangeForCalendar([$calendar_feed_element->getId()], $today, DateValue::makeFromTimestamp(strtotime('+3 month', $today->getTimestamp())));

                        foreach ($ghost_tasks as $ghost_task) {
                            $start_on = DateValue::makeFromTimestamp($ghost_task['start_on']);
                            $due_on = DateValue::makeFromTimestamp($ghost_task['due_on']);

                            /** @var VEvent $vevent */
                            $vevent->add('SUMMARY', $calendar_feed_element->getCalendarFeedSummary($user, $summary_prefix, $summary_sufix));

                            $vevent->add('UID', $uid . '_' . $ghost_task['id']);

                            $dtstart = $vevent->add('DTSTART', $start_on->toICalendar());

                            $due_on->advance(86400); // +1 day
                            $dtend = $vevent->add('DTEND', $due_on->toICalendar());

                            $dtstart['VALUE'] = 'DATE';
                            $dtend['VALUE'] = 'DATE';
                        }
                    } else {
                        $vevent->add('UID', $uid);
                        $vevent->add('SUMMARY', $calendar_feed_element->getCalendarFeedSummary($user, $summary_prefix, $summary_sufix));

                        if ($description = $calendar_feed_element->getCalendarFeedDescription($user)) {
                            $vevent->add('DESCRIPTION', $description);
                        }

                        if ($date_start = $calendar_feed_element->getCalendarFeedDateStart()) {
                            $dtstart = $vevent->add('DTSTART', $date_start->toICalendar());
                            if (!($date_start instanceof DateTimeValue)) {
                                $dtstart['VALUE'] = 'DATE';
                            }
                        }

                        if ($date_end = $calendar_feed_element->getCalendarFeedDateEnd()) {
                            $dtend = $vevent->add('DTEND', $date_end->toICalendar());
                            if (!($date_end instanceof DateTimeValue)) {
                                $dtend['VALUE'] = 'DATE';
                            }
                        }

                        if ($rrule = $calendar_feed_element->getCalendarFeedRepeatingRule()) {
                            $vevent->add('RRULE', $rrule);
                        }
                    }
                }

                fwrite($handle, $vcalendar->serialize());
            } finally {
                fclose($handle);
            }
        } else {
            throw new FileCreateError($filename);
        }

        return $filename;
    }

    /**
     * {@inheritdoc}
     */
    public function getCalendarElementSummaryPrefix()
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function getCalendarElementSummarySufix()
    {
        return '';
    }

    /**
     * Create and return new VCalendar object instance.
     *
     * @return \Sabre\VObject\Component\VCalendar
     */
    protected function getVCalendarObject()
    {
        return new \Sabre\VObject\Component\VCalendar();
    }

    /**
     * Return calendar elements that $user has access to.
     *
     * @param  IUser                  $user
     * @return ICalendarFeedElement[]
     */
    abstract protected function getCalendarFeedElements(IUser $user);

    /**
     * Return proposed calendar file name.
     *
     * For objects that implemented IUpdatedOn behavior, system will return:
     *
     * type-#CALENDAR_ID#-for-#USER_ID#-#UPDATED_ON#.ics
     *
     * If object does not implement IUpdatedOn, system will return:
     *
     * type-#CALENDAR_ID#-for-#USER_ID#.ics
     *
     * @param  User   $user
     * @return string
     */
    protected function getCalendarExportFilename(User $user)
    {
        $bits = [
            AngieApplication::getAccountId(),
            $this->getModelName(false, true),
            $this->getId(),
        ];

        if ($this instanceof IUpdatedOn) {
            $bits[] = $this->getUpdatedOn()->getTimestamp();
        }

        $bits[] = '-for-';
        $bits[] = $user->getId();
        $bits[] = $user->getUpdatedOn()->getTimestamp();

        return WORK_PATH . '/' . implode('-', $bits) . '.ics';
    }

    /**
     * Return object ID.
     *
     * @return int
     */
    abstract public function getId();

    /**
     * Return name of this model.
     *
     * @param  bool   $underscore
     * @param  bool   $singular
     * @return string
     */
    abstract public function getModelName($underscore = false, $singular = false);
}
