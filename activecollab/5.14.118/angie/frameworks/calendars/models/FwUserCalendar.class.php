<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * User calendar instance.
 *
 * @package angie.frameworks.calendars
 * @subpackage models
 */
abstract class FwUserCalendar extends Calendar
{
}
