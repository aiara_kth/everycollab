<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Calendars framework definition class.
 *
 * @package angie.frameworks.calendars
 */
class CalendarsFramework extends AngieFramework
{
    const NAME = 'calendars';

    /**
     * Short framework name.
     *
     * @var string
     */
    protected $name = 'calendars';

    /**
     * Initialize framework.
     */
    public function init()
    {
        parent::init();

        DataObjectPool::registerTypeLoader(['Calendar', 'UserCalendar'], function ($ids) {
            return Calendars::findByIds($ids);
        });

        DataObjectPool::registerTypeLoader('CalendarEvent', function ($ids) {
            return CalendarEvents::findByIds($ids);
        });
    }

    /**
     * Define classes used by this framework.
     */
    public function defineClasses()
    {
        AngieApplication::setForAutoload([
            'FwCalendar' => __DIR__ . '/models/calendars/FwCalendar.class.php',
            'FwCalendars' => __DIR__ . '/models/calendars/FwCalendars.class.php',

            'FwUserCalendar' => __DIR__ . '/models/FwUserCalendar.class.php',

            'FwCalendarEvent' => __DIR__ . '/models/calendar_events/FwCalendarEvent.class.php',
            'FwCalendarEvents' => __DIR__ . '/models/calendar_events/FwCalendarEvents.class.php',

            'FwNewCalendarEventNotification' => __DIR__ . '/notifications/FwNewCalendarEventNotification.class.php',

            'ICalendarFeed' => __DIR__ . '/models/calendar_feed/ICalendarFeed.php',
            'ICalendarFeedImplementation' => __DIR__ . '/models/calendar_feed/ICalendarFeedImplementation.php',
            'ICalendarFeedElement' => __DIR__ . '/models/calendar_feed/ICalendarFeedElement.php',
            'ICalendarFeedElementImplementation' => __DIR__ . '/models/calendar_feed/ICalendarFeedElementImplementation.php',
        ]);
    }

    /**
     * Define framework routes.
     */
    public function defineRoutes()
    {
        Router::mapResource('calendars', ['module' => self::INJECT_INTO], function ($collection, $single) {
            Router::map("$collection[name]_events", "$collection[path]/events", ['controller' => $collection['controller'], 'action' => ['GET' => 'all_calendar_events'], 'module' => $collection['module']], $collection['requirements']);
            Router::mapResource('calendar_events', ['module' => $collection['module'], 'collection_path' => "$single[path]/events", 'collection_requirements' => $collection['requirements']]);
        });
    }

    /**
     * Define event handlers.
     */
    public function defineHandlers()
    {
        $this->listen('on_trash_sections');
        $this->listen('on_history_field_renderers');
        $this->listen('on_rebuild_activity_logs');
        $this->listen('on_notification_inspector');
    }
}
