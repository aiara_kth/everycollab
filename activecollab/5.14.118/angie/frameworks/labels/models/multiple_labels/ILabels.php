<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Multiple labels interface definition.
 *
 * @package angie.frameworks.labels
 */
interface ILabels
{
    /**
     * Return object labels.
     *
     * @return Label[]
     */
    public function getLabels();

    /**
     * Return a list of object label ID-s.
     *
     * @return int[]
     */
    public function getLabelIds();

    /**
     * Return number of labels that parent object has.
     *
     * @return int
     */
    public function countLabels();

    /**
     * Clear labels.
     *
     * @return DataObject|ILabels
     */
    public function &clearLabels();

    /**
     * Clone attachments to a given object.
     *
     * @param  DataObject|ILabels $to
     * @return ILabels
     */
    public function &cloneLabelsTo(ILabels $to);

    /**
     * Return type of label used.
     *
     * @return string
     */
    public function getLabelType();
}
