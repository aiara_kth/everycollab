<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Multiple labels helper implementation.
 *
 * @package angie.frameworks.labels
 * @subpackage models
 */
trait ILabelsImplementation
{
    /**
     * @var array|null
     */
    private $after_save_set_labels;

    /**
     * Say hello to the parent object.
     */
    public function ILabelsImplementation()
    {
        $this->registerEventHandler('on_json_serialize', function (array &$result) {
            $result['labels'] = $this->getLabelDetails();
        });

        $this->registerEventHandler('on_set_attribute', function ($attribute, $value) {
            if ($attribute == 'labels' && is_array($value)) {
                $this->after_save_set_labels = $value;
            }
        });

        $this->registerEventHandler('on_after_save', function () {
            if ($this->after_save_set_labels !== null && is_array($this->after_save_set_labels)) {
                try {
                    DB::beginWork('Begin: set labels @ ' . __CLASS__);

                    $this->clearLabels();

                    // Clear empty values
                    if (count($this->after_save_set_labels)) {
                        foreach ($this->after_save_set_labels as $k => $v) {
                            $this->after_save_set_labels[$k] = trim($v);

                            if (empty($this->after_save_set_labels[$k])) {
                                unset($this->after_save_set_labels[$k]);
                            }
                        }
                    }

                    // Get what to add, and add
                    if (count($this->after_save_set_labels)) {
                        $new_label_added = false;
                        $labels_to_set = $existing_labels = [];

                        // Get name => id map for existing labels
                        if ($rows = DB::execute('SELECT id, name FROM labels WHERE type = ? AND name IN (?) ORDER BY id', $this->getLabelType(), $this->after_save_set_labels)) {
                            foreach ($rows as $row) {
                                $labels_to_set[] = $row['id'];
                                $existing_labels[strtolower_utf($row['name'])] = $row['id'];
                            }
                        }

                        foreach ($this->after_save_set_labels as $name) {
                            if (array_key_exists(strtolower_utf($name), $existing_labels)) {
                                continue;
                            }

                            if ($label = Labels::create(['type' => $this->getLabelType(), 'name' => $name])) {
                                $new_label_added = true;
                                $labels_to_set[] = $label->getId();
                            }
                        }

                        $batch = new DBBatchInsert('parents_labels', ['parent_type', 'parent_id', 'label_id'], 50, DBBatchInsert::REPLACE_RECORDS);

                        $parent_type = DB::escape(get_class($this));
                        $parent_id = DB::escape($this->getId());

                        foreach ($labels_to_set as $label_id) {
                            $batch->insertEscapedArray([$parent_type, $parent_id, DB::escape($label_id)]);
                        }

                        $batch->done();

                        AngieApplication::cache()->removeByObject($this);

                        // update project when new label is added on project
                        if ($this instanceof IProjectElement && $new_label_added) {
                            $this->getProject()->touch();
                        }
                    }

                    DB::commit('Done: set labels @ ' . __CLASS__);
                } catch (Exception $e) {
                    DB::rollback('Rollback: set labels @ ' . __CLASS__);
                    throw $e;
                }
            }
        });
    }

    /**
     * Return label details (id, name and color).
     *
     * @param  bool  $use_cache
     * @return array
     */
    private function getLabelDetails($use_cache = true)
    {
        return AngieApplication::cache()->getByObject(
            $this,
            'label_details',
            function () {
                return Labels::getDetailsByParent($this);
            },
            !$use_cache
        );
    }

    /**
     * Return object labels.
     *
     * @return DBResult|Label[]
     */
    public function getLabels()
    {
        return Labels::findBySQL(
            'SELECT labels.*
                FROM labels LEFT JOIN parents_labels ON labels.id = parents_labels.label_id
                WHERE parents_labels.parent_type = ? AND parents_labels.parent_id = ?
                ORDER BY labels.name',
            get_class($this),
            $this->getId()
        );
    }

    public function getLabelIds()
    {
        $label_ids = DB::executeFirstColumn(
            'SELECT `labels`.`id`
                FROM `labels` LEFT JOIN `parents_labels` ON `labels`.`id` = `parents_labels`.`label_id`
                WHERE `parents_labels`.`parent_type` = ? AND `parents_labels`.`parent_id` = ?
                ORDER BY labels.name',
            get_class($this),
            $this->getId()
        );

        if (empty($label_ids)) {
            $label_ids = [];
        }

        return $label_ids;
    }

    /**
     * Return number of labels that parent object has.
     *
     * @return int
     */
    public function countLabels()
    {
        return DB::executeFirstCell(
            'SELECT COUNT(labels.id) AS "row_count"
                FROM labels LEFT JOIN parents_labels ON labels.id = parents_labels.label_id
                WHERE parents_labels.parent_type = ? AND parents_labels.parent_id = ?
                ORDER BY labels.name',
            get_class($this),
            $this->getId()
        );
    }

    /**
     * Clear labels.
     *
     * @return DataObject|ILabels
     */
    public function &clearLabels()
    {
        if ($label_ids = DB::executeFirstColumn('SELECT label_id FROM parents_labels WHERE parent_type = ? AND parent_id = ? ORDER BY label_id', get_class($this), $this->getId())) {
            DB::execute('DELETE FROM parents_labels WHERE parent_type = ? AND parent_id = ?', get_class($this), $this->getId());
            DB::execute('UPDATE labels SET updated_on = ? WHERE id IN (?)', DateTimeValue::now(), $label_ids);

            Labels::clearCacheFor($label_ids);
            AngieApplication::cache()->removeByObject($this);
            AngieApplication::invalidateInitialSettingsCache();
        }

        return $this;
    }

    /**
     * Clone attachments to a given object.
     *
     * @param  DataObject|ILabels $to
     * @return ILabels
     */
    public function &cloneLabelsTo(ILabels $to)
    {
        if ($label_ids = DB::executeFirstColumn('SELECT label_id FROM parents_labels WHERE parent_type = ? AND parent_id = ?', get_class($this), $this->getId())) {
            $batch = new DBBatchInsert('parents_labels', ['parent_type', 'parent_id', 'label_id'], 50, DBBatchInsert::REPLACE_RECORDS);

            $to_parent_type = DB::escape(get_class($to));
            $to_parent_id = DB::escape($to->getId());

            foreach ($label_ids as $label_id) {
                $batch->insertEscapedArray([$to_parent_type, $to_parent_id, DB::escape($label_id)]);
            }

            $batch->done();
        }

        return $this;
    }

    // ---------------------------------------------------
    //  Expectations
    // ---------------------------------------------------

    /**
     * Register an internal event handler.
     *
     * @param $event
     * @param $handler
     * @throws InvalidParamError
     */
    abstract protected function registerEventHandler($event, $handler);

    /**
     * Return object ID.
     *
     * @return int
     */
    abstract public function getId();

    /**
     * Return type of label that this object accepts.
     *
     * @return string
     */
    abstract public function getLabelType();
}
