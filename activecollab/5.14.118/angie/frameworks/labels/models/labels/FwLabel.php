<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Framework level label row implementation.
 *
 * @package angie.frameworks.labels
 * @subpackage models
 */
abstract class FwLabel extends BaseLabel implements IRoutingContext
{
    const LABEL_DEFAULT_COLOR = '#999';

    /**
     * List of accepted fields.
     *
     * @var array
     */
    protected $accept = ['name', 'is_default'];

    /**
     * Set if you wish to have name always uppercased for this label type.
     *
     * @var bool
     */
    protected $always_uppercase = true;

    /**
     * Return base type name.
     *
     * @param  bool   $singular
     * @return string
     */
    public function getBaseTypeName($singular = true)
    {
        return $singular ? 'label' : 'labels';
    }

    /**
     * {@inheritdoc}
     */
    public function getColor()
    {
        return parent::getColor() ? parent::getColor() : self::LABEL_DEFAULT_COLOR;
    }

    /**
     * Return array or property => value pairs that describes this object.
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return array_merge(
            parent::jsonSerialize(),
            [
                'color' => $this->getColor(),
                'is_default' => $this->getIsDefault(),
                'position' => $this->getPosition(),
            ]
        );
    }

    /**
     * Return routing context name.
     *
     * @return string
     */
    public function getRoutingContext()
    {
        return 'label';
    }

    /**
     * Return routing context parameters.
     *
     * @return mixed
     */
    public function getRoutingContextParams()
    {
        return ['label_id' => $this->getId()];
    }

    // ---------------------------------------------------
    //  Getters and setters
    // ---------------------------------------------------

    /**
     * Return max label name.
     *
     * @return int
     */
    public function getMaxNameLength()
    {
        return 50;
    }

    /**
     * Set value of specific field.
     *
     * @param  string            $name
     * @param  mixed             $value
     * @return mixed
     * @throws InvalidParamError
     */
    public function setFieldValue($name, $value)
    {
        if ($name == 'name') {
            if (strlen_utf($value) > $this->getMaxNameLength()) {
                $value = substr_utf($value, 0, $this->getMaxNameLength());
            }

            if ($this->getAlwaysUppercase()) {
                $value = strtoupper_utf($value);
            }
        }

        return parent::setFieldValue($name, $value);
    }

    /**
     * Return true if name of this label needs to be displayed in uppercase.
     *
     * @return bool
     */
    public function getAlwaysUppercase()
    {
        return (bool) $this->always_uppercase;
    }

    // ---------------------------------------------------
    //  Permissions
    // ---------------------------------------------------

    /**
     * Returns true if $user can view label.
     *
     * @param  User $user
     * @return bool
     */
    public function canView(User $user)
    {
        return true;
    }

    /**
     * Returns true if $user can update this label.
     *
     * @param  User $user
     * @return bool
     */
    public function canEdit(User $user)
    {
        return $user->isOwner();
    }

    /**
     * Returns true if $user can delete this label.
     *
     * @param  User $user
     * @return bool
     */
    public function canDelete(User $user)
    {
        return !$this->getIsDefault() && $user->isOwner();
    }

    // ---------------------------------------------------
    //  System
    // ---------------------------------------------------

    /**
     * Validate before save.
     *
     * @param ValidationErrors $errors
     */
    public function validate(ValidationErrors &$errors)
    {
        if ($this->validatePresenceOf('name')) {
            $this->validateUniquenessOf('type', 'name') or $errors->fieldValueNeedsToBeUnique('name');
        } else {
            $errors->fieldValueIsRequired('name');
        }
    }

    /**
     * Save label.
     */
    public function save()
    {
        $drop_cache = $this->isNew() || $this->isModifiedField('name') || $this->isModifiedField('color');

        if (!$this->getPosition()) {
            $this->setPosition(DB::executeFirstCell('SELECT MAX(position) FROM labels WHERE type = ?', get_class($this)) + 1);
        }

        $save = parent::save();

        if ($drop_cache) {
            Labels::clearCache();
        }

        return $save;
    }

    /**
     * Delete specific object (and related objects if neccecery).
     *
     * @param  bool      $bulk
     * @throws Exception
     */
    public function delete($bulk = false)
    {
        try {
            DB::beginWork('Begin: delete label @ ' . __CLASS__);

            DB::execute('DELETE FROM parents_labels WHERE label_id = ?', $this->getId());
            parent::delete($bulk);

            DB::commit('Done: delete label @ ' . __CLASS__);
        } catch (Exception $e) {
            DB::rollback('Rollback: delete label @ ' . __CLASS__);
            throw $e;
        }
    }
}
