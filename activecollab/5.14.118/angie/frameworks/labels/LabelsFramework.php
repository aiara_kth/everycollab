<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Labels framework definition.
 *
 * @package angie.frameworks.labels
 */
class LabelsFramework extends AngieFramework
{
    const NAME = 'labels';

    /**
     * Short framework name.
     *
     * @var string
     */
    protected $name = 'labels';

    /**
     * Initialize framework.
     */
    public function init()
    {
        parent::init();

        DataObjectPool::registerTypeLoader('Label', function ($ids) {
            return Labels::findByIds($ids);
        });
    }

    /**
     * Define classes used by this framework.
     */
    public function defineClasses()
    {
        AngieApplication::setForAutoload([
            'FwLabel' => __DIR__ . '/models/labels/FwLabel.php',
            'FwLabels' => __DIR__ . '/models/labels/FwLabels.php',

            // Single label
            'ILabel' => __DIR__ . '/models/single_label/ILabel.php',
            'ILabelImplementation' => __DIR__ . '/models/single_label/ILabelImplementation.php',

            // Multiple labels
            'ILabels' => __DIR__ . '/models/multiple_labels/ILabels.php',
            'ILabelsImplementation' => __DIR__ . '/models/multiple_labels/ILabelsImplementation.php',
        ]);
    }

    /**
     * Define label routes.
     */
    public function defineRoutes()
    {
        Router::mapResource('labels', ['module' => self::INJECT_INTO], function ($collection, $single) {
            Router::map("$collection[name]_reorder", "$collection[path]/reorder", ['action' => ['PUT' => 'reorder'], 'controller' => 'labels', 'module' => LabelsFramework::INJECT_INTO], $collection['requirements']);
            Router::map("$single[name]_set_as_default", "$single[path]/set-as-default", ['action' => ['PUT' => 'set_as_default'], 'controller' => 'labels', 'module' => LabelsFramework::INJECT_INTO], $single['requirements']);
        });
    }

    /**
     * Define framework handlers.
     */
    public function defineHandlers()
    {
        $this->listen('on_reset_manager_states');
    }
}
