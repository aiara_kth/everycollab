<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Labels framework model definition.
 *
 * @package angie.frameworks.labels
 * @subpackage resources
 */
class LabelsFrameworkModel extends AngieFrameworkModel
{
    /**
     * Construct labels framework model definition.
     *
     * @param LabelsFramework $parent
     */
    public function __construct(LabelsFramework $parent)
    {
        parent::__construct($parent);

        $this->addModelFromFile('labels')
            ->setTypeFromField('type')
            ->setOrderBy('position')
            ->setObjectIsAbstract(true)
            ->addModelTrait(null, 'IResetInitialSettingsTimestamp');

        $this->addTableFromFile('parents_labels');
    }
}
