<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Email framework definition.
 *
 * @package angie.frameworks.email
 */
class EmailFramework extends AngieFramework
{
    const NAME = 'email';

    const MAILING_SILENT = 'silent';
    const MAILING_NATIVE = 'native';
    const MAILING_QUEUED = 'queued';

    /**
     * Short name of the framework.
     *
     * @var string
     */
    protected $name = 'email';

    /**
     * Define classes used by this framework.
     */
    public function defineClasses()
    {
        AngieApplication::setForAutoload([
            'EmailNotificationChannel' => __DIR__ . '/models/EmailNotificationChannel.php',

            'EmailIntegration' => __DIR__ . '/models/EmailIntegration.php',

            'IncomingMail' => __DIR__ . '/models/IncomingMail.php',
            'FileMailbox' => __DIR__ . '/models/FileMailbox.php',
            'IIncomingMail' => __DIR__ . '/models/IIncomingMail.php',

            'JsonEmailProcessor' => __DIR__ . '/models/JsonEmailProcessor.php',
            'EmailImporterInterface' => __DIR__ . '/models/EmailImporterInterface.php',
            'OnDemandEmailImporter' => __DIR__ . '/models/OnDemandEmailImporter.php',
            'EmailProcessorInterface' => __DIR__ . '/models/EmailProcessorInterface.php',
            'EmailMessageInterface' => __DIR__ . '/models/EmailMessageInterface.php',
            'EmailMessage' => __DIR__ . '/models/EmailMessage.php',

            'FwNotifyEmailSenderNotification' => __DIR__ . '/notifications/FwNotifyEmailSenderNotification.class.php',
            'IncomingMailMessage' => __DIR__ . '/models/IncomingMailMessage.php',
            'FwBounceEmailNotification' => __DIR__ . '/notifications/FwBounceEmailNotification.class.php',
        ]);
    }

    /**
     * Define framework routes.
     */
    public function defineRoutes()
    {
        Router::map('email_integration_email_log', 'integrations/email/email-log', ['module' => self::INJECT_INTO, 'controller' => 'email_integration', 'action' => ['GET' => 'email_log'], 'integration_type' => 'email']);
        Router::map('email_integration_test_connection', 'integrations/email/test-connection', ['module' => self::INJECT_INTO, 'controller' => 'email_integration', 'action' => ['POST' => 'test_connection'], 'integration_type' => 'email']);
    }

    /**
     * Define event handlers.
     */
    public function defineHandlers()
    {
        $this->listen('on_notification_channels');
        $this->listen('on_system_status');
    }
}
