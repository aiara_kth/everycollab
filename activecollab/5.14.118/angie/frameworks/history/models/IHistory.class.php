<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * History interface.
 *
 * @package angie.frameworks.history
 * @subpackage models
 */
interface IHistory
{
    /**
     * Return full object history.
     *
     * @return array
     */
    public function getHistory();

    /**
     * Return verbose history.
     *
     * @param  Language $language
     * @return array
     */
    public function getVerboseHistory(Language $language);

    /**
     * Return list of fiels tracked by history.
     *
     * @return array
     */
    public function getHistoryFields();

    /**
     * Track changes for the given field(s).
     *
     * @param string[] ...$field_names
     */
    public function addHistoryFields(...$field_names);

    /**
     * Return latest modification entry.
     *
     * @return ModificationLog
     */
    public function getLatestModification();
}
