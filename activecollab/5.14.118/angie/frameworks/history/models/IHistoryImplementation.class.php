<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Framework level history implementation.
 *
 * @package angie.frameworks.history
 * @subpackage models
 */
trait IHistoryImplementation
{
    /**
     * Array of fiels that we want to track changes to.
     *
     * @var array
     */
    private $history_fields = [];

    /**
     * Say hello to the parent object.
     */
    public function IHistoryImplementation()
    {
        $this->registerEventHandler('on_after_save', function ($is_new, $modifications) {
            if ($is_new || empty($modifications)) {
                return;
            }

            $by = AngieApplication::authentication()->getLoggedUser();

            if ($by instanceof IUser) {
                $this->logModifications($modifications, $by);
            }
        });

        $this->registerEventHandler('on_before_delete', function () {
            ModificationLogs::deleteByParent($this);
        });

        foreach (['type', 'name', 'state', 'visibility'] as $common_field) {
            if ($this->fieldExists($common_field)) {
                $this->history_fields[] = $common_field;
            }
        }

        if ($this instanceof IComplete) {
            $this->history_fields[] = 'priority';
            $this->history_fields[] = 'due_on';
            $this->history_fields[] = 'completed_on';
        }

        if ($this instanceof ILabel) {
            $this->history_fields[] = 'label_id';
        }

        if ($this instanceof ICategory) {
            $this->history_fields[] = 'category_id';
        }

        if ($this instanceof IAssignees) {
            $this->history_fields[] = 'assignee_id';
        }
    }

    /**
     * Return for object history.
     *
     * @return array
     */
    public function getHistory()
    {
        $result = [];

        if ($modification_ids = DB::executeFirstColumn('SELECT id FROM modification_logs WHERE ' . ModificationLogs::parentToCondition($this))) {

            /** @var ModificationLog $log */
            foreach (ModificationLogs::find(['conditions' => ['id IN (?)', $modification_ids]]) as $log) {
                $result[$log->getId()] = ['timestamp' => $log->getCreatedOn(), 'created_by_id' => $log->getCreatedById(), 'created_by_name' => $log->getCreatedByName(), 'created_by_email' => $log->getCreatedByEmail(), 'modifications' => []];
            }

            if ($log_values = DB::execute('SELECT * FROM modification_log_values WHERE modification_id IN (?) ORDER BY modification_id', $modification_ids)) {
                foreach ($log_values as $log_value) {
                    $result[$log_value['modification_id']]['modifications'][$log_value['field']] = [unserialize($log_value['old_value']), unserialize($log_value['new_value'])];
                }
            }
        }

        return array_values($result);
    }

    /**
     * Return verbose history.
     *
     * @param  Language $language
     * @return array
     */
    public function getVerboseHistory(Language $language)
    {
        $history = $this->getHistory();

        if (count($history)) {
            $renderers = $this->getHistoryFieldRenderers();

            foreach ($history as $k => $history_entry) {
                foreach ($history_entry['modifications'] as $field => $values) {
                    list($old_value, $new_value) = $values;

                    if (isset($renderers[$field]) && $renderers[$field] instanceof Closure) {
                        $history[$k]['modifications'][$field][2] = $renderers[$field]->__invoke($old_value, $new_value, $language);
                    } else {
                        if ($new_value && $old_value) {
                            $history[$k]['modifications'][$field][2] = lang(':field changed from :old_value to :new_value', ['field' => $field, 'old_value' => $old_value, 'new_value' => $new_value], true, $language);
                        } elseif ($new_value) {
                            $history[$k]['modifications'][$field][2] = lang(':field set to :new_value', ['field' => $field, 'new_value' => $new_value], true, $language);
                        } elseif ($old_value) {
                            $history[$k]['modifications'][$field][2] = lang(':field set to empty value', ['field' => $field], true, $language);
                        }
                    }
                }
            }
        }

        return array_values($history);
    }

    /**
     * Return list of fields tracked by history.
     *
     * @return array
     */
    public function getHistoryFields()
    {
        return $this->history_fields;
    }

    /**
     * {@inheritdoc}
     */
    public function addHistoryFields(...$field_names)
    {
        foreach ($field_names as $field_name) {
            if ($field_name && !in_array($field_name, $this->history_fields)) {
                $this->history_fields[] = $field_name;
            }
        }
    }

    /**
     * Return history field renderers.
     *
     * @return array
     */
    public function getHistoryFieldRenderers()
    {
        $result['name'] = function ($old_value, $new_value, Language $language) {
            return lang('Name changed from <b>:old_value</b> to <b>:new_value</b>', ['old_value' => $old_value, 'new_value' => $new_value], true, $language);
        };

        $result['completed_on'] = function ($old_value, $new_value, Language $language) {
            $new_completed_on = $new_value ? new DateTimeValue($new_value) : null;

            if ($new_completed_on instanceof DateTimeValue && $new_completed_on->getTimestamp() > 0) {
                return lang('Marked as completed', null, true, $language);
            } else {
                return lang('Marked as open', null, true, $language);
            }
        };

        $result['language_id'] = function ($old_value, $new_value, Language $language) {
            $lang = Languages::findById($new_value);
            $new_language = lang('default');
            if ($lang instanceof Language) {
                $new_language = $lang->getName();
            }
            $lang = Languages::findById($old_value);
            $old_language = lang('default');
            if ($lang instanceof Language) {
                $old_language = $lang->getName();
            }

            return lang('Language changed from <b>:old_value</b> to <b>:new_value</b>', ['old_value' => $old_language, 'new_value' => $new_language], true, $language);
        };

        $this->triggerEvent('on_history_field_renderers', [&$result]);

        Angie\Events::trigger(
            'on_history_field_renderers',
            [
                $this,
                &$result,
            ]
        );

        return $result;
    }

    /**
     * Return latest modification entry.
     *
     * @return ModificationLog|DataObject
     */
    public function getLatestModification()
    {
        return ModificationLogs::find(
            [
                'conditions' => ['`parent_type` = ? AND `parent_id` = ?', get_class($this), $this->getId()],
                'order' => '`created_on` DESC',
                'one' => true,
            ]
        );
    }

    /**
     * Commit object modifications.
     *
     * @param  array     $modifications
     * @param  IUser     $by
     * @throws Exception
     */
    public function logModifications($modifications, IUser $by)
    {
        $track_fields = $this->getHistoryFields();

        $to_log = [];
        foreach ($modifications as $field => $value) {
            if (in_array($field, $track_fields) && is_array($value) && count($value) == 2) {
                $to_log[] = $field;
            }
        }

        if (count($to_log)) {
            DB::transact(function () use ($modifications, $by, $to_log) {
                DB::execute('INSERT INTO modification_logs (parent_type, parent_id, created_on, created_by_id, created_by_name, created_by_email) VALUES (?, ?, UTC_TIMESTAMP(), ?, ?, ?)', get_class($this), $this->getId(), $by->getId(), $by->getName(), $by->getEmail());

                $log_id = DB::lastInsertId();

                $batch = new DBBatchInsert('modification_log_values', ['modification_id', 'field', 'old_value', 'new_value']);

                foreach ($to_log as $field) {
                    list($old_value, $new_value) = $modifications[$field];

                    if ($old_value instanceof DateValue) {
                        $old_value = $old_value->toMySQL();
                    }

                    if ($new_value instanceof DateValue) {
                        $new_value = $new_value->toMySQL();
                    }

                    $batch->insert($log_id, $field, serialize($old_value), serialize($new_value));
                }

                $batch->done();
            }, 'Commit object modification');
        }
    }

    // ---------------------------------------------------
    //  Expectations
    // ---------------------------------------------------

    /**
     * Return object ID.
     *
     * @return string
     */
    abstract public function getId();

    /**
     * Check if specific key is defined.
     *
     * @param  string $field Field name
     * @return bool
     */
    abstract public function fieldExists($field);

    /**
     * Return value of specific field and typecast it...
     *
     * @param  string $field   Field value
     * @param  mixed  $default Default value that is returned in case of any error
     * @return mixed
     */
    abstract public function getFieldValue($field, $default = null);

    /**
     * Register an internal event handler.
     *
     * @param $event
     * @param $handler
     * @throws InvalidParamError
     */
    abstract protected function registerEventHandler($event, $handler);

    /**
     * Trigger an internal event.
     *
     * @param string $event
     * @param array  $event_parameters
     */
    abstract protected function triggerEvent($event, $event_parameters = null);
}
