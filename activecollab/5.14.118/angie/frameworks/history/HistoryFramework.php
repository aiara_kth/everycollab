<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Modification logs framework definition.
 *
 * @package angie.frameworks.history
 */
class HistoryFramework extends AngieFramework
{
    const NAME = 'history';

    /**
     * Framework name.
     *
     * @var string
     */
    protected $name = 'history';

    /**
     * Define classes used by this framework.
     */
    public function defineClasses()
    {
        AngieApplication::setForAutoload([
            'FwModificationLog' => __DIR__ . '/models/modification_logs/FwModificationLog.class.php',
            'FwModificationLogs' => __DIR__ . '/models/modification_logs/FwModificationLogs.class.php',

            'IHistory' => __DIR__ . '/models/IHistory.class.php',
            'IHistoryImplementation' => __DIR__ . '/models/IHistoryImplementation.class.php',
        ]);
    }

    /**
     * Define module routes.
     */
    public function defineRoutes()
    {
        Router::map('history', 'history/:parent_type/:parent_id', ['action' => ['GET' => 'index'], 'controller' => 'history', 'module' => self::INJECT_INTO], ['parent_type' => Router::MATCH_SLUG, 'parent_id' => Router::MATCH_ID]);
    }
}
