<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

const CUSTOM_PAYMENT = 'Custom Payment';
const PAYPAL_DIRECT_PAYMENT = 'Paypal Direct Gateway';
const PAYPAL_EXPRESS_CHECKOUT = 'Paypal Express Checkout Gateway';
const AUTHORIZE_AIM = 'Authorize AIM Gateway';
const STRIPE_PAYMENT = 'Stripe Gateway';
const BRAINTREE_PAYMENT = 'Stripe Gateway';

/**
 * Payment framework definiton.
 *
 * @package angie.frameworks.payments
 */
class PaymentsFramework extends AngieFramework
{
    const NAME = 'payments';
    const PATH = __DIR__;

    /**
     * Short framework name.
     *
     * @var string
     */
    protected $name = 'payments';

    /**
     * Initialize framework.
     */
    public function init()
    {
        parent::init();

        DataObjectPool::registerTypeLoader('PaymentGateway', function ($ids) {
            return PaymentGateways::findByIds($ids);
        });

        DataObjectPool::registerTypeLoader('Payment', function ($ids) {
            return Payments::findByIds($ids);
        });

        DataObjectPool::registerTypeLoader('StoredCard', function ($ids) {
            return StoredCards::findByIds($ids);
        });
    }

    /**
     * Define classes used by this framework.
     */
    public function defineClasses()
    {
        AngieApplication::setForAutoload([
            'IPayments' => __DIR__ . '/models/IPayments.php',
            'IPaymentsImplementation' => __DIR__ . '/models/IPaymentsImplementation.php',

            'PaymentGatewayResponse' => __DIR__ . '/models/PaymentGatewayResponse.php',
            'PaymentGatewayError' => __DIR__ . '/models/PaymentGatewayError.php',

            'FwPayment' => __DIR__ . '/models/payments/FwPayment.php',
            'FwPayments' => __DIR__ . '/models/payments/FwPayments.php',

            'FwPaymentGateway' => __DIR__ . '/models/payment_gateways/FwPaymentGateway.php',
            'FwPaymentGateways' => __DIR__ . '/models/payment_gateways/FwPaymentGateways.php',

            'FwStoredCard' => __DIR__ . '/models/stored_cards/FwStoredCard.php',
            'FwStoredCards' => __DIR__ . '/models/stored_cards/FwStoredCards.php',

            'FwPaymentReceivedNotification' => __DIR__ . '/notifications/FwPaymentReceivedNotification.class.php',

            // ---------------------------------------------------
            //  Services
            // ---------------------------------------------------

            'ICardProcessingPaymentGateway' => __DIR__ . '/models/ICardProcessingPaymentGateway.php',
            'ICardProcessingPaymentGatewayImplementation' => __DIR__ . '/models/ICardProcessingPaymentGatewayImplementation.php',

            'AuthorizeGateway' => __DIR__ . '/models/services/AuthorizeGateway.php',
            'StripeGateway' => __DIR__ . '/models/services/StripeGateway.php',
            'BrainTreeGateway' => __DIR__ . '/models/services/BrainTreeGateway.php',
            'PaypalGateway' => __DIR__ . '/models/services/paypal/PaypalGateway.php',
            'PaypalDirectGateway' => __DIR__ . '/models/services/paypal/PaypalDirectGateway.php',
            'PaypalExpressCheckoutGateway' => __DIR__ . '/models/services/paypal/PaypalExpressCheckoutGateway.php',

            // ---------------------------------------------------
            //  Integrations
            // ---------------------------------------------------

            'CreditCardIntegration' => __DIR__ . '/models/integrations/CreditCardIntegration.php',
            'AuthorizenetIntegration' => __DIR__ . '/models/integrations/AuthorizenetIntegration.php',
            'StripeIntegration' => __DIR__ . '/models/integrations/StripeIntegration.php',
            'PaypalDirectIntegration' => __DIR__ . '/models/integrations/PaypalDirectIntegration.php',
            'BraintreeIntegration' => __DIR__ . '/models/integrations/BraintreeIntegration.php',

            'PaypalExpressIntegration' => __DIR__ . '/models/integrations/PaypalExpressIntegration.php',
        ]);
    }

    /**
     * Define payments framework routes.
     */
    public function defineRoutes()
    {
        Router::mapResource('payments', ['module' => self::INJECT_INTO]);
        Router::map('public_payments', 'public_payments', ['controller' => 'public_payments', 'action' => ['GET' => 'view', 'POST' => 'add', 'PUT' => 'update', 'delete' => 'cancel'], 'module' => self::INJECT_INTO]);
        Router::map('public_payment_authorizenet_confirm', 'public_payments/authorizenet-confirm', ['controller' => 'public_payments', 'action' => ['GET' => 'authorizenet_confirm'], 'module' => self::INJECT_INTO]);
        Router::map('public_payment_authorizenet_form', 'public_payments/authorizenet-form', ['controller' => 'public_payments', 'action' => ['GET' => 'authorizenet_form'], 'module' => self::INJECT_INTO]);

        Router::map('payment_gateways', 'payment-gateways', ['controller' => 'payment_gateways', 'action' => ['GET' => 'get_settings', 'PUT' => 'update_settings'], 'module' => self::INJECT_INTO]);
        Router::map('payment_gateway_clear_paypal', 'payment-gateways/clear-paypal', ['controller' => 'payment_gateways', 'action' => ['DELETE' => 'clear_paypal'], 'module' => self::INJECT_INTO]);
        Router::map('payment_gateway_clear_credit_card', 'payment-gateways/clear-credit-card', ['controller' => 'payment_gateways', 'action' => ['DELETE' => 'clear_credit_card'], 'module' => self::INJECT_INTO]);
    }

    /**
     * Define event handlers.
     */
    public function defineHandlers()
    {
        $this->listen('on_available_integrations');
    }
}
