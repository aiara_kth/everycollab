<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Framework level stored cards manager class.
 *
 * @package angie.frameworks.payments
 * @subpackage models
 */
abstract class FwStoredCards extends BaseStoredCards
{
}
