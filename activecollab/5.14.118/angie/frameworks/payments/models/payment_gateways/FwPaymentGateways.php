<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Framework level payments gateways manager class.
 *
 * @package angie.frameworks.payments
 * @subpackage models
 */
abstract class FwPaymentGateways extends BasePaymentGateways
{
}
