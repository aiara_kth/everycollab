<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Attachment framework definition.
 *
 * @package angie.frameworks.attachments
 */
class AttachmentsFramework extends AngieFramework
{
    const NAME = 'attachments';
    const PATH = __DIR__;

    /**
     * Framework name.
     *
     * @var string
     */
    protected $name = 'attachments';

    /**
     * Initialize framework.
     */
    public function init()
    {
        parent::init();

        $attachment_types = [
            'Attachment',
            'LocalAttachment',
            'WarehouseAttachment',
            'GoogleDriveAttachment',
            'DropboxAttachment',
        ];

        DataObjectPool::registerTypeLoader($attachment_types, function ($ids) {
            return Attachments::findByIds($ids);
        });
    }

    /**
     * Define classes used by this framework.
     */
    public function defineClasses()
    {
        AngieApplication::setForAutoload([
            'IAttachments' => __DIR__ . '/models/IAttachments.class.php',
            'IAttachmentsImplementation' => __DIR__ . '/models/IAttachmentsImplementation.class.php',

            'IFile' => __DIR__ . '/models/IFile.class.php',
            'IFileImplementation' => __DIR__ . '/models/IFileImplementation.class.php',

            'FwAttachment' => __DIR__ . '/models/attachments/FwAttachment.class.php',
            'FwAttachments' => __DIR__ . '/models/attachments/FwAttachments.class.php',

            'FwThumbnails' => __DIR__ . '/models/FwThumbnails.class.php',

            'FwAttachmentsArchive' => __DIR__ . '/models/FwAttachmentsArchive.class.php',

            'IRemoteFile' => __DIR__ . '/models/IRemoteFile.class.php',
            'IRemoteFileImplementation' => __DIR__ . '/models/IRemoteFileImplementation.class.php',
            'IWarehouseFileImplementation' => __DIR__ . '/models/IWarehouseFileImplementation.class.php',
            'IGoogleDriveFileImplementation' => __DIR__ . '/models/IGoogleDriveFileImplementation.class.php',
            'IDropboxFileImplementation' => __DIR__ . '/models/IDropboxFileImplementation.class.php',
        ]);
    }

    /**
     * Define routes for this framework.
     */
    public function defineRoutes()
    {
        Router::mapResource('attachments', ['module' => self::INJECT_INTO], function ($collection, $single) {
            Router::map("$single[name]_download", "$single[path]/download", ['controller' => $single['controller'], 'action' => ['GET' => 'download'], 'module' => $single['module']], $single['requirements']);
            Router::map("$collection[name]_batch_download", "$collection[path]/:parent_type/:parent_id/download", ['controller' => 'attachments_archive', 'action' => ['POST' => 'prepare'], 'module' => $collection['module']], $collection['requirements']);
        });
    }

    /**
     * Define event listeners.
     */
    public function defineHandlers()
    {
        $this->listen('on_calculate_storage_usage');
        $this->listen('on_object_from_notification_context');
        $this->listen('on_reset_manager_states');
    }
}
