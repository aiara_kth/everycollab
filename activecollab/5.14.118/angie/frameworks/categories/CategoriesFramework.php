<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Categories framework implementation.
 *
 * @package angie.framework.categories
 */
class CategoriesFramework extends AngieFramework
{
    const NAME = 'categories';

    /**
     * Short framework name.
     *
     * @var string
     */
    protected $name = 'categories';

    /**
     * Initialize module.
     */
    public function init()
    {
        parent::init();

        DataObjectPool::registerTypeLoader('Category', function ($ids) {
            return Categories::findByIds($ids);
        });
    }

    /**
     * Define classes used by this framework.
     */
    public function defineClasses()
    {
        AngieApplication::setForAutoload([
            'FwCategory' => __DIR__ . '/models/categories/FwCategory.class.php',
            'FwCategories' => __DIR__ . '/models/categories/FwCategories.class.php',

            'ICategoriesContext' => __DIR__ . '/models/ICategoriesContext.class.php',
            'ICategoriesContextImplementation' => __DIR__ . '/models/ICategoriesContextImplementation.class.php',

            'ICategory' => __DIR__ . '/models/ICategory.class.php',
            'ICategoryImplementation' => __DIR__ . '/models/ICategoryImplementation.class.php',
        ]);
    }

    /**
     * Define routes.
     */
    public function defineRoutes()
    {
        Router::mapResource('categories', ['module' => self::INJECT_INTO]);
    }
}
