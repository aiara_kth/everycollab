<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Framework level category implementation.
 *
 * @package angie.frameworks.categories
 * @subpackage models
 */
abstract class FwCategory extends BaseCategory implements IRoutingContext, IHistory
{
    use IHistoryImplementation;

    /**
     * Return type name.
     *
     * By default, this function will return 'category', but it can be changed
     * in 'group', 'collection' etc - whatever fits the needs of the specific
     * situation where specific category type is used
     *
     * @return string
     */
    public function getTypeName()
    {
        return 'category';
    }

    /**
     * Set attributes.
     *
     * @param array $attributes
     */
    public function setAttributes($attributes)
    {
        if (is_array($attributes) && isset($attributes['name'])) {
            $attributes['name'] = trim($attributes['name']);
        }

        parent::setAttributes($attributes);
    }

    /**
     * Return routing context name.
     *
     * @return string
     */
    public function getRoutingContext()
    {
        return 'category';
    }

    /**
     * Return routing context name.
     *
     * @return string
     */
    public function getRoutingContextParams()
    {
        return ['category_id' => $this->getId()];
    }

    /**
     * Validate model before save.
     *
     * @param ValidationErrors $errors
     */
    public function validate(ValidationErrors &$errors)
    {
        if ($this->validatepresenceOf('name')) {
            if ($this->getParentType() && $this->getParentId()) {
                $validate_uniqueness = $this->validateUniquenessOf('parent_type', 'parent_id', 'type', 'name');
            } else {
                $validate_uniqueness = $this->validateUniquenessOf('type', 'name');
            }

            if (!$validate_uniqueness) {
                $errors->addError('Name needs to be unique', 'name');
            }
        } else {
            $errors->addError('Name is required', 'name');
        }
    }
}
