<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Comments framework model definition.
 *
 * @package angie.frameworks.comments
 * @subpackage resources
 */
class CommentsFrameworkModel extends AngieFrameworkModel
{
    /**
     * Construct comments framework model definition.
     *
     * @param CommentsFramework $parent
     */
    public function __construct(CommentsFramework $parent)
    {
        parent::__construct($parent);

        $this->addModel(DB::createTable('comments')->addColumns([
            new DBIdColumn(),
            DBStringColumn::create('source', 50),
            new DBParentColumn(),
            DBBodyColumn::create(),
            new DBIpAddressColumn('ip_address'),
            new DBCreatedOnByColumn(true, true),
            new DBUpdatedOnByColumn(),
            DBTrashColumn::create(true),
        ]))->setOrderBy('created_on DESC, id DESC')
            ->implementAttachments()
            ->implementHistory()
            ->implementActivityLog()
            ->implementTrash()
            ->addModelTrait('IWhoCanSeeThis', 'IWhoCanSeeThisImplementation')
            ->addModelTrait('IReactions', 'IReactionsImplementation');
    }
}
