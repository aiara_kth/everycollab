<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

use Angie\HTML;
use Angie\Search\SearchItem\SearchItemInterface as SearchItem;

/**
 * Framework level comment implementation.
 *
 * @package angie.frameworks.comments
 * @subpackage models
 */
abstract class FwComment extends BaseComment
{
    const SOURCE_WEB = 'web';
    const SOURCE_EMAIL = 'email';
    const SOURCE_API = 'api';

    /**
     * Return comment name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->getParent() instanceof IComments ?
            lang('Comment on :name', ['name' => $this->getParent()->getName()], false) :
            lang('Comment');
    }

    /**
     * Return base type name.
     *
     * @param  bool   $singular
     * @return string
     */
    public function getBaseTypeName($singular = true)
    {
        return $singular ? 'comment' : 'comments';
    }

    /**
     * Return proper type name in user's language.
     *
     * @param  bool     $lowercase
     * @param  Language $language
     * @return string
     */
    public function getVerboseType($lowercase = false, $language = null)
    {
        return $lowercase ? lang('comment', $language) : lang('Comment', $language);
    }

    /**
     * Check if comment can be viewed.
     *
     * @return bool
     */
    public function isAccessible()
    {
        return true;
    }

    // ---------------------------------------------------
    //  Interface implementations
    // ---------------------------------------------------

    /**
     * Return routing context name.
     *
     * @return string
     */
    public function getRoutingContext()
    {
        return 'comment';
    }

    /**
     * Return routing context parameters.
     *
     * @return array
     * @throws InvalidParamError
     */
    public function getRoutingContextParams()
    {
        return ['comment_id' => $this->getId()];
    }

    /**
     * Return a list of properties that are watched.
     *
     * @return array
     */
    public function touchParentOnPropertyChange()
    {
        return ['body', 'is_trashed', 'updated_on'];
    }

    /**
     * Include plain text version of body in the JSON response.
     *
     * @return bool
     */
    protected function includePlainTextBodyInJson()
    {
        return true;
    }

    // ---------------------------------------------------
    //  Permissions
    // ---------------------------------------------------

    /**
     * Returns true if $user can access this attachment.
     *
     * @param  User $user
     * @return bool
     */
    public function canView(User $user)
    {
        if ($this->getIsTrashed() && $this->getTrashedById() === $user->getId()) {
            return true;
        }

        return $this->getParent() && $this->getParent()->canView($user);
    }

    /**
     * Returns true if $user can update this comment.
     *
     * Only owner and comment author in given timeframe can update comment text
     *
     * @param  User $user
     * @return bool
     */
    public function canEdit(User $user)
    {
        if ($this->getIsTrashed()) {
            return false;
        }

        if ($user->isOwner()) {
            return true;
        } elseif ($this->isCreatedBy($user)) {
            return ($this->getCreatedOn()->getTimestamp() + 1800) > DateTimeValue::now()->getTimestamp();
        }

        return false;
    }

    /**
     * Returns true if $user can delete this comment.
     *
     * @param  User $user
     * @return bool
     */
    public function canDelete(User $user)
    {
        if ($this->getIsTrashed()) {
            return $user->isOwner() || $this->getTrashedById() === $user->getId();
        }

        return false;
    }

    // ---------------------------------------------------
    //  System
    // ---------------------------------------------------

    /**
     * Validate before save.
     *
     * @param ValidationErrors $errors
     */
    public function validate(ValidationErrors &$errors)
    {
        HTML::validateHTML($this->getBody(), 1) or $errors->addError('Comment is required', 'body');

        $this->validatePresenceOf('created_by_name') or $errors->addError('Author name is required', 'created_by_name');

        if ($this->validatePresenceOf('created_by_email')) {
            is_valid_email($this->getCreatedByEmail()) or $errors->addError('Authors email address is not valid', 'created_by_email');
        } else {
            $errors->addError('Authors email address is required', 'created_by_email');
        }
    }

    /**
     * Save comment into database.
     *
     * @throws Exception
     */
    public function save()
    {
        $search_index_affected = $this->isSearchIndexAffected();

        $parent = $this->getParent();

        try {
            DB::beginWork('Save comment @ ' . __CLASS__);

            // Subscribe mentioned users to parent
            if ($parent instanceof ISubscriptions) {
                $mentioned_users = !empty($this->getNewMentions()) ? Users::find([
                    'conditions' => ['id IN (?) AND is_trashed = ? AND is_archived = ?', $this->getNewMentions(), false, false],
                ]) : null;

                if ($mentioned_users) {
                    foreach ($mentioned_users as $mentioned_user) {
                        if (ConfigOptions::getValueFor('subscribe_on_mention', $mentioned_user)) {
                            $parent->subscribe($mentioned_user);
                        }
                    }
                }
            }

            parent::save();

            DB::commit('Comment saved @ ' . __CLASS__);
        } catch (Exception $e) {
            DB::rollback('Failed to save comment @ ' . __CLASS__);
            throw $e;
        }

        if ($search_index_affected && $parent instanceof SearchItem) {
            AngieApplication::search()->update($parent);
        }
    }

    /**
     * Return true if changes that are in this object affect parent's search index.
     *
     * @return bool
     */
    private function isSearchIndexAffected()
    {
        return $this->isNew()
            || $this->hasAttachmentUpdatesToSave()
            || $this->isModifiedField('is_trashed')
            || $this->isModifiedField('body');
    }

    // ---------------------------------------------------
    //  Activity logs
    // ---------------------------------------------------

    /**
     * Prepare and return creation log entry.
     *
     * @return ActivityLog|null
     */
    protected function getCreatedActivityLog()
    {
        $parent = $this->getParent();

        if ($parent instanceof IActivityLog) {
            $log = new CommentCreatedActivityLog();

            $log->setParent($parent);
            $log->setParentPath($parent->getObjectPath());
            $log->setComment($this);

            $created_by = $this instanceof ICreatedBy && $this->getCreatedBy() instanceof IUser ? $this->getCreatedBy() : AngieApplication::authentication()->getLoggedUser();

            if ($created_by instanceof IUser) {
                $log->setCreatedBy($created_by);
            }

            return $log;
        }

        return null;
    }

    /**
     * Clear all activity logs related to this object.
     */
    public function clearActivityLogs()
    {
        $parent = $this->getParent();

        if ($parent) {
            ActivityLogs::deleteByParentAndAdditionalProperty($parent, 'comment_id', $this->getId());
        } else {
            AngieApplication::log()->warning("Comment {comment_id} can't be deleted by parent ID, because parent was not found.", [
                'comment_id' => $this->getId(),
            ]);
        }
    }
}
