<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Comment created activity log.
 *
 * @package angie.frameworks.comments
 * @subpackage models
 */
abstract class FwCommentCreatedActivityLog extends InstanceCreatedActivityLog
{
    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return array_merge(parent::jsonSerialize(), ['comment_id' => $this->getAdditionalProperty('comment_id')]);
    }

    /**
     * Return comment instance.
     *
     * @return Comment
     */
    public function getComment()
    {
        return DataObjectPool::get('Comment', $this->getCommentId());
    }

    /**
     * @param Comment $comment
     */
    public function setComment(Comment $comment)
    {
        $this->setCreatedOn($comment->getCreatedOn());
        $this->setCreatedBy($comment->getCreatedBy());

        $this->setAdditionalProperty('comment_id', $comment->getId());
    }

    /**
     * Return comment ID.
     *
     * @return int
     */
    public function getCommentId()
    {
        return $this->getAdditionalProperty('comment_id');
    }

    /**
     * This method is called when we need to load related notification objects for API response.
     *
     * @param array $type_ids_map
     */
    public function onRelatedObjectsTypeIdsMap(array &$type_ids_map)
    {
        parent::onRelatedObjectsTypeIdsMap($type_ids_map);

        if (empty($type_ids_map['Comment'])) {
            $type_ids_map['Comment'] = [];
        }

        $type_ids_map['Comment'][] = $this->getAdditionalProperty('comment_id');
    }
}
