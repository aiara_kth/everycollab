<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Comments framework definition.
 *
 * @package angie.frameworks.comments
 */
class CommentsFramework extends AngieFramework
{
    const NAME = 'comments';

    /**
     * Short name of the framework.
     *
     * @var string
     */
    protected $name = 'comments';

    /**
     * Initialize module.
     */
    public function init()
    {
        parent::init();

        DataObjectPool::registerTypeLoader('Comment', function ($ids) {
            return Comments::findByIds($ids);
        });
    }

    /**
     * Define classes used by this framework.
     */
    public function defineClasses()
    {
        AngieApplication::setForAutoload([
            'FwComment' => __DIR__ . '/models/comments/FwComment.php',
            'FwComments' => __DIR__ . '/models/comments/FwComments.php',

            'IComments' => __DIR__ . '/models/IComments.php',
            'ICommentsImplementation' => __DIR__ . '/models/ICommentsImplementation.php',

            'FwCommentCreatedActivityLog' => __DIR__ . '/models/FwCommentCreatedActivityLog.php',
        ]);
    }

    /**
     * Define routes.
     */
    public function defineRoutes()
    {
        Router::map('comments', 'comments/:parent_type/:parent_id', ['module' => self::INJECT_INTO, 'controller' => 'comments', 'action' => ['GET' => 'index', 'POST' => 'add']], ['parent_type' => Router::MATCH_SLUG, 'parent_id' => Router::MATCH_ID]);
        Router::map('comment', 'comments/:comment_id', ['module' => self::INJECT_INTO, 'controller' => 'comments', 'action' => ['GET' => 'view', 'PUT' => 'edit', 'DELETE' => 'delete']], ['comment_id' => Router::MATCH_ID]);
    }

    /**
     * Define handlers.
     */
    public function defineHandlers()
    {
        $this->listen('on_rebuild_activity_logs');
        $this->listen('on_trash_sections');
        $this->listen('on_reset_manager_states');
        $this->listen('on_email_received');
        $this->listen('on_comment_created');
    }
}
