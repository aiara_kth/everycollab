<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * on_rebuild_activity_logs event handler.
 *
 * @package angie.frameworks.comments
 * @subpackage handlers
 */

/**
 * @param Angie\NamedList $actions
 */
function comments_handle_on_rebuild_activity_logs(&$actions)
{
    $actions->add('rebuild_comments', [
        'label' => 'Rebuild comment entries',
        'callback' => function () {
            DB::execute('INSERT INTO activity_logs (type, parent_type, parent_id, parent_path, created_on, created_by_id, created_by_name, created_by_email, raw_additional_properties) SELECT "CommentCreatedActivityLog" AS "type", parent_type, parent_id, "" AS "parent_path", created_on, created_by_id, created_by_name, created_by_email, CONCAT("a:1:{s:10:\"comment_id\";i:", id, ";}") AS "raw_additional_properties" FROM comments');
        },
    ]);
}
