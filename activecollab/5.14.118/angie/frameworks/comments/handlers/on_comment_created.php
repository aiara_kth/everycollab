<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * on_comment_created event handler.
 *
 * @package ActiveCollab.modules.system
 * @subpackage handlers
 */

/**
 * Handle on_comment_created event.
 *
 * @param Comment $comment
 */
function comments_handle_on_comment_created(Comment $comment)
{
    $event_type = 'CommentCreated';

    Webhooks::dispatch($comment, $event_type);

    if (AngieApplication::isEdgeChannel()) {
        Sockets::dispatch($comment, $event_type);
    }
}
