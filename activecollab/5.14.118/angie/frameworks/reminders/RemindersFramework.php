<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Reminders framework.
 *
 * @package angie.frameworks.reminders
 */
class RemindersFramework extends AngieFramework
{
    const NAME = 'reminders';

    /**
     * Short framework name.
     *
     * @var string
     */
    protected $name = 'reminders';

    /**
     * Initialize framework.
     */
    public function init()
    {
        parent::init();

        DataObjectPool::registerTypeLoader(['Reminder', 'CustomReminder'], function ($ids) {
            return Reminders::findByIds($ids);
        });
    }

    /**
     * Define classes used by this framework.
     */
    public function defineClasses()
    {
        AngieApplication::setForAutoload([
            'IReminders' => __DIR__ . '/models/IReminders.php',
            'IRemindersImplementation' => __DIR__ . '/models/IRemindersImplementation.php',

            'FwReminder' => __DIR__ . '/models/reminders/FwReminder.php',
            'FwReminders' => __DIR__ . '/models/reminders/FwReminders.php',

            'FwCustomReminder' => __DIR__ . '/models/FwCustomReminder.php',
            'FwCustomReminderNotification' => __DIR__ . '/notifications/FwCustomReminderNotification.php',
        ]);
    }

    /**
     * Define reminder module routes.
     */
    public function defineRoutes()
    {
        Router::map('reminders', 'reminders/:parent_type/:parent_id', ['module' => self::INJECT_INTO, 'controller' => 'reminders', 'action' => ['GET' => 'index', 'POST' => 'add']], ['parent_type' => Router::MATCH_SLUG, 'parent_id' => Router::MATCH_ID]);
        Router::map('reminder', 'reminders/:reminder_id', ['module' => self::INJECT_INTO, 'controller' => 'reminders', 'action' => ['DELETE' => 'delete']], ['reminder_id' => Router::MATCH_ID]);
    }

    /**
     * Define event handlers.
     */
    public function defineHandlers()
    {
        $this->listen('on_morning_mail');
        $this->listen('on_notification_inspector');
    }
}
