{$context|notification_subject_prefix}{lang type=$context->getVerboseType(true, $language) name=$context->getName() language=$language}Reminder for the :type: :name{/lang}
================================================================================
<h1 style="font-size: 16px; font-weight: bold; margin-top: 20px; margin-bottom: 16px;">
  {lang language=$language}Reminder for the task:{/lang} <span style="color: #ff0000">&#9873;</span> <br />
  <a href="{$context->getViewUrl()}">{$context->getName()}</a> <br />
  <span style="color: #ff0000; font-weight: normal;">{lang date=$reminder->getSendOn()->formatForUser($recipient, 0, $language) language=$language}Due on :date{/lang}</span>
</h1>

{if $reminder->getComment()}
<p>{$reminder->getComment()}</p>
{/if}

{notification_inspector context=$context subcontext=$reminder recipient=$recipient link_style='color: #999999; text-decoration: none;'}