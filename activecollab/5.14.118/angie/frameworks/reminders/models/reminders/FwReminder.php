<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Framework level reminder implementation.
 *
 * @package angie.frameworks.reminders
 * @subpackage models
 */
abstract class FwReminder extends BaseReminder implements IRoutingContext
{
    /**
     * Return true if parent is optional.
     *
     * @return bool
     */
    public function isParentOptional()
    {
        return false;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return array_merge(parent::jsonSerialize(), [
            'comment' => $this->getComment(),
            'send_on' => $this->getSendOn(),
            'subscribers' => $this->getSubscribersAsArray(), // This is recipients list, and it needs to be included in general reminder JSON
        ]);
    }

    /**
     * Send a reminder.
     */
    abstract public function send();

    /**
     * @param  User $user
     * @return bool
     */
    public function canView(User $user)
    {
        return $this->isCreatedBy($user);
    }

    /**
     * @param  User $user
     * @return bool
     */
    public function canEdit(User $user)
    {
        return $this->isCreatedBy($user);
    }

    /**
     * @param  User $user
     * @return bool
     */
    public function canDelete(User $user)
    {
        return $this->isCreatedBy($user);
    }

    /**
     * Return routing context name.
     *
     * @return string
     */
    public function getRoutingContext()
    {
        return 'reminder';
    }

    /**
     * Return routing context parameters.
     *
     * @return mixed
     */
    public function getRoutingContextParams()
    {
        return ['reminder_id' => $this->getId()];
    }

    /**
     * Return list of fields that are watched for changes.
     *
     * @return array|false
     */
    public function touchParentOnPropertyChange()
    {
        return ['parent_type', 'parent_id'];
    }

    /**
     * Validate before save.
     *
     * @param ValidationErrors $errors
     */
    public function validate(ValidationErrors &$errors)
    {
        $this->validatePresenceOf('send_on') or $errors->addError('Reminder time is required', 'send_on');

        parent::validate($errors);
    }
}
