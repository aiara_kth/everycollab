<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

require_once __DIR__ . '/functions.php';

defined('ELASTIC_SEARCH_INDEX_NAME') or define('ELASTIC_SEARCH_INDEX_NAME', null);

// define path to custom ca file
defined('VERIFY_APPLICATION_VENDOR_SSL') or define('VERIFY_APPLICATION_VENDOR_SSL', true);
defined('CUSTOM_CA_FILE') or define('CUSTOM_CA_FILE', __DIR__ . '/resources/ca-bundle.crt');

// Available application object states
const STATE_DELETED = 0;
const STATE_TRASHED = 1;
const STATE_ARCHIVED = 2;
const STATE_VISIBLE = 3;

// Project object priority
const PRIORITY_LOWEST = -2;
const PRIORITY_LOW = -1;
const PRIORITY_NORMAL = 0;
const PRIORITY_HIGH = 1;
const PRIORITY_HIGHEST = 2;

// Scheduled task types
const SCHEDULED_TASK_FREQUENTLY = 'frequently';
const SCHEDULED_TASK_HOURLY = 'hourly';
const SCHEDULED_TASK_DAILY = 'daily';

// Charts
const NON_WORK_DAY_COLOR_CHART = '#F7F7F7';
const DAY_OFF_COLOR_CHART = '#FFEDED';

/**
 * Environment framework definition.
 *
 * @package angie.frameworks.environment
 */
class EnvironmentFramework extends AngieFramework
{
    const NAME = 'environment';
    const PATH = __DIR__;

    /**
     * Framework name.
     *
     * @var string
     */
    protected $name = 'environment';

    /**
     * Initialize framework.
     */
    public function init()
    {
        parent::init();

        DataObjectPool::registerTypeLoader('Language', function ($ids) {
            return Languages::findByIds($ids);
        });

        DataObjectPool::registerTypeLoader('Currency', function ($ids) {
            return Currencies::findByIds($ids);
        });

        DataObjectPool::registerTypeLoader('DayOff', function ($ids) {
            return DayOffs::findByIds($ids);
        });

        DataObjectPool::registerTypeLoader('SystemNotification', function ($ids) {
            return SystemNotifications::findByIds($ids);
        });
    }

    /**
     * Define classes used by this framework.
     */
    public function defineClasses()
    {
        AngieApplication::setForAutoload([
            'ConfigOptions' => __DIR__ . '/models/config_options/ConfigOptions.class.php',
            'IConfigContext' => __DIR__ . '/models/config_options/IConfigContext.class.php',
            'ConfigOptionDnxError' => __DIR__ . '/models/config_options/ConfigOptionDnxError.class.php',

            'FwApplicationObject' => __DIR__ . '/models/application_objects/FwApplicationObject.class.php',
            'FwApplicationObjects' => __DIR__ . '/models/application_objects/FwApplicationObjects.class.php',

            'FwUploadedFile' => __DIR__ . '/models/uploaded_files/FwUploadedFile.class.php',
            'FwUploadedFiles' => __DIR__ . '/models/uploaded_files/FwUploadedFiles.class.php',

            // Notifications
            'FwInfoNotification' => __DIR__ . '/notifications/FwInfoNotification.class.php',

            // Errors
            'InMaintenanceModeError' => __DIR__ . '/models/errors/InMaintenanceModeError.class.php',

            'IAccessLog' => __DIR__ . '/models/access_logs/IAccessLog.class.php',
            'IAccessLogImplementation' => __DIR__ . '/models/access_logs/IAccessLogImplementation.class.php',

            'IAdditionalProperties' => __DIR__ . '/models/additional_properties/IAdditionalProperties.class.php',
            'IAdditionalPropertiesImplementation' => __DIR__ . '/models/additional_properties/IAdditionalPropertiesImplementation.class.php',

            'IChild' => __DIR__ . '/models/child/IChild.class.php',
            'IChildImplementation' => __DIR__ . '/models/child/IChildImplementation.class.php',

            // Created by
            'ICreatedBy' => __DIR__ . '/models/created_on_by/ICreatedBy.class.php',
            'ICreatedByImplementation' => __DIR__ . '/models/created_on_by/ICreatedByImplementation.class.php',
            'ICreatedOn' => __DIR__ . '/models/created_on_by/ICreatedOn.class.php',
            'ICreatedOnImplementation' => __DIR__ . '/models/created_on_by/ICreatedOnImplementation.class.php',

            'IUpdatedBy' => __DIR__ . '/models/updated_on_by/IUpdatedBy.class.php',
            'IUpdatedByImplementation' => __DIR__ . '/models/updated_on_by/IUpdatedByImplementation.class.php',
            'IUpdatedOn' => __DIR__ . '/models/updated_on_by/IUpdatedOn.class.php',
            'IUpdatedOnImplementation' => __DIR__ . '/models/updated_on_by/IUpdatedOnImplementation.class.php',

            // Body
            'IBody' => __DIR__ . '/models/body/IBody.class.php',
            'IBodyImplementation' => __DIR__ . '/models/body/IBodyImplementation.class.php',

            'IArchive' => __DIR__ . '/models/archive/IArchive.php',
            'IArchiveImplementation' => __DIR__ . '/models/archive/IArchiveImplementation.php',

            'ITrash' => __DIR__ . '/models/trash/ITrash.php',
            'ITrashImplementation' => __DIR__ . '/models/trash/ITrashImplementation.php',

            // Delegates
            'AngieFirewallDelegate' => __DIR__ . '/models/AngieFirewallDelegate.php',

            // Errors
            'FirewallError' => __DIR__ . '/errors/FirewallError.class.php',
            'RestoreFromTrashError' => __DIR__ . '/errors/RestoreFromTrashError.class.php',

            'IWhosAsking' => __DIR__ . '/models/IWhosAsking.php',

            // Globalization
            'FwCurrency' => __DIR__ . '/models/currencies/FwCurrency.class.php',
            'FwCurrencies' => __DIR__ . '/models/currencies/FwCurrencies.class.php',

            // Localisation
            'CountriesInterface' => __DIR__ . '/models/countries/CountriesInterface.php',
            'Countries' => __DIR__ . '/models/countries/Countries.php',

            'FwDayOff' => __DIR__ . '/models/day_offs/FwDayOff.class.php',
            'FwDayOffs' => __DIR__ . '/models/day_offs/FwDayOffs.class.php',

            'FwThirdPartyIntegration' => __DIR__ . '/models/integrations/FwThirdPartyIntegration.php',
            'FwGoogleDriveIntegration' => __DIR__ . '/models/integrations/FwGoogleDriveIntegration.php',
            'FwDropboxIntegration' => __DIR__ . '/models/integrations/FwDropboxIntegration.php',

            'CronIntegration' => __DIR__ . '/models/integrations/CronIntegration.php',
            'SearchIntegrationInterface' => __DIR__ . '/models/integrations/SearchIntegrationInterface.php',
            'SearchIntegration' => __DIR__ . '/models/integrations/SearchIntegration.php',

            'FwLanguage' => __DIR__ . '/models/languages/FwLanguage.class.php',
            'FwLanguages' => __DIR__ . '/models/languages/FwLanguages.class.php',

            // Assignee
            'IAssignees' => __DIR__ . '/models/assignee/IAssignees.php',
            'IAssigneesImplementation' => __DIR__ . '/models/assignee/IAssigneesImplementation.php',

            // Complete
            'IComplete' => __DIR__ . '/models/complete/IComplete.php',
            'ICompleteImplementation' => __DIR__ . '/models/complete/ICompleteImplementation.php',

            // Data filters
            'FwDataFilter' => __DIR__ . '/models/data_filters/FwDataFilter.php',
            'FwDataFilters' => __DIR__ . '/models/data_filters/FwDataFilters.php',

            'DataFilterConditionsError' => __DIR__ . '/models/data_filters/DataFilterConditionsError.php',
            'DataFilterExportError' => __DIR__ . '/models/data_filters/DataFilterExportError.php',

            // Favorites
            'FwFavorites' => __DIR__ . '/models/favorites/FwFavorites.php',
            'IFavorite' => __DIR__ . '/models/favorites/IFavorite.php',
            'IFavoriteImplementation' => __DIR__ . '/models/favorites/IFavoriteImplementation.php',

            'AbstractInitialSettingsCollection' => __DIR__ . '/models/initial_settings/AbstractInitialSettingsCollection.php',
            'FwInitialSettingsCollection' => __DIR__ . '/models/initial_settings/FwInitialSettingsCollection.php',
            'FwInitialUserSettingsCollection' => __DIR__ . '/models/initial_settings/FwInitialUserSettingsCollection.php',
            'IResetInitialSettingsTimestamp' => __DIR__ . '/models/initial_settings/IResetInitialSettingsTimestamp.php',

            'FwSystemNotification' => __DIR__ . '/models/system_notifications/FwSystemNotification.class.php',
            'FwSystemNotifications' => __DIR__ . '/models/system_notifications/FwSystemNotifications.class.php',

            'DiskSpaceSystemNotification' => __DIR__ . '/models/system_notifications/disk_space/DiskSpaceSystemNotification.class.php',
            'DiskSpaceSystemNotifications' => __DIR__ . '/models/system_notifications/disk_space/DiskSpaceSystemNotifications.class.php',

            'FreeTrialSystemNotification' => __DIR__ . '/models/system_notifications/free_trial/FreeTrialSystemNotification.class.php',
            'FreeTrialSystemNotifications' => __DIR__ . '/models/system_notifications/free_trial/FreeTrialSystemNotifications.class.php',

            'MembersExceededSystemNotification' => __DIR__ . '/models/system_notifications/members_exceeded/MembersExceededSystemNotification.class.php',
            'MembersExceededSystemNotifications' => __DIR__ . '/models/system_notifications/members_exceeded/MembersExceededSystemNotifications.class.php',

            'SubscriptionCancelledSystemNotification' => __DIR__ . '/models/system_notifications/subscription_cancelled/SubscriptionCancelledSystemNotification.class.php',
            'SubscriptionCancelledSystemNotifications' => __DIR__ . '/models/system_notifications/subscription_cancelled/SubscriptionCancelledSystemNotifications.class.php',

            'SupportExpirationSystemNotification' => __DIR__ . '/models/system_notifications/support_expiration/SupportExpirationSystemNotification.class.php',
            'SupportExpirationSystemNotifications' => __DIR__ . '/models/system_notifications/support_expiration/SupportExpirationSystemNotifications.class.php',

            'PaymentFailedSystemNotification' => __DIR__ . '/models/system_notifications/payment_failed/PaymentFailedSystemNotification.class.php',
            'PaymentFailedSystemNotifications' => __DIR__ . '/models/system_notifications/payment_failed/PaymentFailedSystemNotifications.class.php',

            'UpgradeAvailableSystemNotification' => __DIR__ . '/models/system_notifications/upgrade/UpgradeAvailableSystemNotification.class.php',
            'UpgradeAvailableSystemNotifications' => __DIR__ . '/models/system_notifications/upgrade/UpgradeAvailableSystemNotifications.class.php',

            // Test data objects
            'FwTestDataObject' => __DIR__ . '/models/test_data_objects/FwTestDataObject.class.php',
            'FwTestDataObjects' => __DIR__ . '/models/test_data_objects/FwTestDataObjects.class.php',

            'WebhookPayloadTransformatorInterface' => __DIR__ . '/models/webhooks/WebhookPayloadTransformatorInterface.php',
            'WebhookPayloadTransformator' => __DIR__ . '/models/webhooks/WebhookPayloadTransformator.php',

            'WebhooksIntegration' => __DIR__ . '/models/integrations/WebhooksIntegration.php',

            'LocalFilesStorage' => __DIR__ . '/models/storage/LocalFilesStorage.php',

            // who can see this
            'IWhoCanSeeThis' => __DIR__ . '/models/who_can_see_this/IWhoCanSeeThis.class.php',
            'IWhoCanSeeThisImplementation' => __DIR__ . '/models/who_can_see_this/IWhoCanSeeThisImplementation.class.php',
        ]);

        DataObjectPool::registerTypeLoader('DataFilter', function ($ids) {
            return DataFilters::findByIds($ids);
        });
    }

    /**
     * Define environment framework updates.
     */
    public function defineRoutes()
    {
        Router::map('api_info', 'info', ['controller' => 'utilities', 'action' => ['GET' => 'info'], 'module' => self::INJECT_INTO]);

        Router::map('trash', 'trash', ['action' => ['GET' => 'show_content', 'DELETE' => 'empty_trash'], 'controller' => 'trash', 'module' => self::INJECT_INTO], ['parent_type' => Router::MATCH_SLUG, 'parent_id' => Router::MATCH_ID]);
        Router::map('move_to_archive', 'move-to-archive/:parent_type/:parent_id', ['action' => ['PUT' => 'archive'], 'controller' => 'state', 'module' => self::INJECT_INTO], ['parent_type' => Router::MATCH_SLUG, 'parent_id' => Router::MATCH_ID]);
        Router::map('restore_from_archive', 'restore-from-archive/:parent_type/:parent_id', ['action' => ['PUT' => 'restore_from_archive'], 'controller' => 'state', 'module' => self::INJECT_INTO], ['parent_type' => Router::MATCH_SLUG, 'parent_id' => Router::MATCH_ID]);
        Router::map('move_to_trash', 'move-to-trash/:parent_type/:parent_id', ['action' => ['PUT' => 'trash'], 'controller' => 'state', 'module' => self::INJECT_INTO], ['parent_type' => Router::MATCH_SLUG, 'parent_id' => Router::MATCH_ID]);
        Router::map('restore_from_trash', 'restore-from-trash/:parent_type/:parent_id', ['action' => ['PUT' => 'restore_from_trash'], 'controller' => 'state', 'module' => self::INJECT_INTO], ['parent_type' => Router::MATCH_SLUG, 'parent_id' => Router::MATCH_ID]);
        Router::map('permanently_delete', 'permanently-delete/:parent_type/:parent_id', ['action' => ['DELETE' => 'permanently_delete'], 'controller' => 'state', 'module' => self::INJECT_INTO], ['parent_type' => Router::MATCH_SLUG, 'parent_id' => Router::MATCH_ID]);
        Router::map('reactivate', 'reactivate/:parent_type/:parent_id', ['action' => ['PUT' => 'reactivate'], 'controller' => 'state', 'module' => self::INJECT_INTO], ['parent_type' => Router::MATCH_SLUG, 'parent_id' => Router::MATCH_ID]);

        Router::map('access_logs', 'access-logs/:parent_type/:parent_id', ['action' => ['GET' => 'index', 'PUT' => 'log_access'], 'controller' => 'access_logs', 'module' => self::INJECT_INTO], ['parent_type' => Router::MATCH_SLUG, 'parent_id' => Router::MATCH_ID]);
        Router::map('upload_files', 'upload-files', ['action' => ['POST' => 'index', 'GET' => 'prepare'], 'controller' => 'upload_files', 'module' => self::INJECT_INTO]);
        Router::map('search', 'search', ['action' => ['GET' => 'query'], 'controller' => 'search', 'module' => self::INJECT_INTO]);

        Router::map('config_options', 'config-options', ['action' => ['GET' => 'get', 'PUT' => 'set'], 'controller' => 'config_options', 'module' => self::INJECT_INTO]);
        Router::map('personalized_config_options', 'personalized-config-options', ['action' => ['GET' => 'personalized_get', 'PUT' => 'personalized_set'], 'controller' => 'config_options', 'module' => self::INJECT_INTO]);

        Router::map('initial', 'initial', ['controller' => 'initial', 'action' => 'index', 'module' => self::INJECT_INTO]);
        Router::map('initial_speed_test', 'initial/test-speed', ['controller' => 'initial', 'action' => 'test_action_speed', 'module' => self::INJECT_INTO]);
        Router::map('system_status', 'system-status', ['controller' => 'system_status', 'action' => 'index', 'module' => self::INJECT_INTO]);
        Router::map('check_for_updates', 'system-status/check-for-updates', ['controller' => 'system_status', 'action' => 'check_for_updates', 'module' => self::INJECT_INTO]);
        Router::map('download_release', 'system-status/download-release', ['controller' => 'system_status', 'action' => ['GET' => 'get_download_progress', 'POST' => 'start_download'], 'module' => self::INJECT_INTO]);
        Router::map('check_environment', 'system-status/check-environment', ['controller' => 'system_status', 'action' => 'check_environment', 'module' => self::INJECT_INTO]);
        Router::map('upgrade', 'upgrade', ['controller' => 'upgrade', 'action' => ['GET' => 'index', 'POST' => 'finish'], 'module' => self::INJECT_INTO]);
        Router::map('upgrade_release_notes', 'upgrade/release-notes', ['controller' => 'upgrade', 'action' => 'release_notes', 'module' => self::INJECT_INTO]);

        Router::map('wallpapers', 'wallpapers', ['action' => ['GET' => 'index'], 'controller' => 'wallpapers', 'module' => self::INJECT_INTO]);

        Router::mapResource('day_offs', ['module' => self::INJECT_INTO]);

        Router::mapResource('languages', ['module' => self::INJECT_INTO, 'collection_actions' => ['GET' => 'index'], 'single_actions' => ['GET' => 'view']], function ($collection) {
            Router::map("$collection[name]_default", "$collection[path]/default", ['controller' => $collection['controller'], 'action' => ['GET' => 'view_default', 'PUT' => 'set_default'], 'module' => EnvironmentFramework::INJECT_INTO], $collection['requirements']);
        });

        Router::mapResource('currencies', ['module' => self::INJECT_INTO], function ($collection) {
            Router::map("$collection[name]_default", "$collection[path]/default", ['controller' => $collection['controller'], 'action' => ['GET' => 'view_default', 'PUT' => 'set_default'], 'module' => EnvironmentFramework::INJECT_INTO], $collection['requirements']);
        });
        Router::map('integration_singletons', 'integrations/:integration_type', ['module' => self::INJECT_INTO, 'controller' => 'integration_singletons', 'action' => ['GET' => 'get', 'PUT' => 'set', 'DELETE' => 'forget']], ['integration_type' => Router::MATCH_SLUG]);
        Router::mapResource('integrations', ['module' => self::INJECT_INTO]);
        Router::map('search_integration_configure', 'integrations/search/configure', ['module' => self::INJECT_INTO, 'controller' => 'search_integration', 'action' => ['PUT' => 'configure'], 'integration_type' => 'search']);
        Router::map('search_integration_test_connection', 'integrations/search/test-connection', ['module' => self::INJECT_INTO, 'controller' => 'search_integration', 'action' => ['POST' => 'test_connection'], 'integration_type' => 'search']);
        Router::map('search_integration_disconnect', 'integrations/search/disconnect', ['module' => self::INJECT_INTO, 'controller' => 'search_integration', 'action' => ['POST' => 'disconnect'], 'integration_type' => 'search']);
        Router::map('cron_integration', 'integrations/cron', ['module' => self::INJECT_INTO, 'controller' => 'cron_integration', 'action' => 'get', 'integration_type' => 'cron']);

        Router::map('localization', 'localization', ['controller' => 'localization', 'action' => ['GET' => 'show_settings', 'PUT' => 'save_settings'], 'module' => self::INJECT_INTO]);
        Router::map('localization_timezones', 'localization/timezones', ['controller' => 'localization', 'action' => ['GET' => 'show_timezones'], 'module' => self::INJECT_INTO]);
        Router::map('localization_date_formats', 'localization/date-formats', ['controller' => 'localization', 'action' => ['GET' => 'show_date_formats'], 'module' => self::INJECT_INTO]);
        Router::map('localization_time_formats', 'localization/time-formats', ['controller' => 'localization', 'action' => ['GET' => 'show_time_formats'], 'module' => self::INJECT_INTO]);
        Router::map('localization_countries', 'localization/countries', ['controller' => 'localization', 'action' => ['GET' => 'show_countries'], 'module' => self::INJECT_INTO]);
        Router::map('localization_eu_countries', 'localization/eu-countries', ['controller' => 'localization', 'action' => ['GET' => 'show_eu_countries'], 'module' => self::INJECT_INTO]);
        Router::map('localization_states', 'localization/states', ['controller' => 'localization', 'action' => ['GET' => 'show_states'], 'module' => self::INJECT_INTO]);
        Router::map('workweek', 'workweek', ['controller' => 'workweek', 'action' => ['GET' => 'show_settings', 'PUT' => 'save_settings'], 'module' => self::INJECT_INTO]);

        Router::map('reports', 'reports', ['controller' => 'reports', 'action' => ['GET' => 'index'], 'module' => self::INJECT_INTO]);
        Router::map('reports_run', 'reports/run', ['controller' => 'reports', 'action' => ['GET' => 'run'], 'module' => self::INJECT_INTO]);
        Router::map('reports_export', 'reports/export', ['controller' => 'reports', 'action' => ['GET' => 'export'], 'module' => self::INJECT_INTO]);

        Router::map('complete', 'complete/:parent_type/:parent_id', ['action' => ['PUT' => 'complete'], 'controller' => 'complete', 'module' => self::INJECT_INTO], ['parent_type' => Router::MATCH_SLUG, 'parent_id' => Router::MATCH_ID]);
        Router::map('open', 'open/:parent_type/:parent_id', ['action' => ['PUT' => 'open'], 'controller' => 'complete', 'module' => self::INJECT_INTO], ['parent_type' => Router::MATCH_SLUG, 'parent_id' => Router::MATCH_ID]);

        Router::mapResource('data_filters', ['module' => self::INJECT_INTO]);

        Router::map('favorites', 'favorites', ['action' => ['GET' => 'index'], 'controller' => 'favorites', 'module' => self::INJECT_INTO]);
        Router::map('favorite', 'favorites/:parent_type/:parent_id', ['action' => ['GET' => 'check', 'PUT' => 'add', 'DELETE' => 'remove'], 'controller' => 'favorites', 'module' => self::INJECT_INTO], ['parent_type' => Router::MATCH_SLUG, 'parent_id' => Router::MATCH_ID]);

        Router::map('compare_text', 'compare-text', ['action' => ['POST' => 'compare'], 'controller' => 'compare_text', 'module' => self::INJECT_INTO], ['parent_type' => Router::MATCH_SLUG, 'parent_id' => Router::MATCH_ID]);

        Router::map('system_notifications_dismiss', '/system-notifications/:notification_id/dismiss', ['controller' => 'system_notifications', 'action' => ['GET' => 'dismiss'], 'module' => self::INJECT_INTO]);

        Router::map(
            'warehouse_pingback',
            'integrations/warehouse/pingback',
            [
                'controller' => 'warehouse',
                'action' => ['POST' => 'pingback'],
                'module' => self::INJECT_INTO,
            ]
        );
        Router::map(
            'warehouse_store_export_complete_pingback',
            'integrations/warehouse/store/export',
            [
                'controller' => 'warehouse',
                'action' => ['POST' => 'store_export_pingback'],
                'module' => self::INJECT_INTO,
            ]
        );

        Router::map(
            'google_drive_batch',
            'integrations/google-drive/batch',
            [
                'controller' => 'google_drive',
                'action' => ['POST' => 'batch_add'],
                'module' => self::INJECT_INTO,
            ]
        );
        Router::map(
            'dropbox_batch',
            'integrations/dropbox/batch',
            [
                'controller' => 'dropbox',
                'action' => ['POST' => 'batch_add'],
                'module' => self::INJECT_INTO,
            ]
        );

        Router::map(
            'since_last_visit',
            'since-last-visit/:parent_type/:parent_id',
            [
                'action' => [
                    'GET' => 'index',
                ],
                'controller' => 'since_last_visit',
                'module' => self::INJECT_INTO,
            ],
            [
                'parent_type' => Router::MATCH_SLUG,
                'parent_id' => Router::MATCH_ID,
            ]
        );
    }

    /**
     * Define event handlers.
     */
    public function defineHandlers()
    {
        $this->listen('on_available_integrations');
        $this->listen('on_daily_maintenance');
        $this->listen('on_protected_config_options');
        $this->listen('on_rawtext_to_richtext');
        $this->listen('on_resets_initial_settings_timestamp');
        $this->listen('on_search_filters');
        $this->listen('on_system_status');
    }
}
