<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * rich_text helper implementation.
 *
 * @package angie.frameworks.environment
 * @subpackage helpers
 */

/**
 * Convert raw text to rich text (HTML).
 *
 * @param  string $content
 * @param  string $for
 * @return string
 */
function smarty_modifier_rich_text($content, $for = null)
{
    return Angie\HTML::toRichText($content, $for);
}
