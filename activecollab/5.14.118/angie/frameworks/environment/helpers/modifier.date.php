<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * date modifier implementation.
 *
 * @package angie.frameworks.environment
 */

/**
 * Return formated date.
 *
 * @param  string|DateTimeValue|DateValue $content
 * @param  int                            $offset
 * @param  IUser                          $user
 * @param  Language                       $language
 * @return string
 * @throws InvalidInstanceError
 */
function smarty_modifier_date($content, $offset = 0, $user = null, $language = null)
{
    if ($content && is_string($content)) {
        $content = DateTimeValue::makeFromString($content); // first try making object from string
    }

    if (!($user instanceof IUser)) {
        $user = AngieApplication::authentication()->getLoggedUser();
    }

    if ($content instanceof DateTimeValue) {
        return $content->formatDateForUser($user, $offset, $language);
    } elseif ($content instanceof DateValue) {
        return $content->formatForUser($user, $offset, $language);
    } else {
        throw new InvalidInstanceError('content', $content, 'DateValue');
    }
}
