<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

AngieApplication::useController('auth_required', EnvironmentFramework::INJECT_INTO);

/**
 * Wallpapers controller.
 *
 * @package angie.frameworks.environment
 * @subpackage controllers
 */
class FwWallpapersController extends AuthRequiredController
{
    /**
     * List available wallpapers.
     */
    public function index()
    {
        $wallpapers = [];
        $path = PUBLIC_PATH . '/wallpapers';

        $file_list = scandir($path);
        if (is_foreachable($file_list)) {
            foreach ($file_list as $file) {
                if (!in_array($file, ['.', '..'])) {
                    $full_path = $path . '/' . $file;
                    $path_info = pathinfo($full_path);
                    $extension = strtolower($path_info['extension']);
                    if (in_array($extension, ['png', 'jpg', 'jpeg'])) {
                        if (strpos($file, '.thumb.') !== false) {
                            $full_name = str_replace('.thumb.', '.', $file);
                            $full_image_path = $path . '/' . $full_name;
                            if (is_file($full_image_path)) {
                                $wallpapers[] = [
                                    'wallpaper_url' => AngieApplication::getWallpaperUrl($full_name),
                                    'thumbnail_url' => AngieApplication::getWallpaperUrl($file),
                                    'name' => $full_name,
                                ];
                            }
                        }
                    }
                }
            }
        }

        return $wallpapers;
    }
}
