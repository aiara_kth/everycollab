<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

use Angie\Http\Request;
use Angie\Http\Response;

AngieApplication::useController('selected_object', EnvironmentFramework::INJECT_INTO);

/**
 * Framework level state controller.
 *
 * @package angie.frameworks.environment
 * @subpackage controllers
 */
abstract class FwStateController extends SelectedObjectController
{
    /**
     * Selected object.
     *
     * @var ApplicationObject|IArchive|ITrash
     */
    protected $active_object;

    /**
     * Move to archive.
     *
     * @param  Request                               $request
     * @param  User                                  $user
     * @return ApplicationObject|IArchive|int|ITrash
     */
    public function archive(Request $request, User $user)
    {
        if ($this->active_object instanceof IArchive && $this->active_object->canArchive($user)) {
            $this->active_object->moveToArchive();

            return $this->active_object;
        }

        return Response::NOT_FOUND;
    }

    /**
     * Restore from archive.
     *
     * @param  Request                               $request
     * @param  User                                  $user
     * @return ApplicationObject|IArchive|int|ITrash
     */
    public function restore_from_archive(Request $request, User $user)
    {
        if ($this->active_object instanceof IArchive && $this->active_object->canArchive($user)) {
            $this->active_object->restoreFromArchive();

            return $this->active_object;
        }

        return Response::NOT_FOUND;
    }

    /**
     * Move to trash.
     *
     * @param  Request                               $request
     * @param  User                                  $user
     * @return ApplicationObject|IArchive|int|ITrash
     */
    public function trash(Request $request, User $user)
    {
        if ($this->active_object instanceof ITrash && $this->active_object->canTrash($user)) {
            $this->active_object->moveToTrash($user);

            return $this->active_object;
        }

        return Response::NOT_FOUND;
    }

    /**
     * Restore from trash.
     *
     * @param  Request                               $request
     * @param  User                                  $user
     * @return ApplicationObject|IArchive|int|ITrash
     */
    public function restore_from_trash(Request $request, User $user)
    {
        if ($this->active_object instanceof ITrash && $this->active_object->canRestoreFromTrash($user)) {
            $this->active_object->restoreFromTrash();

            return $this->active_object;
        }

        return Response::NOT_FOUND;
    }

    /**
     * Permanently delete an individual object.
     *
     * @param  Request $request
     * @param  User    $user
     * @return int
     */
    public function permanently_delete(Request $request, User $user)
    {
        if ($this->active_object->canDelete($user) && $this->active_object instanceof ITrash && $this->active_object->getIsTrashed()) {
            $this->active_object->delete();

            return Response::OK;
        }

        return Response::NOT_FOUND;
    }

    /**
     * Reactivate the object.
     *
     * @param  Request $request
     * @param  User    $user
     * @return int
     */
    public function reactivate(Request $request, User $user)
    {
        if ($this->active_object->canEdit($user) && ($this->active_object instanceof IArchive || $this->active_object instanceof ITrash)) {
            return DataManager::reactivate($this->active_object);
        }

        return Response::NOT_FOUND;
    }
}
