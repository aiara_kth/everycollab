<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Who can see this implementation.
 *
 * @package angie.frameworks.environment
 * @subpackage models
 */
trait IWhoCanSeeThisImplementation
{
    /**
     * {@inheritdoc}
     */
    public function canUserSeeThis(User $user)
    {
        return in_array($user->getId(), $this->whoCanSeeThis());
    }

    /**
     * {@inheritdoc}
     */
    public function whoCanSeeThis()
    {
        $result = [];

        if ($this instanceof IChild) {
            $parent = $this->getParent();

            if ($parent instanceof IChild && $parent instanceof IWhoCanSeeThis) {
                return $parent->whoCanSeeThis();
            }

            if ($parent instanceof IProjectElement) {
                $project = $parent->getProject();

                if ($project instanceof IMembers) {
                    foreach ($project->getMembers() as $member) {
                        if ($member->getIsTrashed()) {
                            continue;
                        }

                        if (
                            $parent instanceof IHiddenFromClients &&
                            $parent->getIsHiddenFromClients() &&
                            $member instanceof Client
                        ) {
                            continue;
                        }

                        $result[] = $member->getId();
                    }
                }

                $leader = $project->getLeader();

                if ($leader instanceof IUser && !$leader->getIsTrashed()) {
                    $result[] = $leader->getId();
                }
            }
        }

        return array_unique(array_merge(Users::findOwnerIds(), $result));
    }
}
