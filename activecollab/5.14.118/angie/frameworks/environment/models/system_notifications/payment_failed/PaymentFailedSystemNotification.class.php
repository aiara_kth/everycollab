<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Payment Failed system notification.
 *
 * @package angie.environment.payment_failed
 * @subpackage models
 */
class PaymentFailedSystemNotification extends SystemNotification
{
    /**
     * Return notification title.
     *
     * @return mixed
     */
    public function getTitle()
    {
        return lang('Payment failed!');
    }

    /**
     * Return notification body.
     *
     * @return mixed
     */
    public function getBody()
    {
        return lang('There was a problem with charging for your subscription. It may be that your card has expired. Please check if everything is ok or your account might become inaccessible in 7 days.');
    }

    /**
     * Return notification action.
     *
     * @return mixed
     */
    public function getAction()
    {
        return lang('Go to account settings');
    }

    /**
     * Return notification url.
     *
     * @return mixed
     */
    public function getUrl()
    {
        return AngieApplication::activeCollabId()->getChangeAccountUrl();
    }

    /**
     * Return is permanent.
     *
     * @return mixed
     */
    public function isPermanent()
    {
        return true;
    }
}
