<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * PaymentFailedSystemNotifications system notification.
 *
 * @package angie.environment
 * @subpackage models
 */
class PaymentFailedSystemNotifications extends SystemNotifications
{
    /**
     * Return 'type' attribute for polymorh model creation.
     *
     * @return string
     */
    public static function getType()
    {
        return 'PaymentFailedSystemNotification';
    }

    /**
     * Return true if this notification should ne raised.
     *
     * @return mixed|void
     */
    public static function shouldBeRaised()
    {
        return AngieApplication::isOnDemand() && ON_DEMAND_ACCOUNT_STATUS == OnDemand::STATUS_FAILED_PAYMENT;
    }
}
