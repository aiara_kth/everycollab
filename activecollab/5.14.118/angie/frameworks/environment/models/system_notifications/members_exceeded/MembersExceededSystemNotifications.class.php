<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * MembersExceededSystemNotifications system notification.
 *
 * @package angie.environment
 * @subpackage models
 */
class MembersExceededSystemNotifications extends SystemNotifications
{
    /**
     * Return 'type' attribute for polymorh model creation.
     *
     * @return string
     */
    public static function getType()
    {
        return 'MembersExceededSystemNotification';
    }

    /**
     * Return true if this notification should ne raised.
     *
     * @return bool
     */
    public static function shouldBeRaised()
    {
        if (defined('ON_DEMAND_PLAN_MAX_USERS')) {
            if (empty(ON_DEMAND_PLAN_MAX_USERS)) {
                return false;
            }

            return AngieApplication::isOnDemand() && Users::countActiveUsers() > ON_DEMAND_PLAN_MAX_USERS;
        } else {
            return false;
        }
    }
}
