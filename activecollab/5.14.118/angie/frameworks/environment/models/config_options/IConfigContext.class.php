<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Interface that objects which want to be able to use getValueFor and
 * setValueFor need to implement.
 *
 * @package angie.library
 */
interface IConfigContext
{
}
