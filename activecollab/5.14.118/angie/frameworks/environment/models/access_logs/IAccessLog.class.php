<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Access log interface.
 *
 * @package angie.frameworks.environment
 * @subpackage models
 */
interface IAccessLog
{
    /**
     * Return true if $user can view access logs.
     *
     * @param  User $user
     * @return bool
     */
    public function canViewAccessLogs(User $user);

    /**
     * Return number of downloads.
     *
     * @return int
     */
    public function getDownloadsCount();

    // ---------------------------------------------------
    //  Requirements
    // ---------------------------------------------------

    /**
     * @return int
     */
    public function getId();
}
