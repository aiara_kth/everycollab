<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

use Angie\Events;
use Angie\Globalization;

/**
 * Initial settings and collections collection.
 *
 * @package angie.frameworks.environment
 * @subpackage models
 */
abstract class FwInitialSettingsCollection extends AbstractInitialSettingsCollection
{
    /**
     * Cached tag value.
     *
     * @var string
     */
    private $tag = false;

    /**
     * Return collection etag.
     *
     * @param  IUser  $user
     * @param  bool   $use_cache
     * @return string
     */
    public function getTag(IUser $user, $use_cache = true)
    {
        if ($this->tag === false || empty($use_cache)) {
            $timestamps = [ConfigOptions::getValue('initial_settings_timestamp')];

            if (is_file(CONFIG_PATH . '/config.php')) {
                if ($config_file_modification_time = filemtime(CONFIG_PATH . '/config.php')) {
                    $timestamps[] = $config_file_modification_time;
                }
            }

            $this->tag = $this->prepareTagFromBits($user->getEmail(), implode('-', $timestamps));
        }

        return $this->tag;
    }

    /**
     * Run the query and return DB result.
     *
     * @return DbResult|DataObject[]
     */
    public function execute()
    {
        return array_merge(parent::execute(), ['timestamp' => ConfigOptions::getValue('initial_settings_timestamp')]);
    }

    /**
     * @var array
     */
    private $settings = false;

    /**
     * @return array
     */
    protected function getSettings()
    {
        if ($this->settings === false) {
            $this->settings = [
                'identity_name' => ConfigOptions::getValue('identity_name'),
                'on_demand' => AngieApplication::isOnDemand(),
                'on_demand_next_gen' => AngieApplication::isOnDemandNextGen(),
                'is_angie_in_test' => AngieApplication::isInTestMode(),
                'in_development' => AngieApplication::isInDevelopment(),
                'deployment_channel' => AngieApplication::getDeploymentChannel(),
                'built_in_language' => Languages::getBuiltIn(),
                'default_currency_id' => Currencies::getDefaultId(),
                'default_labels_name_max_length' => Labels::LABELS_NAME_MAX_LENGTH,
                'gd_loaded' => extension_loaded('gd'),
                'wallpaper_url' => AngieApplication::getWallpaperUrl('--WALLPAPER--'),
                'avatar_url' => AngieApplication::getProxyUrl(
                    'avatar',
                    \SystemModule::NAME,
                    [
                        'user_id' => '--USER-ID--',
                        'user_name' => '--USER-NAME--',
                        'user_email' => '--USER-EMAIL--',
                        'size' => '--SIZE--',
                        'timestamp' => '--UPDATED-ON--',
                    ]
                ),
                'show_visual_editor_toolbar' => ConfigOptions::getValue('show_visual_editor_toolbar'),
                'timezone' => ConfigOptions::getValue('time_timezone'),
                'gmt_offset' => Globalization::getGmtOffset(),
            ];

            $this->onLoadSettings($this->settings, $this->getWhosAsking());

            Events::trigger('on_initial_settings', [&$this->settings, $this->getWhosAsking()]);
        }

        return $this->settings;
    }

    /**
     * @var array
     */
    private $collections = false;

    /**
     * @return ModelCollection[]
     */
    protected function &getCollections()
    {
        if ($this->collections === false) {
            $this->collections = [
                'currencies' => Currencies::prepareCollection(DataManager::ALL, $this->getWhosAsking()),
                'languages' => Languages::prepareCollection(DataManager::ALL, $this->getWhosAsking()),
            ];

            $this->onLoadCollections($this->collections, $this->getWhosAsking());

            Events::trigger('on_initial_collections', [&$this->collections, $this->getWhosAsking()]);
        }

        return $this->collections;
    }
}
