<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * @package angie.frameworks.environment
 * @subpackage models
 */
abstract class WebhookPayloadTransformator implements WebhookPayloadTransformatorInterface
{
    /**
     * {@inheritdoc}
     */
    public function shouldTransform($url)
    {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function transform($event_type, DataObject $payload)
    {
        return $payload;
    }

    /**
     * {@inheritdoc}
     */
    public function getSupportedEvents()
    {
        return [];
    }
}
