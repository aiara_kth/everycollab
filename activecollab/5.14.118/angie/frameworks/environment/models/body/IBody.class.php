<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Body interface.
 *
 * @package angie.frameworks.environment
 * @subpackage models
 */
interface IBody
{
    /**
     * Return value of body field.
     *
     * @return string
     */
    public function getBody();

    /**
     * Set value of body field.
     *
     * @param  string $value
     * @return string
     */
    public function setBody($value);

    /**
     * Return array of newly mentioned users.
     *
     * @return array
     */
    public function getNewMentions();

    /**
     * Return formatted body.
     *
     * @return string
     */
    public function getFormattedBody();

    /**
     * Return plain text body.
     *
     * @return string
     */
    public function getPlainTextBody();
}
