<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

use Angie\HTML;

/**
 * Default IBody implementation.
 *
 * @package angie.frameworks.environment
 * @subpackage models
 */
trait IBodyImplementation
{
    /**
     * @var array
     */
    private $inline_image_codes = [];

    /**
     * Return array of new mentions.
     *
     * @var array
     */
    private $new_mentions = [];

    /**
     * Say hello to the parent object.
     */
    public function IBodyImplementation()
    {
        if ($this instanceof IHistory) {
            $this->addHistoryFields('body');
        }

        $this->registerEventHandler('on_history_field_renderers', function (&$renderers) {
            $renderers['body'] = function ($old_value, $new_value, Language $language) {
                if ($new_value && $old_value) {
                    return lang('Description updated', null, true, $language);
                } elseif ($new_value) {
                    return lang('Description added', null, true, $language);
                } elseif ($old_value) {
                    return lang('Description removed', null, true, $language);
                }
            };
        });

        $this->registerEventHandler('on_json_serialize', function (array &$result) {
            $result['body'] = (string) $this->getBody();
            $result['body_formatted'] = $this->getFormattedBody();

            if ($this->includePlainTextBodyInJson()) {
                $result['body_plain_text'] = $this->getPlainTextBody();
            }
        });

        $this->registerEventHandler('on_after_save', function ($is_new, $modifications) {
            if ($this->supportsInlineImages()) {
                if (!$is_new && !empty($modifications['body'])) {

                    /** @var Attachment[] $attachments */
                    if ($attachments = $this->getInlineAttachments()) {
                        foreach ($attachments as $attachment) {
                            if (strpos($modifications['body'][1], 'image-type="attachment" object-id="' . $attachment->getId() . '"') === false) {
                                $attachment->delete(true);
                            }
                        }
                    }
                }

                if (count($this->inline_image_codes)) {
                    if ($files = UploadedFiles::findByCodes($this->inline_image_codes)) {
                        $body = $this->getBody();

                        DB::transact(function () use ($files, &$body) {
                            foreach ($files as $file) {
                                /** @var $this IAttachments */
                                if ($attachment = $this->attachUploadedFile($file, IAttachments::INLINE)) {
                                    $body = str_replace('object-id="' . $file->getCode() . '"', 'object-id="' . $attachment->getId() . '"', $body);
                                }
                            }
                        }, 'Attaching files');

                        $this->setBody($body);
                        $this->save();
                    }

                    $this->inline_image_codes = [];
                }
            }
        });

        $this->registerEventHandler('on_prepare_field_value_before_set', function ($field, &$value) {
            if ($field == 'body') {
                $newly_mentioned_users = $inline_image_codes = [];

                $value = HTML::cleanUpHtml($value, function (simple_html_dom &$dom) use (&$newly_mentioned_users, &$inline_image_codes) {
                    $elements = $dom->find('span.new_mention');

                    if ($elements) {
                        foreach ($elements as $element) {
                            $user_id = (int) array_var($element->attr, 'data-user-id');

                            if ($user_id && !in_array($user_id, $newly_mentioned_users)) {
                                $newly_mentioned_users[] = $user_id;
                            }

                            $element->outertext = '<span class="mention">' . $element->innertext . '</span>';
                        }
                    }

                    if ($this->supportsInlineImages()) {
                        if ($elements = $dom->find('img[image-type=attachment]')) {
                            foreach ($elements as $element) {
                                $object_id = array_var($element->attr, 'object-id');

                                if ($object_id && strlen($object_id) == 40) {
                                    $inline_image_codes[] = $object_id;
                                }
                            }
                        }
                    }
                });

                if (count($newly_mentioned_users)) {
                    sort($newly_mentioned_users);
                }

                $this->new_mentions = $newly_mentioned_users;
                $this->inline_image_codes = $inline_image_codes;
            }
        });
    }

    /**
     * Register an internal event handler.
     *
     * @param $event
     * @param $handler
     * @throws InvalidParamError
     */
    abstract protected function registerEventHandler($event, $handler);

    /**
     * Return value of body field.
     *
     * @return string
     */
    abstract public function getBody();

    /**
     * Return formatted body.
     *
     * @return string
     */
    public function getFormattedBody()
    {
        return AngieApplication::cache()->getByObject($this, 'formatted_body', function () {
            return (string) HTML::toRichText($this->getBody(), null, get_class($this), $this->getId());
        });
    }

    // ---------------------------------------------------
    //  Mentions
    // ---------------------------------------------------

    /**
     * Include plain text version of body in the JSON response.
     *
     * @return bool
     */
    protected function includePlainTextBodyInJson()
    {
        return false;
    }

    /**
     * Return plain text body.
     *
     * @return string
     */
    public function getPlainTextBody()
    {
        return AngieApplication::cache()->getByObject($this, 'plain_text_body', function () {
            return (string) HTML::toPlainText($this->getFormattedBody());
        });
    }

    // ---------------------------------------------------
    //  Expectations
    // ---------------------------------------------------

    /**
     * Returns true if this object supports inline images.
     *
     * @return bool
     */
    private function supportsInlineImages()
    {
        return $this instanceof IAttachments;
    }

    /**
     * Set value of body field.
     *
     * @param  string $value
     * @return string
     */
    abstract public function setBody($value);

    /**
     * Save to database.
     */
    abstract public function save();

    /**
     * {@inheritdoc}
     */
    public function getNewMentions()
    {
        return $this->new_mentions;
    }

    /**
     * Set specific field value.
     *
     * Set value of the $field. This function will make sure that everything
     * runs fine - modifications are saved, in case of primary key old value
     * will be remembered in case we need to update the row and so on
     *
     * @param  string            $field
     * @param  mixed             $value
     * @return mixed
     * @throws InvalidParamError
     */
    abstract public function setFieldValue($field, $value);
}
