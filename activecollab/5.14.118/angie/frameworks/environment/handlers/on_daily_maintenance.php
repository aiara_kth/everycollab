<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

use Angie\Trash;

/**
 * on_daily_maintenance event handler.
 *
 * @package angie.frameworks.environment
 * @subpackage handlers
 */

/**
 * Handle on daily task.
 */
function environment_handle_on_daily_maintenance()
{
    Router::cleanUpCache();
    UploadedFiles::cleanUp();

    if ($owner = Users::findFirstOwner()) {
        Trash::emptyTrash($owner, null, DateTimeValue::makeFromString('-30 days'));
    }

    SystemNotifications::toggle();

    AngieApplication::jobs()->getQueue()->cleanUp();
}
