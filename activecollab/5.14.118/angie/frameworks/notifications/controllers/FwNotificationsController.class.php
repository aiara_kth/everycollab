<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

AngieApplication::useController('auth_required', NotificationsFramework::INJECT_INTO);

use Angie\Http\Request;

/**
 * Framework level notifications controller.
 *
 * @package angie.frameworks.notifications
 * @subpackage controllers
 */
class FwNotificationsController extends AuthRequiredController
{
    /**
     * List all notifications for the logged user.
     *
     * @param  Request                     $request
     * @param  User                        $user
     * @return UserNotificationsCollection
     */
    public function index(Request $request, User $user)
    {
        return Users::prepareCollection('notifications_for_recipient_' . $user->getId(), $user);
    }

    /**
     * List unread notifications for the logged user.
     *
     * @param  Request                     $request
     * @param  User                        $user
     * @return UserNotificationsCollection
     */
    public function unread(Request $request, User $user)
    {
        return Users::prepareCollection('unread_notifications_for_recipient_' . $user->getId(), $user);
    }

    /**
     * Get the count of unread object updates for the logged user.
     *
     * @param  Request $request
     * @param  User    $user
     * @return array
     */
    public function object_updates_unread_count(Request $request, User $user)
    {
        /** @var UserObjectUpdatesCollection $collection */
        $collection = Users::prepareCollection('recent_object_updates_for_recipient_' . $user->getId(), $user);

        return [
            'is_ok' => true,
            'count' => $collection->countUnread(),
        ];
    }

    /**
     * Return object updates collection for the given user.
     *
     * @param  Request                     $request
     * @param  User                        $user
     * @return UserObjectUpdatesCollection
     */
    public function object_updates(Request $request, User $user)
    {
        return Users::prepareCollection('object_updates_for_recipient_' . $user->getId() . '_page_' . $request->getPage(), $user);
    }

    /**
     * Return recent object updates collection for the given user.
     *
     * @param  Request                     $request
     * @param  User                        $user
     * @return UserObjectUpdatesCollection
     */
    public function recent_object_updates(Request $request, User $user)
    {
        return Users::prepareCollection('recent_object_updates_for_recipient_' . $user->getId(), $user);
    }

    /**
     * Mark all notifications as read.
     *
     * @param  Request $request
     * @param  User    $user
     * @return array
     */
    public function mark_all_as_read(Request $request, User $user)
    {
        return Notifications::updateReadStatusForRecipient($user, true);
    }
}
