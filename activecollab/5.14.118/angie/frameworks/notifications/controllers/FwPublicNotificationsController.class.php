<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

use Angie\Http\Request;
use Angie\Http\Response;

AngieApplication::useController('auth_not_required', EnvironmentFramework::INJECT_INTO);

/**
 * Framework level public notifications controller.
 *
 * @package angie.frameworks.subscriptions
 * @subpackage controllers
 */
abstract class FwPublicNotificationsController extends AuthNotRequiredController
{
    /**
     * Subscribe user with one click.
     *
     * @param Request $request
     */
    public function subscribe(Request $request)
    {
        if ($code = $request->get('code')) {
            $parts = explode('-', strtoupper($code));

            if (count($parts) > 1) {
                $notification = array_shift($parts);

                $subscribed = null;
                $subscribed_message = $undo_code = '';

                Angie\Events::trigger('on_handle_public_subscribe', [$notification, $parts, &$subscribed, &$subscribed_message, &$undo_code]);

                if ($subscribed !== null && $subscribed_message) {
                    return [
                        'subscribed' => $subscribed,
                        'subscribed_message' => $subscribed_message,
                        'undo_code' => $undo_code,
                        'undo_url' => $undo_code ? Router::assemble('public_notifications_subscribe', ['code' => $undo_code]) : '',
                    ];
                } else {
                    return Response::NOT_FOUND;
                }
            } else {
                return Response::BAD_REQUEST;
            }
        } else {
            return Response::BAD_REQUEST;
        }
    }

    /**
     * One click unsubscribe, no login required.
     *
     * @param Request $request
     */
    public function unsubscribe(Request $request)
    {
        if ($code = $request->get('code')) {
            $parts = explode('-', strtoupper($code));

            if (count($parts) > 1) {
                $notification = array_shift($parts);

                $unsubscribed = null;
                $unsubscribed_message = $undo_code = '';

                Angie\Events::trigger('on_handle_public_unsubscribe', [$notification, $parts, &$unsubscribed, &$unsubscribed_message, &$undo_code]);

                if ($unsubscribed !== null && $unsubscribed_message) {
                    return [
                        'unsubscribed' => $unsubscribed,
                        'unsubscribed_message' => $unsubscribed_message,
                        'undo_code' => $undo_code,
                        'undo_url' => $undo_code ? Router::assemble('public_notifications_subscribe', ['code' => $undo_code]) : '',
                    ];
                } else {
                    return Response::NOT_FOUND;
                }
            } else {
                return Response::BAD_REQUEST;
            }
        } else {
            return Response::BAD_REQUEST;
        }
    }
}
