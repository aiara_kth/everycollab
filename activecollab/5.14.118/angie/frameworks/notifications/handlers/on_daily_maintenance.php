<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * on_daily_maintenance event handler.
 *
 * @package angie.frameworks.notifications
 * @subpackage handlers
 */

/**
 * Handle on_daily_maintenance event.
 */
function notifications_handle_on_daily_maintenance()
{
    Notifications::cleanUp();
}
