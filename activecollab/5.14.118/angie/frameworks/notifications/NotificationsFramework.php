<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Notifications framework definition.
 *
 * @package angie.frameworks.notifications
 */
class NotificationsFramework extends AngieFramework
{
    const NAME = 'notifications';
    const PATH = __DIR__;

    /**
     * Framework name.
     *
     * @var string
     */
    protected $name = 'notifications';

    /**
     * Define classes used by this framework.
     */
    public function defineClasses()
    {
        AngieApplication::setForAutoload([
            'AngieNotificationsDelegate' => __DIR__ . '/models/AngieNotificationsDelegate.php',

            'UserNotificationsCollection' => __DIR__ . '/models/UserNotificationsCollection.php',
            'FwUserObjectUpdatesCollection' => __DIR__ . '/models/FwUserObjectUpdatesCollection.php',

            'NotificationChannel' => __DIR__ . '/models/channels/NotificationChannel.php',
            'WebInterfaceNotificationChannel' => __DIR__ . '/models/channels/WebInterfaceNotificationChannel.php',
            'RealTimeNotificationChannel' => __DIR__ . '/models/channels/RealTimeNotificationChannel.php',

            'INewInstanceUpdate' => __DIR__ . '/models/INewInstanceUpdate.php',
        ]);
    }

    /**
     * Define notification framework routes.
     */
    public function defineRoutes()
    {
        Router::mapResource('notifications', ['module' => self::INJECT_INTO], function ($collection) {
            Router::map("$collection[name]_unread", "$collection[path]/unread", ['controller' => $collection['controller'], 'action' => ['GET' => 'unread'], 'module' => $collection['module']], $collection['requirements']);
            Router::map("$collection[name]_object_updates", "$collection[path]/object-updates", ['controller' => $collection['controller'], 'action' => ['GET' => 'object_updates'], 'module' => $collection['module']], $collection['requirements']);
            Router::map("$collection[name]_object_updates_unread_count", "$collection[path]/object-updates/unread-count", ['controller' => $collection['controller'], 'action' => ['GET' => 'object_updates_unread_count'], 'module' => $collection['module']], $collection['requirements']);
            Router::map("$collection[name]_recent_object_updates", "$collection[path]/object-updates/recent", ['controller' => $collection['controller'], 'action' => ['GET' => 'recent_object_updates'], 'module' => $collection['module']], $collection['requirements']);
            Router::map("$collection[name]_mark_all_as_read", "$collection[path]/mark-all-as-read", ['controller' => $collection['controller'], 'action' => ['PUT' => 'mark_all_as_read'], 'module' => $collection['module']], $collection['requirements']);
        });

        Router::map('public_notifications_subscribe', 'public/notifications/subscribe', ['controller' => 'public_notifications', 'action' => 'subscribe', 'module' => self::INJECT_INTO]);
        Router::map('public_notifications_unsubscribe', 'public/notifications/unsubscribe', ['controller' => 'public_notifications', 'action' => 'unsubscribe', 'module' => self::INJECT_INTO]);
    }

    /**
     * Define event handlers.
     */
    public function defineHandlers()
    {
        $this->listen('on_daily_maintenance');
    }
}
