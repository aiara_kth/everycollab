<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Subscriptions framework definition.
 *
 * @package angie.frameworks.subscriptions
 */
class SubscriptionsFramework extends AngieFramework
{
    const NAME = 'subscriptions';

    /**
     * Short framework name.
     *
     * @var string
     */
    protected $name = 'subscriptions';

    /**
     * Define classes used by this framework.
     */
    public function defineClasses()
    {
        AngieApplication::setForAutoload([
            'ISubscriptions' => __DIR__ . '/models/ISubscriptions.class.php',
            'ISubscriptionsImplementation' => __DIR__ . '/models/ISubscriptionsImplementation.class.php',

            'FwSubscription' => __DIR__ . '/models/subscriptions/FwSubscription.class.php',
            'FwSubscriptions' => __DIR__ . '/models/subscriptions/FwSubscriptions.class.php',
        ]);
    }

    /**
     * Define framework routes.
     */
    public function defineRoutes()
    {
        Router::map('subscribers', 'subscribers/:parent_type/:parent_id', ['action' => ['GET' => 'index', 'POST' => 'bulk_subscribe', 'PUT' => 'bulk_update', 'DELETE' => 'bulk_unsubscribe'], 'controller' => 'subscribers', 'module' => self::INJECT_INTO], ['parent_type' => Router::MATCH_SLUG, 'parent_id' => Router::MATCH_ID]);
        Router::map('subscriber', 'subscribers/:parent_type/:parent_id/users/:user_id', ['action' => ['POST' => 'subscribe', 'DELETE' => 'unsubscribe'], 'controller' => 'subscribers', 'module' => self::INJECT_INTO], ['parent_type' => Router::MATCH_SLUG, 'parent_id' => Router::MATCH_ID, 'user_id' => Router::MATCH_ID]);
    }

    /**
     * Event handlers.
     */
    public function defineHandlers()
    {
        $this->listen('on_handle_public_unsubscribe');
    }
}
