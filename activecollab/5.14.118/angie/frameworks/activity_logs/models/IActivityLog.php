<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Activity log interface.
 *
 * @package angie.frameworks.activity_logs
 * @subpackage models
 */
interface IActivityLog
{
    /**
     * Gag.
     */
    public function gag();

    /**
     * Ungag.
     */
    public function ungag();

    /**
     * @return bool
     */
    public function isGagged();

    /**
     * Clear all activity logs related to this object.
     */
    public function clearActivityLogs();
}
