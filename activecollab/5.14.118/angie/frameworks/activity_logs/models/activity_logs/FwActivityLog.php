<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Framework level activity log row.
 *
 * @package angie.frameworks.activity_logs
 * @subpackage models
 */
abstract class FwActivityLog extends BaseActivityLog
{
    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return array_merge(parent::jsonSerialize(), [
            'created_by_name' => $this->getCreatedByName(),
            'created_by_email' => $this->getCreatedByEmail(),
            'parent_path' => $this->getParentPath(),
        ]);
    }

    /**
     * Return routing context name.
     *
     * @return string
     */
    public function getRoutingContext()
    {
        return 'activity_log';
    }

    /**
     * Return routing context parameters.
     *
     * @return mixed
     */
    public function getRoutingContextParams()
    {
        return ['activity_log_id' => $this->getId()];
    }

    /**
     * This method is called when we need to load related notification objects for API response.
     *
     * @param array $type_ids_map
     */
    public function onRelatedObjectsTypeIdsMap(array &$type_ids_map)
    {
    }
}
