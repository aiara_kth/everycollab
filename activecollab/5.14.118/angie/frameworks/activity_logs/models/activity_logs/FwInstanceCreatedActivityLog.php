<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Framework level instance created activity log implementation.
 *
 * @package angie.frameworks.activity_logs
 * @subpackage models
 */
abstract class FwInstanceCreatedActivityLog extends ActivityLog
{
}
