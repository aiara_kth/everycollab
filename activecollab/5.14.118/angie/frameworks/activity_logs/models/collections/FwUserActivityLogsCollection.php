<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Framwwork level user activity logs collection.
 *
 * @package angie.frameworks.notifications
 * @subpackage models
 */
abstract class FwUserActivityLogsCollection extends CompositeCollection
{
    use IWhosAsking, IActivityLogsCollection;

    /**
     * Return model name.
     *
     * @return string
     */
    public function getModelName()
    {
        return 'Users';
    }

    /**
     * @var User
     */
    private $for_or_by;

    /**
     * Return for or by instance.
     *
     * @return User
     */
    protected function &getForOrBy()
    {
        return $this->for_or_by;
    }

    /**
     * Set who the logs are for.
     *
     * @param  User              $for_or_by
     * @return $this
     * @throws InvalidParamError
     */
    public function &setForOrBy(User $for_or_by)
    {
        if ($for_or_by instanceof User) {
            $this->for_or_by = $for_or_by;
        } else {
            throw new InvalidParamError('for_or_by', $for_or_by, 'User');
        }

        return $this;
    }

    // ---------------------------------------------------
    //  Utility methods
    // ---------------------------------------------------

    /**
     * Return timestamp hash.
     *
     * @return string
     */
    public function getTimestampHash()
    {
        return sha1($this->for_or_by->getUpdatedOn()->toMySQL() . ',' . $this->getActivityLogsCollection()->getTimestampHash('updated_on'));
    }
}
