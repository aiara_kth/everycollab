<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Common code for all activity log collections.
 *
 * @package angie.frameworks.activity_logs
 * @subpackage models
 */
trait IActivityLogsCollection
{
    /**
     * Cached tag value.
     *
     * @var string
     */
    private $tag = false;

    /**
     * Return collection etag.
     *
     * @param  IUser  $user
     * @param  bool   $use_cache
     * @return string
     */
    public function getTag(IUser $user, $use_cache = true)
    {
        if ($this->tag === false || empty($use_cache)) {
            $this->tag = $this->prepareTagFromBits($user->getEmail(), $this->getTimestampHash());
        }

        return $this->tag;
    }

    /**
     * Run the query and return DB result.
     *
     * @return DbResult|DataObject[]
     */
    public function execute()
    {
        foreach (['getWhosAsking', 'getForOrBy'] as $method_name) {
            if (method_exists($this, $method_name)) {
                /** @var User $user */
                if ($user = $this->$method_name()) {
                    $this->preload_user_details[] = $user->getId();
                }
            }
        }

        /** @var ActivityLog[] $activity_logs */
        if ($activity_logs = $this->getActivityLogsCollection()->execute()) {
            $type_ids_map = [];

            foreach ($activity_logs as $activity_log) {
                $this->analyzeForPreload($activity_log);

                $parent_type = $activity_log->getParentType();

                if (empty($type_ids_map[$parent_type])) {
                    $type_ids_map[$parent_type] = [];
                }

                $type_ids_map[$parent_type][] = $activity_log->getParentId();

                $activity_log->onRelatedObjectsTypeIdsMap($type_ids_map);
            }

            $this->preload();

            $related = DataObjectPool::getByTypeIdsMap($type_ids_map);
        } else {
            $activity_logs = $related = [];
        }

        return ['activity_logs' => $activity_logs, 'related' => $related];
    }

    /**
     * Return number of records that match conditions set by the collection.
     *
     * @return int
     */
    public function count()
    {
        return $this->getActivityLogsCollection()->count();
    }

    /**
     * @var array
     */
    protected $preload_user_details = [];

    /**
     * @var array
     */
    private $types_with_attachments = [], $preload_attachment_details = [];

    /**
     * @var array
     */
    private $types_with_comments = [], $preload_comment_details = [];

    /**
     * @var array
     */
    private $types_with_labels = [], $preload_label_details = [];

    /**
     * Analyze record for preload().
     *
     * @param Activitylog $activity_log
     */
    protected function analyzeForPreload(Activitylog $activity_log)
    {
        $parent_type = $activity_log->getParentType();
        $parent_id = $activity_log->getParentId();

        if ($activity_log instanceof CommentCreatedActivityLog) {
            if (empty($this->preload_attachment_details['Comment'])) {
                $this->preload_attachment_details['Comment'] = [];
            }

            in_array($activity_log->getCommentId(), $this->preload_attachment_details['Comment']) or $this->preload_attachment_details['Comment'][] = $activity_log->getCommentId();
        }

        if ($activity_log instanceof InstanceUpdatedActivityLog) {
            $modificaitons = $activity_log->getModifications();

            if (isset($modificaitons['assignee_id'])) {
                if ($modificaitons['assignee_id'][0]) {
                    $this->preload_user_details[] = $modificaitons['assignee_id'][0];
                }

                if ($modificaitons['assignee_id'][1]) {
                    $this->preload_user_details[] = $modificaitons['assignee_id'][1];
                }
            }
        }

        if ($activity_log->getCreatedById()) {
            $this->preload_user_details[] = $activity_log->getCreatedById();
        }

        if ((new ReflectionClass($parent_type))->isSubclassOf('User')) {
            $this->preload_user_details[] = $parent_id;
        }

        // Comments
        if ($this->parentTypeImplementsComments($parent_type)) {
            if (empty($this->preload_comment_details[$parent_type])) {
                $this->preload_comment_details[$parent_type] = [$parent_id];
            } else {
                if (!in_array($parent_id, $this->preload_comment_details[$parent_type])) {
                    $this->preload_comment_details[$parent_type][] = $parent_id;
                }
            }
        }

        // Attachments
        if ($this->parentTypeImplementsAttachments($parent_type)) {
            if (empty($this->preload_attachment_details[$parent_type])) {
                $this->preload_attachment_details[$parent_type] = [$parent_id];
            } else {
                if (!in_array($parent_id, $this->preload_attachment_details[$parent_type])) {
                    $this->preload_attachment_details[$parent_type][] = $parent_id;
                }
            }
        }

        // Labels
        if ($this->parentTypeImplementsLabels($parent_type)) {
            if (empty($this->preload_label_details[$parent_type])) {
                $this->preload_label_details[$parent_type] = [$parent_id];
            } else {
                if (!in_array($parent_id, $this->preload_label_details[$parent_type])) {
                    $this->preload_label_details[$parent_type][] = $parent_id;
                }
            }
        }
    }

    /**
     * Return true if $parent_type implements comments.
     *
     * @param  string $parent_type
     * @return bool
     */
    private function parentTypeImplementsComments($parent_type)
    {
        if (!isset($this->types_with_comments[$parent_type])) {
            $this->types_with_comments[$parent_type] = (new ReflectionClass($parent_type))->implementsInterface('IComments');
        }

        return $this->types_with_comments[$parent_type];
    }

    /**
     * Return true if $parent_type implements attachments.
     *
     * @param  string $parent_type
     * @return bool
     */
    private function parentTypeImplementsAttachments($parent_type)
    {
        if (!isset($this->types_with_attachments[$parent_type])) {
            $this->types_with_attachments[$parent_type] = (new ReflectionClass($parent_type))->implementsInterface('IAttachments');
        }

        return $this->types_with_attachments[$parent_type];
    }

    /**
     * Return true if $parent_type implements labels.
     *
     * @param  string $parent_type
     * @return bool
     */
    private function parentTypeImplementsLabels($parent_type)
    {
        if (!isset($this->types_with_labels[$parent_type])) {
            $this->types_with_labels[$parent_type] = (new ReflectionClass($parent_type))->implementsInterface('ILabels');
        }

        return $this->types_with_labels[$parent_type];
    }

    /**
     * Preload details when possible.
     */
    protected function preload()
    {
        foreach ($this->preload_comment_details as $parent_type => $parent_ids) {
            Comments::preloadCountByParents($parent_type, $parent_ids);
        }

        foreach ($this->preload_attachment_details as $parent_type => $parent_ids) {
            Attachments::preloadDetailsByParents($parent_type, $parent_ids);
        }

        foreach ($this->preload_label_details as $parent_type => $parent_ids) {
            Labels::preloadDetailsByParents($parent_type, $parent_ids);
        }

        $this->preload_user_details = array_unique($this->preload_user_details);

        Users::preloadLastLoginOn($this->preload_user_details);
        Users::preloadAdditionalEmailAddresses($this->preload_user_details);
    }

    // ---------------------------------------------------
    //  Expectations
    // ---------------------------------------------------

    /**
     * @return string
     */
    abstract public function getTimestampHash();

    /**
     * Return assigned tasks collection.
     *
     * @return ModelCollection
     * @throws ImpossibleCollectionError
     */
    abstract protected function &getActivityLogsCollection();

    /**
     * Prepare collection tag from bits of information.
     *
     * @param  string $user_email
     * @param  string $hash
     * @return string
     */
    abstract protected function prepareTagFromBits($user_email, $hash);
}
