<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Activity logs framework definition file.
 *
 * @package angie.frameworks.activity_logs
 */
class ActivityLogsFramework extends AngieFramework
{
    const NAME = 'activity_logs';

    /**
     * Short framework name.
     *
     * @var string
     */
    protected $name = 'activity_logs';

    /**
     * Define classes used by this framework.
     */
    public function defineClasses()
    {
        AngieApplication::setForAutoload([
            'FwActivityLog' => __DIR__ . '/models/activity_logs/FwActivityLog.php',
            'FwActivityLogs' => __DIR__ . '/models/activity_logs/FwActivityLogs.php',

            'FwInstanceCreatedActivityLog' => __DIR__ . '/models/activity_logs/FwInstanceCreatedActivityLog.php',
            'FwInstanceUpdatedActivityLog' => __DIR__ . '/models/activity_logs/FwInstanceUpdatedActivityLog.php',

            'IActivityLog' => __DIR__ . '/models/IActivityLog.php',
            'IActivityLogImplementation' => __DIR__ . '/models/IActivityLogImplementation.php',

            'IActivityLogsCollection' => __DIR__ . '/models/collections/IActivityLogsCollection.php',
            'FwUserActivityLogsCollection' => __DIR__ . '/models/collections/FwUserActivityLogsCollection.php',

            'UserActivityLogsForCollection' => __DIR__ . '/models/collections/UserActivityLogsForCollection.php',
            'DailyUserActivityLogsForCollection' => __DIR__ . '/models/collections/DailyUserActivityLogsForCollection.php',
            'UserActivityLogsByCollection' => __DIR__ . '/models/collections/UserActivityLogsByCollection.php',

            'FwActivityLogsInCollection' => __DIR__ . '/models/collections/FwActivityLogsInCollection.php',
        ]);
    }

    /**
     * Define routes.
     */
    public function defineRoutes()
    {
        Router::mapResource('activity_logs'); // We needs so ActivityLog can be described :(

        Router::map('whats_new', 'whats-new', ['controller' => 'whats_new', 'action' => ['GET' => 'index'], 'module' => self::INJECT_INTO]);
        Router::map('whats_new_daily', 'whats-new/daily/:day', ['controller' => 'whats_new', 'action' => ['GET' => 'daily'], 'module' => self::INJECT_INTO], ['day' => Router::MATCH_DATE]);
    }
}
