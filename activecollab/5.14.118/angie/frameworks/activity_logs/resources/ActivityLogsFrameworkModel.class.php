<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Activity logs framework model definition.
 *
 * @package angie.frameworks.activity_logs
 * @subpackage resources
 */
class ActivityLogsFrameworkModel extends AngieFrameworkModel
{
    /**
     * Construct activity logs framework model.
     *
     * @param ActivityLogsFramework $parent
     */
    public function __construct(ActivityLogsFramework $parent)
    {
        parent::__construct($parent);

        $this->addModel(DB::createTable('activity_logs')->addColumns([
            new DBIdColumn(),
            DBTypeColumn::create('ActivityLog'),
            new DBParentColumn(),
            DBStringColumn::create('parent_path', DBStringColumn::MAX_LENGTH, ''),
            new DBCreatedOnByColumn(true, true),
            new DBUpdatedOnColumn(),
            new DBAdditionalPropertiesColumn(),
        ])->addIndices([
            DBIndex::create('parent_path', DBIndex::KEY, ['parent_path', 'parent_id']),
        ]))->setTypeFromField('type')->setObjectIsAbstract(true)->setOrderBy('created_on DESC, id DESC');
    }
}
