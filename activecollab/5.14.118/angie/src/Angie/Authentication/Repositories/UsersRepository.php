<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

namespace Angie\Authentication\Repositories;

use ActiveCollab\Authentication\AuthenticatedUser\RepositoryInterface;
use DataObjectPool;
use User;
use Users;

/**
 * @package Angie\Authentication\Repositories
 */
class UsersRepository implements RepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function findById($user_id)
    {
        return DataObjectPool::get(User::class, $user_id);
    }

    /**
     * {@inheritdoc}
     */
    public function findByUsername($username)
    {
        return Users::findByEmail($username);
    }
}
