<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

namespace Angie\Authentication\Policies;

use ActiveCollab\Authentication\LoginPolicy\LoginPolicy as BaseLoginPolicy;

/**
 * Password policy.
 *
 * @package ActiveCollab.modules.system
 * @subpackage models
 */
class LoginPolicy extends BaseLoginPolicy
{
}
