<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

namespace Angie\Authentication\SecurityLog\EventHandlers;

use ActiveCollab\Authentication\AuthenticatedUser\AuthenticatedUserInterface;
use Angie\Globalization;
use User;

/**
 * @package Angie\Authentication\SecurityLog\EventHandlers
 */
class UserSetEventHander
{
    /**
     * @param AuthenticatedUserInterface $user
     */
    public function __invoke(AuthenticatedUserInterface $user)
    {
        if ($user instanceof User) {
            Globalization::setCurrentLocaleByUser($user);
        }
    }
}
