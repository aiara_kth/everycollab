<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

namespace Angie\Command;

use Symfony\Component\Console\Output\OutputInterface;

/**
 * @package ActiveCollab\Command
 */
abstract class BuildCommand extends Command
{
    /**
     * Return command name prefix.
     *
     * @return string
     */
    protected function getCommandNamePrefix()
    {
        return 'build:';
    }

    /**
     * @param string          $path
     * @param OutputInterface $output
     */
    protected function deletePath($path, OutputInterface $output)
    {
        if (is_file($path) || is_link($path)) {
            $this->deleteFile($path, $output);
        } elseif (is_dir($path)) {
            $this->deleteDir($path, $output);
        } else {
            $output->writeln("<comment>Notice</comment> '$path' is not a file or directory");
        }
    }

    /**
     * @param string          $path
     * @param OutputInterface $output
     */
    protected function deleteFile($path, OutputInterface $output)
    {
        if (!unlink($path)) {
            $output->writeln("<error>Error</error> Failed to delete '$path' file");
        }
    }

    /**
     * @param string          $path
     * @param OutputInterface $output
     */
    protected function deleteDir($path, OutputInterface $output)
    {
        foreach (array_diff(scandir($path), ['.', '..']) as $file) {
            if (is_file("$path/$file") || is_link("$path/$file")) {
                $this->deleteFile("$path/$file", $output);
            } elseif (is_dir("$path/$file")) {
                $this->deleteDir("$path/$file", $output);
            }
        }

        if (!rmdir($path)) {
            $output->writeln("<error>Error</error> Failed to delete '$path' directory");
        }
    }
}
