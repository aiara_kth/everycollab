<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

namespace Angie\Command;

/**
 * @package ActiveCollab\Command\SampleProject
 */
abstract class SampleProjectCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function getCommandNamePrefix()
    {
        return 'sample_project:';
    }
}
