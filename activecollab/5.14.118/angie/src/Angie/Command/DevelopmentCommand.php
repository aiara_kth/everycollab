<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

namespace Angie\Command;

/**
 * @package ActiveCollab\Command
 */
abstract class DevelopmentCommand extends Command
{
    /**
     * Return command name prefix.
     *
     * @return string
     */
    protected function getCommandNamePrefix()
    {
        return 'dev:';
    }
}
