<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

namespace Angie\Command;

use Symfony\Component\Console\Input\InputOption;

/**
 * @package ActiveCollab\Command
 */
abstract class OnDemandCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        parent::configure();

        $this->addOption('log-to-file', '', InputOption::VALUE_OPTIONAL, 'Shepherd compatibility, should be removed');
    }

    /**
     * Return command name prefix.
     *
     * @return string
     */
    protected function getCommandNamePrefix()
    {
        return 'ondemand:';
    }
}
