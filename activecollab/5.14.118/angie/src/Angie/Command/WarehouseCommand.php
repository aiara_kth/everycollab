<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

namespace Angie\Command;

use Integrations;
use RuntimeException;
use WarehouseIntegration;
use WarehouseIntegrationInterface;

abstract class WarehouseCommand extends Command
{
    /**
     * Return command name prefix.
     *
     * @return string
     */
    protected function getCommandNamePrefix()
    {
        return 'warehouse:';
    }

    protected function getWarehouseIntegration(): WarehouseIntegrationInterface
    {
        $integration = Integrations::findFirstByType(WarehouseIntegration::class);

        if (!$integration instanceof WarehouseIntegrationInterface) {
            throw new RuntimeException('Failed to load Warehouse integration.');
        }

        return $integration;
    }

    protected function isWarehouseIntegrationConfigured(): bool
    {
        return (bool) $this->getWarehouseIntegration()->isInUse();
    }

    protected function getStoreId(): ?int
    {
        return $this->getWarehouseIntegration()->getStoreId();
    }

    protected function getAccessToken(): ?string
    {
        return $this->getWarehouseIntegration()->getAccessToken();
    }
}
