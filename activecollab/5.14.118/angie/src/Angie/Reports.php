<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

namespace Angie;

use Angie\Reports\Report;
use Angie\Reports\Report as ReportInterface;
use DataFilter;
use DirectoryIterator;
use ReflectionClass;
use User;

/**
 * Reports manager.
 *
 * @package Angie
 */
final class Reports
{
    /**
     * @var array
     */
    private static $available_reports_for = [];

    /**
     * Return report instance based on type and attributes.
     *
     * @param  string                          $type
     * @param  array                           $attributes
     * @return ReportInterface|DataFilter|null
     */
    public static function getReport(string $type, array $attributes = []): ?ReportInterface
    {
        /** @var Report $report */
        $report = null;

        if (self::isValidReportType($type)) {
            if (isset($attributes['id']) && $attributes['id']) {
                $report = new $type($attributes['id']);

                /** @var DataFilter $report */
                if ($report->isNew()) {
                    return null; // Failed to load report
                }
            } else {
                $report = new $type();
                $report->setAttributes($attributes);
            }
        }

        return $report;
    }

    private static function isValidReportType(string $type): bool
    {
        return class_exists($type, true)
            && (new ReflectionClass($type))->implementsInterface(ReportInterface::class);
    }

    /**
     * @param  User  $user
     * @return array
     */
    public static function getAvailableReportsFor(User $user)
    {
        if (!array_key_exists($user->getId(), self::$available_reports_for)) {
            $reports = self::prepareCollection();

            Events::trigger('on_reports', [&$reports]);

            self::$available_reports_for[$user->getId()] = $reports;
        }

        return self::$available_reports_for[$user->getId()];
    }

    /**
     * Prepare a collection that will be populated with reports.
     *
     * @return array
     */
    private static function prepareCollection()
    {
        $collection = [];

        foreach (self::getSections() as $k => $v) {
            $collection[$k] = ['label' => $v, 'reports' => new NamedList()];
        }

        $collection['custom']['reports'] = self::getCustomReports();

        return $collection;
    }

    /**
     * @var array
     */
    private static $sections = false;

    /**
     * Return report sections.
     *
     * @return NamedList
     */
    private static function getSections()
    {
        if (self::$sections === false) {
            self::$sections = new NamedList();

            Events::trigger('on_report_sections', [&self::$sections]);

            self::$sections->add('custom', lang('Custom'));
        }

        return self::$sections;
    }

    /**
     * @var array
     */
    private static $custom_reports = false;

    /**
     * Load custom reports.
     *
     * Custom reports should be located in /instance/custom/reports folder, and they need to be instances that implement
     * \Angie\Reports|Report interface. File name is ReportClass.php
     *
     * @return array
     */
    public static function getCustomReports()
    {
        if (self::$custom_reports === false) {
            self::$custom_reports = [];

            if (is_dir(CUSTOM_PATH . '/reports')) {
                foreach (new DirectoryIterator(CUSTOM_PATH . '/reports') as $file) {
                    if ($file->isFile()) {
                        $bits = explode('.', $file->getFilename());

                        if (count($bits) === 2 && $bits[1] === 'php') {
                            $report_class = $bits[0];

                            if (class_exists($report_class, false)) {
                                continue; // Report already exists
                            }

                            require_once $file->getPathname();

                            if (class_exists($report_class, false) && (new ReflectionClass($report_class))->implementsInterface('\Angie\Reports\Report')) {
                                self::$custom_reports[] = new $report_class();
                            }
                        }
                    }
                }
            }
        }

        return self::$custom_reports;
    }
}
