<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

namespace Angie;

use Closure;
use HTMLPurifier;
use HTMLPurifier_Config;
use Michelf\MarkdownExtra;
use simple_html_dom;

/**
 * HTML tag generator class.
 *
 * @package angie.library
 */
final class HTML
{
    /**
     * Open HTML tag.
     *
     * @param  string              $name
     * @param  array               $attributes
     * @param  Closure|string|null $content
     * @return string
     */
    public static function openTag($name, $attributes = null, $content = null)
    {
        if ($attributes) {
            $result = "<$name";

            foreach ($attributes as $k => $v) {
                if ($k) {
                    if (is_bool($v)) {
                        if ($v) {
                            $result .= " $k";
                        }
                    } else {
                        $result .= ' ' . $k . '="' . ($v ? clean($v) : $v) . '"';
                    }
                }
            }

            $result .= '>';
        } else {
            $result = "<$name>";
        }

        if ($content) {
            if ($content instanceof Closure) {
                $result .= $content();
            } else {
                $result .= $content;
            }

            $result .= "</$name>";
        }

        return $result;
    }

    /**
     * Used ID-s in this request.
     *
     * @var array
     */
    private static $used_ids = [];

    /**
     * Prefix used for all elements that don't have $precix defined.
     *
     * @var string
     */
    private static $random_for_id = false;

    /**
     * Return session wide unique ID for given prefix.
     *
     * @param  string $prefix
     * @return string
     */
    public static function uniqueId($prefix = '')
    {
        if (empty(self::$random_for_id)) {
            self::$random_for_id = 'element_' . time() . '_' . mt_rand();
        }

        if (empty($prefix)) {
            $prefix = APPLICATION_NAME . '_' . self::$random_for_id . '_element';
        } else {
            $prefix = APPLICATION_NAME . '_' . self::$random_for_id . '_' . $prefix;
        }

        do {
            $id = $prefix . '_' . mt_rand();
        } while (in_array($id, self::$used_ids));

        self::$used_ids[] = $id;

        return $id;
    }

    // ---------------------------------------------------
    //  Converters
    // ---------------------------------------------------

    /**
     * Convert HTML to plain text (email style).
     *
     * @param  string $html
     * @param  bool   $clean
     * @return string
     */
    public static function toPlainText($html, $clean = false)
    {
        // store it in local variable
        if ($clean) {
            $plain = (string) self::cleanUpHtml($html);
        } else {
            $plain = (string) $html;
        }

        // strip slashes
        $plain = (string) trim(stripslashes($plain));

        // strip unnecessary characters
        $plain = (string) preg_replace([
            "/\r/", // strip carriage returns
            "/<script[^>]*>.*?<\/script>/si", // strip immediately, because we don't need any data from it
            "/<style[^>]*>.*?<\/style>/is", // strip immediately, because we don't need any data from it
            '/style=".*?"/',   //was: '/style=\"[^\"]*/'
        ], '', $plain);

        // entities to convert (this is not a definite list)
        $entities = [
            ' ' => ['&nbsp;', '&#160;'],
            '"' => ['&quot;', '&rdquo;', '&ldquo;', '&#8220;', '&#8221;', '&#147;', '&#148;'],
            '\'' => ['&apos;', '&rsquo;', '&lsquo;', '&#8216;', '&#8217;'],
            '>' => ['&gt;'],
            '<' => ['&lt;'],
            '&' => ['&amp;', '&#38;'],
            '(c)' => ['&copy;', '&#169;'],
            '(R)' => ['&reg;', '&#174;'],
            '(tm)' => ['&trade;', '&#8482;', '&#153;'],
            '--' => ['&mdash;', '&#151;', '&#8212;'],
            '-' => ['&ndash;', '&minus;', '&#8211;', '&#8722;'],
            '*' => ['&bull;', '&#149;', '&#8226;'],
            '£' => ['&pound;', '&#163;'],
            'EUR' => ['&euro;', '&#8364;'],
        ];

        // convert specified entities
        foreach ($entities as $character => $entity) {
            $plain = (string) str_replace_utf($entity, $character, $plain);
        }

        // strip other not previously converted entities
        $plain = (string) preg_replace([
            '/&[^&;]+;/si',
        ], '', $plain);

        // <p> converts to 2 newlines
        $plain = (string) preg_replace('/<p[^>]*>/i', "\n\n", $plain); // <p>

        // uppercase html elements
        $plain = (string) preg_replace_callback('/<h[123456][^>]*>(.*?)<\/h[123456]>/i', function ($matches) {
            return "\n\n" . strtoupper_utf($matches[1]) . "\n\n";
        }, $plain); // <h1-h6>

        $plain = (string) preg_replace_callback(['/<b[^>]*>(.*?)<\/b>/i', '/<strong[^>]*>(.*?)<\/strong>/i'], function ($matches) {
            return strtoupper_utf($matches[1]);
        }, $plain); // <b> <strong>

        // deal with italic elements
        $plain = (string) preg_replace(['/<i[^>]*>(.*?)<\/i>/i', '/<em[^>]*>(.*?)<\/em>/i'], '_\\1_', $plain); // <i> <em>

        // elements that convert to 2 newlines
        $plain = (string) preg_replace(['/(<ul[^>]*>|<\/ul>)/i', '/(<ol[^>]*>|<\/ol>)/i', '/(<table[^>]*>|<\/table>)/i'], "\n\n", $plain); // <ul> <ol> <table>

        // elements that convert to single newline
        $plain = (string) preg_replace(['/<br[^>]*>/i', '/(<tr[^>]*>|<\/tr>)/i'], "\n", $plain); // <br> <tr>

        // <hr> converts to -----------------------
        $plain = (string) preg_replace('/<hr[^>]*>/i', "\n-------------------------\n", $plain); // <hr>

        // other table tags
        $plain = (string) preg_replace('/<td[^>]*>(.*?)<\/td>/i', "\\1\n", $plain); // <td>
        $plain = (string) preg_replace_callback('/<th[^>]*>(.*?)<\/th>/i', function ($matches) {
            return strtoupper_utf($matches[1]) . "\n";
        }, $plain); // <th>

        // list elements
        $plain = (string) preg_replace('/<li[^>]*>(.*?)<\/li>/i', "* \\1\n", $plain); // <li>with content</li>
        $plain = (string) preg_replace('/<li[^>]*>/i', "\n* ", $plain); // <li />

        // handle anchors
        $plain = (string) preg_replace_callback('/<a [^>]*href="([^"]+)"[^>]*>(.*?)<\/a>/i', function ($matches) {
            return HTML::toPlainTextProcessUrl($matches[1], $matches[2]);
        }, $plain); // <li />

        // handle blockquotes
        $plain = (string) preg_replace_callback('/<blockquote[^>]*>(.*?)<\/blockquote>/is', function ($blockquote_content) {
            $blockquote_content = isset($blockquote_content[1]) ? $blockquote_content[1] : '';

            $lines = (array) explode("\n", $blockquote_content);
            $return = [];
            if (is_foreachable($lines)) {
                foreach ($lines as $line) {
                    $return[] = '> ' . $line;
                }
            }

            return "\n\n" . implode("\n", $return) . "\n\n";
        }, $plain);

        // strip other tags
        $plain = (string) strip_tags($plain);

        // clean up unneccessary newlines
        $plain = (string) preg_replace("/\n\s+\n/", "\n\n", $plain);
        $plain = (string) preg_replace("/[\n]{3,}/", "\n\n", $plain);

        return trim($plain);
    }

    /**
     * Validate that HTML data exists in provided HTML.
     *
     * @param  string $html
     * @param  bool   $min_length
     * @return bool
     */
    public static function validateHTML($html, $min_length = false)
    {
        if ($html) {
            // strip (excluding specified tags)
            $html = (string) strip_tags($html, '<div><img><a>');

            // remove non ascii characters
            //$html = preg_replace('/[^(\x20-\x7F)]*/', '', $html);

            // trim the html
            $html = (string) trim($html);

            if ($min_length) {
                return strlen_utf($html) >= $min_length;
            } else {
                return (bool) $html;
            }
        } else {
            return false;
        }
    }

    /**
     * This function is used as a callback in html_to_text function to process
     * links found in the text.
     *
     * @param  string $url
     * @param  string $text
     * @return string
     */
    public static function toPlainTextProcessUrl($url, $text)
    {
        if (str_starts_with($url, 'http://') || str_starts_with($url, 'https://')) {
            return $text == $url ? $url : "$text [$url]";
        } elseif (str_starts_with($url, 'mailto:')) {
            $email_address = substr($url, 7);

            return $text == $email_address ? $email_address : $text . ' [' . $email_address . ']';
        } else {
            return $text;
        }
    }

    /**
     * Cached whitelisted tags.
     *
     * @var array
     */
    public static $whitelisted_tags = false;

    /**
     * load whitelisted tags from config option.
     */
    public static function loadWhitelistedTags()
    {
        if (self::$whitelisted_tags === false) {
            self::$whitelisted_tags = [];

            $config_whitelisted_tags = [
                'environment' => [
                    'br' => null,
                    'div' => ['class', 'placeholder-type', 'placeholder-object-id', 'placeholder-extra', 'align'],
                    'span' => ['style', 'class', 'align', 'object-id', 'data-user-id'],
                    'a' => ['href', 'title', 'class', 'object-id', 'object-class', 'align', 'target'],
                    'img' => ['src', 'alt', 'title', 'class', 'align'],
                    'p' => ['class', 'align'],
                    'blockquote' => ['class'],
                    'ul' => ['class', 'align'],
                    'ol' => ['class', 'align'],
                    'li' => ['class', 'align'],
                    'b' => null, 'strong' => null,
                    'i' => null, 'em' => null,
                    'u' => null,
                    'del' => null,
                    'table' => null,
                    'thead' => null,
                    'tbody' => null,
                    'tfoot' => null,
                    'tr' => null,
                    'td' => ['align', 'class', 'colspan', 'rowspan'],
                    'th' => ['align'],
                    'h1' => ['align'],
                    'h2' => ['align'],
                    'h3' => ['align'],
                ],
                'visual_editor' => [
                    'p' => ['class', 'style'],
                    'img' => ['image-type', 'object-id', 'class'],
                    'strike' => ['class', 'style'],
                    'span' => ['class', 'data-redactor-inlinemethods', 'data-redactor'],
                    'a' => ['class', 'href'],
                    'blockquote' => null,
                    'br' => null,
                    'b' => null, 'strong' => null,
                    'i' => null, 'em' => null,
                    'u' => null,
                    'pre' => ['data-syntax'],
                ],
            ];

            if (is_foreachable($config_whitelisted_tags)) {
                foreach ($config_whitelisted_tags as $module) {
                    if (is_foreachable($module)) {
                        foreach ($module as $whitelisted_tag => $whitelisted_tag_attributes) {
                            self::$whitelisted_tags[$whitelisted_tag] = array_merge(isset(self::$whitelisted_tags[$whitelisted_tag]) ? self::$whitelisted_tags[$whitelisted_tag] : [], (array) $whitelisted_tag_attributes);
                        }
                    }
                }
            }
        }
    }

    /**
     * Return whitelisted tags specially for purifier.
     *
     * @return array
     */
    public static function getWhitelistedTagsForPurifier()
    {
        if (self::$whitelisted_tags === false) {
            self::loadWhitelistedTags();
        }

        return self::$whitelisted_tags;
    }

    /**
     * Return whitelisted tags for editor.
     *
     * @return string
     */
    public static function getWhitelistedTagsForEditor()
    {
        if (self::$whitelisted_tags === false) {
            self::loadWhitelistedTags();
        }

        if (is_foreachable(self::$whitelisted_tags)) {
            $result = [];
            foreach (self::$whitelisted_tags as $whitelisted_tag => $whitelisted_attributes) {
                $result[] = $whitelisted_tag . '[' . implode('|', $whitelisted_attributes) . ']';
            }

            return implode(',', $result);
        } else {
            return '';
        }
    }

    /**
     * Convert raw text to rich text.
     *
     * @param  string      $raw_text
     * @param  string|null $for
     * @param  string|null $parent_type
     * @param  int|null    $parent_id
     * @return string
     */
    public static function toRichText($raw_text, $for = null, $parent_type = null, $parent_id = null)
    {
        if (!trim($raw_text)) {
            return $raw_text;
        }

        $parser = self::getDOM($raw_text);
        if ($parser === false) {
            $parser = self::getDOM(nl2br($raw_text));
        }

        Events::trigger('on_rawtext_to_richtext', [$parser, $for, $parent_type, $parent_id]);
        $html = (string) $parser;

        return $html;
    }

    /**
     * Clean up HTML code.
     *
     * @param  string  $html
     * @param  Closure $extra_dom_manipulation
     * @return string
     */
    public static function cleanUpHtml($html, $extra_dom_manipulation = null)
    {
        $html = trim($html);

        if ($html) {
            $html = preg_replace('/<img[^>]+src[\\s=\'"]+data\:(image\/.*)\;base64\,([^"\'>\\s]+)[^>]+>/is', '', $html); // strip raw embeded images
            $html = preg_replace('/<img[^>]+src[\\s=\'"]+webkit-fake-url\:\/\/[^"\'>\\s]+[^>]+>/is', '', $html); // strips images with webkit-fake-url://

            $html = self::purify($html);

            $dom = self::getDOM($html);

            if ($dom) {
                // Remove Apple style class SPAN-s
                $elements = $dom->find('span[class=Apple-style-span]');
                if (is_foreachable($elements)) {
                    foreach ($elements as $element) {
                        $element->outertext = $element->plaintext;
                    }
                }

                // Remove empty paragraphs
                if (defined('REMOVE_EMPTY_PARAGRAPHS') && REMOVE_EMPTY_PARAGRAPHS) {
                    foreach ($dom->find('p') as $element) {
                        $cleaned_up_content = trim(str_replace('&nbsp;', ' ', strip_tags($element->innertext)));

                        // Empty paragraph (non-breaking spaces are converted to spaces so trim can remove them)?
                        if (empty($cleaned_up_content)) {
                            if (strpos($element->innertext, 'img')) {
                                continue;
                            }

                            $element->outertext = '';
                        }
                    }
                }

                if ($extra_dom_manipulation instanceof Closure) {
                    $extra_dom_manipulation->__invoke($dom);
                }

                $html = (string) $dom;
            }

            return $html;
        } else {
            return '';
        }
    }

    /**
     * Strip links for anonnymous user.
     *
     * @param  string $html
     * @return string
     */
    public static function stripLinksForAnonymousUser($html)
    {
        $parser = self::getDOM($html);
        $anchors = $parser->find('a');
        if (is_foreachable($anchors)) {
            foreach ($anchors as $anchor) {
                if (strpos($anchor->class, 'public_link') === false) {
                    $anchor->outertext = '<span style="' . $anchor->style . '">' . $anchor->innertext . '</span>';
                }
            }
        }

        return (string) $parser;
    }

    /**
     * Strips single tag from the string.
     *
     * @param  string $tag
     * @param  string $html
     * @return string
     */
    public static function stripSingleTag($tag, $html)
    {
        $html = preg_replace('/<' . $tag . '[^>]*>/i', '', $html);
        $html = preg_replace('/<\/' . $tag . '>/i', '', $html);

        return $html;
    }

    /**
     * Prepare Simple HTML DOM instance from input $html.
     *
     * @param  string               $html
     * @return bool|simple_html_dom
     */
    public static function getDOM($html)
    {
        $dom = new simple_html_dom(null, true, true, 'UTF-8', "\r\n");
        if (empty($html)) {
            $dom->clear();

            return false;
        }
        $dom->load($html, true, true);

        return $dom;
    }

    /**
     * Purifier instance.
     *
     * @var HTMLPurifier::
     */
    private static $purifier;

    /**
     * Default configuration instance.
     *
     * @var HTMLPurifier_Config
     */
    private static $default_config;

    /**
     * Do code purification.
     *
     * @param  string $html
     * @return string
     */
    public static function purify($html)
    {
        if (defined('PURIFY_HTML') && PURIFY_HTML) {
            if (empty(self::$purifier) || empty(self::$default_config)) {
                self::$purifier = new HTMLPurifier();
                self::$default_config = HTMLPurifier_Config::createDefault();

                // Enable likification
                self::$default_config->set('AutoFormat.Linkify', true);
                self::$default_config->set('AutoFormat.PurifierLinkify', true);

                // whitelisted attributes needed for editor
                $whitelisted_tags = self::getWhitelistedTagsForPurifier();

                if (is_foreachable($whitelisted_tags)) {
                    $formatted_whitelisted_tags = [];
                    foreach ($whitelisted_tags as $whitelisted_tag => $whitelisted_tag_attributes) {
                        $formatted_whitelisted_tags[] = $whitelisted_tag;
                        if (is_foreachable($whitelisted_tag_attributes)) {
                            foreach ($whitelisted_tag_attributes as $whitelisted_tag_attribute) {
                                $formatted_whitelisted_tags[] = $whitelisted_tag . '[' . $whitelisted_tag_attribute . ']';
                            }
                        }
                    }
                    self::$default_config->set('HTML.Allowed', implode(',', $formatted_whitelisted_tags));
                }

                if (is_foreachable($whitelisted_tags)) {
                    $definition = self::$default_config->getHTMLDefinition(true);
                    foreach ($whitelisted_tags as $whitelisted_tag => $whitelisted_tag_attributes) {
                        if (is_foreachable($whitelisted_tag_attributes)) {
                            foreach ($whitelisted_tag_attributes as $whitelisted_tag_attribute) {
                                $definition->addAttribute($whitelisted_tag, $whitelisted_tag_attribute, 'Text');
                            }
                        }
                    }
                }
            }

            return self::$purifier->purify($html, self::$default_config);
        } else {
            return $html;
        }
    }

    /**
     * Markdown to HTML.
     *
     * @param  string $markdown
     * @return string
     */
    public static function markdownToHtml($markdown)
    {
        return MarkdownExtra::defaultTransform($markdown);
    }
}
