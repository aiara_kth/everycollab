<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

namespace Angie\Utils;

use ActiveCollab\CurrentTimestamp\CurrentTimestampInterface;
use DateTimeValue;

/**
 * @package Angie\Authentication\Utils
 */
class CurrentTimestamp implements CurrentTimestampInterface
{
    /**
     * {@inheritdoc}
     */
    public function getCurrentTimestamp()
    {
        return DateTimeValue::getCurrentTimestamp();
    }
}
