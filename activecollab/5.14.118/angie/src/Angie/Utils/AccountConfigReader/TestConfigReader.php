<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

declare(strict_types=1);

namespace Angie\Utils\AccountConfigReader;

use ClassicAccountStatusInterface;
use DateValue;

class TestConfigReader implements AccountConfigReaderInterface
{
    /**
     * @var string
     */
    private $plan;

    /**
     * @var float
     */
    private $plan_price;

    /**
     * @var string
     */
    private $billing_period;

    /**
     * @var int
     */
    private $status;

    /**
     * @var string|null
     */
    private $status_expires_on;

    /**
     * @var string|null
     */
    private $next_billing_date;

    /**
     * @var string|null
     */
    private $reference_billing_date;

    /**
     * @var bool
     */
    private $is_activated;

    /**
     * @var int
     */
    private $max_members;

    /**
     * @var int
     */
    private $max_disk_space;

    /**
     * @var int
     */
    private $max_projects;

    public function __construct(
        string $plan = 'XL',
        float $plan_price = 0.0,
        string $billing_period = 'monthly',
        int $status = ClassicAccountStatusInterface::CLASSIC_STATUS_ACTIVE_FREE,
        ?string $status_expires_on = null,
        ?string $next_billing_date = null,
        int $max_members = 0,
        int $max_disk_space = 0,
        int $max_projects = 0
    ) {
        $this->plan = $plan;
        $this->plan_price = $plan_price;
        $this->billing_period = $billing_period;
        $this->status = $status;
        $this->status_expires_on = $status_expires_on;
        $this->max_members = $max_members;
        $this->max_disk_space = $max_disk_space;
        $this->max_projects = $max_projects;
        $this->next_billing_date = $next_billing_date;
    }

    /**
     * @return string
     */
    public function getPlan(): string
    {
        return $this->plan;
    }

    /**
     * @return float
     */
    public function getPlanPrice(): float
    {
        return $this->plan_price;
    }

    /**
     * @return string
     */
    public function getBillingPeriod(): string
    {
        return $this->billing_period;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->getStatusFromClassicStatus();
    }

    /**
     * @return DateValue
     */
    public function getStatusExpiresOn(): DateValue
    {
        return new DateValue($this->status_expires_on);
    }

    /**
     * @return DateValue|null
     */
    public function getReferenceBillingDate(): ?DateValue
    {
        return new DateValue($this->reference_billing_date);
    }

    /**
     * @return DateValue|null
     */
    public function getNextBillingDate(): ?DateValue
    {
        return new DateValue($this->next_billing_date);
    }

    /**
     * @return bool
     */
    public function isActivated(): bool
    {
        return $this->getIsActivatedFromClassicStatus();
    }

    /**
     * @return int
     */
    public function getMaxMembers(): int
    {
        return $this->max_members;
    }

    /**
     * @return int
     */
    public function getMaxDiskSpace(): int
    {
        return $this->max_disk_space;
    }

    /**
     * @return int
     */
    public function getMaxProjects(): int
    {
        return $this->max_projects;
    }

    /**
     * @return bool
     */
    public function isPaid(): bool
    {
        return $this->getIsPaidFromClassicStatus();
    }

    private function getStatusFromClassicStatus(): string
    {
        switch ($this->status) {
            case ClassicAccountStatusInterface::CLASSIC_STATUS_PENDING_ACTIVATION:
            case ClassicAccountStatusInterface::CLASSIC_STATUS_ACTIVE_FREE:
                return ClassicAccountStatusInterface::STATUS_TRIAL;
            case ClassicAccountStatusInterface::CLASSIC_STATUS_ACTIVE_PAID:
                return ClassicAccountStatusInterface::STATUS_ACTIVE;
            case ClassicAccountStatusInterface::CLASSIC_STATUS_FAILED_PAYMENT:
                return ClassicAccountStatusInterface::STATUS_FAILED_PAYMENT;
            case ClassicAccountStatusInterface::CLASSIC_STATUS_SUSPENDED_FREE:
            case ClassicAccountStatusInterface::CLASSIC_STATUS_SUSPENDED_PAID:
                return ClassicAccountStatusInterface::STATUS_SUSPENDED;
            case ClassicAccountStatusInterface::CLASSIC_STATUS_RETIRED_FREE:
            case ClassicAccountStatusInterface::CLASSIC_STATUS_RETIRED_PAID:
            case ClassicAccountStatusInterface::CLASSIC_STATUS_PENDING_DELETION:
            default:
                return ClassicAccountStatusInterface::STATUS_RETIRED;
        }
    }

    private function getIsActivatedFromClassicStatus(): bool
    {
        return !in_array(
            $this->status,
            [
                ClassicAccountStatusInterface::CLASSIC_STATUS_PENDING_ACTIVATION,
                ClassicAccountStatusInterface::CLASSIC_STATUS_ACTIVE_FREE,
                ClassicAccountStatusInterface::CLASSIC_STATUS_SUSPENDED_FREE,
                ClassicAccountStatusInterface::CLASSIC_STATUS_RETIRED_FREE,
            ]
        );
    }

    private function getIsPaidFromClassicStatus(): bool
    {
        return in_array(
            $this->status,
            [
                ClassicAccountStatusInterface::CLASSIC_STATUS_ACTIVE_PAID,
                ClassicAccountStatusInterface::CLASSIC_STATUS_FAILED_PAYMENT,
                ClassicAccountStatusInterface::CLASSIC_STATUS_SUSPENDED_PAID,
                ClassicAccountStatusInterface::CLASSIC_STATUS_RETIRED_PAID,
            ]
        );
    }
}
