<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

namespace Angie\TestCase;

use PHPUnit\Framework\TestCase;

/**
 * Base Angie test case, mostly implemented to offer SimpleTest compat methods.
 *
 * @package Angie
 */
abstract class BaseTestCase extends TestCase
{
    /**
     * Reversed parameters order from PHPUNit's assertInstanceOf.
     *
     * @param mixed  $instance
     * @param string $expected
     */
    public function assertIsA($instance, $expected)
    {
        $this->assertInstanceOf($expected, $instance);
    }

    /**
     * SimpleTest compatible pass() method that increases number of assertions.
     *
     * @param string $message
     */
    public function pass($message = '')
    {
        $this->assertTrue(true, $message);
    }
}
