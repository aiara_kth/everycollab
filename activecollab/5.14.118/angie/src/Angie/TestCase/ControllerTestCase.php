<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

namespace Angie\TestCase;

use ActiveCollab\Authentication\AuthenticatedUser\AuthenticatedUserInterface;
use ActiveCollab\Cookies\CookiesInterface;
use ActiveCollab\Encryptor\EncryptorInterface;
use Angie\Authentication\Repositories\SessionsRepository;
use Angie\Http\RequestFactory;
use AngieApplication;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use RuntimeException;
use UserSession;
use Zend\Diactoros\Response;

/**
 * Test case that can produce requests and route them through controller layer and all stacked middlewares.
 *
 * @package Angie\TestCase
 */
abstract class ControllerTestCase extends EnvironmentTestCase
{
    /**
     * @var string
     */
    protected $session_id_cookie_name;

    /**
     * @var CookiesInterface
     */
    protected $cookies;

    /**
     * @var EncryptorInterface
     */
    protected $encryptor;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        parent::setUp();

        $this->session_id_cookie_name = AngieApplication::getSessionIdCookieName();
        $this->cookies = AngieApplication::cookies();
        $this->encryptor = AngieApplication::encryptor();
    }

    /**
     * Execute a GET request and return resulting request and response.
     *
     * @param  string            $path
     * @param  array             $query_params
     * @param  callable|null     $modify_request_and_response
     * @return ResponseInterface
     */
    protected function executeGetRequest($path, $query_params = [], callable $modify_request_and_response = null)
    {
        $query_params['path_info'] = trim((string) $path, '/');

        $request = (new RequestFactory())
            ->create($this->getServerParams(), [], ROOT_URL . '/api.php', 'GET', 'php://input', [], [], $query_params)
            ->withAttribute('test', 123);

        return $this->executeRequest($request, null, $modify_request_and_response);
    }

    /**
     * Execute GET request as a given user.
     *
     * @param  AuthenticatedUserInterface $user
     * @param  string                     $path
     * @param  array                      $query_params
     * @param  callable|null              $modify_request_and_response
     * @return ResponseInterface
     */
    public function executeGetRequestAs(AuthenticatedUserInterface $user, $path, $query_params = [], callable $modify_request_and_response = null)
    {
        $query_params['path_info'] = trim((string) $path, '/');
        $request = (new RequestFactory())
            ->create($this->getServerParams(), [], ROOT_URL . '/api.php', 'GET', 'php://input', [], [], $query_params);

        /** @var ServerRequestInterface $request */
        /** @var ResponseInterface $response */
        list($request, $response) = $this->prepareRequestAndResponseFor($user, $request);

        return $this->executeRequest($request, $response, $modify_request_and_response);
    }

    /**
     * Execute POST request.
     *
     * @param  string            $path
     * @param  array             $payload
     * @param  callable|null     $modify_request_and_response
     * @return ResponseInterface
     */
    public function executePostRequest($path, $payload = [], callable $modify_request_and_response = null)
    {
        $query_params['path_info'] = trim((string) $path, '/');
        $request = (new RequestFactory())
            ->create($this->getServerParams(), [], ROOT_URL . '/api.php', 'POST', 'php://input', [], [], $query_params, $payload);

        return $this->executeRequest($request, null, $modify_request_and_response);
    }

    /**
     * Execute POST request as $user.
     *
     * @param  AuthenticatedUserInterface $user
     * @param  string                     $path
     * @param  array                      $payload
     * @param  callable|null              $modify_request_and_response
     * @return ResponseInterface
     */
    public function executePostRequestAs(AuthenticatedUserInterface $user, $path, $payload = [], callable $modify_request_and_response = null)
    {
        $query_params['path_info'] = trim((string) $path, '/');
        $request = (new RequestFactory())
            ->create($this->getServerParams(), [], ROOT_URL . '/api.php', 'POST', 'php://input', [], [], $query_params, $payload);

        /** @var ServerRequestInterface $request */
        /** @var ResponseInterface $response */
        list($request, $response) = $this->prepareRequestAndResponseFor($user, $request);

        return $this->executeRequest($request, $response, $modify_request_and_response);
    }

    /**
     * Execute POST request.
     *
     * @param  string            $path
     * @param  array             $payload
     * @param  callable|null     $modify_request_and_response
     * @return ResponseInterface
     */
    public function executePutRequest($path, $payload = [], callable $modify_request_and_response = null)
    {
        $query_params['path_info'] = trim((string) $path, '/');
        $request = (new RequestFactory())
            ->create($this->getServerParams(), [], ROOT_URL . '/api.php', 'PUT', 'php://input', [], [], $query_params, $payload);

        return $this->executeRequest($request, null, $modify_request_and_response);
    }

    /**
     * Execute POST request as $user.
     *
     * @param  AuthenticatedUserInterface $user
     * @param  string                     $path
     * @param  array                      $payload
     * @param  callable|null              $modify_request_and_response
     * @return ResponseInterface
     */
    public function executePutRequestAs(AuthenticatedUserInterface $user, $path, $payload = [], callable $modify_request_and_response = null)
    {
        $query_params['path_info'] = trim((string) $path, '/');
        $request = (new RequestFactory())
            ->create($this->getServerParams(), [], ROOT_URL . '/api.php', 'PUT', 'php://input', [], [], $query_params, $payload);

        /** @var ServerRequestInterface $request */
        /** @var ResponseInterface $response */
        list($request, $response) = $this->prepareRequestAndResponseFor($user, $request);

        return $this->executeRequest($request, $response, $modify_request_and_response);
    }

    /**
     * Execute delete action.
     *
     * @param  string            $path
     * @param  array             $payload
     * @param  callable|null     $modify_request_and_response
     * @return ResponseInterface
     */
    public function executeDeleteRequest($path, $payload = [], callable $modify_request_and_response = null)
    {
        $query_params['path_info'] = trim((string) $path, '/');
        $request = (new RequestFactory())->create($this->getServerParams(), [], ROOT_URL . '/api.php', 'DELETE', 'php://input', [], [], $query_params, $payload);

        return $this->executeRequest($request, null, $modify_request_and_response);
    }

    /**
     * Execute DELETE request as $user.
     *
     * @param  AuthenticatedUserInterface $user
     * @param  string                     $path
     * @param  callable|null              $modify_request_and_response
     * @return ResponseInterface
     */
    public function executeDeleteRequestAs(AuthenticatedUserInterface $user, $path, $payload = [], callable $modify_request_and_response = null)
    {
        $query_params['path_info'] = trim((string) $path, '/');
        $request = (new RequestFactory())
            ->create($this->getServerParams(), [], ROOT_URL . '/api.php', 'DELETE', 'php://input', [], [], $query_params, $payload);

        /** @var ServerRequestInterface $request */
        /** @var ResponseInterface $response */
        list($request, $response) = $this->prepareRequestAndResponseFor($user, $request);

        return $this->executeRequest($request, $response, $modify_request_and_response);
    }

    /**
     * Execute request and optionaly modify request and response.
     *
     * @param  ServerRequestInterface $request
     * @param  ResponseInterface      $response
     * @param  callable|null          $modify_request_and_response
     * @return ResponseInterface
     */
    private function executeRequest(ServerRequestInterface $request, ResponseInterface $response = null, callable $modify_request_and_response = null)
    {
        if ($response === null) {
            $response = new Response();
        }

        if (is_callable($modify_request_and_response)) {
            list($request, $response) = $modify_request_and_response($request, $response);

            if (!$request instanceof RequestInterface || !$response instanceof ResponseInterface) {
                throw new RuntimeException('Request/response modification callback is expected to return a modified request');
            }
        }

        return AngieApplication::executeHttpMiddlewareStack($request, $response);
    }

    /**
     * Prepare request and response for requests that are being made by an authenticated user.
     *
     * @param  AuthenticatedUserInterface $user
     * @param  RequestInterface           $request
     * @param  ResponseInterface|null     $response
     * @return array
     */
    private function prepareRequestAndResponseFor(AuthenticatedUserInterface $user, RequestInterface $request, ResponseInterface $response = null)
    {
        $session = $this->createUserSession($user);

        if ($response === null) {
            $response = new Response();
        }

        /** @var ServerRequestInterface $request */
        $request = $request->withHeader('X-Angie-CsrfValidator', $this->encryptor->encrypt($session->getCsrfValidator()));

        return $this->cookies->set($request, $response, $this->session_id_cookie_name, $session->getSessionId());
    }

    /**
     * @param  AuthenticatedUserInterface $user
     * @param  bool                       $remember
     * @return UserSession
     */
    private function createUserSession(AuthenticatedUserInterface $user, $remember = false)
    {
        return (new SessionsRepository())->createSession(
            $user,
            [
                'remember' => (bool) $remember,
            ]
        );
    }

    /**
     * @return array
     */
    private function getServerParams()
    {
        return [
            'REMOTE_ADDR' => '127.0.0.1',
            'HTTP_USER_AGENT' => 'Mozilla/5.0 (iPad; U; CPU OS 3_2_1 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Mobile/7B405',
        ];
    }
}
