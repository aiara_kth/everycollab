<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

namespace Angie\TestCase;

use Angie\Mailer;
use Angie\Mailer\Adapter\Silent as SilentMailer;
use AngieApplication;
use AnonymousUser;
use DateTimeValue;
use DB;
use Owner;

/**
 * Test case that properly initializes model.
 *
 * @package Angie\TestCase
 */
abstract class EnvironmentTestCase extends ModelTestCase
{
    /**
     * @var array
     */
    protected $mailing_log;

    /**
     * Drop and initialize model before each test.
     */
    public function setUp()
    {
        parent::setUp();

        // Initialize authentication
        AngieApplication::authentication();

        // Set logged user
        AngieApplication::authentication()->setAuthenticatedUser(new Owner(1));

        $this->mailing_log = [];

        Mailer::setAdapter(new SilentMailer());
        Mailer::setDefaultSender(new AnonymousUser('Default From', 'default@from.com'));
        Mailer::onSent(function ($from, $to, $subject, $body, $reply_to) {
            $this->mailing_log[] = [
                'from' => $from,
                'to' => $to,
                'subject' => $subject,
                'body' => $body,
                'reply_to' => $reply_to,
            ];
        });
    }

    /**
     * Tear down.
     */
    public function tearDown()
    {
        AngieApplication::unsetAuthentication();
        Mailer::onSent(null);

        if (DateTimeValue::isCurrentTimestampLocked()) {
            DateTimeValue::unlockCurrentTimestamp();
        }

        parent::tearDown();
    }

    /**
     * Clear notification and email log.
     */
    protected function clearNotificationAndEmailLog()
    {
        foreach (['notifications', 'notification_recipients'] as $table_name) {
            DB::execute('TRUNCATE TABLE ' . $table_name);
        }

        $this->mailing_log = [];
        $this->assertEmptyNotificationAndEmailLog();
    }

    /**
     * Assert that email and notification log is empty.
     */
    public function assertEmptyNotificationAndEmailLog()
    {
        $this->assertCount(0, $this->mailing_log);

        $this->assertEquals(0, DB::executeFirstCell('SELECT COUNT(id) AS "row_count" FROM notifications'));
        $this->assertEquals(0, DB::executeFirstCell('SELECT COUNT(id) AS "row_count" FROM notification_recipients'));
    }
}
