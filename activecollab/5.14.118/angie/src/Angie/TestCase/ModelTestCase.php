<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

namespace Angie\TestCase;

use Angie\Events;
use AngieApplication;
use AngieApplicationModel;
use DataObjectPool;
use DateTimeValue;

/**
 * Test case that properly initializes model.
 *
 * @package Angie\TestCase
 */
abstract class ModelTestCase extends BaseTestCase
{
    public function setUp()
    {
        parent::setUp();

        AngieApplicationModel::revert('test');

        AngieApplication::cache()->clear();
        empty_dir(CACHE_PATH, true);

        DataObjectPool::clear();

        Events::trigger('on_reset_manager_states');
    }

    public function tearDown()
    {
        AngieApplication::setForcedIsOnDemand(null);

        if (DateTimeValue::isCurrentTimestampLocked()) {
            DateTimeValue::unlockCurrentTimestamp();
        }

        parent::tearDown();
    }
}
