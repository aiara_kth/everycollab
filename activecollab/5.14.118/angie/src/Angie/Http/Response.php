<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

namespace Angie\Http;

use Zend\Diactoros\Response as BaseResponse;

/**
 * @package Angie\Http
 */
class Response extends BaseResponse implements ResponseInterface
{
}
