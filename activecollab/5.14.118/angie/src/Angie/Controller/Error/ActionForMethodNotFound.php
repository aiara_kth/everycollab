<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

namespace Angie\Controller\Error;

use Angie\Error;

/**
 * Controller action does not exist for the given method error.
 *
 * @package angie.library.controller
 * @subpackage errors
 */
class ActionForMethodNotFound extends Error
{
    /**
     * Constructor.
     *
     * @param string $controller
     * @param string $action
     * @param string $method
     * @param string $message
     */
    public function __construct($controller, $action, $method, $message = null)
    {
        if (empty($message)) {
            $message = "Controller action $controller::$action() is not available for $method method";
        }

        parent::__construct($message, ['controller' => $controller, 'action' => $action, 'method' => $method]);
    }
}
