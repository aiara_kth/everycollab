<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

namespace Angie\Middleware;

use Angie\Controller\Error\ActionForMethodNotFound;
use Angie\Http\Response\StatusResponse\NotFoundStatusResponse;
use Angie\Http\Response\StatusResponse\StatusResponse;
use Angie\Inflector;
use Angie\Middleware\Base\EncoderMiddleware;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Router;
use RoutingError;

/**
 * @package Angie\Middleware
 */
class RouterMiddleware extends EncoderMiddleware
{
    /**
     * {@inheritdoc}
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        if ($request->getMethod() == 'OPTIONS') {
            return $response->withStatus(200)->withHeader('Allow', 'GET,PUT,DELETE,OPTIONS');
        }

        $query_string_params = $request->getQueryParams();

        $path_info = isset($query_string_params['path_info']) && $query_string_params['path_info'] ? $query_string_params['path_info'] : '/';

        try {
            Router::match($path_info, $request->getUri()->getQuery());

            $url_params = Router::getUrlParams();

            if (empty($url_params['action'])) {
                $url_params['action'] = 'index';
            }

            if (is_array($url_params) && !empty($url_params['module']) && !empty($url_params['controller'])) {
                $request = $this->overrideQueryParams($request, $url_params);
            } else {
                return $this->getEncoder()->encode(new StatusResponse(500, 'Runtime error when routing user request.'), $request, $response)[1];
            }
        } catch (RoutingError $e) {
            if ($this->getLogger()) {
                $this->getLogger()->error('Path {path_info} does not match any of the routes', [
                    'path_info' => $path_info,
                    'exception' => $e,
                ]);
            }

            return $this->getEncoder()->encode(new NotFoundStatusResponse(), $request, $response)[1];
        } catch (ActionForMethodNotFound $e) {
            if ($this->getLogger()) {
                $this->getLogger()->error('Action not found for the method', array_merge($e->getParams(), ['exception' => $e]));
            }

            return $this->getEncoder()->encode(new StatusResponse(405, 'Method Not Allowed.'), $request, $response)[1];
        }

        if ($next) {
            $response = $next($request, $response);
        }

        return $response;
    }

    /**
     * Override GET request params.
     *
     * @param  RequestInterface|ServerRequestInterface $request
     * @param  array                                   $url_params
     * @return RequestInterface
     * @throws ActionForMethodNotFound
     */
    private function overrideQueryParams(RequestInterface $request, $url_params)
    {
        $initial_query_params = $request->getQueryParams();
        $url_params = is_array($url_params) ? $url_params : [];

        $module = isset($url_params['module']) && $url_params['module'] ? $url_params['module'] : DEFAULT_MODULE;
        $controller = isset($url_params['controller']) && $url_params['controller'] ? $url_params['controller'] : DEFAULT_CONTROLLER;
        if (isset($url_params['action']) && $url_params['action']) {
            $action = [];

            if (is_string($url_params['action'])) {
                $action['GET'] = $action['POST'] = $action['PUT'] = $action['DELETE'] = Inflector::underscore(trim($url_params['action'])); // @TODO Should be reduced to GET only!
            } elseif (is_array($url_params['action'])) {
                foreach ($url_params['action'] as $method => $controller_action) {
                    $action[strtoupper($method)] = Inflector::underscore($controller_action);
                }
            }
        } else {
            $action = ['GET' => 'index', 'POST' => 'index'];
        }

        if (array_key_exists($request->getMethod(), $action)) {
            $action_for_method = $action[$request->getMethod()];
        } else {
            throw new ActionForMethodNotFound($controller, first($action), $request->getMethod());
        }

        $request = $request
            ->withAttribute('module', $module)
            ->withAttribute('controller', $controller)
            ->withAttribute('action', $action_for_method)
            ->withQueryParams(array_merge($initial_query_params, $url_params));

        return $request;
    }
}
