<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

namespace Angie\Middleware;

use ActiveCollab\CurrentTimestamp\CurrentTimestampInterface;
use Angie\Http\Encoder\EncoderInterface;
use Angie\Middleware\Base\EncoderMiddleware;
use Angie\Utils\CurrentTimestamp;
use IEtag;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;
use User;

/**
 * @package Angie\Middleware
 */
class ActionResultEncoderMiddleware extends EncoderMiddleware
{
    /**
     * @var CurrentTimestampInterface|null
     */
    private $current_timestamp;

    /**
     * EtagMiddleware constructor.
     *
     * @param EncoderInterface               $encoder
     * @param CurrentTimestampInterface|null $current_timestamp
     * @param LoggerInterface|null           $logger
     */
    public function __construct(EncoderInterface $encoder, CurrentTimestampInterface $current_timestamp = null, LoggerInterface $logger = null)
    {
        parent::__construct($encoder, $logger);

        $this->current_timestamp = $current_timestamp ? $current_timestamp : new CurrentTimestamp();
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $action_result = $request->getAttribute(self::ACTION_RESULT_ATTRIBUTE);

        if ($action_result !== null) {
            /** @var ResponseInterface $response */
            $response = $this->getEncoder()->encode($request->getAttribute(self::ACTION_RESULT_ATTRIBUTE), $request, $response)[1];

            if ($request->getAttribute('authenticated_user') instanceof User && $action_result instanceof IEtag) {
                try {
                    $etag = (string) $action_result->getTag($request->getAttribute('authenticated_user'));

                    if ($etag) {
                        $response = $response
                            ->withHeader('Cache-Control', 'public, max-age=0')
                            ->withHeader('Expires', gmdate('D, d M Y H:i:s', ($this->current_timestamp->getCurrentTimestamp() + 315360000)) . ' GMT')
                            ->withHeader('Etag', $etag);
                    }
                } catch (\Exception $e) {
                    if ($this->getLogger()) {
                        $this->getLogger()->error('Failed to etag an action result due to an exception', [
                            'action_result_type' => gettype($action_result),
                            'exception' => $e,
                        ]);
                    }
                }
            }
        }

        if ($next) {
            $response = $next($request, $response);
        }

        return $response;
    }
}
