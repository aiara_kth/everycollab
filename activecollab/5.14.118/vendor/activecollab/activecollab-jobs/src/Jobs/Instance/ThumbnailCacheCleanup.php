<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

namespace ActiveCollab\ActiveCollabJobs\Jobs\Instance;

use ActiveCollab\FileSystem\Adapter\LocalAdapter;
use ActiveCollab\FileSystem\FileSystem;
use RuntimeException;

/**
 * @package ActiveCollab\ActiveCollabJobs\Jobs\Instance
 */
class ThumbnailCacheCleanup extends Job
{
    /**
     * Clean up thumbnails folder for the given instance.
     */
    public function execute()
    {
        $thumbnails_path = $this->getInstancePath() . '/thumbnails';

        if (is_link($thumbnails_path)) {
            $thumbnails_path = readlink($thumbnails_path);
        }

        if (is_dir($thumbnails_path)) {
            $filesystem = new FileSystem(new LocalAdapter($thumbnails_path));
            $filesystem->emptyDir('/', ['.htaccess']);
        } else {
            throw new RuntimeException("Directory '$thumbnails_path' not found");
        }
    }
}
