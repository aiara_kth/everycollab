<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

namespace ActiveCollab\ActiveCollabJobs\Jobs\Instance;

use ActiveCollab\FileSystem\Adapter\LocalAdapter;
use ActiveCollab\FileSystem\FileSystem;
use RuntimeException;

/**
 * @package ActiveCollab\ActiveCollabJobs\Jobs\Instance
 */
class WorkFolderCleanup extends Job
{
    /**
     * Clean up work folder for the given instance.
     */
    public function execute()
    {
        $work_path = $this->getInstancePath() . '/work';

        if (is_link($work_path)) {
            $work_path = readlink($work_path);
        }

        if (is_dir($work_path)) {
            $filesystem = new FileSystem(new LocalAdapter($work_path));
            $filesystem->emptyDir('/', ['export']);
            $filesystem->emptyDir('/export');
        } else {
            throw new RuntimeException("Directory '$work_path' not found");
        }
    }
}
