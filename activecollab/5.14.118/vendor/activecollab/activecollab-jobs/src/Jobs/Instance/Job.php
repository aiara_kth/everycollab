<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

namespace ActiveCollab\ActiveCollabJobs\Jobs\Instance;

use ActiveCollab\ActiveCollabJobs\Jobs\Job as BaseJob;
use InvalidArgumentException;

/**
 * @package ActiveCollab\ActiveCollabJobs\Jobs\Instance
 */
abstract class Job extends BaseJob
{
    const CLASSIC = 'classic';
    const FEATHER = 'feather';

    /**
     * @var string
     */
    private $instance_path = false;

    /**
     * Construct a new Job instance.
     *
     * @param  array|null               $data
     * @throws InvalidArgumentException
     */
    public function __construct(array $data = null)
    {
        if (empty($data['instance_type'])) {
            throw new InvalidArgumentException("'instance_type' property is required");
        } elseif (!in_array($data['instance_type'], [self::CLASSIC, self::FEATHER])) {
            throw new InvalidArgumentException("'instance_type' can be 'classic' or 'feather'");
        }

        if (empty($data['tasks_path'])) {
            $data['tasks_path'] = '';
        }

        parent::__construct($data);
    }

    /**
     * Return instance path based on parameters.
     *
     * @return string
     */
    protected function getInstancePath()
    {
        if ($this->instance_path === false) {
            $this->instance_path = $this->getData('instance_type') == self::CLASSIC
                ? '/var/www/activecollab/instances/' . $this->getData('instance_name')
                : '/var/www/feather/instances/' . $this->getData('instance_id');
        }

        return $this->instance_path;
    }

    /**
     * Return path to Active Collab's activecollab-cli.php.
     *
     * @return string
     */
    public function getActiveCollabCliPhpPath()
    {
        if ($tasks_path = $this->getData('tasks_path')) {
            return "$tasks_path/activecollab-cli.php";
        } else {
            return $this->getInstancePath() . '/tasks/activecollab-cli.php';
        }
    }

    /**
     * @return string
     */
    protected function getShepherdPath()
    {
        return '/var/www/shepherd';
    }
}
