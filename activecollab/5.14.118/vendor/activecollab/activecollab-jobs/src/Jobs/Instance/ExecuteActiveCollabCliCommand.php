<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

namespace ActiveCollab\ActiveCollabJobs\Jobs\Instance;

use RuntimeException;

/**
 * @package ActiveCollab\ActiveCollabJobs\Jobs\Instance
 */
class ExecuteActiveCollabCliCommand extends ExecuteCliCommand
{
    public function execute()
    {
        $activecollab_cli_php_path = $this->getActiveCollabCliPhpPath();

        if (!is_file($activecollab_cli_php_path)) {
            throw new RuntimeException("Failed to find Active Collab CLI script at '$activecollab_cli_php_path'");
        }

        return $this->runCommand(
            'php ' . escapeshellarg($activecollab_cli_php_path) . ' ' . $this->prepareCommandFromData(),
            dirname(dirname($activecollab_cli_php_path))
        );
    }
}
