<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Files module definition.
 *
 * @package ActiveCollab.modules.files
 * @subpackage models
 */
class FilesModule extends AngieModule
{
    const NAME = 'files';
    const PATH = __DIR__;

    /**
     * Plain module name.
     *
     * @var string
     */
    protected $name = 'files';

    /**
     * Module version.
     *
     * @var string
     */
    protected $version = '5.0';

    /**
     * Define project task routes.
     *
     * @param array $collection
     * @param array $single
     */
    public static function defineProjectFileRoutes($collection, $single)
    {
        Router::mapResource('files', ['module' => self::NAME, 'collection_path' => "$single[path]/files", 'collection_requirements' => $collection['requirements']], function ($collection, $single) {
            Router::map("$collection[name]_batch", "$collection[path]/batch", ['action' => ['GET' => 'batch_download', 'POST' => 'batch_add'], 'controller' => 'files', 'module' => FilesModule::NAME], $collection['requirements']);

            Router::map("$single[name]_download", "$single[path]/download", ['action' => ['GET' => 'download'], 'controller' => 'files', 'module' => FilesModule::NAME], $single['requirements']);
            Router::map("$single[name]_move_to_project", "$single[path]/move-to-project", ['action' => ['PUT' => 'move_to_project'], 'controller' => 'files', 'module' => FilesModule::NAME], $single['requirements']);
        });
    }

    /**
     * Initialize module.
     */
    public function init()
    {
        parent::init();

        $file_types = [
            'File',
            'LocalFile',
            'WarehouseFile',
            'GoogleDriveFile',
            'DropboxFile',
        ];

        DataObjectPool::registerTypeLoader($file_types, function ($ids) {
            return Files::findByIds($ids);
        });
    }

    /**
     * Define module classes.
     */
    public function defineClasses()
    {
        require_once __DIR__ . '/resources/autoload_model.php';

        AngieApplication::setForAutoload(
            [
                'ProjectFilesAndAttachmentsCollection' => __DIR__ . '/models/ProjectFilesAndAttachmentsCollection.class.php',
                'LocalFile' => __DIR__ . '/models/file_types/local/LocalFile.php',
                'RemoteFile' => __DIR__ . '/models/file_types/remote/RemoteFile.php',
                'WarehouseFile' => __DIR__ . '/models/file_types/remote/WarehouseFile.php',
                'GoogleDriveFile' => __DIR__ . '/models/file_types/remote/GoogleDriveFile.php',
                'DropboxFile' => __DIR__ . '/models/file_types/remote/DropboxFile.php',
            ]
        );
    }

    /**
     * Define handlers.
     */
    public function defineHandlers()
    {
        $this->listen('on_calculate_storage_usage');
        $this->listen('on_rebuild_activity_logs');
        $this->listen('on_reset_manager_states');
        $this->listen('on_trash_sections');
        $this->listen('on_local_file_created');
        $this->listen('on_warehouse_file_created');
        $this->listen('on_initial_settings');
    }
}
