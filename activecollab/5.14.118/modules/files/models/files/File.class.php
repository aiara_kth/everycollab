<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * File class.
 *
 * @package ActiveCollab.modules.files
 * @subpackage models
 */
abstract class File extends BaseFile implements IRoutingContext
{
    use IRoutingContextImplementation;

    /**
     * {@inheritdoc}
     */
    public function getRoutingContext()
    {
        return 'file';
    }

    /**
     * {@inheritdoc}
     */
    public function getRoutingContextParams()
    {
        return [
            'project_id' => $this->getProjectId(),
            'file_id' => $this->getId(),
        ];
    }

    /**
     * Create a copy of this object and optionally save it.
     *
     * @param  bool $save
     * @return File
     */
    public function copy($save = false)
    {
        /** @var File $copy */
        $copy = parent::copy(false);

        if (is_file($this->getPath())) {
            $copy->setLocation(AngieApplication::storeFile($this->getPath())[1]);
        }

        if ($save) {
            $copy->save();
        }

        return $copy;
    }

    /**
     * {@inheritdoc}
     */
    public function getHistoryFieldRenderers()
    {
        $renderers = parent::getHistoryFieldRenderers();

        $renderers['is_hidden_from_clients'] = function ($old_value, $new_value, Language $language) {
            if ($new_value) {
                return lang('Marked as hidden from clients', null, true, $language);
            } else {
                return lang('No longer hidden from clients', null, true, $language);
            }
        };

        return $renderers;
    }

    /**
     * {@inheritdoc}
     */
    public function getSearchDocument()
    {
        return new ProjectElementSearchDocument($this);
    }

    // ---------------------------------------------------
    //  System
    // ---------------------------------------------------

    public function save()
    {
        if ($this->isNew()) {
            $this->setName(Files::getProjectSafeName($this->getName(), $this->getProject()));
        }

        parent::save();
    }
}
