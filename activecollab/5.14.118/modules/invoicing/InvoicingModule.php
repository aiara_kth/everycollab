<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Invoicing module definition.
 *
 * @package ActiveCollab.modules.invoicing
 * @subpackage models
 */
class InvoicingModule extends AngieModule
{
    const NAME = 'invoicing';
    const PATH = __DIR__;

    /**
     * Plain module name.
     *
     * @var string
     */
    protected $name = 'invoicing';

    /**
     * Module version.
     *
     * @var string
     */
    protected $version = '5.0';

    public function init()
    {
        parent::init();

        DataObjectPool::registerTypeLoader('Invoice', function ($ids) {
            return Invoices::findByIds($ids);
        });

        DataObjectPool::registerTypeLoader('InvoiceItem', function ($ids) {
            return InvoiceItems::findByIds($ids);
        });

        DataObjectPool::registerTypeLoader('Estimate', function ($ids) {
            return Estimates::findByIds($ids);
        });

        DataObjectPool::registerTypeLoader('RecurringProfile', function ($ids) {
            return RecurringProfiles::findByIds($ids);
        });

        DataObjectPool::registerTypeLoader('TaxRate', function ($ids) {
            return TaxRates::findByIds($ids);
        });

        DataObjectPool::registerTypeLoader('InvoiceItemTemplate', function ($ids) {
            return InvoiceItemTemplates::findByIds($ids);
        });

        DataObjectPool::registerTypeLoader('InvoiceNoteTemplate', function ($ids) {
            return InvoiceNoteTemplates::findByIds($ids);
        });

        DataObjectPool::registerTypeLoader('QuickbooksInvoice', function ($ids) {
            return QuickbooksInvoices::findByIds($ids);
        });

        DataObjectPool::registerTypeLoader('XeroInvoice', function ($ids) {
            return XeroInvoices::findByIds($ids);
        });
    }

    /**
     * Define module classes.
     */
    public function defineClasses()
    {
        require_once __DIR__ . '/resources/autoload_model.php';

        AngieApplication::setForAutoload([
            'IInvoice' => __DIR__ . '/models/IInvoice.class.php',
            'IInvoiceImplementation' => __DIR__ . '/models/IInvoiceImplementation.class.php',

            'IInvoiceExport' => __DIR__ . '/models/IInvoiceExport.class.php',

            'InvoiceTemplate' => __DIR__ . '/models/InvoiceTemplate.class.php',
            'InvoicePDFGenerator' => __DIR__ . '/models/InvoicePDFGenerator.class.php',

            'IRoundFieldValueToDecimalPrecisionImplementation' => __DIR__ . '/models/IRoundFieldValueToDecimalPrecisionImplementation.class.php',

            // Invoice Based On
            'IInvoiceBasedOn' => __DIR__ . '/models/invoice_based_on/IInvoiceBasedOn.php',
            'IInvoiceBasedOnImplementation' => __DIR__ . '/models/invoice_based_on/IInvoiceBasedOnImplementation.php',
            'IInvoiceBasedOnTrackedDataImplementation' => __DIR__ . '/models/invoice_based_on/IInvoiceBasedOnTrackedDataImplementation.php',
            'IInvoiceBasedOnTrackingFilterResultImplementation' => __DIR__ . '/models/invoice_based_on/IInvoiceBasedOnTrackingFilterResultImplementation.php',

            'InvoicesFilter' => __DIR__ . '/models/reports/InvoicesFilter.php',
            'InvoicePaymentsFilter' => __DIR__ . '/models/reports/InvoicePaymentsFilter.php',

            // Search
            'InvoicesSearchBuilder' => __DIR__ . '/models/search_builders/InvoicesSearchBuilder.php',
            'EstimatesSearchBuilder' => __DIR__ . '/models/search_builders/EstimatesSearchBuilder.php',

            'BaseInvoiceSearchDocument' => __DIR__ . '/models/search_documents/BaseInvoiceSearchDocument.php',
            'InvoiceSearchDocument' => __DIR__ . '/models/search_documents/InvoiceSearchDocument.php',
            'EstimateSearchDocument' => __DIR__ . '/models/search_documents/EstimateSearchDocument.php',

            // Notifications
            'InvoiceNotification' => __DIR__ . '/notifications/InvoiceNotification.class.php',
            'SendInvoiceNotification' => __DIR__ . '/notifications/SendInvoiceNotification.class.php',
            'InvoicePaidNotification' => __DIR__ . '/notifications/InvoicePaidNotification.class.php',
            'InvoiceCanceledNotification' => __DIR__ . '/notifications/InvoiceCanceledNotification.class.php',
            'InvoiceReminderNotification' => __DIR__ . '/notifications/InvoiceReminderNotification.class.php',

            'EstimateNotification' => __DIR__ . '/notifications/EstimateNotification.class.php',
            'SendEstimateNotification' => __DIR__ . '/notifications/SendEstimateNotification.class.php',
            'EstimateUpdatedNotification' => __DIR__ . '/notifications/EstimateUpdatedNotification.class.php',

            'RecurringProfileNotification' => __DIR__ . '/notifications/RecurringProfileNotification.class.php',
            'InvoiceGeneratedViaRecurringProfileNotification' => __DIR__ . '/notifications/InvoiceGeneratedViaRecurringProfileNotification.class.php',
            'DraftInvoiceCreatedViaRecurringProfileNotification' => __DIR__ . '/notifications/DraftInvoiceCreatedViaRecurringProfileNotification.class.php',

            // Quickbooks
            'QuickbooksInvoice' => __DIR__ . '/models/quickbooks_invoices/QuickbooksInvoice.class.php',
            'QuickbooksInvoices' => __DIR__ . '/models/quickbooks_invoices/QuickbooksInvoices.class.php',

            // Quickbooks Integration
            'QuickbooksIntegration' => __DIR__ . '/models/integrations/QuickbooksIntegration.php',

            // Xero Invoice
            'XeroInvoice' => __DIR__ . '/models/xero_invoices/XeroInvoice.class.php',
            'XeroInvoices' => __DIR__ . '/models/xero_invoices/XeroInvoices.class.php',

            'InvoiceNumberSuggesterInterface' => __DIR__ . '/models/InvoiceNumberSuggesterInterface.php',
            'InvoiceNumberSuggester' => __DIR__ . '/models/InvoiceNumberSuggester.php',

            // Xero Integration
            'XeroIntegration' => __DIR__ . '/models/integrations/XeroIntegration.php',
        ]);
    }

    /**
     * Define module routes.
     */
    public function defineRoutes()
    {
        Router::mapResource('invoice_items');

        Router::mapResource('invoices', [], function ($collection, $single) {
            Router::map("$collection[name]_archive", "$collection[path]/archive", ['controller' => $collection['controller'], 'action' => ['GET' => 'archive']], $collection['requirements']);
            Router::map("$collection[name]_private_notes", "$collection[path]/private-notes", ['controller' => $collection['controller'], 'action' => ['GET' => 'private_notes']], $collection['requirements']);
            Router::map("$collection[name]_preview_items", "$collection[path]/preview-items", ['controller' => $collection['controller'], 'action' => ['GET' => 'preview_items']], $collection['requirements']);

            Router::map("$single[name]_send", "$single[path]/send", ['controller' => $single['controller'], 'action' => ['PUT' => 'send']], $single['requirements']);
            Router::map("$single[name]_export", "$single[path]/export", ['controller' => $single['controller'], 'action' => ['GET' => 'export']], $single['requirements']);
            Router::map("$single[name]_duplicate", "$single[path]/duplicate", ['controller' => $single['controller'], 'action' => ['POST' => 'duplicate']], $single['requirements']);
            Router::map("$single[name]_cancel", "$single[path]/cancel", ['controller' => $single['controller'], 'action' => ['PUT' => 'cancel']], $single['requirements']);
            Router::map("$single[name]_related_records", "$single[path]/related-records", ['controller' => $single['controller'], 'action' => ['DELETE' => 'release_related_records']], $single['requirements']);
            Router::map("$single[name]_mark_as_sent", "$single[path]/mark-as-sent", ['controller' => $single['controller'], 'action' => ['POST' => 'mark_as_sent']], $single['requirements']);
            Router::map("$single[name]_mark_zero_invoice_as_paid", "$single[path]/mark-zero-invoice-as-paid", ['controller' => $single['controller'], 'action' => ['POST' => 'mark_zero_invoice_as_paid']], $single['requirements']);
        });

        Router::map('invoice_public', 's/invoice', ['controller' => 'public_invoice', 'action' => ['GET' => 'view', 'PUT' => 'make_payment']]);

        Router::mapResource('estimates', [], function ($collection, $single) {
            Router::map("$collection[name]_archive", "$collection[path]/archive", ['controller' => $collection['controller'], 'action' => ['GET' => 'archive']], $collection['requirements']);
            Router::map("$collection[name]_private_notes", "$collection[path]/private-notes", ['controller' => $collection['controller'], 'action' => ['GET' => 'private_notes']], $collection['requirements']);

            Router::map("$single[name]_send", "$single[path]/send", ['controller' => $single['controller'], 'action' => ['PUT' => 'send']], $single['requirements']);
            Router::map("$single[name]_export", "$single[path]/export", ['controller' => $single['controller'], 'action' => ['GET' => 'export']], $single['requirements']);
            Router::map("$single[name]_duplicate", "$single[path]/duplicate", ['controller' => $single['controller'], 'action' => ['POST' => 'duplicate']], $single['requirements']);
        });

        Router::mapResource('recurring_profiles', [], function ($collection, $single) {
            Router::map("$collection[name]_archive", "$collection[path]/archive", ['controller' => $collection['controller'], 'action' => ['GET' => 'archive']], $collection['requirements']);
            Router::map("$collection[name]_trigger", "$collection[path]/trigger", ['controller' => $collection['controller'], 'action' => ['POST' => 'trigger']], $collection['requirements']);
            Router::map("$single[name]_next_trigger_on", "$single[path]/next-trigger-on", ['controller' => $single['controller'], 'action' => ['GET' => 'next_trigger_on']], $single['requirements']);
        });

        Router::map('invoice_template', 'invoice-template', ['controller' => 'invoice_template', 'action' => ['GET' => 'show_settings', 'PUT' => 'save_settings']]);
        Router::map('invoices_suggest_number', 'invoices/suggest-number', ['controller' => 'invoices', 'action' => ['GET' => 'suggest_number']]);

        Router::mapResource('invoice_note_templates', null, function ($collection) {
            Router::map("$collection[name]_default", "$collection[path]/default", ['controller' => $collection['controller'], 'action' => ['GET' => 'view_default', 'PUT' => 'set_default', 'DELETE' => 'unset_default']], $collection['requirements']);
        });

        Router::mapResource('invoice_item_templates');

        Router::mapResource('tax_rates', null, function ($collection) {
            Router::map("$collection[name]_default", "$collection[path]/default", ['controller' => $collection['controller'], 'action' => ['GET' => 'view_default', 'PUT' => 'set_default', 'DELETE' => 'unset_default']], $collection['requirements']);
        });

        Router::map('company_addresses_for_invoicing', 'companies/addresses-for-invoicing', ['controller' => 'company_addresses']);

        // Quickbooks Integration
        Router::map('quickbooks_payments', '/integrations/quickbooks/payments', ['controller' => 'quickbooks_integration', 'action' => ['GET' => 'sync_payments']]);
        Router::map('quickbooks_request_url', '/integrations/quickbooks/request-url', ['controller' => 'quickbooks_integration', 'action' => ['GET' => 'get_request_url']]);
        Router::map('quickbooks_authorize', '/integrations/quickbooks/authorize', ['controller' => 'quickbooks_integration', 'action' => ['PUT' => 'authorize']]);
        Router::map('quickbooks_integration', '/integrations/quickbooks/data', ['controller' => 'quickbooks_integration', 'action' => ['GET' => 'get_data']]);

        // Quickbooks Invoices
        Router::mapResource('quickbooks_invoices', ['collection_path' => '/quickbooks/invoices'], function ($collection, $single) {
            Router::map("$collection[name]_sync", "$collection[path]/sync", ['controller' => $collection['controller'], 'action' => ['PUT' => 'sync']], $collection['requirements']);
        });

        // Xero Integration
        Router::map('xero_payments', '/integrations/xero/payments', ['controller' => 'xero_integration', 'action' => ['GET' => 'sync_payments']]);
        Router::map('xero_request_url', '/integrations/xero/request-url', ['controller' => 'xero_integration', 'action' => ['GET' => 'get_request_url']]);
        Router::map('xero_authorize', '/integrations/xero/authorize', ['controller' => 'xero_integration', 'action' => ['PUT' => 'authorize']]);
        Router::map('xero_integration', '/integrations/xero/data', ['controller' => 'xero_integration', 'action' => ['GET' => 'get_data']]);

        // Xero Invoices
        Router::mapResource('xero_invoices', ['collection_path' => '/xero/invoices'], function ($collection, $single) {
            Router::map("$collection[name]_sync", "$collection[path]/sync", ['controller' => $collection['controller'], 'action' => ['PUT' => 'sync']], $collection['requirements']);
        });
    }

    /**
     * Define event handlers.
     */
    public function defineHandlers()
    {
        $this->listen('on_daily_maintenance');

        $this->listen('on_rebuild_activity_logs');

        $this->listen('on_object_from_notification_context');
        $this->listen('on_notification_context_view_url');

        $this->listen('on_history_field_renderers');

        $this->listen('on_protected_config_options');
        $this->listen('on_initial_settings');
        $this->listen('on_resets_initial_settings_timestamp');

        $this->listen('on_visible_object_paths');

        $this->listen('on_search_rebuild_index');
        $this->listen('on_user_access_search_filter');

        $this->listen('on_trash_sections');

        $this->listen('on_available_integrations');
    }

    // ---------------------------------------------------
    //  Un(Install)
    // ---------------------------------------------------

    /**
     * Install this module.
     */
    public function install()
    {
        recursive_mkdir(WORK_PATH . '/invoices', 0777, WORK_PATH);

        parent::install();
    }
}
