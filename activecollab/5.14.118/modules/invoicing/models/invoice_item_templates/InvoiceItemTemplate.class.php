<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * InvoiceItemTemplate class.
 */
class InvoiceItemTemplate extends BaseInvoiceItemTemplate implements IRoutingContext
{
    /**
     * Second tax rate cache.
     *
     * @var bool
     */
    public $second_tax_rate = false;
    /**
     * cached value of tax.
     *
     * @var TaxRate
     */
    private $first_tax_rate = false;

    /**
     * Return array or property => value pairs that describes this object.
     *
     * @return array
     */
    public function jsonSerialize()
    {
        $result = parent::jsonSerialize();

        $result['description'] = $this->getDescription();
        if ($first_tax_rate = $this->getFirstTaxRate()) {
            $result['first_tax_rate_id'] = $first_tax_rate->getId();
            $result['first_tax_rate_value'] = $first_tax_rate->getPercentage();
        }
        if ($second_tax_rate = $this->getSecondTaxRate()) {
            $result['second_tax_rate_id'] = $second_tax_rate->getId();
            $result['second_tax_rate_value'] = $second_tax_rate->getPercentage();
        }
        $result['quantity'] = $this->getQuantity();
        $result['unit_cost'] = $this->getUnitCost();

        return $result;
    }

    /**
     * Return tax rate.
     *
     * @return TaxRate
     */
    public function getFirstTaxRate()
    {
        if ($this->first_tax_rate === false) {
            $this->first_tax_rate = DataObjectPool::get('TaxRate', $this->getFirstTaxRateId());
        }

        return $this->first_tax_rate;
    }

    /**
     * Get Second Tax Rate.
     *
     * @return TaxRate
     */
    public function getSecondTaxRate()
    {
        if ($this->second_tax_rate === false) {
            $this->second_tax_rate = DataObjectPool::get('TaxRate', $this->getSecondTaxRateId());
        }

        return $this->second_tax_rate;
    }

    // ---------------------------------------------------
    //  Interface implementations
    // ---------------------------------------------------

    /**
     * Return routing context name.
     *
     * @return string
     */
    public function getRoutingContext()
    {
        return 'invoice_item_template';
    }

    /**
     * Return routing context parameters.
     *
     * @return mixed
     */
    public function getRoutingContextParams()
    {
        return ['invoice_item_template_id' => $this->getId()];
    }

    /**
     * {@inheritdoc}
     */
    public function validate(ValidationErrors &$errors)
    {
        if (!$this->validatePresenceOf('description')) {
            $errors->fieldValueIsRequired('description');
        }

        if (!$this->validatePresenceOf('quantity')) {
            $errors->fieldValueIsRequired('quantity');
        }

        parent::validate($errors);
    }

    /**
     * {@inheritdoc}
     */
    public function save()
    {
        if (!$this->getUnitCost()) {
            $this->setUnitCost(0);
        }

        parent::save();
    }

    // ---------------------------------------------------
    //  Permissions
    // ---------------------------------------------------

    /**
     * Returns true if $user can view this object.
     *
     * @param  User $user
     * @return bool
     */
    public function canView(User $user)
    {
        return $user->isOwner();
    }

    /**
     * Returns true if $user can update this object.
     *
     * @param  User $user
     * @return bool
     */
    public function canEdit(User $user)
    {
        return $user->isOwner();
    }

    /**
     * Returns true if $user can delete or move to trash this object.
     *
     * @param  User $user
     * @return bool
     */
    public function canDelete(User $user)
    {
        return $user->isOwner();
    }
}
