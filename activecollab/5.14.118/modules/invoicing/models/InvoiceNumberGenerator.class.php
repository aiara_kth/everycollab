<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Invoice number generator.
 *
 * @package activeCollab.modules.invoicing
 * @subpackage models
 */
final class InvoiceNumberGenerator
{
    const COUNTER_TOTAL = ':invoice_in_total';
    const COUNTER_YEAR = ':invoice_in_year';
    const COUNTER_MONTH = ':invoice_in_month';

    const CURRENT_YEAR = ':current_year';
    const CURRENT_MONTH = ':current_month';
    const CURRENT_MONTH_SHORT = ':current_short_month';
    const CURRENT_MONTH_LONG = ':current_long_month';

    /**
     * Prepare a number for this invoice.
     *
     * @param  Invoice           $invoice
     * @return string
     * @throws InvalidParamError
     */
    public static function prepareAndSet(Invoice &$invoice)
    {
        if ($invoice->getIssuedOn() instanceof DateValue) {
            $pattern = self::getPattern();
            $padding = self::getCounterPadding();

            // retrieve counters
            list($total_counter, $year_counter, $month_counter) = self::getDateInvoiceCounters();
            ++$total_counter;
            ++$year_counter;
            ++$month_counter;

            // Apply padding, if needed
            $prepared_total_counter = $padding ? str_pad($total_counter, $padding, '0', STR_PAD_LEFT) : $total_counter;
            $prepared_year_counter = $padding ? str_pad($year_counter, $padding, '0', STR_PAD_LEFT) : $year_counter;
            $prepared_month_counter = $padding ? str_pad($month_counter, $padding, '0', STR_PAD_LEFT) : $month_counter;

            $timestamp = $invoice->getIssuedOn()->getTimestamp();

            // retrieve variables
            $variable_year = date('Y', $timestamp);
            $variable_month = date('n', $timestamp);
            $variable_month_short = date('M', $timestamp);
            $variable_month_long = date('F', $timestamp);

            $generated_invoice_id = str_ireplace([
                self::COUNTER_TOTAL,
                self::COUNTER_YEAR,
                self::COUNTER_MONTH,
                self::CURRENT_YEAR,
                self::CURRENT_MONTH,
                self::CURRENT_MONTH_SHORT,
                self::CURRENT_MONTH_LONG,
            ], [
                $prepared_total_counter,
                $prepared_year_counter,
                $prepared_month_counter,
                $variable_year,
                $variable_month,
                $variable_month_short,
                $variable_month_long,
            ], $pattern);

            $invoice->setNumber($generated_invoice_id);
            self::incrementDateInvoiceCounters();

            return $generated_invoice_id;
        } else {
            throw new InvalidParamError('invoice', $invoice, 'Issue date needs to be set');
        }
    }

    /**
     * Return invoice number pattern.
     *
     * @return string
     */
    public static function getPattern()
    {
        return ConfigOptions::getValue('invoicing_number_pattern');
    }

    /**
     * Return padding for counter values.
     *
     * @return int
     */
    public static function getCounterPadding()
    {
        return (int) ConfigOptions::getValue('invoicing_number_counter_padding');
    }

    /**
     * Retrieves invoice counters.
     *
     * @param  int   $year
     * @param  int   $month
     * @return array
     */
    public static function getDateInvoiceCounters($year = null, $month = null)
    {
        if ($year === null) {
            $year = date('Y');
        }

        if ($month === null) {
            $month = date('n');
        }

        $counters = ConfigOptions::getValue('invoicing_number_date_counters');

        $previous_month_counter = array_var($counters, $year . '_' . $month, 0);
        $previous_year_counter = array_var($counters, $year, 0);
        $previous_total_counter = array_var($counters, 'total', 0);

        return [$previous_total_counter, $previous_year_counter, $previous_month_counter];
    }

    /**
     * Increment invoice counters.
     *
     * @param  int  $year
     * @param  int  $month
     * @return bool
     */
    public static function incrementDateInvoiceCounters($year = null, $month = null)
    {
        if ($year === null) {
            $year = date('Y');
        }

        if ($month === null) {
            $month = date('n');
        }

        list($previous_total_counter, $previous_year_counter, $previous_month_counter) = self::getDateInvoiceCounters($year, $month);

        return ConfigOptions::setValue('invoicing_number_date_counters', [
            $year . '_' . $month => $previous_month_counter + 1,
            $year => $previous_year_counter + 1,
            'total' => $previous_total_counter + 1,
        ]);
    }

    /**
     * Sets invoice counters.
     *
     * @param  int   $total_counter
     * @param  int   $year_counter
     * @param  int   $month_counter
     * @return array
     */
    public static function setDateInvoiceCounters($total_counter = null, $year_counter = null, $month_counter = null)
    {
        $year = date('Y');
        $month = date('n');

        $counters = self::getDateInvoiceCounters();

        if ($total_counter) {
            $counters['total'] = $total_counter;
        }

        if ($month_counter) {
            $counters["{$year}_{$month}"] = $month_counter;
        }

        if ($year_counter) {
            $counters[$year] = $year_counter;
        }

        return ConfigOptions::setValue('invoicing_number_date_counters', $counters);
    }

    /**
     * Set invoice number pattern.
     *
     * @param string $pattern
     */
    public static function setPattern($pattern)
    {
        ConfigOptions::setValue('invoicing_number_pattern', $pattern);
    }

    /**
     * Set padding for counter values.
     *
     * @param int $padding
     */
    public static function setCounterPadding($padding)
    {
        ConfigOptions::setValue('invoicing_number_counter_padding', (int) $padding);
    }
}
