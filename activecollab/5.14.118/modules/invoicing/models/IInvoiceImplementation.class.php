<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

use Angie\Search\SearchItem\SearchItemInterface;

/**
 * Invoice implementation shared between all invoice like objects.
 *
 * @package activeCollab.modules.invoicing
 * @subpackage models
 */
trait IInvoiceImplementation
{
    use IRoundFieldValueToDecimalPrecisionImplementation;

    /**
     * Fields that.
     *
     * @var array
     */
    protected $roundable_fields = [];
    /**
     * Tax grouped by type.
     *
     * @var bool
     */
    private $tax_grouped_by_type = false;

    /**
     * Say hello to the parent object.
     */
    public function IInvoiceImplementation()
    {
        $this->registerEventHandler('on_before_save', function ($is_new, $modifications) {
            if (method_exists($this, 'getHash') && !$this->getHash()) {
                $this->setHash(make_string(40));
            }

            if ($is_new) {
                if (!$this->isModifiedField('second_tax_is_enabled')) {
                    $this->setSecondTaxIsEnabled(Invoices::isSecondTaxEnabled());
                }

                if (!$this->isModifiedField('second_tax_is_compound')) {
                    $this->setSecondTaxIsCompound(Invoices::isSecondTaxCompound());
                }
            } else {
                if (isset($modifications['discount_rate']) && $items = $this->getItems()) {
                    foreach ($this->getItems() as $item) {
                        $item->setDiscountRate($modifications['discount_rate'][1]);
                        $item->save();
                    }
                }

                $this->recalculate();
            }
        });

        $this->registerEventHandler('on_before_delete', function () {
            InvoiceItems::delete(InvoiceItems::parentToCondition($this));
            InvoiceItems::clearCache();
        });

        $this->registerEventHandler('on_validate', function (ValidationErrors &$errors) {
            if (!$this->validateMinValueOf('discount_rate', 0) || !$this->validateMaxValueOf('discount_rate', 100)) {
                $errors->addError('Discount rate needs to be between 0 and 100', 'discount_rate');
            }
        });

        $this->registerEventHandler('on_json_serialize', function (array &$result) {
            $email_from = $this->getEmailFrom();

            if ($email_from instanceof User) {
                $email_from = $email_from->getId();
            } elseif ($email_from instanceof AnonymousUser) {
                $email_from = [$email_from->getEmail(), $email_from->getName()];
            } else {
                $email_from = null;
            }

            $result['company_id'] = $this->getCompanyId();
            $result['company_name'] = $this->getCompanyName();
            $result['company_address'] = $this->getCompanyAddress();
            $result['note'] = $this->getNote();
            $result['currency_id'] = $this->getCurrencyId();
            $result['language_id'] = $this->getLanguageId();
            $result['discount_rate'] = $this->getDiscountRate();
            $result['subtotal'] = $this->getSubtotal();
            $result['discount'] = $this->getDiscount();
            $result['tax'] = $this->getTax();
            $result['rounding_difference'] = $this->getRoundingDifference();
            $result['rounded_total'] = $this->getRoundedTotal();
            $result['total'] = $this->getTotal();
            $result['paid_amount'] = $this->getPaidAmount();
            $result['balance_due'] = $this->getBalanceDue();
            $result['recipients'] = $this->getRecipients();
            $result['email_from'] = $email_from;
            $result['email_subject'] = $this->getEmailSubject();
            $result['email_body'] = $this->getEmailBody();

            $result['second_tax_is_enabled'] = $this->getSecondTaxIsEnabled();
            $result['second_tax_is_compound'] = $this->getSecondTaxIsCompound();
        });

        $this->registerEventHandler('on_describe_single', function (array &$result) {
            $result['items'] = $this->getItems();

            if (empty($result['items'])) {
                $result['items'] = [];
            }
        });

        if ($this instanceof SearchItemInterface) {
            $this->addSearchFields(
                'company_name',
                'company_address',
                'recipients',
                'note',
                'private_note'
            );
        }

        if ($this instanceof IHistory) {
            $this->addHistoryFields(
                'company_id',
                'company_name',
                'company_address',
                'currency_id',
                'language_id',
                'discount_rate',
                'note',
                'private_note',
                'email_from_name',
                'email_from_email',
                'email_subject',
                'email_body',
                'recipients'
            );
        }
    }

    /**
     * Register an internal event handler.
     *
     * @param  string            $event
     * @param  callable|string   $handler
     * @throws InvalidParamError
     */
    abstract protected function registerEventHandler($event, $handler);

    /**
     * Returns true if specific field is modified.
     *
     * @param  string $field
     * @return bool
     */
    abstract public function isModifiedField($field);

    /**
     * Set value of second_tax_is_enabled field.
     *
     * @param  bool $value
     * @return bool
     */
    abstract public function setSecondTaxIsEnabled($value);

    /**
     * Set value of second_tax_is_compound field.
     *
     * @param  bool $value
     * @return bool
     */
    abstract public function setSecondTaxIsCompound($value);

    /**
     * Return item that belong to this object.
     *
     * @return InvoiceItem[]
     */
    public function getItems()
    {
        return InvoiceItems::find(['conditions' => ['parent_type = ? AND parent_id = ?', get_class($this), $this->getId()]]);
    }

    /**
     * Return object ID.
     *
     * @return int
     */
    abstract public function getId();

    /**
     * Calculate total by walking through list of items.
     */
    public function recalculate()
    {
        $subtotal = $discount = 0;
        $taxes = [];

        if ($items = $this->getItems()) {
            foreach ($items as $item) {
                $subtotal += $item->getSubTotal();
                $discount += $item->getDiscount();

                if ($item->getFirstTaxRateId()) {
                    if (!array_key_exists($item->getFirstTaxRateId(), $taxes)) {
                        $taxes[$item->getFirstTaxRateId()] = [
                            'amount' => 0,
                        ];
                    }
                    $taxes[$item->getFirstTaxRateId()]['amount'] += $item->getFirstTax();
                }

                if ($this->getSecondTaxIsEnabled() && $item->getSecondTaxRateId()) {
                    if (!array_key_exists($item->getSecondTaxRateId(), $taxes)) {
                        $taxes[$item->getSecondTaxRateId()] = [
                            'amount' => 0,
                        ];
                    }
                    $taxes[$item->getSecondTaxRateId()]['amount'] += $item->getSecondTax();
                }
            }
        }

        $decimal_spaces = $this->getCurrency()->getDecimalSpaces();

        $sum_tax = 0.000;
        if (count($taxes)) {
            foreach ($taxes as $key => $tax) {
                $sum_tax += round($tax['amount'], $decimal_spaces);
            }
        }

        $this->setSubtotal(round($subtotal, $decimal_spaces));
        $this->setDiscount(round($discount, $decimal_spaces));
        $this->setTax($sum_tax);
        $this->setTotal($this->getSubtotal() - $this->getDiscount() + $this->getTax());

        if ($this instanceof Invoice) {
            $this->setPaidAmount(round(Payments::sumByParent($this), $decimal_spaces));
            $this->setBalanceDue(round($this->getRoundedTotal() - $this->getPaidAmount(), $decimal_spaces));
        } else {
            $this->setBalanceDue($this->getTotal());
        }
    }

    /**
     * Return TRUE if second tax is enabled for this invoice.
     *
     * @return bool
     */
    abstract public function getSecondTaxIsEnabled();

    /**
     * Return invoice currency.
     *
     * @return Currency
     */
    public function getCurrency()
    {
        $currency = DataObjectPool::get('Currency', $this->getCurrencyId());

        return $currency instanceof Currency ? $currency : Currencies::getDefault();
    }

    /**
     * Return invoice currency ID.
     *
     * @return int
     */
    abstract public function getCurrencyId();

    // ---------------------------------------------------
    //  Calculation
    // ---------------------------------------------------

    /**
     * Set value of subtotal field.
     *
     * @param  float $value
     * @return float
     */
    abstract public function setSubtotal($value);

    /**
     * Set value of discount field.
     *
     * @param  float $value
     * @return float
     */
    abstract public function setDiscount($value);

    /**
     * Set value of tax field.
     *
     * @param  float $value
     * @return float
     */
    abstract public function setTax($value);

    /**
     * Set value of total field.
     *
     * @param  float $value
     * @return float
     */
    abstract public function setTotal($value);

    /**
     * Return total rounded to a precision defined by the invoice currency.
     *
     * @return float
     */
    public function getRoundedTotal()
    {
        if ($this->isLoaded()) {
            return AngieApplication::cache()->getByObject($this, 'rounded_total', function () {
                return Currencies::roundDecimal($this->getTotal(), $this->getCurrency());
            });
        } else {
            return Currencies::roundDecimal($this->getTotal(), $this->getCurrency());
        }
    }

    /**
     * Return true if object is loaded from the database.
     *
     * @return bool
     */
    abstract public function isLoaded();

    // ---------------------------------------------------
    //  Permissions
    // ---------------------------------------------------

    /**
     * Return total.
     *
     * @return float
     */
    abstract public function getTotal();

    /**
     * Valicate minimal value of specific field.
     *
     * If string minimal lenght is checked (string is trimmed before it is
     * compared). In any other case >= operator is used
     *
     * @param  string $field
     * @param  int    $min   Minimal value
     * @return bool
     */
    abstract public function validateMinValueOf($field, $min);

    /**
     * Validate max value of specific field. If that field is string time
     * max lenght will be validated.
     *
     * @param  string $field
     * @param  int    $max
     * @return bool
     */
    abstract public function validateMaxValueOf($field, $max);

    /**
     * Get who sent the email to the client.
     *
     * @return User|null
     */
    public function getEmailFrom()
    {
        return $this->getUserFromFieldSet('email_from');
    }

    /**
     * Returns user instance (or NULL) for given field set.
     *
     * @param  string $field_set_prefix
     * @return IUser
     */
    abstract public function getUserFromFieldSet($field_set_prefix);

    // ---------------------------------------------------
    //  Requirements
    // ---------------------------------------------------

    /**
     * Return client company ID.
     *
     * @return int
     */
    abstract public function getCompanyId();

    /**
     * Return value of company_name field.
     *
     * @return string
     */
    abstract public function getCompanyName();

    /**
     * Return value of company_address field.
     *
     * @return string
     */
    abstract public function getCompanyAddress();

    /**
     * Return value of note field.
     *
     * @return string
     */
    abstract public function getNote();

    /**
     * Return invoice langauge ID.
     *
     * @return int
     */
    abstract public function getLanguageId();

    /**
     * Return discount rate.
     *
     * @return int
     */
    abstract public function getDiscountRate();

    /**
     * Return value of subtotal field.
     *
     * @return float
     */
    abstract public function getSubtotal();

    /**
     * Return value of discount field.
     *
     * @return float
     */
    abstract public function getDiscount();

    /**
     * Return value of tax field.
     *
     * @return float
     */
    abstract public function getTax();

    /**
     * Get rounding difference.
     *
     * @return float
     */
    public function getRoundingDifference()
    {
        return $this->requireRounding() ? $this->getRoundedTotal() - $this->getTotal() : 0;
    }

    /**
     * Check if invoice total require rounding.
     *
     * @return bool
     */
    public function requireRounding()
    {
        return $this->getCurrency()->getDecimalRounding() > 0;
    }

    /**
     * Return value of paid_amount field.
     *
     * @return float
     */
    abstract public function getPaidAmount();

    /**
     * Return value of balance_due field.
     *
     * @return float
     */
    abstract public function getBalanceDue();

    /**
     * Return value of recipients field.
     *
     * @return string
     */
    abstract public function getRecipients();

    /**
     * Return value of email_subject field.
     *
     * @return string
     */
    abstract public function getEmailSubject();

    /**
     * Return value of email_body field.
     *
     * @return string
     */
    abstract public function getEmailBody();

    /**
     * Return TRUE if second tax is compound.
     *
     * @return bool
     */
    abstract public function getSecondTaxIsCompound();

    /**
     * Return document language.
     *
     * @return Language
     */
    public function getLanguage()
    {
        $language = DataObjectPool::get('Language', $this->getLanguageId());

        return $language instanceof Language ? $language : Languages::findDefault();
    }

    /**
     * Return invoice company.
     *
     * @return Company
     */
    public function &getCompany()
    {
        return DataObjectPool::get('Company', $this->getCompanyId());
    }

    /**
     * Return recipient instances.
     *
     * @return IUser[]
     */
    public function getRecipientInstances()
    {
        $recipients = $this->getRecipients() ? Users::findByAddressList($this->getRecipients()) : null;

        return empty($recipients) ? [] : $recipients;
    }

    /**
     * Return number of invoice items.
     *
     * @return int
     */
    public function countItems()
    {
        return AngieApplication::cache()->getByObject($this, 'items_count', function () {
            return DB::executeFirstCell('SELECT COUNT(id) AS "row_count" FROM invoice_items WHERE ' . InvoiceItems::parentToCondition($this));
        });
    }

    /**
     * Add items from attributes.
     *
     * @param  array     $attributes
     * @throws Exception
     */
    public function addItemsFromAttributes(array $attributes)
    {
        try {
            DB::beginWork('Begin: adding items from attributes @ ' . __CLASS__);

            if (isset($attributes['items']) && is_foreachable($attributes['items'])) {
                $counter = 1;

                foreach ($attributes['items'] as $item) {
                    if (($item['unit_cost'] && $item['quantity']) || $item['description']) {
                        if (!$item['description']) {
                            $item['description'] = lang('Item :item_number', ['item_number' => $counter]);
                        }
                        $this->addItem($item, $counter++, true);
                    }
                }
            }

            $this->recalculate();
            $this->save();

            DB::commit('Done: adding items from attributes @ ' . __CLASS__);
        } catch (Exception $e) {
            DB::rollback('Rollback: adding items from attributes @ ' . __CLASS__);
            throw $e;
        }
    }

    /**
     * Add a new item at a given position.
     *
     * @param  array       $attributes
     * @param  int         $position
     * @param  bool        $bulk
     * @return InvoiceItem
     * @throws Exception
     */
    public function addItem(array $attributes, $position, $bulk = false)
    {
        try {
            DB::beginWork('Begin: add invoice item @ ' . __CLASS__);

            $item = InvoiceItems::create(array_merge($attributes, [
                'parent_type' => get_class($this),
                'parent_id' => $this->getId(),
                'second_tax_is_enabled' => $this->getSecondTaxIsEnabled(),
                'second_tax_is_compound' => $this->getSecondTaxIsCompound(),
                'discount_rate' => $this->getDiscountRate(),
                'position' => $position,
            ]));

            if ($item instanceof InvoiceItems && empty($bulk)) {
                $this->recalculate();
                $this->save();
            }

            DB::commit('Done: add invoice item @ ' . __CLASS__);

            return $item;
        } catch (Exception $e) {
            DB::rollback('Rollback: add invoice item @ ' . __CLASS__);
            throw $e;
        }
    }

    /**
     * Save to database.
     */
    abstract public function save();

    /**
     * Update items from attributes.
     *
     * @param  array     $attributes
     * @throws Exception
     */
    public function updateItemsFromAttributes(array $attributes)
    {
        try {
            DB::beginWork('Begin: update items from attributes @ ' . __CLASS__);

            if (isset($attributes['items']) && is_array($attributes['items'])) {
                $current_item_ids = DB::executeFirstColumn('SELECT id FROM invoice_items WHERE parent_type = ? AND parent_id = ?', get_class($this), $this->getId());
                $counter = 1;

                foreach ($attributes['items'] as $item) {
                    $existing_item = isset($item['id']) && $item['id'] ? DataObjectPool::get('InvoiceItem', $item['id']) : null;

                    if ($existing_item instanceof InvoiceItem) {
                        if (!$item['description']) {
                            $item['description'] = lang('Item :item_number', ['item_number' => $counter]);
                        }

                        InvoiceItems::update($existing_item, $item, false);

                        $existing_item->setParent($this);
                        $existing_item->setPosition($counter++);
                        $existing_item->save();

                        if (($k = array_search($existing_item->getId(), $current_item_ids)) !== false) {
                            unset($current_item_ids[$k]);
                        }
                    } else {
                        if (($item['unit_cost'] && $item['quantity']) || $item['description']) {
                            if (!$item['description']) {
                                $item['description'] = lang('Item :item_number', ['item_number' => $counter]);
                            }
                            $this->addItem($item, $counter++, true);
                        }
                    }
                }

                if ($current_item_ids && is_foreachable($current_item_ids)) {
                    $items_to_delete = InvoiceItems::findByIds($current_item_ids);

                    /** @var InvoiceItem[] $items_to_delete */
                    if ($items_to_delete) {
                        foreach ($items_to_delete as $item_to_delete) {
                            $item_to_delete->delete();
                        }
                    }
                }

                $this->recalculate();
                $this->save();
            }

            DB::commit('Done: update items from attributes @ ' . __CLASS__);
        } catch (Exception $e) {
            DB::rollback('Rollback: update items from attributes @ ' . __CLASS__);
            throw $e;
        }
    }

    /**
     * Set who sends email.
     *
     * @param  User      $from
     * @return User|null
     */
    public function setEmailFrom($from)
    {
        return $this->setUserFromFieldSet($from, 'email_from', true, false);
    }

    /**
     * Set by user for given field set.
     *
     * @param  IUser                   $by_user
     * @param  string                  $field_set_prefix
     * @param  bool                    $optional
     * @param  bool                    $can_be_anonymous
     * @return User|AnonymousUser|null
     * @throws InvalidInstanceError
     */
    abstract public function setUserFromFieldSet($by_user, $field_set_prefix, $optional = true, $can_be_anonymous = true);

    /**
     * Get tax grouped by tax type.
     *
     * @return array
     */
    public function getTaxGroupedByType()
    {
        if ($this->tax_grouped_by_type === false) {
            $this->tax_grouped_by_type = [];

            if ($items = $this->getItems()) {
                foreach ($items as $item) {
                    if ($item->getFirstTaxRateId()) {
                        if (!array_key_exists($item->getFirstTaxRateId(), $this->tax_grouped_by_type)) {
                            $this->tax_grouped_by_type[$item->getFirstTaxRateId()] = [
                                'id' => $item->getFirstTaxRateId(),
                                'name' => $item->getFirstTaxRate()->getName(),
                                'amount' => 0,
                                'percentage' => $item->getFirstTaxRate()->getPercentage(),
                            ];
                        }
                        $this->tax_grouped_by_type[$item->getFirstTaxRateId()]['amount'] += $item->getFirstTax();
                    }

                    if ($this->getSecondTaxIsEnabled() && $item->getSecondTaxRateId()) {
                        if (!array_key_exists($item->getSecondTaxRateId(), $this->tax_grouped_by_type)) {
                            $this->tax_grouped_by_type[$item->getSecondTaxRateId()] = [
                                'id' => $item->getSecondTaxRateId(),
                                'name' => $item->getSecondTaxRate()->getName(),
                                'amount' => 0,
                                'percentage' => $item->getSecondTaxRate()->getPercentage(),
                            ];
                        }
                        $this->tax_grouped_by_type[$item->getSecondTaxRateId()]['amount'] += $item->getSecondTax();
                    }
                }
            }
        }

        return $this->tax_grouped_by_type;
    }

    /**
     * Returns true if $user can view invoice.
     *
     * @param  User $user
     * @return bool
     */
    public function canView(User $user)
    {
        return $user->isFinancialManager();
    }

    /**
     * Returns true if $user can edit this invoice.
     *
     * @param  User $user
     * @return bool
     */
    public function canEdit(User $user)
    {
        return $user->isFinancialManager();
    }

    /**
     * Returns true if $user can delete this invoice.
     *
     * @param  User $user
     * @return bool
     */
    public function canDelete(User $user)
    {
        return $user->isFinancialManager();
    }

    /**
     * Return true if $user can view access logs.
     *
     * @param  User $user
     * @return bool
     */
    public function canViewAccessLogs(User $user)
    {
        return $user->isFinancialManager();
    }

    /**
     * Check if this object has CJK (Chinese, Japanese, Korean Characters).
     *
     * @return bool
     */
    public function hasCJKCharacters()
    {
        if (has_cjk_characters($this->getCompanyName())) {
            return true;
        }
        if (has_cjk_characters($this->getCompanyAddress())) {
            return true;
        }
        if (has_cjk_characters($this->getNote())) {
            return true;
        }

        $items = $this->getItems();

        if (!empty($items)) {
            foreach ($items as $item) {
                if (has_cjk_characters($item->getDescription())) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    protected function getSearchEngine()
    {
        return AngieApplication::search();
    }

    /**
     * Set value of balance_due field.
     *
     * @param  float $value
     * @return float
     */
    abstract public function setBalanceDue($value);
}
