<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Invoice number suggester interface.
 *
 * @package ActiveCollab.modules.invoicing
 * @subpackage models
 */
interface InvoiceNumberSuggesterInterface
{
    /**
     * Make a suggestion of the next invoice number based on the last invoice number.
     *
     * @param  string $last_invoice_number
     * @param  array  $existing_invoice_numbers
     * @return string
     */
    public function suggest($last_invoice_number, array $existing_invoice_numbers = []);
}
