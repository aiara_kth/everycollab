<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Invoice note template instance.
 *
 * @package ActiveCollab.modules.invoicing
 * @subpackage models
 */
class InvoiceNoteTemplate extends BaseInvoiceNoteTemplate implements IRoutingContext
{
    /**
     * Return array or property => value pairs that describes this object.
     *
     * @return array
     */
    public function jsonSerialize()
    {
        $result = parent::jsonSerialize();

        $result['name'] = $this->getName();
        $result['content'] = $this->getContent();
        $result['is_default'] = $this->getIsDefault();

        return $result;
    }

    // ---------------------------------------------------
    //  Interface implementations
    // ---------------------------------------------------

    /**
     * Return routing context name.
     *
     * @return string
     */
    public function getRoutingContext()
    {
        return 'invoice_note_template';
    }

    /**
     * Return routing context parameters.
     *
     * @return mixed
     */
    public function getRoutingContextParams()
    {
        return ['invoice_note_template_id' => $this->getId()];
    }

    // ---------------------------------------------------
    //  Permissions
    // ---------------------------------------------------

    /**
     * Returns true if $user can view this object.
     *
     * @param  User $user
     * @return bool
     */
    public function canView(User $user)
    {
        return $user->isFinancialManager();
    }

    /**
     * Returns true if $user can update this object.
     *
     * @param  User $user
     * @return bool
     */
    public function canEdit(User $user)
    {
        return $user->isFinancialManager();
    }

    /**
     * Returns true if $user can delete or move to trash this object.
     *
     * @param  User $user
     * @return bool
     */
    public function canDelete(User $user)
    {
        return $user->isFinancialManager();
    }

    // ---------------------------------------------------
    //  System
    // ---------------------------------------------------

    /**
     * Validate before save.
     *
     * @param ValidationErrors $errors
     */
    public function validate(ValidationErrors &$errors)
    {
        $this->validatePresenceOf('name') or $errors->fieldValueIsRequired('name');
        $this->validateUniquenessOf('name') or $errors->fieldValueNeedsToBeUnique('name');
        $this->validatePresenceOf('content') or $errors->fieldValueIsRequired('content');

        return parent::validate($errors);
    }
}
