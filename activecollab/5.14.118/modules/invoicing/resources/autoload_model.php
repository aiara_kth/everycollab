<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * @package ActiveCollab.modules.invoicing
 * @subpackage resources
 */
AngieApplication::useModel(['invoices', 'recurring_profiles', 'estimates', 'invoice_items', 'invoice_item_templates', 'invoice_note_templates', 'tax_rates', 'remote_invoices'], 'invoicing');
