<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

function invoicing_handle_on_daily_maintenance()
{
    RecurringProfiles::trigger(DateTimeValue::now()->getSystemDate());

    /** @var QuickbooksIntegration $quckbooks_integration */
    $quckbooks_integration = Integrations::findFirstByType(QuickbooksIntegration::class);

    if ($quckbooks_integration->needReconnect()) {
        try {
            $quckbooks_integration->reconnect();
        } catch (Exception $e) {
            unset($e);
        }
    }

    // Send invoice overdue reminders
    require_once InvoicingModule::PATH . '/models/InvoiceOverdueReminders.class.php';
    InvoiceOverdueReminders::send();
}
