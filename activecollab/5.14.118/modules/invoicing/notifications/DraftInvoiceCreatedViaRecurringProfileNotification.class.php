<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Draft invoice created via recurring profile notification.
 *
 * @package ActiveCollab.modules.invoicing
 * @subpackage notification
 */
class DraftInvoiceCreatedViaRecurringProfileNotification extends RecurringProfileNotification
{
}
