<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Recurring Task label implementation.
 *
 * @package ActiveCollab.modules.tasks
 * @subpackage models
 */
class RecurringTaskLabel
{
}
