<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Subtask activity log trait.
 *
 * @package ActiveCollab.modules.tasks
 * @subpackage models
 */
trait SubtaskActivityLog
{
    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return array_merge(parent::jsonSerialize(), ['subtask_id' => $this->getAdditionalProperty('subtask_id')]);
    }

    /**
     * @param Subtask $subtask
     */
    public function setSubtask(Subtask $subtask)
    {
        $this->setAdditionalProperty('subtask_id', $subtask->getId());
    }

    /**
     * This method is called when we need to load related notification objects for API response.
     *
     * @param array $type_ids_map
     */
    public function onRelatedObjectsTypeIdsMap(array &$type_ids_map)
    {
        parent::onRelatedObjectsTypeIdsMap($type_ids_map);

        if ($subtask = $this->getSubtask()) {
            if (empty($type_ids_map['Subtask'])) {
                $type_ids_map['Subtask'] = [];
            }

            $type_ids_map['Subtask'][] = $subtask->getId();
        }
    }

    /**
     * Return subtask instance.
     *
     * @return Subtask
     */
    public function getSubtask()
    {
        return DataObjectPool::get('Subtask', $this->getAdditionalProperty('subtask_id'));
    }
}
