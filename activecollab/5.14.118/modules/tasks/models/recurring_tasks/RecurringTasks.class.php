<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * RecurringTasks class.
 *
 * @package ActiveCollab.modules.tasks
 * @subpackage models
 */
class RecurringTasks extends BaseRecurringTasks
{
    /**
     * Name of the key to check in memories table when was the last time that storage overused notification
     * has been sent to administrators.
     */
    const LAST_NOTIFICATION_FOR_STORAGE_OVERUSED_MEMORY_KEY = 'recurring_task_not_created_due_to_disk_space';

    /**
     * Return new collection.
     *
     * @param  string                    $collection_name
     * @param  User|null                 $user
     * @return ModelCollection
     * @throws InvalidParamError
     * @throws ImpossibleCollectionError
     */
    public static function prepareCollection($collection_name, $user)
    {
        $collection = parent::prepareCollection($collection_name, $user);

        if (str_starts_with($collection_name, 'project_recurring_tasks')) {
            return self::prepareRecurringTasksCollectionByProject($collection_name, $user);
        } else {
            if (str_starts_with($collection_name, 'all_recurring_tasks_in_project')) {
                self::prepareAssignmentsCollectionByProject($collection, $collection_name, $user);
            } elseif (str_starts_with($collection_name, 'assignments_as_calendar_events')) {
                self::prepareCalendarEventsCollection($collection, $collection_name, $user);
            } else {
                throw new InvalidParamError('collection_name', $collection_name, 'Invalid collection name');
            }
        }

        return $collection;
    }

    /**
     * Prepare recurring tasks collection filtered by project ID.
     *
     * @param  string                          $collection_name
     * @param  User                            $user
     * @return ProjectRecurringTasksCollection
     * @throws InvalidParamError
     */
    private static function prepareRecurringTasksCollectionByProject($collection_name, User $user)
    {
        $bits = explode('_', $collection_name);

        /** @var Project $project */
        if ($project = DataObjectPool::get('Project', array_pop($bits))) {
            return (new ProjectRecurringTasksCollection($collection_name))->setProject($project)->setWhosAsking($user);
        } else {
            throw new InvalidParamError('collection_name', $collection_name, 'Project ID expected in collection name');
        }
    }

    /**
     * Prepare recurring tasks collection filtered by project ID.
     *
     * @param  ModelCollection           $collection
     * @param  string                    $collection_name
     * @param  User                      $user
     * @throws ImpossibleCollectionError
     * @throws InvalidParamError
     */
    private static function prepareAssignmentsCollectionByProject(ModelCollection &$collection, $collection_name, $user)
    {
        $bits = explode('_', $collection_name);
        $project_id = array_pop($bits);

        $project = DataObjectPool::get('Project', $project_id);

        if ($project instanceof Project) {
            $collection->setOrderBy('position');

            if (str_starts_with($collection_name, 'all_recurring_tasks_in_project')) {
                if ($user instanceof Client) {
                    $collection->setConditions('project_id = ? AND is_trashed = ? AND is_hidden_from_clients = ?',
                        $project->getId(), false, false);
                } else {
                    $collection->setConditions('project_id = ? AND is_trashed = ?', $project->getId(), false, false);
                }
            } else {
                throw new InvalidParamError('collection_name', $collection_name);
            }
        } else {
            throw new ImpossibleCollectionError("Project #{$project_id} not found");
        }
    }

    /**
     * Prepare calendar events collection.
     *
     * @param  ModelCollection           $collection
     * @param  string                    $collection_name
     * @param  User|null                 $user
     * @return ModelCollection
     * @throws InvalidParamError
     * @throws ImpossibleCollectionError
     */
    private static function prepareCalendarEventsCollection(ModelCollection &$collection, $collection_name, $user)
    {
        $bits = explode('_', $collection_name);

        $parts = [
            DB::prepare('start_in IS NOT NULL AND due_in IS NOT NULL AND is_trashed = ? AND repeat_frequency <> ?',
                false, RecurringTask::REPEAT_FREQUENCY_NEVER),
        ];

        if ($user instanceof Client) {
            $parts[] = DB::prepare('is_hidden_from_clients = ?', false);
        }

        $additional_conditions = implode(' AND ', $parts);

        // everything in all projects
        if (str_starts_with($collection_name, 'assignments_as_calendar_events_everything_in_all_projects')) {
            if ($user->isPowerUser()) {
                $collection->setConditions($additional_conditions);
            } else {
                throw new ImpossibleCollectionError('Only project managers can see everything in all projects');
            }

            // everything in my projects
        } elseif (str_starts_with($collection_name, 'assignments_as_calendar_events_everything_in_my_projects')) {
            $project_ids = Projects::findIdsByUser($user, false, DB::prepare('is_trashed = ?', false));

            if ($project_ids && is_foreachable($project_ids)) {
                $collection->setConditions("project_id IN (?) AND $additional_conditions", $project_ids);
            } else {
                throw new ImpossibleCollectionError('User not involved in any of the projects');
            }

            // only my assignments
        } elseif (str_starts_with($collection_name, 'assignments_as_calendar_events_only_my_assignments')) {
            if ($user->isPowerUser() || $user->isMember() || $user->isSubcontractor()) {
                $project_ids = Projects::findIdsByUser($user, false, DB::prepare('is_trashed = ?', false));

                if ($project_ids && is_foreachable($project_ids)) {
                    $collection->setConditions("project_id IN (?) AND assignee_id = ? AND $additional_conditions", $project_ids, $user->getId());
                } else {
                    throw new ImpossibleCollectionError('User not involved in any of the projects');
                }
            } else {
                throw new ImpossibleCollectionError('User need to be Member or Subcontractor');
            }

            // assignments for specified user
        } elseif (str_starts_with($collection_name, 'assignments_as_calendar_events')) {
            $for_id = array_pop($bits);

            if ($user->isPowerUser()) {
                $for = DataObjectPool::get('User', $for_id);

                if ($for instanceof User) {
                    $project_ids = Projects::findIdsByUser($for, false, DB::prepare('is_trashed = ?', false));

                    if ($project_ids && is_foreachable($project_ids)) {
                        $collection->setConditions("project_id IN (?) AND assignee_id = ? AND $additional_conditions", $project_ids, $for->getId());
                    } else {
                        throw new ImpossibleCollectionError('User not involved in any of the projects');
                    }
                } else {
                    throw new ImpossibleCollectionError("User #{$for_id} not found");
                }
            } else {
                throw new ImpossibleCollectionError('Only project managers can see assignments for specified user');
            }

            // invalid collection name
        } else {
            throw new InvalidParamError('collection_name', $collection_name, 'Invalid collection name');
        }

        return $collection;
    }

    /**
     * Make sure that we have this list in one place.
     *
     * @return array
     */
    public static function whatIsWorthRemembering()
    {
        return ['name', 'task_list_id', 'assignee_id', 'estimate', 'job_type_id', 'due_in', 'is_important', 'completed_on', 'is_trashed'];
    }

    /**
     * Returns true if $user can create a new recurring task in $project.
     *
     * @param  User|Client $user
     * @param  Project     $project
     * @return bool
     */
    public static function canAdd(User $user, Project $project)
    {
        // no new recurring tasks allowed in trashed projects
        if ($project instanceof ITrash && $project->getIsTrashed()) {
            return false;
        }

        // no new recurring tasks allowed in completed projects
        if ($project instanceof IComplete && $project->isCompleted()) {
            return false;
        }

        // clients aren't allowed to add recurring tasks
        if ($user->isClient() && !$user->canManageTasks()) {
            return false;
        }

        return $user instanceof User && ($user->isOwner() || $project->isMember($user));
    }

    public static function create(array $attributes, bool $save = true, bool $announce = true)
    {
        self::prepareAttributes($attributes);

        try {
            DB::beginWork('Begin: create new recurring task @ ' . __CLASS__);

            $recurring_task = parent::create($attributes, $save, false);

            DB::commit('Recurring task created @ ' . __CLASS__);

            if ($recurring_task instanceof RecurringTask) {
                if (isset($attributes['attachments'])) {
                    $ids = [0];
                    foreach ($attributes['attachments'] as $attachment) {
                        $ids[] = $attachment['id'];
                    }

                    if ($attachments = Attachments::findByIds($ids)) {
                        foreach ($attachments as $attachment) {
                            if ($attachment instanceof WarehouseAttachment) {
                                $recurring_task->attachWarehouseFile($attachment, AngieApplication::authentication()->getLoggedUser());
                            } elseif ($attachment instanceof GoogleDriveAttachment || $attachment instanceof DropboxAttachment) {
                                $recurring_task->attachExternalFile($attachment, AngieApplication::authentication()->getLoggedUser());
                            } else {
                                $recurring_task->attachFile($attachment->getPath(), $attachment->getName(), $attachment->getMimeType(), AngieApplication::authentication()->getLoggedUser());
                            }
                        }
                    }
                }

                self::processRecurringTask($recurring_task, DateTimeValue::now()->getSystemDate());
            }

            AngieApplication::log()->event('recurring_task_created', 'Recurring task #{recurring_task_id} has been created ({repeat_frequency})', [
                'recurring_task_id' => $recurring_task->getId(),
                'repeat_frequency' => $recurring_task->getRepeatFrequency(),
            ]);

            return DataObjectPool::announce($recurring_task, DataObjectPool::OBJECT_CREATED, $attributes);
        } catch (Exception $e) {
            DB::rollback('Rollback: create new recurring task @ ' . __CLASS__);
            throw $e;
        }
    }

    /**
     * Prepare attributes for new recurring task.
     *
     * @param array $attributes
     */
    private static function prepareAttributes(array &$attributes)
    {
        if (!array_key_exists('repeat_frequency', $attributes)) {
            $attributes['repeat_frequency'] = RecurringTask::REPEAT_FREQUENCY_NEVER;
        }

        if ((array_key_exists('job_type_id', $attributes) && empty($attributes['job_type_id'])) ||
            (array_key_exists('estimate', $attributes) && empty($attributes['estimate']))
        ) {
            $attributes['job_type_id'] = 0;
            $attributes['estimate'] = 0;
        }

        if (!array_key_exists('start_in', $attributes)) {
            $attributes['start_in'] = null;
        }
        if (!array_key_exists('due_in', $attributes)) {
            $attributes['due_in'] = null;
        }

        self::prepareRepeatAmount($attributes);

        if (!empty($attributes['is_hidden_from_clients'])) {
            if (!empty($attributes['assignee_id']) && DataObjectPool::get('User', $attributes['assignee_id'])->isClient()) {
                throw new LogicException("Recurring task can not be assigned to a client if it's hidden from clients");
            }
        }

        // prepare Subtasks
        if (!empty($attributes['subtasks']) && is_array($attributes['subtasks'])) {
            foreach ($attributes['subtasks'] as $subtask_id => $subtask) {
                if (empty($subtask['body'])) {
                    unset($attributes['subtasks'][$subtask_id]);
                } else {
                    if (!empty($attributes['is_hidden_from_clients']) && !empty($subtask['assignee_id']) && DataObjectPool::get('User', $subtask['assignee_id'])->isClient()) {
                        throw new LogicException("Recurring task can not be assigned to a client if it's hidden from clients");
                    }
                }
            }
        }
    }

    /**
     * Prepare repeat_amount value based on repeat_frequency.
     *
     * @param array $attributes
     */
    private static function prepareRepeatAmount(array &$attributes)
    {
        if (!array_key_exists('repeat_amount', $attributes)) {
            $attributes['repeat_amount'] = null; // We have to start somewhere...
        }

        if ($attributes['repeat_frequency'] == 'daily' && $attributes['repeat_amount'] === null) {
            $attributes['repeat_amount'] = 1; // Case for repeat_frequently 'Daily' and repeat_amount if is null to set 1
        }

        if (empty($attributes['repeat_amount'])) {
            $attributes['repeat_amount'] = 0; // Repeat amount can't be NULL, so lets set it to 0 if we still have NULL
        }
    }

    /**
     * Update an instance.
     *
     * @param  DataObject|RecurringTask $instance
     * @param  array                    $attributes
     * @param  bool                     $save
     * @return DataObject
     * @throws Exception
     */
    public static function &update(DataObject &$instance, array $attributes, $save = true)
    {
        self::prepareAttributes($attributes);

        try {
            DB::beginWork('Begin: update existing recurring task @ ' . __CLASS__);

            if (isset($attributes['subscribers'])) {
                $instance->setSubscribers($attributes['subscribers']);
            }

            $recurring_task = parent::update($instance, $attributes, $save);

            DB::commit('Recurring task created @ ' . __CLASS__);

            if ($recurring_task instanceof RecurringTask) {
                self::processRecurringTask($recurring_task, DateTimeValue::now()->getSystemDate());
            }

            return $recurring_task;
        } catch (Exception $e) {
            DB::rollback('Failed to update a recurring task @ ' . __CLASS__);
            throw $e;
        }
    }

    /**
     * Revoke assignee on all recurring tasks and subtasks where $user is assigned.
     *
     * @param  User                         $user
     * @param  User                         $by
     * @throws InsufficientPermissionsError
     */
    public static function revokeAssignee(User $user, User $by)
    {
        if ($user->canChangeRole($by, null)) {
            // first get all recurring tasks
            if ($recurring_tasks = self::findBySQL('SELECT * FROM recurring_tasks')) {
                /** @var RecurringTask $task */
                foreach ($recurring_tasks as $task) {
                    $task->removeAssignee($user, $by);
                }
            }
        } else {
            throw new InsufficientPermissionsError();
        }
    }

    /**
     * Return next field value in a given project id.
     *
     * @param  int    $project_id
     * @param  string $field
     * @return int
     */
    public static function findNextFieldValueByProject($project_id, $field)
    {
        return DB::executeFirstCell("SELECT MAX($field) FROM recurring_tasks WHERE project_id = ?", $project_id) + 1;
    }

    /**
     * Trigger cron job.
     *
     * @param  DateValue $date
     * @return array
     */
    public static function trigger(DateValue $date)
    {
        $tasks = [];

        /** @var RecurringTask[] $recurring_tasks */
        if ($recurring_tasks = self::getRecurringTasksToTrigger()) {
            foreach ($recurring_tasks as $recurring_task) {
                $task = self::processRecurringTask($recurring_task, $date);

                if ($task instanceof Task) {
                    $tasks[] = $task->getId();
                }
            }
        }

        return $tasks;
    }

    /**
     * Process creating task from recurring task.
     *
     * @param  RecurringTask $recurring_task
     * @param  DateValue     $date
     * @return Task
     */
    private static function processRecurringTask(RecurringTask $recurring_task, DateValue $date)
    {
        if ($recurring_task->shouldSendOn($date)) {
            if ($recurring_task->countAttachments(false) && self::isDiskFull()) {
                $last_notification_sent = AngieApplication::memories()
                    ->get(self::LAST_NOTIFICATION_FOR_STORAGE_OVERUSED_MEMORY_KEY);
                $current_timestamp = AngieApplication::currentTimestamp()->getCurrentTimestamp();

                // send notification if one hasn't been sent ever before, or if it has been sent more then 7 days ago
                if (!$last_notification_sent || strtotime('+7 days', $last_notification_sent) < $current_timestamp) {

                    /** @var StorageOverusedNotification $notification */
                    $notification = AngieApplication::notifications()->notifyAbout('system/storage_overused');

                    $notification->setDiskSpaceLimit(
                        format_file_size(AngieApplication::storageCapacityCalculator()->getCapacity())
                    )->sendToAdministrators();

                    // remember when last notification has been sent
                    AngieApplication::memories()->set(
                        self::LAST_NOTIFICATION_FOR_STORAGE_OVERUSED_MEMORY_KEY,
                        $current_timestamp
                    );
                }

                return null;
            }

            return $recurring_task->createTask(
                [
                    'trigger_date' => $date,
                ]
            );
        }

        return null;
    }

    private static function isDiskFull()
    {
        return AngieApplication::isOnDemand() && AngieApplication::storage()->isDiskFull(true);
    }

    /**
     * Return recurring tasks that need to be sent on a given date.
     *
     * @return DBResult|RecurringTask[]
     */
    private static function getRecurringTasksToTrigger()
    {
        return self::findBySQL(
            'SELECT rt.*
                FROM recurring_tasks AS rt JOIN projects AS p ON p.id = rt.project_id
                WHERE rt.is_trashed=? AND rt.repeat_frequency != ? AND p.completed_on IS ?',
            false,
            'never',
            null
        );
    }

    /**
     * Generate ghost tasks from range fro calendar.
     *
     * @param  array     $ids
     * @param  DateValue $from_date
     * @param  DateValue $to_date
     * @return array
     */
    public static function getRangeForCalendar($ids, $from_date, $to_date)
    {
        $ids = !empty($ids) ? $ids : [0];
        $today = DateValue::now()->beginningOfDay();

        // Check if range in current month, and if is take $from_date to $today day
        if ($today->getTimestamp() > $from_date->getTimestamp()) {
            $from_date = $today;
        }

        $recurring_tasks = self::findBySQL('SELECT * FROM recurring_tasks WHERE id IN (?)', $ids);

        $ghost_tasks = [];
        $i = 0; // Increment for calendar order

        if (!empty($recurring_tasks)) {
            /** @var RecurringTask $recurring_task */
            foreach ($recurring_tasks as $recurring_task) {
                $start_in_value = $recurring_task->getStartIn();
                $due_in_value = $recurring_task->getDueIn();

                $last_trigger_on = !empty($recurring_task->getLastTriggerOn()) ? $recurring_task->getLastTriggerOn()->beginningOfDay()->getTimestamp() : null;
                $created_on = !empty($recurring_task->getCreatedOn()) ? $recurring_task->getCreatedOn()->beginningOfDay()->getTimestamp() : null;

                $repeat_amount = $recurring_task->getRepeatAmount();

                if ($recurring_task->getRepeatFrequency() == RecurringTask::REPEAT_FREQUENCY_MONTHLY) {
                    $next_trigger_on = DateValue::makeFromTimestamp(strtotime('-' . $due_in_value - 2 . ' day', $from_date->getTimestamp()));
                } elseif ($recurring_task->getRepeatFrequency() == RecurringTask::REPEAT_FREQUENCY_DAILY && $repeat_amount == 0) {
                    $range = $recurring_task->getStartDueOnRangeSkipWeekend($from_date->getTimestamp(), true);
                    $next_trigger_on = DateValue::makeFromTimestamp(strtotime('-' . $range['due_in']  . ' day', $from_date->getTimestamp()));
                } else {
                    $next_trigger_on = DateValue::makeFromTimestamp(strtotime('-' . $due_in_value - 1 . ' day', $from_date->getTimestamp()));
                }

                do {
                    $timestamp = $next_trigger_on->getTimestamp();
                    switch ($recurring_task->getRepeatFrequency()) {
                        case RecurringTask::REPEAT_FREQUENCY_DAILY:
                            $next_trigger_on = DateValue::makeFromTimestamp(strtotime('+1 day', $timestamp));

                            // Jump Weekend
                            if ($repeat_amount == 0) {
                                if ($next_trigger_on->isWeekend()) {
                                    $next_trigger_on = DateValue::makeFromTimestamp(strtotime('next monday', $timestamp));
                                }

                                $skipWeekend = $recurring_task->getStartDueOnRangeSkipWeekend($next_trigger_on->getTimestamp());
                                $start_in_value = $skipWeekend['start_in'];
                                $due_in_value = $skipWeekend['due_in'];
                            }
                            break;
                        case RecurringTask::REPEAT_FREQUENCY_WEEKLY:
                            $next_trigger_on = DateValue::makeFromTimestamp(strtotime('next ' . $recurring_task->getDayString($repeat_amount), $timestamp));
                            break;
                        case RecurringTask::REPEAT_FREQUENCY_MONTHLY:
                            $monthName = date('F', $timestamp);

                            // 29 represents last day in month
                            if ($repeat_amount == 29) {
                                $repeat_amount = 'last day of';
                            }

                            $timestamp_date = DateValue::makeFromTimestamp(strtotime($repeat_amount . ' ' . $monthName, $timestamp));

                            $next_trigger_on = DateValue::makeFromTimestamp(strtotime('this month', $timestamp_date->getTimestamp()));

                            // If current $timestamp same like $next_trigger_on then set next month
                            if ($timestamp == $next_trigger_on->getTimestamp()) {
                                if ($recurring_task->getRepeatAmount() == 29) {
                                    $next_trigger_on = $next_trigger_on = DateValue::makeFromTimestamp(strtotime('last day of next month', $timestamp));
                                } else {
                                    $next_trigger_on = $next_trigger_on = DateValue::makeFromTimestamp(strtotime('next month', $timestamp_date->getTimestamp()));
                                }
                            }

                            break;
                    }

                    $start_in = DateValue::makeFromTimestamp(strtotime('+' . $start_in_value . ' day', $next_trigger_on->getTimestamp()));
                    $due_in = DateValue::makeFromTimestamp(strtotime('+' . $due_in_value . ' day', $next_trigger_on->getTimestamp()));

                    if ($start_in->getTimestamp() <= $to_date->getTimestamp()
                        && $due_in->getTimestamp() >= $from_date->getTimestamp()
                        && $created_on <= $next_trigger_on->getTimestamp()
                        && $last_trigger_on != $next_trigger_on->getTimestamp()
                    ) {
                        $ghost_tasks[$i]['class'] = 'GhostTask';
                        $ghost_tasks[$i]['id'] = $i;
                        $ghost_tasks[$i]['project_id'] = $recurring_task->getProjectId();
                        $ghost_tasks[$i]['project_name'] = $recurring_task->getProject()->getName();
                        $ghost_tasks[$i]['assignee_id'] = $recurring_task->getAssigneeId();
                        $ghost_tasks[$i]['name'] = $recurring_task->getName();
                        $ghost_tasks[$i]['start_on'] = $start_in->getTimestamp();
                        $ghost_tasks[$i]['due_on'] = $due_in->getTimestamp();
                        $ghost_tasks[$i]['created_on'] = $next_trigger_on->getTimestamp();
                        $ghost_tasks[$i]['is_hidden_form_clients'] = $recurring_task->getIsHiddenFromClients();
                        $ghost_tasks[$i]['is_important'] = $recurring_task->getIsImportant();
                        $ghost_tasks[$i]['url_path'] = $recurring_task->getUrlPath();
                        $ghost_tasks[$i]['start_in'] = date('Y-m-d', $start_in->getTimestamp());
                        $ghost_tasks[$i]['due_in'] = date('Y-m-d', $due_in->getTimestamp());
                        $ghost_tasks[$i]['created_on1'] = date('Y-m-d', $next_trigger_on->getTimestamp());
                        $ghost_tasks[$i]['repeat_frequency'] = $recurring_task->getRepeatFrequency();

                        ++$i;
                    }
                } while ($start_in->getTimestamp() <= $to_date->getTimestamp());
            }
        }

        return $ghost_tasks;
    }
}
