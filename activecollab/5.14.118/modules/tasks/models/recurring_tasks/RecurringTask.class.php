<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * RecurringTask class.
 *
 * @package ActiveCollab.modules.tasks
 * @subpackage models
 */
class RecurringTask extends BaseRecurringTask implements IAssignees, IRoutingContext, ICalendarFeedElement
{
    use IRoutingContextImplementation, ICalendarFeedElementImplementation;

    // repeat frequency constants
    const REPEAT_FREQUENCY_NEVER = 'never';
    const REPEAT_FREQUENCY_DAILY = 'daily';
    const REPEAT_FREQUENCY_WEEKLY = 'weekly';
    const REPEAT_FREQUENCY_MONTHLY = 'monthly';

    // repeat amount constants
    const REPEAT_NEVER_VALUES = [0];
    const REPEAT_DAILY_VALUES = [0, 1]; // 0 - skip weekend, 1 - include weekend
    const REPEAT_WEEKLY_VALUES = [1, 2, 3, 4, 5, 6, 7];
    const REPEAT_MONTHLY_VALUES = [
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
        24, 25, 26, 27, 28, 29,
    ]; // 29 value indicates 'last day in month', which can be 29., 30. or 31.

    /**
     * Return array or property => value pairs that describes this object.
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return array_merge(
            parent::jsonSerialize(),
            [
                'project_id' => $this->getProjectId(),
                'task_list_id' => $this->getTaskListId(),
                'position' => $this->getPosition(),
                'is_important' => $this->getIsImportant(),
                'start_in' => $this->getStartIn(),
                'due_in' => $this->getDueIn(),
                'estimate' => $this->getEstimate(),
                'job_type_id' => $this->getJobTypeId(),
                'last_trigger_on' => $this->getLastTriggerOn(),
                'repeat_frequency' => $this->getRepeatFrequency(),
                'repeat_amount' => $this->getRepeatAmount(),
                'triggered_number' => $this->getTriggeredNumber(),
                'next_trigger_on' => $this->getNextTriggerOn(),
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getSearchDocument()
    {
        return new ProjectElementSearchDocument($this);
    }

    /**
     * Register execution recurring task.
     *
     * @param DateValue $last_trigger_on
     */
    private function registerRecurringTaskCreated($last_trigger_on = null)
    {
        $this->setTriggeredNumber($this->getTriggeredNumber() + 1);
        $this->setLastTriggerOn($last_trigger_on ? $last_trigger_on : DateValue::now());
        $this->save();
    }

    /**
     * Remove assignee on recurring task and subtasks if $user is assigned.
     *
     * @param User $user
     * @param User $by
     */
    public function removeAssignee(User $user, User $by)
    {
        // if $user is assignee on task, remove assignee
        if ($this->getAssigneeId() === $user->getId()) {
            $this->setAssignee(null, $by);
        }

        // get all subtasks for recurring task and remove $user if assignee
        if ($recurring_subtasks = $this->getSubtasks()) {
            $this->setSubtasks(null);
            $new_subtasks = []; // new subtasks needs to be set

            foreach ($recurring_subtasks as $subtask) {
                $assignee_id = $subtask['assignee_id'];

                if ($assignee_id === $user->getId()) {
                    $assignee_id = 0;
                }

                $new_subtasks[] = ['assignee_id' => $assignee_id, 'body' => $subtask['body']];
            }

            $this->setSubtasks($new_subtasks);
            $this->save();
        }
    }

    /**
     * Create subtasks based on recurring task.
     *
     * @param int   $task_id
     * @param array $subtasks
     */
    public function createSubtasksFromRecurringTask($task_id, $subtasks)
    {
        foreach ($subtasks as $subtask) {
            Subtasks::create([
                'task_id' => $task_id,
                'assignee_id' => $subtask['assignee_id'],
                'body' => $subtask['body'],
                'created_by_id' => $this->getCreatedById(),
                'created_by_name' => $this->getCreatedByName(),
                'created_by_email' => $this->getCreatedByEmail(),
            ]);
        }
    }

    /**
     * Create task from recurring task.
     *
     * @param  array     $additional
     * @return Task
     * @throws Exception
     */
    public function createTask($additional = null)
    {
        try {
            DB::beginWork('Begin: create task from recurring task @ ' . __CLASS__);

            $start_in = $this->getStartIn();
            $due_in = $this->getDueIn();
            $task_list_id = $this->getTaskListId();

            if (!empty($additional['trigger_date'])) {
                $trigger_date = $additional['trigger_date']->getTimestamp();
            } else {
                $trigger_date = DateValue::now()->getTimestamp();
            }

            // Prepare user
            /** @var User[] $additional */
            if (isset($additional['user']) && $additional['user'] instanceof User) {
                $user['created_by_id'] = $additional['user']->getId();
                $user['created_by_name'] = $additional['user']->getName();
                $user['created_by_email'] = $additional['user']->getEmail();

                $is_make_one = true;
            } else {
                $user['created_by_id'] = $this->getCreatedById();
                $user['created_by_name'] = $this->getCreatedByName();
                $user['created_by_email'] = $this->getCreatedByEmail();
                $is_make_one = false;
            }

            if ($this->getRepeatFrequency() == self::REPEAT_FREQUENCY_DAILY && $this->getRepeatAmount() == 0) {
                $range = $this->getStartDueOnRangeSkipWeekend($trigger_date);
                $start_in = $range['start_in'];
                $due_in = $range['due_in'];
            }

            $start_on = $due_on = null;

            if (!empty($start_in) || !empty($due_in)) {
                $start_on = DateValue::makeFromTimestamp(strtotime('+' . $start_in . 'day', $trigger_date));
            }

            if (!empty($due_in)) {
                $due_on = DateValue::makeFromTimestamp(strtotime('+' . $due_in . 'day', $trigger_date));
            }

            if ($due_in === 0) {
                $start_on = DateTimeValue::now()->getSystemDate();
                $due_on = DateTimeValue::now()->getSystemDate();
            }

            /** @var TaskList $check_task_list */
            $check_task_list = TaskLists::findById($task_list_id);
            if (empty($check_task_list) || $check_task_list->isCompleted() || $check_task_list->getIsTrashed()) {
                $task_list_id = TaskLists::getFirstTaskListId($this->getProject(), false);
            }

            /** @var Task $task */
            $task = Tasks::create([
                'project_id' => $this->getProjectId(),
                'task_list_id' => $task_list_id,
                'assignee_id' => $this->getAssigneeId(),
                'name' => empty($additional['name']) ? $this->getName() : $additional['name'],
                'body' => $this->getBody(),
                'is_important' => $this->getIsImportant(),
                'created_by_id' => $user['created_by_id'],
                'created_by_name' => $user['created_by_name'],
                'created_by_email' => $user['created_by_email'],
                'start_on' => $start_on,
                'due_on' => $due_on,
                'job_type_id' => $this->getJobTypeId(),
                'estimate' => $this->getEstimate(),
                'is_hidden_from_clients' => $this->getIsHiddenFromClients(),
                'created_from_recurring_task_id' => $this->getId(),
            ]);

            if (is_array($this->getSubtasks()) && count($this->getSubtasks())) {
                $this->createSubtasksFromRecurringTask($task->getId(), $this->getSubtasks());
            }

            if (!empty($additional['trigger_date'])) {
                $this->registerRecurringTaskCreated();
            }

            $this->cloneLabelsTo($task);
            $this->cloneAttachmentsTo($task);
            $this->cloneSubscribersTo($task, $this->getSubscriberIds());

            DB::commit('Done: create task & subtasks from recurring task @ ' . __CLASS__);

            $notification = AngieApplication::notifications()->notifyAbout('tasks/new_task', $task, $task->getCreatedBy());
            if (!$is_make_one) {
                $notification->setSender(null);
            }
            $notification->sendToSubscribers();

            // Engagement + 1
            AngieApplication::log()->event('task_created_from_recurring_task', 'Task #{task_id} has been created from {repeat_frequency} recurring task #{recurring_task_id}', [
                'task_id' => $task->getId(),
                'recurring_task_id' => $this->getId(),
                'repeat_frequency' => $this->getRepeatFrequency(),
                'manually_triggered_by' => $is_make_one ? $task->getCreatedById() : 0,
            ]);

            return $task;
        } catch (Exception $e) {
            DB::rollback('Rollback: create task from recurring task @ ' . __CLASS__);
            throw $e;
        }
    }

    /**
     * @param        $created_on
     * @param  bool  $reverse
     * @return array
     */
    public function getStartDueOnRangeSkipWeekend($created_on, $reverse = false)
    {
        $operand = '+';

        if (empty($created_on)) {
            $created_on = $this->getCreatedOn()->getTimestamp();
        }

        if ($reverse == true) {
            $operand = '-';
        }

        $start_on = $this->getStartIn();
        $due_on = $this->getDueIn();

        $i = 0;
        while ($start_on >= $i) {
            $dateStart = DateValue::makeFromTimestamp(strtotime($operand . $i . ' day', $created_on));
            if ($dateStart->isWeekend()) {
                ++$start_on;
            }
            ++$i;
        }

        $i = 0;
        while ($due_on >= $i) {
            $dateDue = DateValue::makeFromTimestamp(strtotime($operand . $i . ' day', $created_on));
            if ($dateDue->isWeekend()) {
                ++$due_on;
            }
            ++$i;
        }

        return ['start_in' => $start_on, 'due_in' => $due_on];
    }

    // ---------------------------------------------------
    //  System
    // ---------------------------------------------------

    /**
     * Get values of available frequencies.
     *
     * @return array
     */
    private function getAvailableFrequencies()
    {
        return [self::REPEAT_FREQUENCY_NEVER, self::REPEAT_FREQUENCY_DAILY, self::REPEAT_FREQUENCY_WEEKLY, self::REPEAT_FREQUENCY_MONTHLY];
    }

    /**
     * Get appropriate values of frequency.
     *
     * @param  string $frequency
     * @return array
     */
    private function getAppropriateFrequencyValues($frequency)
    {
        switch (strtolower($frequency)) {
            case self::REPEAT_FREQUENCY_NEVER:
                return self::REPEAT_NEVER_VALUES;
            case self::REPEAT_FREQUENCY_DAILY:
                return self::REPEAT_DAILY_VALUES;
            case self::REPEAT_FREQUENCY_WEEKLY:
                return self::REPEAT_WEEKLY_VALUES;
            case self::REPEAT_FREQUENCY_MONTHLY:
                return self::REPEAT_MONTHLY_VALUES;
            default:
                return [];
        }
    }

    /**
     * Validate before save.
     *
     * @param ValidationErrors $errors
     */
    public function validate(ValidationErrors &$errors)
    {
        $this->validatePresenceOf('name') || $errors->fieldValueIsRequired('name');
        $this->validatePresenceOf('task_list_id') || $errors->fieldValueIsRequired('task_list_id');

        if ($this->validatePresenceOf('estimate') && !$this->validatePresenceOf('job_type_id')) {
            $errors->addError('Job type is required for recurring tasks with estimates', 'job_type_id');
        }

        if ($this->getStartIn() && $this->getDueIn() && $this->getStartIn() > $this->getDueIn()) {
            $errors->addError('Start date should be before due date', 'start_in');
        }

        if ($this->getRepeatFrequency() && !in_array($this->getRepeatFrequency(), $this->getAvailableFrequencies())) {
            $errors->addError('Frequency value is not correct', 'repeat_frequency');
        }

        if ($this->getRepeatAmount() && !in_array($this->getRepeatAmount(), $this->getAppropriateFrequencyValues($this->getRepeatFrequency()))) {
            $errors->addError('Frequency repeat value is not correct', 'repeat_amount');
        }

        parent::validate($errors);
    }

    /**
     * Set value of specific field.
     *
     * @param  string            $name
     * @param  mixed             $value
     * @return mixed
     * @throws InvalidParamError
     */
    public function setFieldValue($name, $value)
    {
        if ($name == 'estimate') {
            if ($value) {
                if (strpos($value, ':') !== false) {
                    $value = time_to_float($value);
                }

                if ($value > 0 && $value < 0.01) {
                    $value = 0.01;
                }
            } else {
                $value = 0; // Make sure that empty values are always stored as 0
            }
        }

        if ($name == 'job_type_id' && empty($value)) {
            $value = 0; // Make sure that empty values are always stored as 0
        }

        return parent::setFieldValue($name, $value);
    }

    /**
     * Save recurring task to database.
     */
    public function save()
    {
        if ($this->isNew() && !$this->getPosition()) {
            $this->setPosition(RecurringTasks::findNextFieldValueByProject($this->getProjectId(), 'position'));
        }

        if ($this->getStartIn() >= 0 && is_null($this->getDueIn())) {
            $this->setDueIn($this->getStartIn()); // if not specified due_in, it assumed to be same as start_in
        } elseif (is_null($this->getStartIn()) && $this->getDueIn() >= 0) {
            $this->setStartIn($this->getDueIn()); // if not specified start_in, it assumed to be as due_in
        }

        parent::save();
    }

    /**
     * Return true if $user can edit recurring task.
     *
     * @param  User $user
     * @return bool
     */
    public function canEdit(User $user)
    {
        return $this->canView($user) && ($this->isCreatedBy($user) || $user->isMember() || $user->isPowerClient(true));
    }

    // ---------------------------------------------------
    //  Interface implementations
    // ---------------------------------------------------

    /**
     * {@inheritdoc}
     */
    public function getSubtasks($include_trashed = false)
    {
        return $this->getAdditionalProperty('recurring_subtasks');
    }

    /**
     * Set a list of recurring subtasks.
     *
     * @param  array $recurring_subtasks
     * @return mixed
     */
    public function setSubtasks($recurring_subtasks)
    {
        if (empty($recurring_subtasks)) {
            $recurring_subtasks = null;
        }

        return $this->setAdditionalProperty('recurring_subtasks', $recurring_subtasks);
    }

    /**
     * Return routing context name.
     *
     * @return string
     */
    public function getRoutingContext()
    {
        return 'recurring_task';
    }

    /**
     * Return routing context parameters.
     *
     * @return mixed
     */
    public function getRoutingContextParams()
    {
        return ['project_id' => $this->getProjectId(), 'recurring_task_id' => $this->getId()];
    }

    /**
     * Return label type.
     *
     * @return string
     */
    public function getLabelType()
    {
        return 'TaskLabel';
    }

    // ---------------------------------------------------
    //  Activity logs
    // ---------------------------------------------------

    /**
     * Return which modifications should we remember.
     *
     * @return array
     */
    protected function whatIsWorthRemembering()
    {
        return RecurringTasks::whatIsWorthRemembering();
    }

    /**
     * Check should send task from this recurring task.
     *
     * @param  DateValue $date
     * @return bool
     */
    public function shouldSendOn(DateValue $date)
    {
        if ($this->getRepeatFrequency() == self::REPEAT_FREQUENCY_NEVER) {
            return false;
        }

        $now = DateTimeValue::now();
        $next_trigger_on = $this->getNextTriggerOn();
        $is_same_day = false; // To prevent execution on same day if run twice

        if (!empty($this->getLastTriggerOn())) {
            $last_trigger_on = DateTimeValue::makeFromTimestamp($this->getLastTriggerOn()->getTimestamp())->setTime($now->getHour(), $now->getMinute(), $now->getSecond())->getSystemDate();
            $is_same_day = $last_trigger_on->getTimestamp() == $next_trigger_on->getTimestamp() ? true : false;
        }

        if ($is_same_day) {
            return false;
        }

        if ($next_trigger_on instanceof DateValue) {
            return !$this->getIsTrashed() && $next_trigger_on->toMySQL() == ($date instanceof DateTimeValue ? $date->dateToMySQL() : $date->toMySQL());
        } else {
            return false;
        }
    }

    /**
     * Get day string.
     *
     * @param  int    $day
     * @return string
     */
    public function getDayString($day)
    {
        switch ($day) {
            case 1:
                return 'monday';
            case 2:
                return 'tuesday';
            case 3:
                return 'wednesday';
            case 4:
                return 'thursday';
            case 5:
                return 'friday';
            case 6:
                return 'saturday';
            case 7:
                return 'sunday';
            default:
                return 'monday';
        }
    }

    /**
     * Return date for next trigger.
     *
     * @return DateValue|bool|null
     * @throws Exception
     */
    public function getNextTriggerOn()
    {
        // Dates for calculation
        $now = DateTimeValue::now();
        $today = $now->getSystemDate()->beginningOfDay();
        $start_on = DateValue::makeFromTimestamp($this->getCreatedOn()->getTimestamp() - 86400); // Start on minus one day if starts for first time
        $last_trigger_on = $this->getLastTriggerOn();

        // Paremeters
        $repeat_amount = $this->getRepeatAmount();
        $repeat_frequency = $this->getRepeatFrequency();

        // If last trigger on doesn't exits
        if (!$last_trigger_on instanceof DateValue) {
            $next_trigger_on = $last_trigger_on = $start_on;
        } else {
            $next_trigger_on = $last_trigger_on->getTimestamp() < $start_on->getTimestamp() ? $start_on : $last_trigger_on;
        }

        $next_trigger_on = DateTimeValue::makeFromTimestamp($next_trigger_on->getTimestamp())->setTime($now->getHour(), $now->getMinute(), $now->getSecond())->getSystemDate();

        $i = 1; // For monthly and prevent infinite loop
        do {
            $timestamp = $next_trigger_on->getTimestamp();

            switch ($repeat_frequency) {
                case self::REPEAT_FREQUENCY_NEVER:
                    return false;

                case self::REPEAT_FREQUENCY_DAILY:
                    // For first execution
                    if (empty($this->getLastTriggerOn())) {
                        return DateValue::makeFromTimestamp($today->getTimestamp());
                    }

                    $next_trigger_on = DateValue::makeFromTimestamp(strtotime('+1 day', $timestamp));

                    // Jump Weekend
                    if ($repeat_amount == 0 && $next_trigger_on->isWeekend()) {
                        $next_trigger_on = DateValue::makeFromTimestamp(strtotime('next monday', $timestamp));
                    }
                    break;
                case self::REPEAT_FREQUENCY_WEEKLY:
                    $next_trigger_on = DateValue::makeFromTimestamp(strtotime('next ' . $this->getDayString($repeat_amount), $timestamp));
                    break;
                case self::REPEAT_FREQUENCY_MONTHLY:

                    // Check if is for this month if is not go to next month
                    if ($i == 1 && empty($this->getLastTriggerOn())) {
                        if (empty($this->getLastTriggerOn()) || $timestamp < $today->getTimestamp()) {
                            $timestamp = $today->getTimestamp();
                        }
                        $month = (int) date('m', $timestamp);
                        $year = (int) date('Y', $timestamp);
                    } else {
                        $month = (int) date('m', strtotime('next month', $next_trigger_on->getTimestamp()));
                        $year = (int) date('Y', $next_trigger_on->getTimestamp());

                        if ($month < 2) {
                            $year++;
                        }
                    }

                    $next_trigger_on = DateValue::makeFromString("{$year}/{$month}/{$repeat_amount}");

                    // Check if is last day in month
                    if ($repeat_amount == 29) {
                        if (empty($this->getLastTriggerOn()) || $timestamp < $today->getTimestamp()) {
                            $timestamp = $today->getTimestamp();
                        }

                        $next_trigger_on = DateValue::makeFromTimestamp(strtotime('last day of this month', $timestamp));
                    }

                    ++$i;
                    break;
                default:
                    throw new \Exception('Invalid recurring profile frequency');
            }
        } while ($next_trigger_on->getTimestamp() < $today->getTimestamp());

        return $next_trigger_on;
    }

    /**
     * {@inheritdoc}
     */
    public function getCalendarFeedSummary(IUser $user, $prefix = '', $sufix = '')
    {
        return $prefix . ' ↻ ' . lang('Recurring Task', true, $user->getLanguage()) . ': ' . $this->getName() . $sufix;
    }
}
