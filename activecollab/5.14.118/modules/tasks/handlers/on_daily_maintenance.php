<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Invoicing handle maintenance tasks.
 *
 * @package ActiveCollab.modules.invoicing
 * @subpackage handlers
 */

/**
 * Do daily taks.
 */
function tasks_handle_on_daily_maintenance()
{
    $reference = microtime(true);
    $tasks = RecurringTasks::trigger(DateTimeValue::now()->getSystemDate());

    if (!empty($tasks)) {
        AngieApplication::log()->info('Daily recurring tasks created', [
            'tasks_created' => count($tasks),
            'exec_time' => round(microtime(true) - $reference, 5),
        ]);
    }
}
