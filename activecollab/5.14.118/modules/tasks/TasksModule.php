<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Tasks module definition.
 *
 * @package ActiveCollab.modules.tasks
 * @subpackage models
 */
class TasksModule extends AngieModule
{
    const NAME = 'tasks';

    /**
     * Plain module name.
     *
     * @var string
     */
    protected $name = 'tasks';

    /**
     * Module version.
     *
     * @var string
     */
    protected $version = '5.0';

    /**
     * Define project task lists routes.
     *
     * @param array $collection
     * @param array $single
     */
    public static function defineProjectTaskListRoutes($collection, $single)
    {
        Router::mapResource('task_lists', ['module' => self::NAME, 'collection_path' => "$single[path]/task-lists", 'collection_requirements' => $collection['requirements']], function ($collection, $single) {
            Router::map("$collection[name]_archive", "$collection[path]/archive", ['action' => ['GET' => 'archive'], 'controller' => 'task_lists', 'module' => TasksModule::NAME], $collection['requirements']);
            Router::map("$collection[name]_reorder", "$collection[path]/reorder", ['action' => ['PUT' => 'reorder'], 'controller' => 'task_lists', 'module' => TasksModule::NAME], $collection['requirements']);

            Router::map("$single[name]_move_to_project", "$single[path]/move-to-project", ['action' => ['PUT' => 'move_to_project'], 'controller' => 'task_lists', 'module' => TasksModule::NAME], $single['requirements']);
            Router::map("$single[name]_completed_tasks", "$single[path]/completed-tasks", ['action' => ['GET' => 'completed_tasks'], 'controller' => 'task_lists', 'module' => TasksModule::NAME], $single['requirements']);
        });
    }

    /**
     * Define project task routes.
     *
     * @param array $collection
     * @param array $single
     */
    public static function defineProjectTaskRoutes($collection, $single)
    {
        Router::mapResource('tasks', ['module' => self::NAME, 'collection_path' => "$single[path]/tasks", 'collection_requirements' => $collection['requirements'], 'collection_actions' => ['GET' => 'index', 'POST' => 'add', 'PUT' => 'batch_update']], function ($collection, $single) {
            Router::map("$collection[name]_archive", "$collection[path]/archive", ['action' => ['GET' => 'archive'], 'controller' => 'tasks', 'module' => TasksModule::NAME], $collection['requirements']);
            Router::map("$collection[name]_reorder", "$collection[path]/reorder", ['action' => ['PUT' => 'reorder'], 'controller' => 'tasks', 'module' => TasksModule::NAME], $collection['requirements']);

            Router::map("$single[name]_time_records", "$single[path]/time-records", ['action' => ['GET' => 'time_records'], 'controller' => 'tasks', 'module' => TasksModule::NAME], $single['requirements']);
            Router::map("$single[name]_expenses", "$single[path]/expenses", ['action' => ['GET' => 'expenses'], 'controller' => 'tasks', 'module' => TasksModule::NAME], $single['requirements']);

            Router::mapResource('subtasks', ['module' => TasksModule::NAME, 'collection_path' => "$single[path]/subtasks", 'collection_requirements' => $collection['requirements']], function ($collection, $single) {
                Router::map("$collection[name]_reorder", "$collection[path]/reorder", ['action' => ['PUT' => 'reorder'], 'controller' => 'subtasks', 'module' => TasksModule::NAME], $collection['requirements']);
                Router::map("$single[name]_promote_to_task", "$single[path]/promote-to-task", ['action' => ['POST' => 'promote_to_task'], 'controller' => 'subtasks', 'module' => TasksModule::NAME], $single['requirements']);
            });

            Router::map("$single[name]_move_to_project", "$single[path]/move-to-project", ['action' => ['PUT' => 'move_to_project'], 'controller' => 'tasks', 'module' => TasksModule::NAME], $single['requirements']);
            Router::map("$single[name]_duplicate", "$single[path]/duplicate", ['action' => ['POST' => 'duplicate'], 'controller' => 'tasks', 'module' => TasksModule::NAME], $single['requirements']);
        });
    }

    /**
     * Define project recurring task routes.
     *
     * @param array $collection
     * @param array $single
     */
    public static function defineProjectRecurringTaskRoutes($collection, $single)
    {
        Router::mapResource('recurring_tasks', ['module' => self::NAME, 'collection_path' => "$single[path]/recurring-tasks", 'collection_requirements' => $collection['requirements'], 'collection_actions' => ['GET' => 'index', 'POST' => 'add']], function ($collection, $single) {
            Router::map("$single[name]_create_task", "$single[path]/create-task", ['action' => ['POST' => 'create_task'], 'controller' => 'recurring_tasks', 'module' => TasksModule::NAME], $single['requirements']);
        });
    }

    // ---------------------------------------------------
    //  Events and Routes
    // ---------------------------------------------------

    /**
     * Initialize module.
     */
    public function init()
    {
        parent::init();

        DataObjectPool::registerTypeLoader('TaskList', function ($ids) {
            return TaskLists::findByIds($ids);
        });

        DataObjectPool::registerTypeLoader('Task', function ($ids) {
            return Tasks::findByIds($ids);
        });

        DataObjectPool::registerTypeLoader('Subtask', function ($ids) {
            return Subtasks::findByIds($ids);
        });

        DataObjectPool::registerTypeLoader('RecurringTask', function ($ids) {
            return RecurringTasks::findByIds($ids);
        });
    }

    /**
     * Define module classes.
     */
    public function defineClasses()
    {
        require_once __DIR__ . '/resources/autoload_model.php';

        AngieApplication::setForAutoload([
            'ProjectTasksCollection' => __DIR__ . '/models/ProjectTasksCollection.php',
            'ProjectRecurringTasksCollection' => __DIR__ . '/models/ProjectRecurringTasksCollection.php',

            'TaskLabelInterface' => __DIR__ . '/models/TaskLabelInterface.php',
            'TaskLabel' => __DIR__ . '/models/TaskLabel.php',

            'AssignmentsCollection' => __DIR__ . '/models/assignment_collections/AssignmentsCollection.class.php',
            'OpenAssignmentsForAssigneeCollection' => __DIR__ . '/models/assignment_collections/OpenAssignmentsForAssigneeCollection.class.php',
            'OpenAssignmentsForTeamCollection' => __DIR__ . '/models/assignment_collections/OpenAssignmentsForTeamCollection.class.php',
            'AssignmentsAsCalendarEventsCollection' => __DIR__ . '/models/assignment_collections/AssignmentsAsCalendarEventsCollection.class.php',

            'NewTaskNotification' => __DIR__ . '/notifications/NewTaskNotification.class.php',
            'TaskReassignedNotification' => __DIR__ . '/notifications/TaskReassignedNotification.class.php',

            'BaseSubtaskNotification' => __DIR__ . '/notifications/BaseSubtaskNotification.class.php',
            'NewSubtaskNotification' => __DIR__ . '/notifications/NewSubtaskNotification.class.php',

            // ---------------------------------------------------
            //  Subtasks
            // ---------------------------------------------------

            'SubtaskActivityLog' => __DIR__ . '/models/activity_logs/SubtaskActivityLog.class.php',
            'SubtaskCreatedActivityLog' => __DIR__ . '/models/activity_logs/SubtaskCreatedActivityLog.class.php',
            'SubtaskUpdatedActivityLog' => __DIR__ . '/models/activity_logs/SubtaskUpdatedActivityLog.class.php',

            // Notifications
            'SubtaskReassignedNotification' => __DIR__ . '/notifications/SubtaskReassignedNotification.class.php',

            // Calendar Event Context
            'ISubtaskCalendarEventContextImplementation' => __DIR__ . '/models/ISubtaskCalendarEventContextImplementation.class.php',

            'ISubtasks' => __DIR__ . '/models/ISubtasks.class.php',
            'ISubtasksImplementation' => __DIR__ . '/models/ISubtasksImplementation.class.php',

            'TaskSearchDocument' => __DIR__ . '/models/TaskSearchDocument.php',
        ]);
    }

    /**
     * Define module routes.
     */
    public function defineRoutes()
    {
        Router::map(
            'team_tasks',
            'teams/:team_id/tasks',
            [
                'controller' => 'team_tasks',
                'action' => [
                    'GET' => 'index',
                ],
            ],
            [
                'team_id' => Router::MATCH_ID,
            ]
        );
        Router::map(
            'user_tasks',
            'users/:user_id/tasks',
            [
                'controller' => 'user_tasks',
                'action' => [
                    'GET' => 'index',
                ],
            ],
            [
                'user_id' => Router::MATCH_ID,
            ]
        );

        Router::map(
            'unscheduled_task_counts',
            'reports/unscheduled-tasks/count-by-project',
            [
                'controller' => 'unscheduled_tasks',
                'action' => [
                    'GET' => 'count_by_project',
                ],
            ]
        );
    }

    /**
     * Define event handlers.
     */
    public function defineHandlers()
    {
        $this->listen('on_daily_maintenance');
        $this->listen('on_all_indices');
        $this->listen('on_rebuild_activity_logs');
        $this->listen('on_rebuild_all_indices');
        $this->listen('on_object_from_notification_context');
        $this->listen('on_history_field_renderers');

        $this->listen('on_trash_sections');
        $this->listen('on_user_access_search_filter');

        $this->listen('on_notification_inspector');

        $this->listen('on_protected_config_options');
        $this->listen('on_initial_settings');
        $this->listen('on_resets_initial_settings_timestamp');

        $this->listen('on_reset_manager_states');
        $this->listen('on_email_received');
        $this->listen('on_task_updated');
        $this->listen('on_extra_stats');
    }
}
