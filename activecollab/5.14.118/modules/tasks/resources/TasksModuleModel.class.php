<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

require_once APPLICATION_PATH . '/resources/ActiveCollabModuleModel.class.php';

/**
 * Tasks module model.
 *
 * @package ActiveCollab.modules.tasks
 * @subpackage models
 */
class TasksModuleModel extends ActiveCollabModuleModel
{
    /**
     * Construct tasks module model.
     *
     * @param TasksModule $parent
     */
    public function __construct(TasksModule $parent)
    {
        parent::__construct($parent);

        $this->addModel(DB::createTable('task_lists')->addColumns([
            new DBIdColumn(),
            DBIntegerColumn::create('project_id', 10, 0)->setUnsigned(true),
            DBNameColumn::create(150),
            DBDateColumn::create('start_on'),
            DBDateColumn::create('due_on'),
            DBActionOnByColumn::create('completed', true),
            new DBCreatedOnByColumn(true, true),
            new DBUpdatedOnByColumn(),
            DBTrashColumn::create(true),
            DBIntegerColumn::create('position', 10, 0)->setUnsigned(true),
        ])->addIndices([
            DBIndex::create('project_id'),
            DBIndex::create('span', DBIndex::KEY, ['start_on', 'due_on']),
            DBIndex::create('due_on'),
        ]))->setOrderBy('position')
            ->implementHistory()
            ->implementTrash()
            ->implementComplete()
            ->implementSearch()
            ->implementActivityLog()
            ->addModelTrait('IProjectElement', 'IProjectElementImplementation')
            ->addModelTrait('IInvoiceBasedOn', 'IInvoiceBasedOnTrackedDataImplementation')
            ->addModelTraitTweak('IProjectElementImplementation::whatIsWorthRemembering insteadof IActivityLogImplementation');

        $this->addModel(DB::createTable('tasks')->addColumns([
            new DBIdColumn(),
            DBFkColumn::create('project_id', 0, true),
            DBIntegerColumn::create('task_number', 10, 0)->setUnsigned(true),
            DBFkColumn::create('task_list_id', 0, true),
            DBFkColumn::create('assignee_id', 0, true),
            DBFkColumn::create('delegated_by_id', 0, true),
            DBFkColumn::create('created_from_recurring_task_id', 0, true),
            DBNameColumn::create(150),
            DBBodyColumn::create(),
            DBBoolColumn::create('is_important'),
            new DBCreatedOnByColumn(true, true),
            new DBUpdatedOnByColumn(),
            DBDateColumn::create('start_on'),
            DBDateColumn::create('due_on'),
            DBFkColumn::create('job_type_id')->setSize(DBColumn::SMALL),
            DBDecimalColumn::create('estimate', 12, 2, 0)->setUnsigned(true),
            DBActionOnByColumn::create('completed'),
            DBIntegerColumn::create('position', 10, 0)->setUnsigned(true),
            DBBoolColumn::create('is_hidden_from_clients'),
            DBTrashColumn::create(true),
            DBStringColumn::create('fake_assignee_name'),
            DBStringColumn::create('fake_assignee_email'),
        ])->addIndices([
            DBIndex::create('project_task_number', DBIndex::UNIQUE, ['project_id', 'task_number']),
            DBIndex::create('task_number'),
            DBIndex::create('start_on'),
            DBIndex::create('due_on'),
        ]))->implementAssignees()
            ->implementComplete()
            ->implementHistory()
            ->implementAccessLog()
            ->implementComments(true, true)
            ->implementAttachments()
            ->implementLabels()
            ->implementTrash()
            ->implementSearch()
            ->implementActivityLog()
            ->implementReminders()
            ->addModelTrait('IHiddenFromClients')
            ->addModelTrait('IProjectElement', 'IProjectElementImplementation')
            ->addModelTrait('ITracking', 'ITrackingImplementation')
            ->addModelTrait('IInvoiceBasedOn', 'IInvoiceBasedOnTrackedDataImplementation')
            ->addModelTraitTweak('IProjectElementImplementation::canViewAccessLogs insteadof IAccessLogImplementation')
            ->addModelTraitTweak('IProjectElementImplementation::whatIsWorthRemembering insteadof IActivityLogImplementation');

        $this->addModel(DB::createTable('subtasks')->addColumns([
            new DBIdColumn(),
            DBIntegerColumn::create('task_id', 10, 0)->setUnsigned(true),
            DBIntegerColumn::create('assignee_id', 10, 0)->setUnsigned(true),
            DBIntegerColumn::create('delegated_by_id', 10, 0)->setUnsigned(true),
            DBTextColumn::create('body')->setSize(DBTextColumn::BIG),
            DBDateColumn::create('due_on'),
            new DBCreatedOnByColumn(true),
            new DBUpdatedOnColumn(),
            DBActionOnByColumn::create('completed', true),
            DBIntegerColumn::create('position', 10, '0')->setUnsigned(true),
            DBTrashColumn::create(true),
            DBStringColumn::create('fake_assignee_name'),
            DBStringColumn::create('fake_assignee_email'),
        ])->addIndices([
            DBIndex::create('task_id'),
            DBIndex::create('created_on'),
            DBIndex::create('position'),
            DBIndex::create('completed_on'),
            DBIndex::create('due_on'),
            DBIndex::create('assignee_id'),
            DBIndex::create('delegated_by_id'),
        ]))->implementAssignees()
            ->implementComplete()
            ->implementHistory()
            ->implementTrash()
            ->implementActivityLog()
            ->setOrderBy('ISNULL(position) ASC, position, created_on');

        $this->addModel(DB::createTable('recurring_tasks')->addColumns([
            new DBIdColumn(),
            DBFkColumn::create('project_id', 0, true),
            DBFkColumn::create('task_list_id', 0, true),
            DBFkColumn::create('assignee_id', 0, true),
            DBFkColumn::create('delegated_by_id', 0, true),
            DBNameColumn::create(150),
            DBBodyColumn::create(),
            DBBoolColumn::create('is_important'),
            new DBCreatedOnByColumn(true, true),
            new DBUpdatedOnByColumn(),
            DBIntegerColumn::create('start_in')->setUnsigned(true),
            DBIntegerColumn::create('due_in')->setUnsigned(true),
            DBFkColumn::create('job_type_id')->setSize(DBColumn::SMALL),
            DBDecimalColumn::create('estimate', 12, 2, 0)->setUnsigned(true),
            DBIntegerColumn::create('position', 10, 0)->setUnsigned(true),
            DBBoolColumn::create('is_hidden_from_clients'),
            DBTrashColumn::create(true),
            DBEnumColumn::create('repeat_frequency', ['never', 'daily', 'weekly', 'monthly'], 'never'),
            DBIntegerColumn::create('repeat_amount', 10, 0)->setUnsigned(true),
            DBIntegerColumn::create('triggered_number', 10, 0)->setUnsigned(true),
            DBDateColumn::create('last_trigger_on'),
            DBStringColumn::create('fake_assignee_name'),
            DBStringColumn::create('fake_assignee_email'),
            new DBAdditionalPropertiesColumn(),
        ]))->implementAssignees()
            ->implementHistory()
            ->implementAccessLog()
            ->implementSubscriptions()
            ->implementAttachments()
            ->implementLabels()
            ->implementSearch()
            ->implementTrash()
            ->implementActivityLog()
            ->addModelTrait(IHiddenFromClients::class)
            ->addModelTrait(ISubtasks::class, ISubtasksImplementation::class)
            ->addModelTrait(IProjectElement::class, IProjectElementImplementation::class)
            ->addModelTraitTweak('IProjectElementImplementation::canViewAccessLogs insteadof IAccessLogImplementation')
            ->addModelTraitTweak('IProjectElementImplementation::whatIsWorthRemembering insteadof IActivityLogImplementation');

        $this->addTable(DB::createTable('custom_hourly_rates')->addColumns([
            new DBParentColumn(true, false),
            DBIntegerColumn::create('job_type_id', DBColumn::NORMAL, 0)->setUnsigned(true),
            (new DBMoneyColumn('hourly_rate', 0))
                ->setUnsigned(true),
        ])->addIndices([
            new DBIndexPrimary(['parent_type', 'parent_id', 'job_type_id']),
        ]));

        // Add is_global field to labels model
        AngieApplicationModel::getTable('labels')->addColumn(DBBoolColumn::create('is_global'), 'is_default');
    }

    /**
     * Load initial framework data.
     */
    public function loadInitialData()
    {
        $this->addConfigOption('task_options', []);
        $this->addConfigOption('show_project_id', false);
        $this->addConfigOption('show_task_id', false);
        $this->addConfigOption('task_estimates_enabled', false);
        $this->addConfigOption('display_mode_project_tasks', 'list');

        DB::execute("ALTER TABLE tasks ADD created_from_discussion_id INT UNSIGNED NOT NULL DEFAULT '0'");

        $labels = [
            ['NEW', '#00B25C'], // Yellow
            ['CONFIRMED', '#F26522'], // Orange
            ['WORKS FOR ME', '#00B25C'], // Green
            ['DUPLICATE', '#00B25C'], // Green
            ['WONT FIX', '#00B25C'], // Green
            ['ASSIGNED', '#FF0000'], // Red
            ['BLOCKED', '#ACACAC'], // Grey
            ['IN PROGRESS', '#00B25C'], // Green
            ['FIXED', '#0000FF'], // BlueU
            ['REOPENED', '#FF0000'], // Red
            ['VERIFIED', '#00B25C'], // Green
        ];

        $counter = 1;
        $to_insert = [];

        foreach ($labels as $label) {
            $to_insert[] = DB::prepare("('TaskLabel', ?, ?, ?, ?)", $label[0], $label[1], true, $counter++);
        }

        DB::execute('INSERT INTO labels (type, name, color, is_global, position) VALUES ' . implode(', ', $to_insert));

        parent::loadInitialData();
    }
}
