<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * @package ActiveCollab.modules.tasks
 * @subpackage resources
 */
AngieApplication::useModel(['task_lists', 'tasks', 'subtasks', 'recurring_tasks'], 'tasks');
