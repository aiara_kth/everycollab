<?php

declare(strict_types=1);

namespace ActiveCollab\Module\Tasks\Events\DataObjectLifeCycleEvents\TaskEvents;

interface TaskListChangedEventInterface extends TaskUpdatedEventInterface
{
}