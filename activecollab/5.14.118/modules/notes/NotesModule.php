<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Notes module definition.
 *
 * @package ActiveCollab.modules.notes
 * @subpackage models
 */
class NotesModule extends AngieModule
{
    const NAME = 'notes';

    /**
     * Plain module name.
     *
     * @var string
     */
    protected $name = 'notes';

    /**
     * Module version.
     *
     * @var string
     */
    protected $version = '5.0';

    /**
     * Define project task routes.
     *
     * @param array $collection
     * @param array $single
     */
    public static function defineProjectNoteRoutes($collection, $single)
    {
        Router::mapResource('notes', ['module' => self::NAME, 'collection_path' => "$single[path]/notes", 'collection_requirements' => $collection['requirements']], function ($collection, $single) {
            Router::map("$collection[name]_reorder", "$collection[path]/reorder", ['action' => ['PUT' => 'reorder'], 'controller' => 'notes', 'module' => NotesModule::NAME], $single['requirements']);
            Router::map("$single[name]_move_to_group", "$single[path]/move-to-group", ['action' => ['PUT' => 'move_to_group'], 'controller' => 'notes', 'module' => NotesModule::NAME], $single['requirements']);
            Router::map("$single[name]_move_to_project", "$single[path]/move-to-project", ['action' => ['PUT' => 'move_to_project'], 'controller' => 'notes', 'module' => NotesModule::NAME], $single['requirements']);
            Router::map("$single[name]_versions", "$single[path]/versions", ['action' => ['GET' => 'versions'], 'controller' => 'notes', 'module' => NotesModule::NAME], $single['requirements']);
        });

        Router::mapResource('note_groups', ['module' => self::NAME, 'collection_path' => "$single[path]/note-groups", 'collection_requirements' => $collection['requirements']], function ($collection, $single) {
            Router::map("$single[name]_notes", "$single[path]/notes", ['controller' => 'note_groups', 'action' => ['GET' => 'notes'], 'module' => NotesModule::NAME], $single['requirements']);
            Router::map("$single[name]_reorder_notes", "$single[path]/reorder-notes", ['controller' => 'note_groups', 'action' => ['PUT' => 'reorder_notes'], 'module' => NotesModule::NAME], $single['requirements']);
            Router::map("$single[name]_move_to_group", "$single[path]/move-to-group", ['controller' => 'note_groups', 'action' => ['PUT' => 'move_to_group'], 'module' => NotesModule::NAME], $single['requirements']);
        });
    }

    /**
     * Initialize module.
     */
    public function init()
    {
        parent::init();

        DataObjectPool::registerTypeLoader('NoteGroup', function ($ids) {
            return NoteGroups::findByIds($ids);
        });

        DataObjectPool::registerTypeLoader('Note', function ($ids) {
            return Notes::findByIds($ids);
        });
    }

    /**
     * Define module classes.
     */
    public function defineClasses()
    {
        require_once __DIR__ . '/resources/autoload_model.php';

        AngieApplication::setForAutoload([
            'NewNoteNotification' => __DIR__ . '/notifications/NewNoteNotification.class.php',
            'NewNoteVersionNotification' => __DIR__ . '/notifications/NewNoteVersionNotification.class.php',
        ]);
    }

    /**
     * Define event handlers.
     */
    public function defineHandlers()
    {
        $this->listen('on_rebuild_activity_logs');
        $this->listen('on_trash_sections');
        $this->listen('on_reset_manager_states');
        $this->listen('on_note_created');
    }
}
