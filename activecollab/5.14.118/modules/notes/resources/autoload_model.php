<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * @package ActiveCollab.modules.notes
 * @subpackage resources
 */
AngieApplication::useModel(['note_groups', 'notes'], 'notes');
