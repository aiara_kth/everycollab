<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Discussion class.
 *
 * @package ActiveCollab.modules.discussions
 * @subpackage models
 */
class Discussion extends BaseDiscussion implements IRoutingContext
{
    use IRoutingContextImplementation;

    /**
     * {@inheritdoc}
     */
    public function getRoutingContext()
    {
        return 'discussion';
    }

    /**
     * {@inheritdoc}
     */
    public function getRoutingContextParams()
    {
        return [
            'project_id' => $this->getProjectId(),
            'discussion_id' => $this->getId(),
        ];
    }

    /**
     * Return history field renderers.
     *
     * @return array
     */
    public function getHistoryFieldRenderers()
    {
        $renderers = parent::getHistoryFieldRenderers();

        $renderers['is_hidden_from_clients'] = function ($old_value, $new_value, Language $language) {
            if ($new_value) {
                return lang('Marked as hidden from clients', null, true, $language);
            } else {
                return lang('No longer hidden from clients', null, true, $language);
            }
        };

        return $renderers;
    }

    /**
     * {@inheritdoc}
     */
    public function getSearchDocument()
    {
        return new ProjectElementSearchDocument($this);
    }

    /**
     * {@inheritdoc}
     */
    public function validate(ValidationErrors &$errors)
    {
        $this->validatePresenceOf('name') or $errors->addError('Summary is required', 'name');

        parent::validate($errors);
    }
}
