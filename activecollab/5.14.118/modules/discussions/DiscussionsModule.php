<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Discussions module definition.
 *
 * @package activeCollab.modules.discussions
 * @subpackage models
 */
class DiscussionsModule extends AngieModule
{
    const NAME = 'discussions';

    /**
     * Plain module name.
     *
     * @var string
     */
    protected $name = 'discussions';

    /**
     * Module version.
     *
     * @var string
     */
    protected $version = '5.0';

    /**
     * Define project task routes.
     *
     * @param array $collection
     * @param array $single
     */
    public static function defineProjectDiscussionRoutes($collection, $single)
    {
        Router::mapResource('discussions', ['module' => self::NAME, 'collection_path' => "$single[path]/discussions", 'collection_requirements' => $collection['requirements']], function ($collection, $single) {
            Router::map("$collection[name]_read_status", "$collection[path]/read-status", ['action' => ['GET' => 'read_status'], 'controller' => 'discussions', 'module' => DiscussionsModule::NAME], $collection['requirements']);
            Router::map("$single[name]_move_to_project", "$single[path]/move-to-project", ['action' => ['PUT' => 'move_to_project'], 'controller' => 'discussions', 'module' => DiscussionsModule::NAME], $single['requirements']);
            Router::map("$single[name]_promote_to_task", "$single[path]/promote-to-task", ['action' => ['POST' => 'promote_to_task'], 'controller' => 'discussions', 'module' => DiscussionsModule::NAME], $single['requirements']);
        });
    }

    /**
     * Initialize module.
     */
    public function init()
    {
        parent::init();

        DataObjectPool::registerTypeLoader('Discussion', function ($ids) {
            return Discussions::findByIds($ids);
        });
    }

    /**
     * Define module classes.
     */
    public function defineClasses()
    {
        require_once __DIR__ . '/resources/autoload_model.php';

        AngieApplication::setForAutoload([
            'ProjectDiscussionsCollection' => __DIR__ . '/models/ProjectDiscussionsCollection.php',
            'NewDiscussionNotification' => __DIR__ . '/notifications/NewDiscussionNotification.class.php',
        ]);
    }

    /**
     * Define event handlers.
     */
    public function defineHandlers()
    {
        $this->listen('on_rebuild_activity_logs');
        $this->listen('on_object_from_notification_context');
        $this->listen('on_trash_sections');
        $this->listen('on_reset_manager_states');
        $this->listen('on_discussion_created');
    }
}
