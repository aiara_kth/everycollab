<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * on_trash_sections event handler.
 *
 * @package ActiveCollab.modules.discussions
 * @subpackage handlers
 */

/**
 * Handle on_trash_sections event.
 *
 * @param \Angie\Trash\Sections $sections
 * @param User                  $user
 */
function discussions_handle_on_trash_sections(\Angie\Trash\Sections &$sections, User $user)
{
    if ($user->isOwner()) {
        $id_name_map = DB::executeIdNameMap(
            'SELECT d.id, d.name FROM discussions AS d
                INNER JOIN projects AS p ON p.id = d.project_id AND p.is_trashed = ?
                WHERE d.is_trashed = ?
                ORDER BY d.trashed_on DESC',
            false,
            true
        );
    } elseif ($user->isMember() && $project_ids = $user->getProjectIds()) {
        $id_name_map = DB::executeIdNameMap(
            'SELECT d.id, d.name FROM discussions AS d
                INNER JOIN projects AS p ON p.id = d.project_id AND p.is_trashed = ?
                WHERE d.project_id IN (?) AND d.trashed_by_id = ? AND d.is_trashed = ?
                ORDER BY d.trashed_on DESC',
            false,
            $project_ids,
            $user->getId(),
            true
        );
    }

    if (!empty($id_name_map)) {
        $sections->registerTrashedObjects('Discussion', $id_name_map);
    }
}
