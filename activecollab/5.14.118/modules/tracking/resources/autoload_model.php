<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * @package ActiveCollab.modules.tracking
 * @subpackage resources
 */
AngieApplication::useModel(['time_records', 'job_types', 'expenses', 'expense_categories'], 'tracking');
