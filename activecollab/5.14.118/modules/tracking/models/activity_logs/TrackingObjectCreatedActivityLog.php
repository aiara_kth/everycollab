<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Tracking object created activity log.
 *
 * @package ActiveCollab.modules.tasks
 * @subpackage models
 */
class TrackingObjectCreatedActivityLog extends InstanceCreatedActivityLog
{
    use ITrackingObjectActivityLog;
}
