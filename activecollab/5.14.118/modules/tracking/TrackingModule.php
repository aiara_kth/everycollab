<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Time and expense tracking module definition.
 *
 * @package ActiveCollab.modules.tracking
 * @subpackage models
 */
class TrackingModule extends AngieModule
{
    const NAME = 'tracking';

    /**
     * Plain module name.
     *
     * @var string
     */
    protected $name = 'tracking';

    /**
     * Module version.
     *
     * @var string
     */
    protected $version = '5.0';

    /**
     * Define project time records routes.
     *
     * @param array $collection
     * @param array $single
     */
    public static function defineProjectTimeRecordRoutes($collection, $single)
    {
        Router::mapResource('time_records', ['module' => self::NAME, 'collection_path' => "$single[path]/time-records", 'collection_requirements' => $collection['requirements']], function ($collection, $single) {
            Router::map("$collection[name]_filtered_by_date", "$collection[path]/filtered-by-date", ['module' => $collection['module'], 'controller' => $collection['controller'], 'action' => ['GET' => 'filtered_by_date']], $collection['requirements']);
            Router::map("$single[name]_move", "$single[path]/move", ['module' => $single['module'], 'controller' => $single['controller'], 'action' => ['PUT' => 'move']], $single['requirements']);
        });
    }

    /**
     * Define project expense routes.
     *
     * @param array $collection
     * @param array $single
     */
    public static function defineProjectExpenseRoutes($collection, $single)
    {
        Router::mapResource('expenses', ['module' => self::NAME, 'collection_path' => "$single[path]/expenses", 'collection_requirements' => $collection['requirements']], function ($collection, $single) {
            Router::map("$single[name]_move", "$single[path]/move", ['module' => $single['module'], 'controller' => $single['controller'], 'action' => ['PUT' => 'move']], $single['requirements']);
        });
    }

    // ---------------------------------------------------
    //  Events and Routes
    // ---------------------------------------------------

    /**
     * Initialize module.
     */
    public function init()
    {
        parent::init();

        DataObjectPool::registerTypeLoader('TimeRecord', function ($ids) {
            return TimeRecords::findByIds($ids);
        });

        DataObjectPool::registerTypeLoader('JobType', function ($ids) {
            return JobTypes::findByIds($ids);
        });

        DataObjectPool::registerTypeLoader('Expense', function ($ids) {
            return Expenses::findByIds($ids);
        });

        DataObjectPool::registerTypeLoader('ExpenseCategory', function ($ids) {
            return ExpenseCategories::findByIds($ids);
        });

        DataObjectPool::registerTypeLoader('Integration', function ($ids) {
            return Integrations::findByIds($ids);
        });
    }

    /**
     * Define module classes.
     */
    public function defineClasses()
    {
        require_once __DIR__ . '/resources/autoload_model.php';

        AngieApplication::setForAutoload([
            'TrackingObjects' => __DIR__ . '/models/tracking_objects/TrackingObjects.class.php',

            'ITracking' => __DIR__ . '/models/ITracking.php',
            'ITrackingImplementation' => __DIR__ . '/models/ITrackingImplementation.php',

            'ITrackingObject' => __DIR__ . '/models/tracking_objects/ITrackingObject.php',
            'ITrackingObjectImplementation' => __DIR__ . '/models/tracking_objects/ITrackingObjectImplementation.php',
            'ITrackingObjectsImplementation' => __DIR__ . '/models/tracking_objects/ITrackingObjectsImplementation.php',

            'TrackingFilter' => __DIR__ . '/models/reports/TrackingFilter.php',

            'ITrackingObjectActivityLog' => __DIR__ . '/models/activity_logs/ITrackingObjectActivityLog.php',
            'TrackingObjectCreatedActivityLog' => __DIR__ . '/models/activity_logs/TrackingObjectCreatedActivityLog.php',
            'TrackingObjectUpdatedActivityLog' => __DIR__ . '/models/activity_logs/TrackingObjectUpdatedActivityLog.php',

            'TimeRecordsCollection' => __DIR__ . '/models/time_record_collections/TimeRecordsCollection.php',
            'ProjectTimeRecordsCollection' => __DIR__ . '/models/time_record_collections/ProjectTimeRecordsCollection.php',
            'TaskTimeRecordsCollection' => __DIR__ . '/models/time_record_collections/TaskTimeRecordsCollection.php',
            'UserTimeRecordsCollection' => __DIR__ . '/models/time_record_collections/UserTimeRecordsCollection.php',

            'ExpensesCollection' => __DIR__ . '/models/expense_collections/ExpensesCollection.php',
            'ProjectExpensesCollection' => __DIR__ . '/models/expense_collections/ProjectExpensesCollection.php',
            'TaskExpensesCollection' => __DIR__ . '/models/expense_collections/TaskExpensesCollection.php',

            'TimerIntegration' => __DIR__ . '/models/integrations/TimerIntegration.php',
        ]);
    }

    /**
     * Define module routes.
     */
    public function defineRoutes()
    {
        Router::map('user_time_records', 'users/:user_id/time-records', ['controller' => 'user_time_records', 'action' => ['GET' => 'index']], ['user_id' => Router::MATCH_ID]);
        Router::map('user_time_records_filtered_by_date', 'users/:user_id/time-records/filtered-by-date', ['controller' => 'user_time_records', 'action' => ['GET' => 'filtered_by_date']], ['user_id' => Router::MATCH_ID]);

        Router::mapResource('job_types', null, function ($collection) {
            Router::map("$collection[name]_default", "$collection[path]/default", ['controller' => $collection['controller'], 'action' => ['GET' => 'view_default', 'PUT' => 'set_default']], $collection['requirements']);
            Router::map("$collection[name]_batch_edit", "$collection[path]/edit-batch", ['controller' => $collection['controller'], 'action' => ['PUT' => 'batch_edit']], $collection['requirements']);
        });

        Router::mapResource('expense_categories', null, function ($collection) {
            Router::map("$collection[name]_default", "$collection[path]/default", ['controller' => $collection['controller'], 'action' => ['GET' => 'view_default', 'PUT' => 'set_default']], $collection['requirements']);
            Router::map("$collection[name]_batch_edit", "$collection[path]/edit-batch", ['controller' => $collection['controller'], 'action' => ['PUT' => 'batch_edit']], $collection['requirements']);
        });
    }

    /**
     * Define event handlers.
     */
    public function defineHandlers()
    {
        $this->listen('on_rebuild_activity_logs');
        $this->listen('on_trash_sections');
        $this->listen('on_initial_settings');
        $this->listen('on_resets_initial_settings_timestamp');
        $this->listen('on_initial_collections');
        $this->listen('on_initial_user_collections');
        $this->listen('on_protected_config_options');
        $this->listen('on_available_integrations');
        $this->listen('on_visible_object_paths');
        $this->listen('on_time_record_created');
        $this->listen('on_expense_created');
        $this->listen('on_extra_stats');
    }
}
