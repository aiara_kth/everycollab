<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

require_once ANGIE_PATH . '/frameworks/attachments/proxies/FwForwardPreviewProxy.class.php';

/**
 * Forward preview proxy.
 *
 * @package ActiveCollab.modules.system
 * @subpackage proxies
 */
class ForwardPreviewProxy extends FwForwardPreviewProxy
{
    /*
    * @param  string      $context
    * @return string|null
    */
    protected function contextToTableName($context)
    {
        if ($context === 'files') {
            return 'files';
        } elseif ($context === 'project_template_elements') {
            return 'project_template_elements';
        }

        return parent::contextToTableName($context);
    }
}
