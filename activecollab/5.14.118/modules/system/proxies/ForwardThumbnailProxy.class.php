<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

require_once ANGIE_PATH . '/frameworks/attachments/proxies/FwForwardThumbnailProxy.class.php';

/**
 * Forward thumbnail proxy.
 *
 * @package ActiveCollab.modules.system
 * @subpackage proxies
 */
class ForwardThumbnailProxy extends FwForwardThumbnailProxy
{
}
