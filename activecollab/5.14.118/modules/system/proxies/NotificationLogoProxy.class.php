<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

require_once ANGIE_PATH . '/frameworks/notifications/proxies/FwNotificationLogoProxy.class.php';

/**
 * Notification logo proxy.
 *
 * @package ActiveCollab.modules.system
 * @subpackage proxies
 */
class NotificationLogoProxy extends FwNotificationLogoProxy
{
}
