<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Notify email sender notification.
 *
 * @package ActiveCollab.modules.system
 * @subpackage notifications
 */
class NotifyEmailSenderNotification extends FwNotifyEmailSenderNotification
{
}
