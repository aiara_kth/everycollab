<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Application level announcement notification implementation.
 *
 * @package ActiveCollab.modules.system
 * @subpackage notifications
 */
class NewAnnouncementNotification extends Notification
{
    /**
     * Return notification subject.
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->getAdditionalProperty('subject');
    }

    /**
     * Set notification subject.
     *
     * @param  string $value
     * @return $this
     */
    public function &setSubject($value)
    {
        $this->setAdditionalProperty('subject', $value);

        return $this;
    }

    /**
     * Return notification body.
     *
     * @return string
     */
    public function getAnnouncement()
    {
        return $this->getAdditionalProperty('announcement');
    }

    /**
     * Set notification body.
     *
     * @param  string $value
     * @return $this
     */
    public function &setAnnouncement($value)
    {
        $this->setAdditionalProperty('announcement', $value);

        return $this;
    }

    /**
     * Return recipient names.
     *
     * @return string
     */
    public function getRecipientNames()
    {
        return $this->getAdditionalProperty('recipient_names');
    }

    /**
     * Set recipient names.
     *
     * @param  string                      $value
     * @return NewAnnouncementNotification
     */
    public function &setRecipientNames($value)
    {
        $this->setAdditionalProperty('recipient_names', $value);

        return $this;
    }

    /**
     * Return additional template variables.
     *
     * @param  NotificationChannel $channel
     * @return array
     */
    public function getAdditionalTemplateVars(NotificationChannel $channel)
    {
        return ['subject' => $this->getSubject(), 'announcement' => $this->getAnnouncement(), 'recipient_names' => $this->getRecipientNames()];
    }

    /**
     * This notification should not be displayed in web interface.
     *
     * @param  NotificationChannel $channel
     * @param  IUser               $recipient
     * @return bool
     */
    public function isThisNotificationVisibleInChannel(NotificationChannel $channel, IUser $recipient)
    {
        if ($channel instanceof EmailNotificationChannel) {
            return true; // Always deliver this notification via email
        } elseif ($channel instanceof WebInterfaceNotificationChannel) {
            return false; // Never deliver this notification to web interface
        }

        return parent::isThisNotificationVisibleInChannel($channel, $recipient);
    }

    /**
     * Return true if sender should be ignored.
     *
     * @return bool
     */
    public function ignoreSender()
    {
        return false;
    }
}
