<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Framework level new calendar event notification.
 *
 * @package ActiveCollab.modules.system
 * @subpackage notifications
 */
class NewCalendarEventNotification extends FwNewCalendarEventNotification
{
}
