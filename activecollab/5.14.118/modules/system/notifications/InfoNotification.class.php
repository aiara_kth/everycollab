<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Info notification.
 *
 * @package ActiveCollab.modules.system
 * @subpackage notifications
 */
class InfoNotification extends FwInfoNotification
{
}
