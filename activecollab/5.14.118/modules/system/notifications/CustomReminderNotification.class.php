<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Application level custom reminder notification.
 *
 * @package angie.frameworks.reminders
 * @subpackage notification
 */
class CustomReminderNotification extends FwCustomReminderNotification
{
}
