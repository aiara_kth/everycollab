{$subject}
================================================================================
{notification_logo}

{if $payload}
    <p>Hi {$payload.name},</p>
    <p>We just wanted to let you know your free trial expires in {$payload.days} days. After that, you will be able to log in, but you will be required to subscribe to reactivate the account.</p>
    <p>If you want to keep using ActiveCollab without interuption, you can subscribe for a monthly or yearly plan. <a href="{$payload.link}">Click here</a> to get started now.</p>
    <p>We're here to help if you need us.</p>
    <p>ActiveCollab Team</p>
{/if}