{$subject}
================================================================================
{notification_logo}

{if $payload}
    <p>Hi {$payload.name},</p>
    <p>Thanks for trying ActiveCollab, we honestly hope you've enjoyed it! Your trial has expired, but you can still log in and activate the account by subscribing. <a href="{$payload.link}">Click here</a> to get started now.</p>
    <p>If you have any questions about your account, subscription or ActiveCollab in general, you can always reach us at support@activecollab.com or using a live chat from your account.</p>

    <p>ActiveCollab Team</p>
{/if}