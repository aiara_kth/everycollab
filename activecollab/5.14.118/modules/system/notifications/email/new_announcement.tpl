{if $subject}
  {$subject}
{else}
  {lang subject=$subject language=$language}Announcement{/lang}
{/if}
================================================================================
<!-- Message Body -->
<h1 style="font-size: 16px; font-weight: bold; margin-top: 20px; margin-bottom: 16px;">
  {lang sender=$sender->getDisplayName() language=$language}Announcement by :sender:{/lang} <span style="color: #FFCB00">&#9733;</span>
</h1>

<!-- Description -->
<p style="background: #FFF7C4; padding-top: 8px; padding-bottom: 8px; padding-left: 12px; padding-right: 12px; border-width: 1px; border-color: #E3DA9C; border-style: solid;">
  {$announcement|nl2br nofilter}
</p>

<!-- Metadata -->
<div class="metadata" style="color: #999999; font-size: 14px; line-height: 21px;">
  <p>{lang sender_name=$sender->getDisplayName() language=$language}Sent by :sender_name to{/lang} {$recipient_names|notification_recipients:$sender:'color: #999999; text-decoration: none;':$language nofilter}</p>
</div>