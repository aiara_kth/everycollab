{lang language=$language}Disk space limit exceeded{/lang}
================================================================================

<h1 style="font-size: 16px; font-weight: bold; margin-top: 20px; margin-bottom: 16px;">
    {lang language=$language}Disk space limit exceeded{/lang}
</h1>
<p>{lang language=$language disk_space_limit=$disk_space_limit}You've used up more storage than your subscription allows (:disk_space_limit). Please switch to a larger plan to be able to use recurring tasks with attachments.{/lang}</p>