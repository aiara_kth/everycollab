<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * System level payment received notification.
 *
 * @package ActiveCollab.modules.system
 * @subpackage notifications
 */
class PaymentReceivedNotification extends FwPaymentReceivedNotification
{
}
