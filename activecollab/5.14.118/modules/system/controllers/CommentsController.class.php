<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

AngieApplication::useController('fw_comments', CommentsFramework::NAME);

/**
 * Commnents controller delegate implementation.
 *
 * @package activeCollab.modules.system
 * @subpackage models
 */
final class CommentsController extends FwCommentsController
{
}
