<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

use Angie\Http\Request;

AngieApplication::useController('auth_required', ActivityLogsFramework::INJECT_INTO);

/**
 * Application level what's new controller.
 *
 * @package ActiveCollab.modules.system
 * @subpackage controllers
 */
class WhatsNewController extends AuthRequiredController
{
    /**
     * List what's new.
     *
     * @param  Request         $request
     * @param  User            $user
     * @return ModelCollection
     */
    public function index(Request $request, User $user)
    {
        return Users::prepareCollection('activity_logs_for_' . $user->getId() . '_page_' . $request->getPage(), $user);
    }

    /**
     * Return daily activiltiy logs that given user can see.
     *
     * @param  Request         $request
     * @param  User            $user
     * @return ModelCollection
     */
    public function daily(Request $request, User $user)
    {
        return Users::prepareCollection('daily_activity_logs_for_' . $user->getId() . '_' . $request->get('day') . '_page_' . $request->getPage(), $user);
    }
}
