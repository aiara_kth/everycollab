<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

AngieApplication::useController('fw_notifications', NotificationsFramework::NAME);

/**
 * Application level notifications controller.
 *
 * @package activeCollab.modules.system
 * @subpackage controllers
 */
class NotificationsController extends FwNotificationsController
{
}
