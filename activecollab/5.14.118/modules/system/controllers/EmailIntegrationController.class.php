<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

AngieApplication::useController('fw_email_integration', EmailFramework::NAME);

/**
 * Email integrations controller.
 *
 * @package ActiveCollab.modules.system
 * @subpackage controllers
 */
class EmailIntegrationController extends FwEmailIntegrationController
{
}
