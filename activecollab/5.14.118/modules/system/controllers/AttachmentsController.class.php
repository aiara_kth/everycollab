<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

AngieApplication::useController('fw_attachments', AttachmentsFramework::NAME);

/**
 * Attachments controller.
 *
 * @package activeCollab.modules.resources
 * @subpackage controllers
 */
class AttachmentsController extends FwAttachmentsController
{
}
