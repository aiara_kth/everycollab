<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

use ActiveCollab\Authentication\Adapter\AdapterInterface;
use ActiveCollab\Authentication\Adapter\BrowserSessionAdapter;
use ActiveCollab\Authentication\Adapter\TokenBearerAdapter;
use ActiveCollab\Authentication\AuthenticationResult\AuthenticationResultInterface;
use ActiveCollab\Authentication\AuthenticationResult\Transport\TransportInterface;
use ActiveCollab\Authentication\Authorizer\ExceptionAware\ExceptionAwareInterface as ExceptionAwareAuthorizerInterface;
use ActiveCollab\Authentication\Authorizer\RequestAware\RequestAwareInterface as RequestAwareAuthorizerInterface;
use Angie\Http\Request;
use Angie\Http\Response;
use Angie\Http\Response\StatusResponse\StatusResponse;
use Angie\Http\Response\StatusResponse\StatusResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

AngieApplication::useController('auth_not_required', SystemModule::NAME);

/**
 * Application level user session controller.
 *
 * @package activeCollab.modules.system
 * @subpackage controllers
 */
class UserSessionController extends AuthNotRequiredController
{
    /**
     * @param  Request                       $request
     * @param  User                          $user
     * @return array|StatusResponseInterface
     */
    public function who_am_i(Request $request, User $user = null)
    {
        if ($user instanceof User) {
            return Users::prepareCollection('initial_for_logged_user', $user);
        } else {
            return $this->requireAuthorization();
        }
    }

    /**
     * Log user in based on provided data.
     *
     * @param  Request                                        $request
     * @return TransportInterface|StatusResponseInterface|int
     * @throws Exception
     */
    public function login(Request $request)
    {
        if ($this->shouldBlockAuthorizationRequest($request, 'login')) {
            return Response::FORBIDDEN;
        }

        $authorizer = AngieApplication::authentication()->getAuthorizer();

        $credentials = $request->post();
        $payload = null;

        try {
            if ($authorizer instanceof RequestAwareAuthorizerInterface && $authorizer->getRequestProcessor()) {
                $request_processing_result = $authorizer->getRequestProcessor()->processRequest($request);

                $credentials = $request_processing_result->getCredentials();
                $payload = $request_processing_result->getDefaultPayload();
            }

            /** @var \ActiveCollab\Authentication\AuthenticationResult\Transport\Authentication\AuthenticationTransportInterface $auth */
            $auth = AngieApplication::authentication()->authorizeForAdapter($credentials, BrowserSessionAdapter::class, $payload);

            /** @var User $user */
            $user = $auth->getAuthenticatedUser();

            if ($user instanceof User && !$user->getFirstLoginOn() instanceof DateTimeValue) {
                $user->setFirstLoginOn(DateTimeValue::now());
                $user->save();
            }

            return $auth;
        } catch (Exception $e) {
            $this->logFailedAuthorization($request, 'login', $credentials, $e);

            if ($authorizer instanceof ExceptionAwareAuthorizerInterface) {
                $exception_handling_result = $authorizer->handleException($credentials, $e);

                if ($exception_handling_result !== null) {
                    return $exception_handling_result;
                }
            }

            throw($e);
        }
    }

    /**
     * Kill the current user session.
     *
     * @param  Request                $request
     * @param  User|null              $user
     * @return TransportInterface|int
     */
    public function logout(Request $request, User $user = null)
    {
        if ($user) {
            /** @var AdapterInterface $authentication_adapter */
            $authentication_adapter = $request->getAttribute('authentication_adapter');

            /** @var AuthenticationResultInterface $authenticated_with */
            $authenticated_with = $request->getAttribute('authenticated_with');

            if ($authentication_adapter && $authenticated_with) {
                return AngieApplication::authentication()->terminate($authentication_adapter, $authenticated_with);
            }
        }

        return Response::BAD_REQUEST;
    }

    /**
     * Issue token.
     *
     * @param  Request                $request
     * @return TransportInterface|int
     */
    public function issue_token(Request $request)
    {
        if ($this->shouldBlockAuthorizationRequest($request, 'issue_token')) {
            return Response::FORBIDDEN;
        }

        $authorizer = AngieApplication::authentication()->getAuthorizer();

        $credentials = $request->post();
        $payload = null;

        try {
            if ($authorizer instanceof RequestAwareAuthorizerInterface && $authorizer->getRequestProcessor()) {
                $request_processing_result = $authorizer->getRequestProcessor()->processRequest($request);
                $credentials = $request_processing_result->getCredentials();

                $payload = $request_processing_result->getDefaultPayload();
            }

            AngieApplication::log()->debug('Authorising user', [
                'authorizer' => get_class($authorizer),
            ]);

            return AngieApplication::authentication()->authorizeForAdapter($credentials, TokenBearerAdapter::class, $payload);
        } catch (Exception $e) {
            $this->logFailedAuthorization($request, 'issue_token', $credentials, $e);

            return Response::OPERATION_FAILED;
        }
    }

    /**
     * View invitation.
     *
     * @param  Request   $request
     * @return array|int
     */
    public function view_invitation(Request $request)
    {
        $invitation = $this->getInvitationFromParameters($request);

        if ($invitation instanceof UserInvitation) {
            $user = $invitation->getUser();
            $invited_by = $invitation->getCreatedBy();
            $invited_to = $invitation->getInvitedTo();

            return [
                'status' => $invitation->getStatus(),
                'user' => $user instanceof User && $user->isActive() ? ['id' => $user->getId(), 'email' => $user->getEmail(), 'first_name' => $user->getFirstName(), 'last_name' => $user->getLastName()] : null,
                'invited_by' => $invited_by instanceof User ? ['id' => $invited_by->getId(), 'full_name' => $invited_by->getDisplayName(), 'first_name' => $invited_by->getFirstName(), 'last_name' => $invited_by->getLastName()] : null,
                'invited_to' => $invited_to ? ['id' => $invited_to->getId(), 'class' => get_class($invited_to), 'name' => $invited_to->getName()] : null,
            ];
        }

        return Response::NOT_FOUND;
    }

    /**
     * @param  Request                $request
     * @return TransportInterface|int
     */
    public function accept_invitation(Request $request)
    {
        $invitation = $this->getInvitationFromParameters($request);

        if ($invitation instanceof UserInvitation) {
            if ($invitation->getStatus() === UserInvitation::ACCEPTABLE) {
                list($first_name, $last_name, $password, $language_id, $uploaded_avatar_code) = $this->validateInvitationParameters($request);

                $invited_user = $invitation->getUser();

                Users::update($invited_user, [
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'password' => $password,
                    'language_id' => $language_id >= 1 && Languages::findById($language_id) instanceof Language ? $language_id : 0,
                    'uploaded_avatar_code' => $uploaded_avatar_code,
                ]);

                $invited_user->invitationAccepted();

                return AngieApplication::authentication()->authorizeForAdapter([
                    'username' => $invited_user->getEmail(),
                    'password' => $password,
                ], BrowserSessionAdapter::class, Users::prepareCollection('initial_for_logged_user', $invited_user));
            }

            return Response::GONE; // Expired
        }

        return Response::NOT_FOUND;
    }

    /**
     * Return response that will require user to authorize.
     *
     * @return StatusResponseInterface
     */
    private function requireAuthorization()
    {
        return new StatusResponse(401, 'User not authenticated.', Users::prepareCollection('initial_for_logged_user', null));
    }

    /**
     * Return true if $request should be blocked due to too many login attempts.
     *
     * $action is name of the action, like login or issue_token.
     *
     * @param  ServerRequestInterface $request
     * @param  string                 $action
     * @return bool
     */
    private function shouldBlockAuthorizationRequest(ServerRequestInterface $request, $action)
    {
        $ip_address = $request->getAttribute('visitor_ip_address');

        if ($this->shouldBlockIpAddress($ip_address)) {
            AngieApplication::log()->notice('Brute force protection blocked {action} request from {ip_address}.', [
                'action' => $action,
                'ip_address' => $ip_address,
            ]);

            return true;
        }

        return false;
    }

    /**
     * @param ServerRequestInterface $request
     * @param string                 $action
     * @param array                  $credentials
     * @param Exception              $exception
     */
    private function logFailedAuthorization(ServerRequestInterface $request, $action, array $credentials, Exception $exception)
    {
        AngieApplication::log()->notice('Authorization failed for {username} using {action}. Reason: {reason}.', [
            'action' => $action,
            'username' => array_var($credentials, 'username', '--username not set--'),
            'credentials' => $this->prepareCredentialsForLog($credentials),
            'reason' => $exception->getMessage(),
            'exception' => $exception,
        ]);

        $ip_address = $request->getAttribute('visitor_ip_address');

        if ($this->shouldBlockIpAddress($ip_address)) {
            /** @var FailedLoginNotification $failed_login_notification */
            $failed_login_notification = AngieApplication::notifications()->notifyAbout('failed_login');
            $failed_login_notification
                ->setUsername(array_var($credentials, 'username'))
                ->setMaxAttempts(AngieApplication::bruteForceProtector()->getMaxAttempts())
                ->setCooldownInMinutes(ceil(AngieApplication::bruteForceProtector()->getInTimeframe() / 60))
                ->setFromIP($ip_address)
                ->sendToAdministrators(true);
        }
    }

    private function prepareCredentialsForLog(array $credentials)
    {
        $result = [];

        foreach ($credentials as $k => $v) {
            if (strtolower($k) === 'password') {
                $result[$k] = str_repeat('*', strlen((string) $v));
            } else {
                $result[$k] = $v;
            }
        }

        return $result;
    }

    /**
     * Return true if $ip_address should be blocked due to too many login attempts.
     *
     * @param  string $ip_address
     * @return bool
     */
    private function shouldBlockIpAddress($ip_address)
    {
        return AngieApplication::bruteForceProtector()->shouldBlock($ip_address);
    }

    /**
     * Return a valid invitation or proper status code (not found or expired).
     *
     * @param  Request             $request
     * @return UserInvitation|null
     */
    private function getInvitationFromParameters(Request $request): ?UserInvitation
    {
        $invitation = UserInvitations::findByUserIdAndCode(
            $request->get('user_id'),
            $request->get('code')
        );

        if ($invitation instanceof UserInvitation) {
            return $invitation;
        }

        return null;
    }

    /**
     * Read, validate and return invitation accept parameters.
     *
     * @param  Request          $request
     * @return array
     * @throws ValidationErrors
     */
    private function validateInvitationParameters(Request $request)
    {
        $first_name = trim($request->post('first_name'));
        $last_name = trim($request->post('last_name'));
        $password = $request->post('password');
        $language_id = (int) $request->post('language_id');
        $uploaded_avatar_code = $request->post('uploaded_avatar_code');

        $errors = new ValidationErrors();

        if (empty($first_name)) {
            $errors->fieldValueIsRequired('first_name');
        }

        if (empty($last_name)) {
            $errors->fieldValueIsRequired('last_name');
        }

        if (trim($password) === '') {
            $errors->fieldValueIsRequired('password');
        }

        if (empty($language_id)) {
            $errors->fieldValueIsRequired('language');
        }

        if ($errors->hasErrors()) {
            throw $errors;
        }

        return [$first_name, $last_name, $password, $language_id, $uploaded_avatar_code];
    }
}
