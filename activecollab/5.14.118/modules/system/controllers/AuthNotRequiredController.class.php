<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

AngieApplication::useController('fw_auth_not_required', EnvironmentFramework::NAME);

/**
 * Auth not required controller.
 *
 * @package angie.frameworks.environment
 * @subpackage controllers
 */
class AuthNotRequiredController extends FwAuthNotRequiredController
{
}
