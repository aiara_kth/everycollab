<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

use Angie\Http\Request;
use Angie\Http\Response;

AngieApplication::useController('auth_required', EnvironmentFramework::INJECT_INTO);

/**
 * Announcements controller.
 *
 * @package ActiveCollab.modules.system
 * @subpackage controllers
 */
class AnnouncementsController extends AuthRequiredController
{
    /**
     * {@inheritdoc}
     */
    protected function __before(Request $request, $user)
    {
        $before_result = parent::__before($request, $user);

        if ($before_result !== null) {
            return $before_result;
        }

        if (!$user->isOwner()) {
            return Response::NOT_FOUND;
        }
    }

    /**
     * Send an announcement.
     *
     * @param  Request $request
     * @param  User    $user
     * @return int
     */
    public function announce(Request $request, User $user)
    {
        Users::announce($request->post('subject'), $request->post('announcement'), $user, Users::findByAddressList($request->post('recipients')));

        return Response::OK;
    }
}
