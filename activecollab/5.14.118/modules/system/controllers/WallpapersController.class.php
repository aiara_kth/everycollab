<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

AngieApplication::useController('fw_wallpapers', EnvironmentFramework::NAME);

/**
 * Application level wallpapers controller.
 *
 * @package activeCollab.modules.syste,
 * @subpackage controllers
 */
class WallpapersController extends FwWallpapersController
{
}
