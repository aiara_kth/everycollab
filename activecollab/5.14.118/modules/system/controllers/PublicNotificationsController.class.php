<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

AngieApplication::useController('fw_public_notifications', NotificationsFramework::NAME);

/**
 * Public notifications controller.
 *
 * @package ActiveCollab.modules.system
 * @subpackage controllers
 */
class PublicNotificationsController extends FwPublicNotificationsController
{
}
