<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

declare(strict_types=1);

namespace ActiveCollab\Module\System\Utils\NewFeatures;

use ActiveCollab\CurrentTimestamp\CurrentTimestampInterface;
use ConfigOptions;
use DateTimeValue;
use DateValue;
use DB;
use ActiveCollab\Module\System\Utils\NewFeatures\NewFeatureAnnouncementInterface;
use ActiveCollab\Module\System\Utils\NewFeatures\NewFeatureAnnouncementsLoader\NewFeatureAnnouncementsLoaderInterface;
use User;

/**
 * Get the list of the new application features.
 *
 * @package ActiveCollab.modules.system
 * @subpackage model
 */
class NewFeaturesManager implements NewFeaturesManagerInterface
{
    /**
     * @var NewFeatureAnnouncementInterface[]
     */
    private $new_feature_announcements = [];
    private $channel;
    private $current_timestamp;

    public function __construct(
        NewFeatureAnnouncementsLoaderInterface $new_feature_announcements_loader,
        string $channel,
        CurrentTimestampInterface $current_timestamp
    )
    {
        $this->new_feature_announcements = $new_feature_announcements_loader->getNewFeatureAnnouncements();
        $this->channel = $channel;
        $this->current_timestamp = $current_timestamp;
    }

    /**
     * Get the list of new features for the user.
     *
     * @param  User                              $user
     * @param  DateValue|null                    $date
     * @param  bool                              $record_last_visit
     * @return NewFeatureAnnouncementInterface[]
     */
    public function get(User $user, DateValue $date = null, bool $record_last_visit = false): array
    {
        if (empty($date)) {
            $date = new DateValue();
        }

        $result = [];

        foreach ($this->new_feature_announcements as $new_feature_announcement) {
            if (!$new_feature_announcement->isVisibleToUser($user)) {
                continue;
            }

            if (!$new_feature_announcement->isVisibleInChannel($this->channel)) {
                continue;
            }

            if (!$new_feature_announcement->isVisibleOnDate($date)) {
                continue;
            }

            $result[] = $new_feature_announcement;
        }

        if ($record_last_visit) {
            $this->recordLastVisit($user);
        }

        return $result;
    }

    public function getJson(User $user, DateValue $date = null, bool $record_last_visit = false): array
    {
        $result = [];

        $last_visit_timestamp = $this->getLastVisit($user);

        foreach ($this->get($user, $date, $record_last_visit) as $new_feature_announcement) {
            $result[] = array_merge(
                $new_feature_announcement->jsonSerialize(),
                [
                    'is_seen' => $new_feature_announcement->isSeen($last_visit_timestamp),
                ]
            );
        }

        return $result;
    }

    /**
     * Count new features for user.
     *
     * @param  User           $user
     * @param  DateValue|null $date
     * @return int
     */
    public function countUnseen(User $user, DateValue $date = null): int
    {
        if ($this->areMuted($user)) {
            return 0;
        }

        if (empty($date)) {
            $date = new DateValue();
        }

        $new_feature_announcements = $this->get($user, $date, false);
        $unseen_count = count($new_feature_announcements);

        $last_visit_timestamp = $this->getLastVisit($user);

        if (empty($last_visit_timestamp)) {
            return $unseen_count;
        }

        foreach ($new_feature_announcements as $new_feature_announcement) {
            if ($new_feature_announcement->isSeen($last_visit_timestamp)) {
                $unseen_count--;
            }
        }

        return $unseen_count;
    }

    private function areMuted(User $user): bool
    {
        return !ConfigOptions::getValueFor('new_features_notification', $user);
    }

    public function getLastVisit(User $user): ?DateTimeValue
    {
        $new_features_timestamp = ConfigOptions::getValueFor('new_features_timestamp', $user);

        if (empty($new_features_timestamp) || !is_int($new_features_timestamp)) {
            return null;
        } else {
            return new DateTimeValue($new_features_timestamp);
        }
    }

    public function recordLastVisit(User $user, DateTimeValue $timestamp = null): DateTimeValue
    {
        if (empty($timestamp)) {
            $timestamp = new DateTimeValue();
        }

        DB::transact(
            function () use ($user, $timestamp) {
                ConfigOptions::setValueFor(
                    'new_features_timestamp',
                    $user,
                    $timestamp->getTimestamp()
                );

                $user->touch();
            }
        );

        return $timestamp;
    }
}
