<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

declare(strict_types=1);

namespace ActiveCollab\Module\System\EventListeners;

use ActiveCollab\ActiveCollabJobs\Jobs\Http\SendWebhook;
use ActiveCollab\DateValue\DateTimeValue;
use ActiveCollab\Foundation\Events\BillingEvent\BillingEventInterface;
use ActiveCollab\Foundation\Events\EventInterface;
use ActiveCollab\Foundation\Events\WebhookEvent\WebhookEventInterface;
use ActiveCollab\JobsQueue\DispatcherInterface;
use ActiveCollab\JobsQueue\Jobs\JobInterface;
use ActiveCollab\Module\OnDemand\Utils\ShepherdIntegration\ShepherdWebhook;
use Psr\Log\LoggerInterface;
use Webhook;
use WebhooksIntegration;

class WebhookDispatcher implements WebhookDispatcherInterface
{
    private $enabled_webhooks_resolver;
    private $jobs;
    private $account_id;
    private $logger;

    public function __construct(
        callable $enabled_webhooks_resolver,
        DispatcherInterface $jobs,
        int $account_id,
        LoggerInterface $logger
    )
    {
        $this->enabled_webhooks_resolver = $enabled_webhooks_resolver;
        $this->jobs = $jobs;
        $this->account_id = $account_id;
        $this->logger = $logger;
    }

    public function __invoke(EventInterface $event)
    {
        if ($event instanceof WebhookEventInterface) {
            $webhooks = $this->getEnabledWebhooks();

            if (empty($webhooks)) {
                $this->logger->debug(
                    "Skipping '{event_type}' webhook dispatch. No enabled webhooks.",
                    [
                        'event_type' => $event->getWebhookEventType(),
                    ]
                );
            }

            foreach ($webhooks as $webhook) {
                if (!$webhook->filterEvent($event)) {
                    $this->logger->debug(
                        "Skipping '{event_type}' webhook dispatch to '{url}' due to filters settings.",
                        [
                            'event_type' => $event->getWebhookEventType(),
                            'context' => $event->getWebhookContext(),
                            'url' => $webhook->getUrl(),
                        ]
                    );
                    continue;
                }

                $payload = $event->getWebhookPayload($webhook);

                if (empty($payload)) {
                    $this->logger->debug(
                        "Skipping event type '{event_type}' for webhook '{url}', payload empty.",
                        [
                            'event_type' => $event->getWebhookEventType(),
                            'url' => $webhook->getUrl(),
                        ]
                    );

                    continue;
                }

                $url = $webhook->getUrl();
                $custom_query_params = $webhook->getCustomQueryParams($event);

                if (!empty($custom_query_params)) {
                    $url .= parse_url($url, PHP_URL_QUERY) ? '&' : '?';
                    $url .= $custom_query_params;
                }

                //@ToDo switch to version based payload
                if ($webhook instanceof ShepherdWebhook) {
                    $payload = [
                        'instance_id' => $this->account_id,
                        'type' => $event->getWebhookEventType(),
                        'timestamp' => (new DateTimeValue())->getTimestamp(),
                        'payload' => $payload,
                    ];
                }

                $this->jobs->dispatch(
                    new SendWebhook(
                        [
                            'event_type' => $event->getWebhookEventType(),
                            'priority' => JobInterface::HAS_HIGHEST_PRIORITY,
                            'instance_id' => $this->account_id,
                            'url' => $url,
                            'method' => 'POST',
                            'headers' => $webhook->getCustomHeaders($event),
                            'payload' => json_encode($payload),
                        ]
                    ),
                    WebhooksIntegration::JOBS_QUEUE_CHANNEL
                );

                $this->logger->debug(
                    "Webhook for '{event_type}' event dispatched to '{url}'.",
                    [
                        'event_type' => $event->getWebhookEventType(),
                        'url' => $webhook->getUrl(),
                    ]
                );
            }
        }
    }

    /**
     * @return Webhook[]|iterable
     */
    private function getEnabledWebhooks(): iterable
    {
        $enabled_webhooks = call_user_func($this->enabled_webhooks_resolver);

        if (empty($enabled_webhooks) || !is_iterable($enabled_webhooks)) {
            $enabled_webhooks = [];
        }

        return $enabled_webhooks;
    }
}
