<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * @package ActiveCollab.modules.system
 * @subpackage resources
 */
AngieApplication::useModel(['uploaded_files', 'system_notifications', 'access_logs', 'currencies', 'data_filters', 'integrations', 'webhooks', 'day_offs', 'languages', 'test_data_objects', 'activity_logs', 'modification_logs', 'attachments', 'notifications', 'subscriptions', 'comments', 'categories', 'labels', 'payment_gateways', 'stored_cards', 'payments', 'reminders', 'calendars', 'calendar_events', 'users', 'api_subscriptions', 'user_invitations', 'user_sessions', 'companies', 'teams', 'projects', 'project_templates', 'project_template_elements', 'user_workspaces', 'reactions'], 'system');
