<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * on_reaction_deleted event handler.
 *
 * @package ActiveCollab.modules.system
 * @subpackage handlers
 */

/**
 * Handle on_reaction_deleted event.
 *
 * @param Reaction $reaction
 */
function system_handle_on_reaction_deleted(Reaction $reaction)
{
    $event_type = 'ReactionDeleted';

    if (AngieApplication::isEdgeChannel()) {
        Sockets::dispatch($reaction, $event_type);
    }
}
