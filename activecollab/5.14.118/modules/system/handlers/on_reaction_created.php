<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * on_reaction_created event handler.
 *
 * @package ActiveCollab.modules.system
 * @subpackage handlers
 */

/**
 * Handle on_reaction_created event.
 *
 * @param Reaction $reaction
 */
function system_handle_on_reaction_created(Reaction $reaction)
{
    $event_type = 'ReactionCreated';

    if (AngieApplication::isEdgeChannel()) {
        Sockets::dispatch($reaction, $event_type);
    }
}
