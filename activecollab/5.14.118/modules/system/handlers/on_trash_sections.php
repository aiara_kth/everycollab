<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * on_trash_sections event handler.
 *
 * @package activeCollab.modules.system
 * @subpackage handlers
 */

/**
 * Handle on_trash_sections event.
 *
 * @param \Angie\Trash\Sections $sections
 * @param User                  $user
 */
function system_handle_on_trash_sections(\Angie\Trash\Sections &$sections, User $user)
{
    if ($user->isOwner()) {
        $sections->registerTrashedObjects('User', DB::executeIdNameMap('SELECT id, TRIM(CONCAT(first_name, " ", last_name)) AS "full_name", email FROM users WHERE is_trashed = ? ORDER BY trashed_on DESC', true, function ($row) {
            return Users::getUserDisplayName($row);
        }), \Angie\Trash\Sections::THIRD_WAVE);
    } elseif ($user->isPowerUser()) {
        $sections->registerTrashedObjects('User', DB::executeIdNameMap('SELECT id, TRIM(CONCAT(first_name, " ", last_name)) AS "full_name", email FROM users WHERE is_trashed = ? AND trashed_by_id = ? ORDER BY trashed_on DESC', true, $user->getId(), function ($row) {
            return Users::getUserDisplayName($row);
        }), \Angie\Trash\Sections::THIRD_WAVE);
    }

    $companies_id_name_map = $projects_id_name_map = $projects_template_id_name_map = null;

    if ($user->isOwner()) {
        $companies_id_name_map = DB::executeIdNameMap('SELECT id, name FROM companies WHERE is_trashed = ? ORDER BY trashed_on DESC', true);
        $projects_id_name_map = DB::executeIdNameMap('SELECT id, name FROM projects WHERE is_trashed = ? ORDER BY trashed_on DESC', true);
    } elseif ($user->isPowerUser()) {
        $projects_id_name_map = DB::executeIdNameMap('SELECT id, name FROM projects WHERE trashed_by_id = ? AND is_trashed = ? ORDER BY trashed_on DESC', $user->getId(), true);
    }

    if ($user->isOwner() || $user->isPowerUser()) {
        $projects_template_id_name_map = DB::executeIdNameMap('SELECT id, name FROM project_templates WHERE trashed_by_id = ? AND is_trashed = ? ORDER BY trashed_on DESC', $user->getId(), true);
    }

    $sections->registerTrashedObjects('Company', $companies_id_name_map, \Angie\Trash\Sections::FOURTH_WAVE);
    $sections->registerTrashedObjects('Project', $projects_id_name_map, \Angie\Trash\Sections::THIRD_WAVE);
    $sections->registerTrashedObjects('ProjectTemplate', $projects_template_id_name_map, \Angie\Trash\Sections::FOURTH_WAVE);
}
