<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * on_reset_manager_states event handler implementation.
 *
 * @package ActiveCollab.modules.system
 * @subpackage handlers
 */

/**
 * Handle on_reset_manager_states event.
 */
function system_handle_on_reset_manager_states()
{
    UserInvitations::resetState();
    UserWorkspaces::resetState();
    Users::resetState();
}
