<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Currency class.
 *
 * @package ActiveCollab.modules.invoicing
 * @subpackage models
 */
class Currency extends FwCurrency
{
    /**
     * Returns true if $user can delete this currency.
     *
     * @param  User $user
     * @return bool
     */
    public function canDelete(User $user)
    {
        return parent::canDelete($user) ?
            empty(Invoices::countByCurrency($this)) && empty(Projects::countByCurrency($this))
            : false;
    }
}
