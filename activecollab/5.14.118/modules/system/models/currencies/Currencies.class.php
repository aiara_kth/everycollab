<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Currencies management implementation.
 *
 * @package activeCollab.modules.system
 * @subpackage models
 */
class Currencies extends FwCurrencies
{
}
