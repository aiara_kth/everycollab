<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Application level custom reminder implementation.
 *
 * @package ActiveCollab.modules.system
 * @subpackage models
 */
class CustomReminder extends FwCustomReminder
{
}
