<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Application level preloaders for activity log collections.
 *
 * @package ActiveCollab.modules.system
 * @subpackage models
 */
trait IActivityLogColletionPreloaders
{
    /**
     * @var array
     */
    private $task_ids = [], $project_ids = [];

    /**
     * Analyze record for preload().
     *
     * @param ActivityLog $activity_log
     */
    protected function analyzeForPreload(ActivityLog $activity_log)
    {
        parent::analyzeForPreload($activity_log);

        if ($activity_log->getParentType() == 'Task') {
            $this->task_ids[] = $activity_log->getParentId();
        }

        if ($activity_log->getParentType() == 'Project') {
            $this->project_ids[] = $activity_log->getParentId();
        }
    }

    /**
     * Preload details when possible.
     */
    protected function preload()
    {
        parent::preload();

        if (count($this->task_ids)) {
            Subtasks::preloadCountByTasks(array_unique($this->task_ids));
        }

        if (count($this->project_ids)) {
            $this->project_ids = array_unique($this->project_ids);

            if (count($this->project_ids) > 1) {
                Projects::preloadProjectElementCounts($this->project_ids);
            }
        }
    }
}
