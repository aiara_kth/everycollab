<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Application level user activity logs collection implementation.
 *
 * @package ActiveCollab.modules.system
 * @subpackage models
 */
abstract class UserActivityLogsCollection extends FwUserActivityLogsCollection
{
    use IActivityLogColletionPreloaders;
}
