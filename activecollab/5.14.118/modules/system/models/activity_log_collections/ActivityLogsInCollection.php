<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Collection that lists activity logs in a given context.
 *
 * @package ActiveCollab.modules.system
 * @subpackage models
 */
class ActivityLogsInCollection extends FwActivityLogsInCollection
{
    use IActivityLogColletionPreloaders;
}
