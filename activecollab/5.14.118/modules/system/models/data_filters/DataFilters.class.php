<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * DataFilters class.
 *
 * @package ActiveCollab.modules.system
 * @subpackage models
 */
class DataFilters extends FwDataFilters
{
}
