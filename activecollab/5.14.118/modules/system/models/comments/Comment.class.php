<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Application level comment implementation.
 *
 * @package ActiveCollab.modules.system
 * @subpackage models
 */
class Comment extends FwComment
{
    // Additional, application specific comment source
    const SOURCE_SHARED_PAGE = 'shared_page';

    /**
     * Returns true if $user can update this comment.
     *
     * Only owner and comment author in given timeframe can update comment text
     *
     * @param  User $user
     * @return bool
     */
    public function canEdit(User $user)
    {
        if ((new ReflectionClass($this->getParentType()))->implementsInterface('IProjectElement')) {
            if ($this->getIsTrashed()) {
                return false;
            }

            /** @var Project $project */
            $project = DataObjectPool::get('Project', $this->getProjectId());

            if (!$project) {
                return false;
            }

            if ($user->isOwner() || $project->isLeader($user)) {
                return true;
            } else {
                if ($this->isCreatedBy($user)) {
                    return ($this->getCreatedOn()->getTimestamp() + 1800) > DateTimeValue::now()->getTimestamp();
                }
            }

            return false;
        } else {
            return parent::canEdit($user);
        }
    }

    /**
     * Return project ID for this comment.
     *
     * Note: If this comment is not posted on a project element, or project element does not exists, 0 will be returned
     *
     * @return mixed
     */
    public function getProjectId()
    {
        return AngieApplication::cache()->getByObject($this, 'project_id', function () {
            switch ($this->getParentType()) {
                case 'Discussion':
                    $parent_table = 'discussions';
                    break;
                case 'File':
                    $parent_table = 'files';
                    break;
                case 'Note':
                    $parent_table = 'notes';
                    break;
                case 'Task':
                    $parent_table = 'tasks';
                    break;
                default:
                    $parent_table = '';
            }

            return $parent_table ? (int) DB::executeFirstCell("SELECT p.project_id FROM $parent_table AS p LEFT JOIN comments AS c ON p.id = c.parent_id WHERE c.id = ?", $this->getId()) : 0;
        });
    }
}
