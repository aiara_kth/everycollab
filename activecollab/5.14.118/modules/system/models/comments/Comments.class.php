<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Comments manager class.
 *
 * @package ActiveCollab.modules.resources
 * @subpackage models
 */
class Comments extends FwComments
{
}
