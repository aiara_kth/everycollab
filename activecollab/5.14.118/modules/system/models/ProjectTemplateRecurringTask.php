<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Project template task.
 *
 * @package ActiveCollab.modules.system
 * @subpackage models
 */
class ProjectTemplateRecurringTask extends ProjectTemplateElement implements ILabels, IBody
{
    use ILabelsImplementation;
    use IBodyImplementation;

    /**
     * Return array of element properties.
     *
     * Key is name of property, and value is a casting method
     *
     * @return array
     */
    public function getElementProperties()
    {
        return [
            'task_list_id' => 'intval',
            'assignee_id' => 'intval',
            'is_important' => 'boolval',
            'is_hidden_from_clients' => 'boolval',
            'label_id' => 'intval',
            'subtasks' => 'array',
            'repeat_frequency' => 'strval',
            'repeat_amount' => 'intval',
            'start_in' => 'strval',
            'due_in' => 'strval',
        ];
    }

    /**
     * Return type of label used.
     *
     * @return string
     */
    public function getLabelType()
    {
        return 'TaskLabel';
    }
}
