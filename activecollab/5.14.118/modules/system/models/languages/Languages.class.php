<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Languages class.
 *
 * @package ActiveCollab.modules.system
 * @subpackage models
 */
class Languages extends FwLanguages
{
}
