<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * @package ActiveCollab.modules.system
 * @subpackage model
 */
class UserProfilePermissionsChecker
{
    /**
     * @var User
     */
    private $user_who_change;

    /**
     * @var User
     */
    private $user_which_change;

    /**
     * @var bool
     */
    private $is_self_hosted;

    /**
     * @var bool
     */
    private $is_on_demand;

    /**
     * @var bool
     */
    private $is_new_shepherd;

    /**
     * @param User $user_who_change
     * @param User $user_which_change
     * @param bool $is_on_demand
     * @param bool $is_new_shepherd
     */
    public function __construct(User $user_who_change, User $user_which_change, $is_on_demand, $is_new_shepherd)
    {
        $this->user_who_change = $user_who_change;
        $this->user_which_change = $user_which_change;
        $this->is_self_hosted = !$is_on_demand;
        $this->is_on_demand = $is_on_demand;
        $this->is_new_shepherd = $is_new_shepherd;
    }

    /**
     * @return bool
     */
    public function canChangePassword()
    {
        if (AngieApplication::authentication()->getLoginPolicy()->isPasswordChangeEnabled()) {
            if ($this->user_which_change->getId() === $this->user_who_change->getId()) {
                return true;
            } else if ($this->is_on_demand && $this->is_new_shepherd && $this->hasOneAccount()) {
                if ($this->user_which_change->isClient()) {
                    return $this->isOwnerOrInvitedBy();
                } else if ($this->user_which_change->isMember(true) || $this->user_which_change->isOwner()) {
                    return $this->isOwnerOrInvitedBy() && $this->user_which_change->isPendingActivation();
                }
            }  else if ($this->is_self_hosted) {
                if ($this->user_which_change->isClient()) {
                    return $this->isOwnerOrInvitedBy();
                } else if ($this->user_which_change->isMember(true)) {
                    return $this->user_who_change->isOwner() ||
                        ($this->user_which_change->isPendingActivation() &&
                            $this->user_who_change->getId() === $this->user_which_change->getCreatedById());
                } else if ($this->user_which_change->isOwner()) {
                    return $this->user_who_change->isOwner();
                }
            }
        }

        return false;
    }

    /**
     * @return array
     */
    public function changeProfilePermissions()
    {
        $can_change_profile = $this->canChangeProfile();

        return [
            'can_change_profile' => $can_change_profile,
            'can_change_name' => !$can_change_profile ? $this->canChangeName() : true,
        ];
    }

    /**
     * @return bool
     */
    public function canChangeProfile()
    {
        if ($this->user_which_change->getId() === $this->user_who_change->getId()) {
            return true;
        } else if ($this->is_on_demand && $this->is_new_shepherd && $this->hasOneAccount()) {
            if ($this->user_which_change->isClient()) {
                return $this->isOwnerOrInvitedBy();
            } else if (($this->user_which_change->isMember(true) || $this->user_which_change->isOwner()) && $this->user_which_change->isPendingActivation()) {
                return $this->isOwnerOrInvitedBy();
            }
        } else if ($this->is_self_hosted) {
            if ($this->user_which_change->isClient() || $this->user_which_change->isMember(true)) {
                return $this->isOwnerOrInvitedBy();
            } else if ($this->user_which_change->isOwner()) {
                return false;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function canChangeName()
    {
        return $this->isOwnerOrInvitedBy() && $this->isEmptyName(); // for both self-hosted and on demand is same condition
    }

    /**
     * Check is user who change owner or is user who invite.
     *
     * @return bool
     */
    private function isOwnerOrInvitedBy()
    {
        return $this->user_who_change->isOwner() || $this->user_who_change->getId() === $this->user_which_change->getCreatedById();
    }

    /**
     * Check is user which change has empty first or last name.
     *
     * @return bool
     */
    private function isEmptyName()
    {
        return empty($this->user_which_change->getFirstName()) || empty($this->user_which_change->getLastName());
    }

    /**
     * Check if user has one account.
     *
     * @return bool
     */
    private function hasOneAccount()
    {
        return UserWorkspaces::getWorkspaceCountForUser($this->user_which_change) === 1;
    }
}
