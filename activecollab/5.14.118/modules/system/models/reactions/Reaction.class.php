<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Reaction class
 *
 * @package ActiveCollab.modules.system
 * @subpackage models
 */
abstract class Reaction extends BaseReaction implements IRoutingContext
{
    /**
     * Return routing context name.
     *
     * @return string
     */
    public function getRoutingContext()
    {
        return 'reaction';
    }

    /**
     * Return routing context parameters.
     *
     * @return mixed
     */
    public function getRoutingContextParams()
    {
        return ['reaction_id' => $this->getId()];
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return array_merge(
            parent::jsonSerialize(),
            [
                'parent_id' => $this->getParentId(),
                'parent_type' => $this->getParentType()
            ]
        );
    }

    /**
     * Return a list of properties that are watched.
     *
     * @return array
     */
    public function touchParentOnPropertyChange()
    {
        return ['type'];
    }

    /**
     * {@inheritdoc}
     */
    public function delete($bulk = false)
    {
        try {
            DB::beginWork('Deleting reaction @ ' . __CLASS__);

            DataObjectPool::announce($this, DataObjectPool::OBJECT_DELETED);

            parent::delete($bulk);

            /** @var Comment $comment */
            $comment = $this->getParent();
            /** @var Task $task */
            $task = $comment->getParent();

            Notifications::deleteByParentAndAdditionalProperty($task, 'reaction_id', $this->getId());

            DB::commit('Reaction deleted @ ' . __CLASS__);
        } catch (Exception $e) {
            DB::rollback('Failed to delete reaction @ ' . __CLASS__);
            throw $e;
        }
    }
}
