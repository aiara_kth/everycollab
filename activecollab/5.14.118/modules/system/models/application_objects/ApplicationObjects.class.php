<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Application objects manager.
 *
 * @package activeCollab.modules.system
 * @subpackage models
 */
class ApplicationObjects extends FwApplicationObjects
{
}
