<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Class that all application objects inherit.
 *
 * @package activeCollab.modules.system
 * @subpackage models
 */
abstract class ApplicationObject extends FwApplicationObject
{
}
