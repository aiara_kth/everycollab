<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Application level reminder class.
 *
 * @package ActiveCollab.modules.system
 * @subpackage models
 */
abstract class Reminder extends FwReminder
{
}
