<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Application level reminders manager.
 *
 * @package ActiveCollab.modules.resources
 * @subpackage models
 */
class Reminders extends FwReminders
{
}
