<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Category class.
 *
 * @package activeCollab.modules.system
 * @subpackage models
 */
abstract class Category extends FwCategory
{
}
