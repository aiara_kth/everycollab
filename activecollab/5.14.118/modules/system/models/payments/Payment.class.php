<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Payment class.
 *
 * @package ActiveCollab.modules.system
 * @subpackage models
 */
class Payment extends FwPayment
{
}
