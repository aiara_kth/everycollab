<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Local attachment model.
 *
 * @package ActiveCollab.modules.system
 * @subpackage models
 */
final class LocalAttachment extends Attachment
{
}
