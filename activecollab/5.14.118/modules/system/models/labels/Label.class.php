<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Label class.
 *
 * @package activeCollab.modules.system
 * @subpackage models
 */
abstract class Label extends FwLabel implements LabelInterface
{
}
