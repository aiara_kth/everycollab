<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * ModificationLog class.
 *
 * @package activeCollab.modules.system
 * @subpackage models
 */
class ModificationLog extends FwModificationLog
{
}
