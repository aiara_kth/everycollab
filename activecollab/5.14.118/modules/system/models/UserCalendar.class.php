<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * User calendar instance.
 *
 * @package activeCollab.modules.system
 * @subpackage models
 */
class UserCalendar extends FwUserCalendar
{
}
