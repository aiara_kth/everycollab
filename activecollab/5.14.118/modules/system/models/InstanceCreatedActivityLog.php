<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Application level instance created activity log implementation.
 *
 * @package ActiveCollab.modules.system
 * @subpackage models
 */
class InstanceCreatedActivityLog extends FwInstanceCreatedActivityLog
{
    /**
     * This method is called when we need to load related notification objects for API response.
     *
     * @param array $type_ids_map
     */
    public function onRelatedObjectsTypeIdsMap(array &$type_ids_map)
    {
        parent::onRelatedObjectsTypeIdsMap($type_ids_map);

        if ($project_id = $this->getProjectId()) {
            if (empty($type_ids_map['Project'])) {
                $type_ids_map['Project'] = [];
            }

            $type_ids_map['Project'][] = $project_id;
        }
    }

    /**
     * Return project ID for this subtask.
     *
     * Note: If this comment is not posted on a project element, or project element does not exists, 0 will be returned
     *
     * @return mixed
     */
    public function getProjectId()
    {
        return AngieApplication::cache()->getByObject($this, 'project_id', function () {
            switch ($this->getParentType()) {
                case 'Discussion':
                    $parent_table = 'discussions';
                    break;
                case 'LocalFile':
                case 'WarehouseFile':
                    $parent_table = 'files';
                    break;
                case 'Note':
                    $parent_table = 'notes';
                    break;
                case 'Task':
                    $parent_table = 'tasks';
                    break;
                case 'TaskList':
                    $parent_table = 'task_lists';
                    break;
                default:
                    $parent_table = '';
            }

            return $parent_table ? (int) DB::executeFirstCell("SELECT project_id FROM $parent_table WHERE id = ?", $this->getParentId()) : 0;
        });
    }
}
