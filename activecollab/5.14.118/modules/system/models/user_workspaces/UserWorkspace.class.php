<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * UserWorkspace class.
 *
 * @package ActiveCollab.modules.system
 * @subpackage models
 */
class UserWorkspace extends BaseUserWorkspace
{
    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return array_merge([
            'user_id' => $this->getUserId(),
            'shepherd_account_id' => $this->getShepherdAccountId(),
            'shepherd_account_type' => $this->getShepherdAccountType(),
            'shepherd_account_url' => $this->getShepherdAccountUrl(),
            'is_shown_in_launcher' => $this->getIsShownInLauncher(),
            'is_owner' => $this->getIsOwner(),
            'position' => $this->getPosition(),
        ], parent::jsonSerialize());
    }
}
