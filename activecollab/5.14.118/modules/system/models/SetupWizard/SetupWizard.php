<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

use ActiveCollab\HumanNameParser\Parser as HumanNameParser;
use ActiveCollab\Memories\Memories;
use ActiveCollab\User\UserInterface;

class SetupWizard implements SetupWizardInterface
{
    /**
     * @var ActiveCollabIdInterface|null
     */
    private $activecollab_id;

    private $memories;

    private $first_owner;

    private $is_on_demand;

    private $onboarding_survey;

    private $human_name_parser;

    public function __construct(
        ?ActiveCollabIdInterface $activecollab_id,
        Memories $memories,
        User $first_owner,
        OnboardingSurveyInterface $onboarding_survey,
        $is_on_demand,
        HumanNameParser $human_name_parser
    )
    {
        if ($is_on_demand && empty($activecollab_id)) {
            throw new LogicException('ActiveCollab ID is required when running in on demand mode.');
        }

        $this->activecollab_id = $activecollab_id;
        $this->memories = $memories;
        $this->first_owner = $first_owner;
        $this->onboarding_survey = $onboarding_survey;
        $this->is_on_demand = $is_on_demand;
        $this->human_name_parser = $human_name_parser;
    }

    /**
     * @return int
     */
    public function getWhenIsPasswordSetInWizard()
    {
        return $this->memories->get('password_set_at', null);
    }

    /**
     * @param  DateTimeValue|null $datetime
     * @return $this
     */
    private function setGrantedAccessAt(DateTimeValue $datetime = null)
    {
        if (empty($datetime)) {
            $datetime = new DateTimeValue();
        }

        if (is_null($this->getGrantedAccessAt())) {
            $this->memories->set('granted_access_at', $datetime->getTimestamp());
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getGrantedAccessAt()
    {
        return $this->memories->get('granted_access_at', null);
    }

    /**
     * @param  bool  $value
     * @return $this
     */
    public function setOwnerHasRandomPassword($value = true)
    {
        if (is_null($this->getOwnerHasRandomPassword())) {
            $this->memories->set('owner_has_random_password', $value);
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function getOwnerHasRandomPassword()
    {
        return $this->memories->get('owner_has_random_password', null);
    }

    /**
     * @param  DateTimeValue $datetime
     * @return $this
     */
    public function setWhenIsPasswordSetInWizard(DateTimeValue $datetime = null)
    {
        if (empty($datetime)) {
            $datetime = new DateTimeValue();
        }

        $this->memories->set('password_set_at', $datetime->getTimestamp());

        return $this;
    }

    /**
     * @param  DateTimeValue $datetime
     * @return $this
     */
    public function setWhenIsShownSetPasswordForm(DateTimeValue $datetime = null)
    {
        if (empty($datetime)) {
            $datetime = new DateTimeValue();
        }

        if (is_null($this->getWhenIsShownSetPasswordForm())) {
            $this->memories->set('password_set_wizard_shown_at', $datetime->getTimestamp());
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getWhenIsShownSetPasswordForm()
    {
        return $this->memories->get('password_set_wizard_shown_at', null);
    }

    /**
     * @param  User|null $user
     * @return string
     */
    public function getNextWizardStep(User $user = null)
    {
        if ($this->shouldShowOnboardingSurvey($user)) {
            return self::WIZARD_STEP_ONBORDING_SURVEY;
        }

        return self::WIZARD_STEP_CONFORMATION;
    }

    /**
     * @param $user
     * @return bool
     */
    public function shouldShowOnboardingSurvey(UserInterface $user)
    {
        return $this->onboarding_survey->shouldShow($user);
    }

    /**
     * @param  UserInterface $user
     * @return bool
     */
    public function shouldShowSetPassword(UserInterface $user)
    {
        if (!$this->is_on_demand) {
            return false;
        }

        if (!$user instanceof User) {
            return false;
        }

        if ($user->getId() !== $this->first_owner->getId()) {
            return false;
        }

        $should_show = $this->activecollab_id->isUserPasswordGenerated($user->getEmail())
            && empty($this->getWhenIsPasswordSetInWizard());

        if ($should_show) {
            $this->setWhenIsShownSetPasswordForm();
            $this->setOwnerHasRandomPassword();
        } else {
            $this->setOwnerHasRandomPassword(false);
            $this->setGrantedAccessAt();
        }

        return $should_show;
    }

    /**
     * @param  string           $password
     * @param  UserInterface    $user
     * @return $this
     * @throws ValidationErrors
     */
    public function setPasswordForUser($password, UserInterface $user)
    {
        $errors = new ValidationErrors();

        if (empty($password)) {
            $errors->fieldValueIsRequired('password');
        }

        if ($errors->hasErrors()) {
            throw $errors;
        }

        $this->activecollab_id->updateUserPassword($user->getEmail(), $password);
        $this->setWhenIsPasswordSetInWizard();
        $this->setGrantedAccessAt();

        return $this;
    }

    /**
     * @param  User  $user
     * @param        $full_name
     * @return $this
     */
    public function updateUserFirstAndLastName(User $user, $full_name)
    {
        if (empty($user->getFirstName()) || empty($user->getLastName())) {
            if (empty($full_name)) {
                throw new LogicException('First and Last name are required and cannot be empty');
            } else {
                $this->human_name_parser->setName($full_name);

                $user->setFirstName($this->human_name_parser->getFirst());
                $user->setLastName($this->human_name_parser->getLast());
                $user->save();

                $this->activecollab_id->propagateUpdate($user);
            }
        }

        return $this;
    }
}
