<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Socket class.
 *
 * @package ActiveCollab.modules.system
 * @subpackage models
 */
abstract class Socket
{
    /**
     * Return one or more requests.
     *
     * @param  string     $event_type
     * @param  DataObject $object
     * @return array
     */
    abstract public function getRequests($event_type, DataObject $object);
}
