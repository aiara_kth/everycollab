<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

use ActiveCollab\JobsQueue\Batches\BatchInterface;

/**
 * Sockets class.
 *
 * @package ActiveCollab.modules.system
 * @subpackage models
 */
class Sockets
{
    /**
     * Dispatch requests.
     *
     * @param string     $event_type
     * @param DataObject $object
     */
    public static function dispatch(DataObject $object, $event_type)
    {
        $integration = (new RealTimeIntegrationResolver())->getActiveRealTimeIntegration();

        if ($integration instanceof RealTimeIntegration && $socket = $integration->getSocket()) {
            if ($requests = $socket->getRequests($event_type, $object)) {
                AngieApplication::jobs()->batch($event_type, function(BatchInterface $batch) use ($requests) {
                    foreach ($requests as $request) {
                        $batch->dispatch($request, RealTimeIntegration::JOBS_QUEUE_CHANNEL);
                    }
                });
            } else {
                AngieApplication::log()->debug(
                    'Skipping event type {event_type}, no requests defined',
                    ['event_type' => $event_type]
                );
            }
        }
    }
}
