<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Pusher socket class.
 *
 * @package ActiveCollab.modules.system
 * @subpackage models
 */
class PusherSocket extends Socket
{
    const CHANNELS_PER_REQUEST = 100;

    /**
     * {@inheritdoc}
     */
    public function getRequests($event_type, DataObject $object)
    {
        $requests = [];

        /** @var PusherIntegration $pusher_integration */
        $pusher_integration = Integrations::findFirstByType(PusherIntegration::class);
        $channels = (new RealTimeUsersChannelsResolver())->getUsersChannels($object);

        if ($pusher_integration->isInUse() && $channels) {
            $transformator = new PusherSocketPayloadTransformator();
            $chunks = ceil(count($channels) / self::CHANNELS_PER_REQUEST);
            $payload = [
                'name' => $event_type,
                'data' => json_encode($transformator->transform($event_type, $object)),
            ];

            for ($i = 0; $i < $chunks; $i++) {
                $payload['channels'] = array_slice(
                    $channels,
                    $i * self::CHANNELS_PER_REQUEST,
                    self::CHANNELS_PER_REQUEST
                );

                $url = $pusher_integration->getApiUrl() . $pusher_integration->getEventsPath() . '?';
                $url .= $pusher_integration->buildAuthQueryString('POST', $payload);

                $requests[] = new \ActiveCollab\ActiveCollabJobs\Jobs\Http\SendRequest([
                    'priority' => \ActiveCollab\JobsQueue\Jobs\Job::HAS_HIGHEST_PRIORITY,
                    'instance_id' => AngieApplication::getAccountId(),
                    'url' => $url,
                    'method' => 'POST',
                    'headers' => [
                        'Content-Type' => 'application/json',
                    ],
                    'payload' => json_encode($payload),
                ]);
            }
        }

        return $requests;
    }
}
