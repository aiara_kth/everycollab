<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * AccessLog class.
 *
 * @package activeCollab.modules.system
 * @subpackage models
 */
class AccessLog extends BaseAccessLog
{
}
