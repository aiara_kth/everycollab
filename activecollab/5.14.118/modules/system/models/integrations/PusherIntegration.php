<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

use Pusher\Pusher;

/**
 * Pusher integrations class.
 */
class PusherIntegration extends RealTimeIntegration
{
    const DEFAULT_PUSHER_API_HOST = 'api.pusherapp.com';
    const SECURE_PUSHER_API_PORT = '443';

    /**
     * @var string
     */
    private $auth_version = '1.0';

    /**
     * {@inheritdoc}
     */
    public function isInUse(User $user = null)
    {
        return
            (bool) $this->getAppId() &&
            (bool) $this->getAppKey() &&
            (bool) $this->getAppSecret() &&
            (bool) $this->getAppCluster()
        ;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Pusher';
    }

    /**
     * {@inheritdoc}
     */
    public function getShortName()
    {
        return 'pusher';
    }

    /**
     * {@inheritdoc}
     */
    public function getSocket()
    {
        return new PusherSocket();
    }

    /**
     * Return pusher api auth version.
     *
     * @return string
     */
    public function getAuthVersion()
    {
        return $this->auth_version;
    }

    /**
     * Set app id.
     *
     * @param mixed $value
     *
     * @return string
     */
    public function setAppId($value)
    {
        if (!AngieApplication::isOnDemand()) {
            $this->setAdditionalProperty('app_id', $value);
        }

        return $this->getAppId();
    }

    /**
     * Set app key.
     *
     * @param string $value
     *
     * @return string
     */
    public function setAppKey($value)
    {
        if (!AngieApplication::isOnDemand()) {
            $this->setAdditionalProperty('key', $value);
        }

        return $this->getAppKey();
    }

    /**
     * Set app secret.
     *
     * @param string $value
     */
    public function setAppSecret($value)
    {
        if (!AngieApplication::isOnDemand()) {
            $this->setAdditionalProperty('secret', $value);
        }
    }

    /**
     * Set app cluster.
     *
     * @param string $value
     *
     * @return string
     */
    public function setAppCluster($value)
    {
        if (!AngieApplication::isOnDemand()) {
            $this->setAdditionalProperty('cluster', $value);
        }

        return $this->getAppCluster();
    }

    /**
     * Return app id.
     *
     * @return int
     */
    public function getAppId()
    {
        return defined('PUSHER_APP_ID') && AngieApplication::isOnDemand()
            ? PUSHER_APP_ID
            : $this->getAdditionalProperty('app_id');
    }

    /**
     * Return app key.
     *
     * @return string
     */
    public function getAppKey()
    {
        return defined('PUSHER_KEY') && AngieApplication::isOnDemand()
            ? PUSHER_KEY
            : $this->getAdditionalProperty('key');
    }

    /**
     * Return app secret.
     *
     * @return string
     */
    protected function getAppSecret()
    {
        return defined('PUSHER_SECRET') && AngieApplication::isOnDemand()
            ? PUSHER_SECRET
            : $this->getAdditionalProperty('secret');
    }

    /**
     * Return app cluster.
     *
     * @return string
     */
    public function getAppCluster()
    {
        return defined('PUSHER_CLUSTER') && AngieApplication::isOnDemand()
            ? PUSHER_CLUSTER
            : $this->getAdditionalProperty('cluster');
    }

    /**
     * Return api host.
     *
     * @return string
     */
    private function getApiHost()
    {
        return defined('PUSHER_API_HOST') && AngieApplication::isOnDemand()
            ? PUSHER_API_HOST
            : $this->getAdditionalProperty('api_host');
    }

    /**
     * Set api host.
     *
     * @param  string $value
     * @return string
     */
    public function setApiHost($value)
    {
        if (!AngieApplication::isOnDemand()) {
            $this->setAdditionalProperty('api_host', $value);
        }

        return $this->getApiHost();
    }

    /**
     * Return api port.
     *
     * @return string
     */
    private function getApiPort()
    {
        return defined('PUSHER_API_PORT') && AngieApplication::isOnDemand()
            ? PUSHER_API_PORT
            : $this->getAdditionalProperty('api_port');
    }

    /**
     * Set api port.
     *
     * @param  string $value
     * @return string
     */
    public function setApiPort($value)
    {
        if (!AngieApplication::isOnDemand()) {
            $this->setAdditionalProperty('api_port', $value);
        }

        return $this->getApiPort();
    }

    /**
     * Return api url.
     *
     * @return string
     */
    public function getApiUrl()
    {
        $protocol = $this->getApiPort() === self::SECURE_PUSHER_API_PORT ? 'https://' : 'http://';
        $host = $this->getAppCluster() ? 'api-' . $this->getAppCluster() . '.pusher.com' : self::DEFAULT_PUSHER_API_HOST;

        return $protocol.$host;
    }

    /**
     * Return base path.
     *
     * @return string
     */
    public function getBasePath()
    {
        $app_id = $this->getAppId();

        return "/apps/{$app_id}";
    }

    /**
     * Return events path.
     *
     * @return string
     */
    public function getEventsPath()
    {
        return $this->getBasePath().'/events';
    }

    /**
     * Return array or property => value pairs that describes this object.
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return array_merge(parent::jsonSerialize(), [
            'app_key' => $this->getAppKey(),
            'app_cluster' => $this->getAppCluster(),
        ]);
    }

    /**
     * Return auth query string.
     *
     * @param string   $method
     * @param mixed    $payload
     * @param int|null $timestamp
     *
     * @return string
     */
    public function buildAuthQueryString($method, $payload, $timestamp = null)
    {
        return Pusher::build_auth_query_string(
            $this->getAppKey(),
            $this->getAppSecret(),
            strtoupper($method),
            $this->getEventsPath(),
            ['body_md5' => md5(json_encode($payload))],
            $this->getAuthVersion(),
            is_null($timestamp) ? DateTimeValue::now()->getTimestamp() : $timestamp
        );
    }

    /**
     * {@inheritdoc}
     */
    public function authOnChannel($channel_name, $socket_id, IUser $user, $user_info = [])
    {
        $pusher = new Pusher(
            $this->getAppKey(),
            $this->getAppSecret(),
            $this->getAppId(),
            [
                'cluster' => $this->getAppCluster(),
            ]
        );

        if (str_starts_with($channel_name, RealTimeIntegration::SOCKET_CHANNEL_PRIVATE)) {
            $encoded_string = $pusher->socket_auth($channel_name, $socket_id);
        } else {
            $encoded_string = $pusher->presence_auth(
                $channel_name,
                $socket_id,
                $user->getId(),
                $user_info
            );
        }

        return json_decode($encoded_string, true);
    }

    /**
     * {@inheritdoc}
     */
    public function isValidChannel(
        $channel_name,
        IUser $user,
        $is_on_demand,
        $account_id = null
    )
    {
        if (str_starts_with($channel_name, RealTimeIntegration::SOCKET_CHANNEL_PRIVATE)) {
            return $this->isValidPrivateChannel($is_on_demand, $user, $channel_name, $account_id);
        } elseif (str_starts_with($channel_name, RealTimeIntegration::SOCKET_CHANNEL_PRESENCE)) {
            return $this->isValidPresenceChannel($is_on_demand, $user, $channel_name, $account_id);
        }

        return false;
    }

    /**
     * @param  string   $is_on_demand
     * @param  User     $user
     * @param  string   $channel_name
     * @param  int|null $account_id
     * @return bool
     */
    private function isValidPrivateChannel($is_on_demand, User $user, $channel_name, $account_id = null)
    {
        $valid_channel = RealTimeIntegration::SOCKET_CHANNEL_PRIVATE . '-';

        if ($is_on_demand) {
            $valid_channel .= 'instance-' . $account_id . '-';
        }

        $valid_channel .= 'user-' . $user->getId();

        return $valid_channel === $channel_name;
    }

    /**
     * @param  string   $is_on_demand
     * @param  User     $user
     * @param  string   $channel_name
     * @param  int|null $account_id
     * @return bool
     */
    private function isValidPresenceChannel($is_on_demand, User $user, $channel_name, $account_id)
    {
        // "presence-instance-1-task-20" - example of on demand presence channel
        // "presence-task-20" - example of self hosted presence channel

        $bits = explode('-', $channel_name);

        $object_id = (int) array_pop($bits);
        $object_type = ucfirst((string) array_pop($bits)); // -task-

        if (!class_exists($object_type) || !is_subclass_of($object_type, DataObject::class)) {
            return false;
        }

        $object = DataObjectPool::get($object_type, $object_id);

        if (!($object instanceof IProjectElement) || !$object->canView($user)) {
            return false;
        }

        if ($is_on_demand) {
            $instance_id = (int) array_pop($bits);

            return $instance_id === $account_id;
        }

        return true;
    }
}
