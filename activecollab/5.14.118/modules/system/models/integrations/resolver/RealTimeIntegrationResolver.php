<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

class RealTimeIntegrationResolver implements RealTimeIntegrationResolverInterface
{
    /**
     * Get active real time integration.
     *
     * @return mixed|null
     */
    public function getActiveRealTimeIntegration()
    {
        foreach ($this->getAvailableIntegrations() as $integration) {
            if ($integration->isInUse()) {
                return $integration;
            }
        }

        return null;
    }

    /**
     * Get available real time integrations.
     *
     * @return array
     */
    private function getAvailableIntegrations()
    {
        return [
            Integrations::findFirstByType(PusherIntegration::class),
        ];
    }
}
