<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Google Drive integrations class.
 *
 * @package ActiveCollab.modules.system
 * @subpackage models
 */
class GoogleDriveIntegration extends FwGoogleDriveIntegration
{
}
