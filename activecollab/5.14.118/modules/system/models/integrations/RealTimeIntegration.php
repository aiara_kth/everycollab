<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

abstract class RealTimeIntegration extends Integration
{
    const JOBS_QUEUE_CHANNEL = 'socket';
    const SOCKET_CHANNEL_PRIVATE = 'private';
    const SOCKET_CHANNEL_PRESENCE = 'presence';

    public function isSingleton()
    {
        return true;
    }

    public function getDescription()
    {
        return lang('Push events to everyone using ActiveCollab, so their pages update without the need to hit refresh.');
    }

    /**
     * Get socket by integration.
     */
    abstract public function getSocket();

    /**
     * Authenticate on channel.
     *
     * @param  string $channel_name
     * @param  mixed  $socket_id
     * @param  IUser  $user
     * @param  array  $user_info
     * @return mixed
     */
    abstract public function authOnChannel(
        $channel_name,
        $socket_id,
        IUser $user,
        $user_info = []
    );

    /**
     * Check if channel is valid.
     *
     * @param  string   $channel_name
     * @param  IUser    $user
     * @param  bool     $is_on_demand
     * @param  int|null $account_id
     * @return bool
     */
    abstract public function isValidChannel(
        $channel_name,
        IUser $user,
        $is_on_demand,
        $account_id = null
    );
}
