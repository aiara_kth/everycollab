<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Project template class.
 *
 * @package ActiveCollab.modules.system
 * @subpackage models
 */
class ProjectTemplate extends BaseProjectTemplate implements IRoutingContext, IProjectBasedOn, ICreatedBy
{
    use IRoutingContextImplementation;

    /**
     * @var int[]
     */
    private $last_project_day = [];

    /**
     * Cached day numbers and date values.
     *
     * @var string[]
     */
    private $project_day_dates = [];

    /**
     * @var array
     */
    private $elements_with_attachments = false;

    // ---------------------------------------------------
    //  Interface implementations
    // ---------------------------------------------------
    /**
     * @var array
     */
    private $element_with_labels = false;

    /**
     * @return bool
     */
    public function isDraft()
    {
        return !$this->getName();
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $result = parent::jsonSerialize();

        if (empty($result['name'])) {
            $result['name'] = '';
            $result['is_draft'] = true;
        } else {
            $result['is_draft'] = false;
        }

        $result['is_scheduled'] = $this->isScheduled();

        $result['count_recurring_task'] = ProjectTemplateElements::countByProjectTemplate($this, 'ProjectTemplateRecurringTask');
        $result['count_task_lists'] = ProjectTemplateElements::countByProjectTemplate($this, 'ProjectTemplateTaskList');
        $result['count_tasks'] = ProjectTemplateElements::countByProjectTemplate($this, 'ProjectTemplateTask');
        $result['count_subtasks'] = ProjectTemplateElements::countByProjectTemplate($this, 'ProjectTemplateSubtask');
        $result['count_discussions'] = ProjectTemplateElements::countByProjectTemplate($this, 'ProjectTemplateDiscussion');
        $result['count_files'] = ProjectTemplateElements::countByProjectTemplate($this, 'ProjectTemplateFile');
        $result['count_notes'] = ProjectTemplateElements::countByProjectTemplate($this, 'ProjectTemplateNote');

        return $result;
    }

    /**
     * Check if is one or more object of this template scheduled.
     *
     * @return bool
     */
    public function isScheduled()
    {
        return AngieApplication::cache()->getByObject($this, 'is_scheduled', function () {
            if ($rows = DB::execute('SELECT raw_additional_properties FROM project_template_elements WHERE template_id = ? AND raw_additional_properties LIKE ?', $this->getId(), '%due_on%')) {
                foreach ($rows as $row) {
                    $attributes = unserialize($row['raw_additional_properties']);

                    if (isset($attributes['due_on']) && $attributes['due_on'] > 0) {
                        return true;
                    }
                }
            }

            return false;
        });
    }

    /**
     * Return routing context name.
     *
     * @return string
     */
    public function getRoutingContext()
    {
        return 'project_template';
    }

    /**
     * Return routing context parameters.
     *
     * @return mixed
     */
    public function getRoutingContextParams()
    {
        return ['project_template_id' => $this->getId()];
    }

    /**
     * Copy template objects to destination project.
     *
     * @param  Project   $to
     * @param  User      $by
     * @param  DateValue $first_day
     * @throws Exception
     */
    public function copyItems(Project &$to, User $by, $first_day = null)
    {
        if (empty($first_day)) {
            $first_day = DateValue::now();
        }

        DB::transact(function () use (&$to, $by, $first_day) {
            if (count($this->getMemberIds())) {
                $members_to_add = Users::findByIds($this->getMemberIds());

                if (count($members_to_add)) {
                    $to->addMembers($members_to_add, ['by' => $by]);
                }
            }

            $first_day_mysql = $first_day->toMySQL();

            $task_lists = $tasks = $subtasks = $disucssions = $files = $note_groups = $notes = $recurring_tasks = [];

            if ($element_rows = DB::execute('SELECT id, type, name, body, raw_additional_properties, position FROM project_template_elements WHERE template_id = ? ORDER BY id', $this->getId())) {
                foreach ($element_rows as $element_row) {
                    switch ($element_row['type']) {
                        case 'ProjectTemplateTaskList':
                            $task_lists[] = $this->taskListFromRow($first_day_mysql, $element_row);
                            break;
                        case 'ProjectTemplateTask':
                            $tasks[] = $this->taskFromRow($first_day_mysql, $element_row);
                            break;
                        case 'ProjectTemplateSubtask':
                            $subtasks[] = $this->subtaskFromRow($first_day_mysql, $element_row);
                            break;
                        case 'ProjectTemplateRecurringTask':
                            $recurring_tasks[] = $this->recurringTaskFromRow($first_day_mysql, $element_row);
                            break;
                        case 'ProjectTemplateDiscussion':
                            $disucssions[] = $this->discussionFromRow($element_row);
                            break;
                        case 'ProjectTemplateNoteGroup':
                            $note_groups[] = $this->noteGroupFromRow($element_row);
                            break;
                        case 'ProjectTemplateNote':
                            $notes[] = $this->noteFromRow($element_row);
                            break;
                        case 'ProjectTemplateFile':
                            $files[] = $this->fileFromRow($element_row);
                            break;
                    }
                }

                $assignees_map = [];

                foreach ($to->getMembers() as $member) {
                    $assignees_map[$member->getId()] = $member;
                }

                $task_lists_map = $this->createTaskLists($to, $first_day, $first_day_mysql, $task_lists);
                $tasks_map = $this->createTasks($to, $first_day, $first_day_mysql, $assignees_map, $task_lists_map, $tasks);
                $this->createRecurringTasks($to, $first_day, $first_day_mysql, $assignees_map, $task_lists_map, $recurring_tasks);
                $this->createSubtasks($to, $first_day, $first_day_mysql, $assignees_map, $tasks_map, $subtasks);
                $this->createDiscussions($to, $disucssions);
                $note_groups_map = $this->createNoteGroups($to, $note_groups);
                $this->createNotes($to, $notes, $note_groups_map);
                $this->createFiles($to, $files);
            }
        });
    }

    /**
     * Load task list details from element row.
     *
     * @param  string $first_day_mysql
     * @param  array  $row
     * @return array
     */
    private function taskListFromRow($first_day_mysql, $row)
    {
        $attributes = $row['raw_additional_properties'] ? unserialize($row['raw_additional_properties']) : [];

        $due_on = isset($attributes['due_on']) ? (int) $attributes['due_on'] : 0;

        if ($due_on) {
            $this->pushLastProjectDay($first_day_mysql, $due_on);
        }

        return [
            'id' => $row['id'],
            'name' => $row['name'],
            'start_on' => isset($attributes['start_on']) ? (int) $attributes['start_on'] : 0,
            'due_on' => $due_on,
            'position' => empty($row['position']) ? 1 : $row['position'],
        ];
    }

    /**
     * Push last project data.
     *
     * @param string $first_day_mysql
     * @param int    $day
     */
    private function pushLastProjectDay($first_day_mysql, $day)
    {
        if (empty($day)) {
            return;
        }

        if (empty($this->last_project_day[$first_day_mysql]) || $this->last_project_day[$first_day_mysql] < $day) {
            $this->last_project_day[$first_day_mysql] = $day;
        }
    }

    /**
     * Prepare a task record based on task template row.
     *
     * @param  string $first_day_mysql
     * @param  array  $row
     * @return array
     */
    private function taskFromRow($first_day_mysql, $row)
    {
        $attributes = $row['raw_additional_properties'] ? unserialize($row['raw_additional_properties']) : [];

        $start_on = isset($attributes['start_on']) ? (int) $attributes['start_on'] : 0;
        $due_on = isset($attributes['due_on']) ? (int) $attributes['due_on'] : 0;

        if ($due_on) {
            $this->pushLastProjectDay($first_day_mysql, $due_on);
        }

        return [
            'id' => $row['id'],
            'name' => $row['name'],
            'body' => $row['body'],
            'task_list_id' => isset($attributes['task_list_id']) ? (int) $attributes['task_list_id'] : 0,
            'assignee_id' => isset($attributes['assignee_id']) ? (int) $attributes['assignee_id'] : 0,
            'job_type_id' => isset($attributes['job_type_id']) ? (int) $attributes['job_type_id'] : 0,
            'estimate' => isset($attributes['estimate']) ? (float) $attributes['estimate'] : 0.0,
            'start_on' => $start_on > 0 ? $start_on : $due_on, // if not set, $start_on need to be same as $due_on
            'due_on' => $due_on,
            'is_important' => isset($attributes['is_important']) && $attributes['is_important'],
            'is_hidden_from_clients' => isset($attributes['is_hidden_from_clients']) && $attributes['is_hidden_from_clients'],
            'position' => $row['position'],
        ];
    }

    /**
     * Prepare a task record based on recurring task template row.
     *
     * @param  string $first_day_mysql
     * @param  array  $row
     * @return array
     */
    private function recurringTaskFromRow($first_day_mysql, $row)
    {
        $attributes = $row['raw_additional_properties'] ? unserialize($row['raw_additional_properties']) : [];

        if ($attributes['due_in'] == '') {
            $due_in = null;
            $start_in = null;
        } else {
            $due_in = isset($attributes['due_in']) ? (int) $attributes['due_in'] : 0;
            $start_in = isset($attributes['start_in']) ? (int) $attributes['start_in'] : 0;
        }

        return [
            'id' => $row['id'],
            'name' => $row['name'],
            'body' => $row['body'],
            'task_list_id' => isset($attributes['task_list_id']) ? (int) $attributes['task_list_id'] : 0,
            'assignee_id' => isset($attributes['assignee_id']) ? (int) $attributes['assignee_id'] : 0,
            'is_important' => isset($attributes['is_important']) && $attributes['is_important'],
            'is_hidden_from_clients' => isset($attributes['is_hidden_from_clients']) && $attributes['is_hidden_from_clients'],
            'position' => $row['position'],
            'subtasks' => $attributes['subtasks'],
            'repeat_frequency' => $attributes['repeat_frequency'],
            'repeat_amount' => $attributes['repeat_amount'],
            'start_in' => $start_in > 0 ? $start_in : $due_in, // if not set, $start_in need to be same as $due_in
            'due_in' => $due_in,
        ];
    }

    /**
     * Load subtask details from element row.
     *
     * @param  string $first_day_mysql
     * @param  array  $row
     * @return array
     */
    private function subtaskFromRow($first_day_mysql, $row)
    {
        $attributes = $row['raw_additional_properties'] ? unserialize($row['raw_additional_properties']) : [];

        $due_on = isset($attributes['due_on']) ? (int) $attributes['due_on'] : 0;

        if ($due_on) {
            $this->pushLastProjectDay($first_day_mysql, $due_on);
        }

        return [
            'id' => $row['id'],
            'task_id' => isset($attributes['task_id']) ? (int) $attributes['task_id'] : 0,
            'body' => $row['body'],
            'due_on' => $due_on,
            'assignee_id' => isset($attributes['assignee_id']) ? (int) $attributes['assignee_id'] : 0,
            'position' => $row['position'],
        ];
    }

    /**
     * Load discussion details from element row.
     *
     * @param  array $row
     * @return array
     */
    private function discussionFromRow($row)
    {
        $attributes = $row['raw_additional_properties'] ? unserialize($row['raw_additional_properties']) : [];

        return [
            'id' => $row['id'],
            'name' => $row['name'],
            'body' => $row['body'],
            'is_hidden_from_clients' => isset($attributes['is_hidden_from_clients']) && $attributes['is_hidden_from_clients'],
            'position' => $row['position'],
        ];
    }

    /**
     * Load note group details from element row.
     *
     * @param  array $row
     * @return array
     */
    public function noteGroupFromRow($row)
    {
        return [
            'id' => $row['id'],
            'position' => $row['position'],
        ];
    }

    /**
     * Load note details from element row.
     *
     * @param  array $row
     * @return array
     */
    private function noteFromRow($row)
    {
        $attributes = $row['raw_additional_properties'] ? unserialize($row['raw_additional_properties']) : [];

        return [
            'id' => $row['id'],
            'name' => $row['name'],
            'body' => $row['body'],
            'note_group_id' => isset($attributes['note_group_id']) ? (int) $attributes['note_group_id'] : 0,
            'is_hidden_from_clients' => isset($attributes['is_hidden_from_clients']) && $attributes['is_hidden_from_clients'],
            'position' => $row['position'],
        ];
    }

    /**
     * Load discussion details from element row.
     *
     * @param  array $row
     * @return array
     */
    private function fileFromRow($row)
    {
        $attributes = $row['raw_additional_properties'] ? unserialize($row['raw_additional_properties']) : [];

        $result = [
            'id' => $row['id'],
            'name' => $row['name'],
            'type' => isset($attributes['type']) ? $attributes['type'] : '',
            'location' => isset($attributes['location']) ? $attributes['location'] : '',
            'mime_type' => isset($attributes['mime_type']) ? $attributes['mime_type'] : '',
            'size' => isset($attributes['size']) ? (int) $attributes['size'] : 0,
            'is_hidden_from_clients' => isset($attributes['is_hidden_from_clients']) && $attributes['is_hidden_from_clients'],
            'position' => $row['position'],
            'md5' => $attributes['md5'],
        ];

        if (isset($attributes['url'])) {
            $result['url'] = $attributes['url'];
        }

        return $result;
    }

    /**
     * Create task lists using task list element data and return element -> task list ID-s map.
     *
     * @param  Project           $project
     * @param  DateValue         $first_day
     * @param  string            $first_day_mysql
     * @param  array             $task_lists
     * @return array
     * @throws InvalidParamError
     */
    private function createTaskLists(Project &$project, DateValue $first_day, $first_day_mysql, array $task_lists)
    {
        $project_id = $project->getId();

        $task_lists_map = [];

        foreach ($task_lists as $k) {
            $task_list = TaskLists::create([
                'project_id' => $project_id,
                'name' => $k['name'],
                'start_on' => $this->getProjectDayDate($first_day, $first_day_mysql, $k['start_on']),
                'due_on' => $this->getProjectDayDate($first_day, $first_day_mysql, $k['due_on']),
                'position' => $k['position'],
            ]);

            if ($task_list instanceof TaskList) {
                $task_lists_map[$k['id']] = $task_list->getId();
            }
        }

        return $task_lists_map;
    }

    /**
     * Get day date.
     *
     * @param  DateValue   $first_day
     * @param  string      $first_day_mysql
     * @param  int         $day
     * @return string|null
     */
    private function getProjectDayDate(DateValue $first_day, $first_day_mysql, $day)
    {
        if ($day < 1) {
            return null;
        }

        $last_project_day = $this->getLastProjectDay($first_day_mysql);

        if (empty($this->project_day_dates[$first_day_mysql]) && $last_project_day) {
            $this->project_day_dates[$first_day_mysql][1] = $first_day_mysql;

            $current_day = 2;
            $reference = DateValue::makeFromTimestamp($first_day->getTimestamp());

            while ($current_day <= $last_project_day) {
                do {
                    $reference->advance(86400);
                } while (!$reference->isWorkday() || $reference->isDayOff());

                $this->project_day_dates[$first_day_mysql][$current_day] = $reference->toMySQL();

                ++$current_day;
            }
        }

        return $this->project_day_dates[$first_day_mysql][$day];
    }

    /**
     * Return max due on that we got while loading data.
     *
     * @param  string $first_day_mysql
     * @return int
     */
    public function getLastProjectDay($first_day_mysql)
    {
        return isset($this->last_project_day[$first_day_mysql]) ? $this->last_project_day[$first_day_mysql] : 0;
    }

    // ---------------------------------------------------
    //  Project last day
    // ---------------------------------------------------

    /**
     * Create tasks using task element data and return element -> task ID-s map.
     *
     * @param  Project           $project
     * @param  DateValue         $first_day
     * @param  string            $first_day_mysql
     * @param  array             $assignees_map
     * @param  array             $task_lists_map
     * @param  array             $tasks
     * @return array
     * @throws InvalidParamError
     */
    private function createTasks(Project &$project, DateValue $first_day, $first_day_mysql, array &$assignees_map, array $task_lists_map, array $tasks)
    {
        $project_id = $project->getId();

        usort($tasks, function ($a, $b) {
            if ($a['position'] == $b['position']) {
                return 0;
            }

            return ($a['position'] < $b['position']) ? -1 : 1;
        });

        $tasks_map = [];

        $position = 1;

        $task_list = TaskLists::getFirstTaskList($project);

        foreach ($tasks as $k) {
            $job_type_id = $k['job_type_id'];
            $estimate = $k['estimate'];

            if ($job_type_id <= 0 || $estimate <= 0) {
                $job_type_id = $estimate = 0;
            }

            $task = Tasks::create([
                'project_id' => $project_id,
                'name' => $k['name'],
                'body' => $k['body'],
                'task_list_id' => isset($task_lists_map[$k['task_list_id']]) && $task_lists_map[$k['task_list_id']] ? $task_lists_map[$k['task_list_id']] : $task_list->getId(),
                'assignee_id' => $this->getAssigneeId($k['assignee_id'], $assignees_map, $project),
                'start_on' => $this->getProjectDayDate($first_day, $first_day_mysql, $k['start_on']),
                'due_on' => $this->getProjectDayDate($first_day, $first_day_mysql, $k['due_on']),
                'job_type_id' => $job_type_id,
                'estimate' => $estimate,
                'is_important' => $k['is_important'],
                'is_hidden_from_clients' => $k['is_hidden_from_clients'],
                'position' => $position++,
                'notify_subscribers' => false,
            ]);

            if ($task instanceof Task) {
                $this->cloneElementAttachments(['ProjectTemplateTask', $k['id']], $task);
                $this->cloneTaskLabels(['ProjectTemplateTask', $k['id']], $task);

                $tasks_map[$k['id']] = $task->getId();
            }
        }

        return $tasks_map;
    }

    /**
     * Create recurring tasks using recuriing task element data and return element -> recurring task ID-s map.
     *
     * @param  Project   $project
     * @param  DateValue $first_day
     * @param  string    $first_day_mysql
     * @param  array     $assignees_map
     * @param  array     $task_lists_map
     * @param  array     $recurring_tasks
     * @return array
     */
    public function createRecurringTasks(Project &$project, DateValue $first_day, $first_day_mysql, array &$assignees_map, array $task_lists_map, array $recurring_tasks)
    {
        $project_id = $project->getId();

        usort($recurring_tasks, function ($a, $b) {
            if ($a['position'] == $b['position']) {
                return 0;
            }

            return ($a['position'] < $b['position']) ? -1 : 1;
        });

        $recurring_tasks_map = [];

        $position = 1;

        $task_list = TaskLists::getFirstTaskList($project);

        foreach ($recurring_tasks as $k) {

            //$due_in = isset($attributes['due_in']) ? (integer) $attributes['due_in'] : 0;
            //$start_in = isset($attributes['start_in']) ? (integer) $attributes['start_in'] : 0;

            /** @var ProjectTemplateRecurringTask $element_instance */
            $element_instance = DataObjectPool::get('ProjectTemplateRecurringTask', $k['id']);

            // Prepare labels
            /* @var Label $label */
            $labels = $element_instance->getLabels();
            $recurring_task_labels = [];

            if (!empty($labels)) {
                foreach ($labels as $label) {
                    $recurring_task_labels[] = $label->getName();
                }
            }

            // Prepare attachments
            $attachments = $element_instance->getAttachments();
            $recurring_task_attachments = [];

            if (!empty($attachments)) {
                /** @var Attachment $attachment */
                foreach ($attachments as $attachment) {
                    $recurring_task_attachments[] = ['id' => $attachment->getId()];
                }
            }

            $recurring_task = RecurringTasks::create([
                'project_id' => $project_id,
                'name' => $k['name'],
                'body' => $k['body'],
                'task_list_id' => isset($task_lists_map[$k['task_list_id']]) && $task_lists_map[$k['task_list_id']] ? $task_lists_map[$k['task_list_id']] : $task_list->getId(),
                'assignee_id' => $this->getAssigneeId($k['assignee_id'], $assignees_map, $project),
                'is_important' => $k['is_important'],
                'is_hidden_from_clients' => $k['is_hidden_from_clients'],
                'position' => $position++,
                'subtasks' => $k['subtasks'],
                'repeat_frequency' => $k['repeat_frequency'],
                'repeat_amount' => $k['repeat_amount'],
                'start_in' => $k['start_in'],
                'due_in' => $k['due_in'],
                'labels' => $recurring_task_labels,
                'attachments' => $recurring_task_attachments,
            ]);

            if ($recurring_task instanceof RecurringTask) {
                $recurring_tasks_map[$k['id']] = $recurring_task->getId();
            }
        }

        return $recurring_tasks_map;
    }

    /**
     * @param  int     $assignee_id
     * @param  array   $assignees_map
     * @param  Project $project
     * @return int
     */
    private function getAssigneeId($assignee_id, array &$assignees_map, Project &$project)
    {
        if ($assignee_id && empty($assignees_map[$assignee_id])) {
            $assignee = DataObjectPool::get('User', $assignee_id);

            if ($assignee instanceof User && $assignee->isActive()) {
                if (!$project->isMember($assignee)) {
                    $project->addMembers([$assignee]);
                }

                $assignees_map[$assignee_id] = $assignee;
            } else {
                $assignees_map[$assignee_id] = 'skip';
            }
        }

        return $assignee_id && isset($assignees_map[$assignee_id]) && $assignees_map[$assignee_id] instanceof User ? $assignee_id : 0;
    }

    /**
     * Clone element attachments in the most officient way.
     *
     * @param  array|ProjectTemplateElement $element
     * @param  IAttachments                 $to
     * @throws Exception
     * @throws InvalidParamError
     */
    private function cloneElementAttachments($element, IAttachments &$to)
    {
        if ($this->shouldCloneAttachments($element)) {
            $element_instance = DataObjectPool::get($element[0], $element[1]);

            if ($element_instance instanceof IAttachments) {
                $element_instance->cloneAttachmentsTo($to);

                if (method_exists($to, 'getProjectId')) {
                    DB::execute('UPDATE attachments SET project_id = ? WHERE parent_type = ? AND parent_id = ?', $to->getProjectId(), get_class($to), $to->getId());
                }
            }
        }
    }

    /**
     * Return true if we should clone attachments for the given element.
     *
     * @param  array|ProjectTemplateElement $element
     * @return bool
     * @throws InvalidParamError
     */
    public function shouldCloneAttachments($element)
    {
        if ($element instanceof ProjectTemplateElement || $element instanceof ProjectTemplateRecurringTask) {
            $element_type = get_class($element);
            $element_id = $element->getId();
        } elseif (is_array($element) && count($element)) {
            list($element_type, $element_id) = $element;
        } else {
            throw new InvalidParamError('element', $element, 'Expected ProjectTemplateElement instance or type - ID array');
        }

        if ($this->elements_with_attachments === false) {
            $this->elements_with_attachments = [];

            if ($element_ids = DB::executeFirstColumn('SELECT id FROM project_template_elements WHERE template_id = ?', $this->getId())) {
                if ($rows = DB::execute('SELECT parent_type, parent_id FROM attachments WHERE parent_type IN (?) AND parent_id IN (?)', ProjectTemplateElements::getAvailableElementClasses(), $element_ids)) {
                    foreach ($rows as $row) {
                        if (empty($this->elements_with_attachments[$row['parent_type']])) {
                            $this->elements_with_attachments[$row['parent_type']] = [];
                        }

                        $this->elements_with_attachments[$row['parent_type']][] = $row['parent_id'];
                    }
                }
            }
        }

        return !empty($this->elements_with_attachments[$element_type]) && is_array($this->elements_with_attachments[$element_type]) && in_array($element_id, $this->elements_with_attachments[$element_type]);
    }

    /**
     * Clone task labels from $element to task.
     *
     * @param  ProjectTemplateTask|array $element
     * @param  ILabels                   $to
     * @throws InvalidParamError
     */
    private function cloneTaskLabels($element, ILabels &$to)
    {
        if ($this->shouldCloneLabels($element)) {
            $element_instance = DataObjectPool::get($element[0], $element[1]);

            if ($element_instance instanceof ILabels) {
                $element_instance->cloneLabelsTo($to);
            }
        }
    }

    /**
     * Return true if we should clone labels for the given task.
     *
     * @param  array|ProjectTemplateTask $element
     * @return bool
     * @throws InvalidParamError
     */
    public function shouldCloneLabels($element)
    {
        if ($element instanceof ProjectTemplateTask || $element instanceof ProjectTemplateRecurringTask) {
            $element_id = $element->getId();
            $element_type = get_class($element);
        } elseif (is_array($element) && count($element)) {
            list($element_type, $element_id) = $element;
        } else {
            throw new InvalidParamError('element', $element, 'Expected ProjectTemplateTask instance or type - ID array');
        }

        if ($this->element_with_labels === false) {
            $this->element_with_labels = DB::executeFirstColumn('SELECT DISTINCT parent_id AS "id" FROM parents_labels WHERE parent_type = ? AND parent_id IN (SELECT id FROM project_template_elements WHERE template_id = ?)', $element_type, $this->getId());

            if (empty($this->element_with_labels)) {
                $this->element_with_labels = [];
            }
        }

        return in_array($element_id, $this->element_with_labels);
    }

    /**
     * Create subtasks using template data.
     *
     * @param  Project           $project
     * @param  DateValue         $first_day
     * @param  string            $first_day_mysql
     * @param  array             $assignees_map
     * @param  array             $tasks_map
     * @param  array             $subtasks
     * @return array
     * @throws InvalidParamError
     */
    private function createSubtasks(Project &$project, DateValue $first_day, $first_day_mysql, array &$assignees_map, array $tasks_map, array $subtasks)
    {
        usort($subtasks, function ($a, $b) {
            if ($a['position'] == $b['position']) {
                return 0;
            }

            return ($a['position'] < $b['position']) ? -1 : 1;
        });

        foreach ($subtasks as $k) {
            $task_id = $k['task_id'];

            if (empty($tasks_map[$task_id])) {
                continue;
            }

            Subtasks::create([
                'task_id' => $tasks_map[$task_id],
                'body' => $k['body'],
                'assignee_id' => $this->getAssigneeId($k['assignee_id'], $assignees_map, $project),
                'due_on' => $this->getProjectDayDate($first_day, $first_day_mysql, $k['due_on']),
            ]);
        }
    }

    /**
     * Create discussions using template data.
     *
     * @param  Project           $project
     * @param  array             $discussions
     * @return array
     * @throws InvalidParamError
     */
    private function createDiscussions(Project &$project, array $discussions)
    {
        usort($discussions, function ($a, $b) {
            if ($a['position'] == $b['position']) {
                return 0;
            }

            return ($a['position'] < $b['position']) ? -1 : 1;
        });

        $project_id = $project->getId();

        foreach ($discussions as $k) {
            $discussion = Discussions::create([
                'project_id' => $project_id,
                'name' => $k['name'],
                'body' => $k['body'],
                'is_hidden_from_clients' => $k['is_hidden_from_clients'],
            ]);

            if ($discussion instanceof Discussion) {
                $this->cloneElementAttachments(['ProjectTemplateDiscussion', $k['id']], $discussion);
            }
        }
    }

    /**
     * Create note groups using template data.
     *
     * @param  Project           &$project
     * @param  array             $note_groups
     * @return array
     * @throws InvalidParamError
     */
    public function createNoteGroups(Project &$project, array $note_groups)
    {
        usort($note_groups, function ($a, $b) {
            if ($a['position'] == $b['position']) {
                return 0;
            }

            return ($a['position'] < $b['position']) ? -1 : 1;
        });

        $note_groups_map = [];
        $project_id = $project->getId();

        foreach ($note_groups as $k) {
            $note_group = NoteGroups::create(['project_id' => $project_id]);

            if ($note_group instanceof NoteGroup) {
                $note_groups_map[$k['id']] = $note_group->getId();
            }
        }

        return $note_groups_map;
    }

    /**
     * Create subtasks using task list element data.
     *
     * @param  Project           $project
     * @param  array             $notes
     * @param  array             $note_groups_map
     * @return array
     * @throws InvalidParamError
     */
    private function createNotes(Project &$project, array $notes, array $note_groups_map)
    {
        $project_id = $project->getId();

        $grouped_notes = [];

        foreach ($notes as $k => $v) {
            if ($v['note_group_id']) {
                $grouped_notes[] = $v;
                unset($notes[$k]);
            }
        }

        // Sort in revers order and let the project handle position values
        usort($notes, function ($a, $b) {
            if ($a['position'] == $b['position']) {
                return 0;
            }

            return ($a['position'] > $b['position']) ? -1 : 1;
        });

        foreach ($notes as $k) {
            $note = Notes::create([
                'project_id' => $project_id,
                'name' => $k['name'],
                'body' => $k['body'],
                'is_hidden_from_clients' => $k['is_hidden_from_clients'],
            ]);

            if ($note instanceof Note) {
                $this->cloneElementAttachments(['ProjectTemplateNote', $k['id']], $note);
            }
        }

        // Sort in proper order
        usort($grouped_notes, function ($a, $b) {
            if ($a['position'] == $b['position']) {
                return 0;
            }

            return ($a['position'] < $b['position']) ? -1 : 1;
        });

        foreach ($grouped_notes as $k) {
            $note_group_id = $k['note_group_id'];

            if (empty($note_groups_map[$note_group_id])) {
                continue;
            }

            $note = Notes::create([
                'project_id' => $project_id,
                'note_group_id' => $note_groups_map[$note_group_id],
                'name' => $k['name'],
                'body' => $k['body'],
                'is_hidden_from_clients' => $k['is_hidden_from_clients'],
            ]);

            if ($note instanceof Note) {
                $this->cloneElementAttachments(['ProjectTemplateNote', $k['id']], $note);
            }
        }
    }

    /**
     * Create projects using template data.
     *
     * @param  Project           $project
     * @param  array             $files
     * @return array
     * @throws InvalidParamError
     */
    private function createFiles(Project &$project, array $files)
    {
        usort($files, function ($a, $b) {
            if ($a['position'] == $b['position']) {
                return 0;
            }

            return ($a['position'] > $b['position']) ? -1 : 1; // Reverse order
        });

        $project_id = $project->getId();

        foreach ($files as $k) {
            $class = $k['type'];

            if (class_exists($class)) {
                $file = new $class();

                $new_location = $k['location'];

                if ($file instanceof LocalFile) {
                    $file_path = AngieApplication::fileLocationToPath($k['location']);
                    if (is_file($file_path)) {
                        $new_location = AngieApplication::storeFile($file_path)[1];
                    }
                } elseif ($file instanceof WarehouseFile) {
                    /* @var WarehouseIntegration $warehouse_integration */
                    $warehouse_integration = Integrations::findFirstByType(WarehouseIntegration::class);
                    $new_file = $warehouse_integration
                        ->getFileApi()
                        ->duplicateFile($warehouse_integration->getStoreId(), $new_location);
                    $new_location = $new_file->getLocation();
                } else {
                    $new_location = null;
                }

                $additional_properties = [];

                if (isset($k['url'])) {
                    $additional_properties['url'] = $k['url'];
                }

                $file->setAttributes([
                    'type' => $class,
                    'project_id' => $project_id,
                    'name' => $k['name'],
                    'location' => $new_location,
                    'mime_type' => $k['mime_type'],
                    'size' => $k['size'],
                    'is_hidden_from_clients' => $k['is_hidden_from_clients'],
                    'md5' => $k['md5'],
                    'raw_additional_properties' => !empty($additional_properties) ? serialize($additional_properties) : null,
                ]);
                $file->save();
            }
        }
    }

    /**
     * Delete project template.
     *
     * @param  bool      $bulk
     * @throws Exception
     */
    public function delete($bulk = false)
    {
        try {
            DB::beginWork('Begin: drop project template @ ' . __CLASS__);

            /** @var ProjectTemplateElement[] $elements */
            if ($elements = ProjectTemplateElements::find(['conditions' => ['template_id = ?', $this->getId()]])) {
                foreach ($elements as $element) {
                    $element->delete();
                }
            }

            if ($project_ids = DB::executeFirstColumn('SELECT id FROM projects WHERE template_id = ?', $this->getId())) {
                DB::execute('UPDATE projects SET template_id = ?, updated_on = UTC_TIMESTAMP() WHERE id IN (?)', 0, $project_ids);
                Projects::clearCacheFor($project_ids);
            }

            parent::delete();

            DB::commit('Done: drop project template @ ' . __CLASS__);
        } catch (Exception $e) {
            DB::rollback('Rollback: drop project template @ ' . __CLASS__);
            throw $e;
        }
    }

    /**
     * @param  User $user
     * @return bool
     */
    public function canView(User $user)
    {
        return $user->isOwner() || $user->isPowerUser();
    }

    /**
     * @param  User $user
     * @return bool
     */
    public function canTrash(User $user)
    {
        return $user->isOwner() || $user->isPowerUser();
    }

    /**
     * @param  User $user
     * @return bool
     */
    public function canDelete(User $user)
    {
        return $user->isOwner();
    }
}
