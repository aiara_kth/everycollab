<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Interface that all user instances need to implement.
 *
 * @package angie.frameworks.authentication
 * @subpackage models
 */
interface IUser extends \ActiveCollab\User\UserInterface
{
    /**
     * Return user ID.
     *
     * @return int
     */
    public function getId();

    /**
     * Return name of this user.
     *
     * @return string
     */
    public function getName();

    /**
     * Return email address of a given user.
     *
     * @return string
     */
    public function getEmail();

    /**
     * Return display name of this user.
     *
     * @param bool $short
     */
    public function getDisplayName($short = false);

    /**
     * Return first name of this user.
     *
     * @return string
     */
    public function getFirstName();

    /**
     * Return language instance.
     *
     * In case user is using default language, system will return NULL
     *
     * @return Language
     */
    public function getLanguage();

    /**
     * Return date format.
     *
     * @return string
     */
    public function getDateFormat();

    /**
     * Return time format.
     *
     * @return string
     */
    public function getTimeFormat();

    /**
     * Return date time format.
     *
     * @return string
     */
    public function getDateTimeFormat();

    /**
     * Returns true if this user has access to reports section.
     *
     * @return bool
     */
    public function canUseReports();

    /**
     * Returns true if this particular account is active.
     *
     * @return bool
     */
    public function isActive();

    /**
     * Return true if this instance is member.
     *
     * @param  bool $explicit
     * @return bool
     */
    public function isMember($explicit = false);

    /**
     * Returns true if this user has final management permissions.
     *
     * @param  bool $explicit
     * @return bool
     */
    public function isFinancialManager($explicit = false);

    /**
     * Returns true only if this person is application owner.
     *
     * @return bool
     */
    public function isOwner();

    /**
     * Return view URL.
     *
     * @return string
     */
    public function getViewUrl();

    /**
     * Return user avatar URL.
     *
     * @param  string|int $size
     * @return string
     */
    public function getAvatarUrl($size = '--SIZE--');
}
