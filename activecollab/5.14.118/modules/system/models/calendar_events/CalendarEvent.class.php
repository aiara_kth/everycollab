<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Application level calendar event instance implementation.
 *
 * @package ActiveCollab.modules.system
 * @subpackage models
 */
class CalendarEvent extends FwCalendarEvent
{
}
