<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * TestDataObjects class.
 *
 * @package ActiveCollab.modules.system
 * @subpackage models
 */
class TestDataObjects extends FwTestDataObjects
{
}
