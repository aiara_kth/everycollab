<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * @package angie.frameworks.environment
 * @subpackage models
 */
class PusherSocketPayloadTransformator extends WebhookPayloadTransformator
{
    /**
     * Event data payload limit for pusher is 10kb (https://pusher.com/docs/rest_api#method-post-event).
     */
    const PUSHER_PAYLOAD_LIMIT = 10240;

    /**
     * {@inheritdoc}
     */
    public function shouldTransform($url)
    {
        return strpos($url, 'api.pusherapp.com') !== false;
    }

    /**
     * {@inheritdoc}
     */
    public function transform($event_type, DataObject $payload)
    {
        if (!in_array($event_type, $this->getSupportedEvents())) {
            return null;
        }

        $transformator = $event_type . 'PayloadTransformator';

        if (method_exists(self::class, $transformator)) {
            return $this->$transformator($payload);
        } else {
            throw new Exception("Transformator method {$transformator} not implemented");
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getSupportedEvents()
    {
        return [
            'CommentCreated',
            'ReactionCreated',
            'ReactionDeleted',
        ];
    }

    /**
     * Transform payload when comment is created.
     *
     * @param  Comment $comment
     * @return array
     */
    public function CommentCreatedPayloadTransformator(Comment $comment)
    {
        $data = [
            'id' => $comment->getId(),
            'parent_id' => $comment->getParentId(),
            'parent_type' => $comment->getParentType(),
            'body_formatted' => $comment->getFormattedBody(),
            'attachments' => $comment->getAttachments() ? $comment->getAttachments() : [],
            'created_by_id' => $comment->getCreatedById(),
            'created_on' => $comment->getCreatedOn(),
            'url' => $comment->getUrlPath(),
            'is_complete_data' => true,
        ];

        if ($this->calculateDataSize($data) >= self::PUSHER_PAYLOAD_LIMIT) {
            return [
                'id' => $comment->getId(),
                'url' => $comment->getUrlPath(),
                'is_complete_data' => false,
            ];
        } else {
            return $data;
        }
    }

    /**
     * Transform payload when reaction is created.
     *
     * @param  Reaction $reaction
     * @return array
     */
    public function ReactionCreatedPayloadTransformator(Reaction $reaction)
    {
        return $this->reactionPayload($reaction);
    }

    /**
     * Transform payload when reaction is deleted.
     *
     * @param  Reaction $reaction
     * @return array
     */
    public function ReactionDeletedPayloadTransformator(Reaction $reaction)
    {
        return $this->reactionPayload($reaction);
    }

    /**
     * Reaction payload.
     *
     * @param  Reaction $reaction
     * @return array
     */
    private function reactionPayload(Reaction $reaction)
    {
        return [
            'id' => $reaction->getId(),
            'class' => get_class($reaction),
            'parent_id' => $reaction->getParentId(),
            'parent_type' => $reaction->getParentType(),
            'created_by_id' => $reaction->getCreatedById(),
            'created_by_name' => $reaction->getCreatedByName(),
            'created_by_email' => $reaction->getCreatedByEmail(),
            'created_on' => $reaction->getCreatedOn(),
        ];
    }

    /**
     * Calculate size of data array.
     *
     * @param $data
     * @return int
     */
    public function calculateDataSize($data)
    {
        $serialized = serialize(json_encode($data));
        if (function_exists('mb_strlen')) {
            return mb_strlen($serialized, '8bit');
        } else {
            return strlen($serialized);
        }
    }
}
