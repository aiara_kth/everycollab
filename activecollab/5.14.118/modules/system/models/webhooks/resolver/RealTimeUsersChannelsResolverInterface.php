<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

interface RealTimeUsersChannelsResolverInterface
{
    public function getUsersChannels(DataObject $object);
}
