<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * DayOff class.
 *
 * @package activeCollab.modules.system
 * @subpackage models
 */
class DayOff extends FwDayOff
{
}
