<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Project template task.
 *
 * @package ActiveCollab.modules.system
 * @subpackage models
 */
class ProjectTemplateTask extends ProjectTemplateElement implements ILabels, IBody, IHiddenFromClients
{
    use ILabelsImplementation;
    use IBodyImplementation;

    /**
     * Return array of element properties.
     *
     * Key is name of the property, and value is a casting method
     *
     * @return array
     */
    public function getElementProperties()
    {
        return [
            'task_list_id' => 'intval',
            'assignee_id' => 'intval',
            'job_type_id' => 'intval',
            'estimate' => 'floatval',
            'start_on' => 'intval',
            'due_on' => 'intval',
            'is_important' => 'boolval',
            'is_hidden_from_clients' => 'boolval',
            'label_id' => 'intval',
        ];
    }

    /**
     * Return type of label used.
     *
     * @return string
     */
    public function getLabelType()
    {
        return TaskLabel::class;
    }

    public function getIsHiddenFromClients()
    {
        return (bool) $this->getAdditionalProperty('is_hidden_from_clients');
    }

    /**
     * {@inheritdoc}
     */
    public function delete($bulk = false)
    {
        try {
            DB::beginWork();

            $subtasks_to_delete_ids = [];

            if ($rows = DB::execute('SELECT id, raw_additional_properties FROM project_template_elements WHERE type = ? AND template_id = ?', ProjectTemplateSubtask::class, $this->getTemplateId())) {
                foreach ($rows as $row) {
                    $properties = $row['raw_additional_properties'] ? unserialize($row['raw_additional_properties']) : [];

                    if (!empty($properties['task_id']) && $properties['task_id'] == $this->getId()) {
                        $subtasks_to_delete_ids[] = $row['id'];
                    }
                }
            }

            if (count($subtasks_to_delete_ids)) {
                foreach (ProjectTemplateElements::findByIds($subtasks_to_delete_ids) as $subtasks_template) {
                    $subtasks_template->delete();
                }
            }

            parent::delete($bulk);

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
