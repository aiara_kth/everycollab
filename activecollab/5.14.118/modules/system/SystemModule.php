<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

use ActiveCollab\Foundation\Events\DataObjectLifeCycleEvent\DataObjectLifeCycleEventInterface;

/**
 * System module definition.
 *
 * @package activeCollab.modules.system
 */
class SystemModule extends AngieModule
{
    const NAME = 'system';
    const PATH = __DIR__;

    const MAINTENANCE_JOBS_QUEUE_CHANNEL = 'maintenance';

    /**
     * Plain module name.
     *
     * @var string
     */
    protected $name = 'system';

    /**
     * Module version.
     *
     * @var string
     */
    protected $version = '5.0';

    /**
     * Initialize module.
     */
    public function init()
    {
        parent::init();

        DataObjectPool::registerTypeLoader(['User', 'Member', 'Owner', 'Client'], function ($ids) {
            return Users::findByIds($ids);
        });

        DataObjectPool::registerTypeLoader('ApiSubscription', function ($ids) {
            return ApiSubscriptions::findByIds($ids);
        });

        DataObjectPool::registerTypeLoader('UserSession', function ($ids) {
            return UserSessions::findByIds($ids);
        });

        DataObjectPool::registerTypeLoader('UserInvitation', function ($ids) {
            return UserInvitations::findByIds($ids);
        });

        DataObjectPool::registerTypeLoader('Project', function ($ids) {
            return Projects::findByIds($ids);
        });

        DataObjectPool::registerTypeLoader('Company', function ($ids) {
            return Companies::findByIds($ids);
        });

        DataObjectPool::registerTypeLoader('Team', function ($ids) {
            return Teams::findByIds($ids);
        });

        DataObjectPool::registerTypeLoader('ProjectTemplate', function ($ids) {
            return ProjectTemplates::findByIds($ids);
        });

        DataObjectPool::registerTypeLoader(['ProjectTemplateElement', 'ProjectTemplateTaskList', 'ProjectTemplateRecurringTask', 'ProjectTemplateTask', 'ProjectTemplateSubtask', 'ProjectTemplateDiscussion', 'ProjectTemplateNote'], function ($ids) {
            return ProjectTemplateElements::findByIds($ids);
        });

        DataObjectPool::registerTypeLoader('Webhook', function ($ids) {
            return Webhooks::findByIds($ids);
        });

        DataObjectPool::registerTypeLoader('Reaction', function ($ids) {
            return Reactions::findByIds($ids);
        });
    }

    /**
     * Define module classes.
     */
    public function defineClasses()
    {
        require_once __DIR__ . '/resources/autoload_model.php';
        require_once __DIR__ . '/models/application_objects/ApplicationObject.class.php';

        AngieApplication::setForAutoload(
            [

                // Errors
                'ApiSubscriptionError' => __DIR__ . '/errors/ApiSubscriptionError.class.php',
                'LastOwnerRoleChangeError' => __DIR__ . '/errors/LastOwnerRoleChangeError.class.php',

                'ApplicationObjects' => __DIR__ . '/models/application_objects/ApplicationObjects.class.php',

                'ProjectExportInterface' => __DIR__ . '/models/ProjectExportInterface.php',
                'BaseProjectExport' => __DIR__ . '/models/BaseProjectExport.php',
                'SampleProjectExport' => __DIR__ . '/models/SampleProjectExport.php',
                'ProjectImportInterface' => __DIR__ . '/models/ProjectImportInterface.php',
                'SampleProjectImport' => __DIR__ . '/models/SampleProjectImport.php',
                'ProjectExport' => __DIR__ . '/models/ProjectExport.php',
                'SystemExport' => __DIR__ . '/models/SystemExport.php',

                'IProjectBasedOn' => __DIR__ . '/models/IProjectBasedOn.class.php',

                'LabelInterface' => __DIR__ . '/models/LabelInterface.php',
                'ProjectLabelInterface' => __DIR__ . '/models/ProjectLabelInterface.php',
                'ProjectLabel' => __DIR__ . '/models/ProjectLabel.class.php',

                'ProjectCategory' => __DIR__ . '/models/ProjectCategory.class.php',
                'ProjectBudgetCollection' => __DIR__ . '/models/ProjectBudgetCollection.php',

                'Favorites' => __DIR__ . '/models/Favorites.class.php',

                'MoveToProjectControllerAction' => __DIR__ . '/controller_actions/MoveToProjectControllerAction.class.php',

                'AnonymousUser' => __DIR__ . '/models/AnonymousUser.class.php',
                'Thumbnails' => __DIR__ . '/models/Thumbnails.class.php',

                'IProjectElement' => __DIR__ . '/models/project_elements/IProjectElement.class.php',
                'IProjectElementImplementation' => __DIR__ . '/models/project_elements/IProjectElementImplementation.class.php',
                'IProjectElementsImplementation' => __DIR__ . '/models/project_elements/IProjectElementsImplementation.class.php',

                // Filters
                'AssignmentFilter' => __DIR__ . '/models/AssignmentFilter.php',
                'ProjectsFilter' => __DIR__ . '/models/ProjectsFilter.php',
                'ProjectsTimelineFilter' => __DIR__ . '/models/ProjectsTimelineFilter.php',
                'TeamTimelineFilter' => __DIR__ . '/models/TeamTimelineFilter.php',

                // Notifications
                'NewProjectNotification' => __DIR__ . '/notifications/NewProjectNotification.class.php',
                'NewCommentNotification' => __DIR__ . '/notifications/NewCommentNotification.class.php',
                'NewReactionNotification' => __DIR__ . '/notifications/NewReactionNotification.class.php',
                'WelcomeNotification' => __DIR__ . '/notifications/WelcomeNotification.class.php',
                'PasswordRecoveryNotification' => __DIR__ . '/notifications/PasswordRecoveryNotification.class.php',
                'ReplacingProjectUserNotification' => __DIR__ . '/notifications/ReplacingProjectUserNotification.class.php',
                'NotifyEmailSenderNotification' => __DIR__ . '/notifications/NotifyEmailSenderNotification.class.php',
                'InviteToSharedObjectNotification' => __DIR__ . '/notifications/InviteToSharedObjectNotification.class.php',
                'NewCalendarEventNotification' => __DIR__ . '/notifications/NewCalendarEventNotification.class.php',
                'InfoNotification' => __DIR__ . '/notifications/InfoNotification.class.php',

                'PaymentReceivedNotification' => __DIR__ . '/notifications/PaymentReceivedNotification.class.php',

                'FailedLoginNotification' => __DIR__ . '/notifications/FailedLoginNotification.class.php',
                'NewAnnouncementNotification' => __DIR__ . '/notifications/NewAnnouncementNotification.class.php',

                'BounceEmailNotification' => __DIR__ . '/notifications/BounceEmailNotification.class.php',
                'NotifyOwnersNotification' => __DIR__ . '/notifications/NotifyOwnersNotification.class.php',

                // Authentication related
                'IUser' => __DIR__ . '/models/IUser.php',

                'SecurityLogs' => __DIR__ . '/models/authentication/SecurityLogs.php',

                'AuthorizationIntegrationInterface' => __DIR__ . '/models/integrations/authorization/AuthorizationIntegrationInterface.php',
                'AuthorizationIntegration' => __DIR__ . '/models/integrations/authorization/AuthorizationIntegration.php',
                'IdpAuthorizationIntegration' => __DIR__ . '/models/integrations/authorization/IdpAuthorizationIntegration.php',

                'LocalAuthorizationIntegration' => __DIR__ . '/models/integrations/authorization/LocalAuthorizationIntegration.php',
                'ShepherdClassicAuthorizationIntegration' => __DIR__ . '/models/integrations/authorization/ShepherdClassicAuthorizationIntegration.php',
                'ShepherdAuthorizationIntegration' => __DIR__ . '/models/integrations/authorization/idp/ShepherdAuthorizationIntegration.php',
                'OneLoginAuthorizationIntegration' => __DIR__ . '/models/integrations/authorization/idp/OneLoginAuthorizationIntegration.php',

                // User roles
                'Owner' => __DIR__ . '/models/user_roles/Owner.class.php',
                'Member' => __DIR__ . '/models/user_roles/Member.class.php',
                'Client' => __DIR__ . '/models/user_roles/Client.class.php',

                // Members
                'IMembers' => __DIR__ . '/models/members/IMembers.class.php',
                'IBasicMembersImplementation' => __DIR__ . '/models/members/IBasicMembersImplementation.class.php',
                'IMembersImplementation' => __DIR__ . '/models/members/IMembersImplementation.class.php',
                'IMembersViaConnectionTableImplementation' => __DIR__ . '/models/members/IMembersViaConnectionTableImplementation.class.php',

                // Calendars
                'UserCalendar' => __DIR__ . '/models/UserCalendar.class.php',

                // Morning paper
                'MorningPaper' => __DIR__ . '/models/morning_paper/MorningPaper.php',
                'MorningPaperSnapshot' => __DIR__ . '/models/morning_paper/MorningPaperSnapshot.php',

                // Hourly rates
                'IHourlyRates' => __DIR__ . '/models/hourly_rates/IHourlyRates.class.php',
                'IHourlyRatesImplementation' => __DIR__ . '/models/hourly_rates/IHourlyRatesImplementation.class.php',

                // Project template elements
                'ProjectTemplateTaskList' => __DIR__ . '/models/ProjectTemplateTaskList.php',
                'ProjectTemplateTask' => __DIR__ . '/models/ProjectTemplateTask.php',
                'ProjectTemplateRecurringTask' => __DIR__ . '/models/ProjectTemplateRecurringTask.php',
                'ProjectTemplateSubtask' => __DIR__ . '/models/ProjectTemplateSubtask.php',
                'ProjectTemplateDiscussion' => __DIR__ . '/models/ProjectTemplateDiscussion.php',
                'ProjectTemplateNoteGroup' => __DIR__ . '/models/ProjectTemplateNoteGroup.php',
                'ProjectTemplateNote' => __DIR__ . '/models/ProjectTemplateNote.php',
                'ProjectTemplateFile' => __DIR__ . '/models/ProjectTemplateFile.php',

                'InitialSettingsCollection' => __DIR__ . '/models/initial_settings/InitialSettingsCollection.php',
                'InitialUserSettingsCollection' => __DIR__ . '/models/initial_settings/InitialUserSettingsCollection.php',

                // Reminders
                'CustomReminder' => __DIR__ . '/models/CustomReminder.php',
                'CustomReminderNotification' => __DIR__ . '/notifications/CustomReminderNotification.class.php',

                'InstanceCreatedActivityLog' => __DIR__ . '/models/InstanceCreatedActivityLog.php',
                'InstanceUpdatedActivityLog' => __DIR__ . '/models/InstanceUpdatedActivityLog.php',
                'CommentCreatedActivityLog' => __DIR__ . '/models/CommentCreatedActivityLog.php',

                'IActivityLogColletionPreloaders' => __DIR__ . '/models/activity_log_collections/IActivityLogColletionPreloaders.php',
                'UserActivityLogsCollection' => __DIR__ . '/models/activity_log_collections/UserActivityLogsCollection.php',
                'ActivityLogsInCollection' => __DIR__ . '/models/activity_log_collections/ActivityLogsInCollection.php',
                'UserObjectUpdatesCollection' => __DIR__ . '/models/UserObjectUpdatesCollection.php',

                // Search
                'UsersSearchBuilder' => __DIR__ . '/models/search_builders/UsersSearchBuilder.php',
                'CompaniesSearchBuilder' => __DIR__ . '/models/search_builders/CompaniesSearchBuilder.php',
                'ProjectsSearchBuilder' => __DIR__ . '/models/search_builders/ProjectsSearchBuilder.php',
                'ProjectElementsSearchBuilder' => __DIR__ . '/models/search_builders/ProjectElementsSearchBuilder.php',

                'ProjectElementsSearchBuilderInterface' => __DIR__ . '/models/search_builders/ProjectElementsSearchBuilderInterface.php',

                'CompanySearchDocument' => __DIR__ . '/models/search_documents/CompanySearchDocument.php',
                'UserSearchDocument' => __DIR__ . '/models/search_documents/UserSearchDocument.php',
                'ProjectSearchDocument' => __DIR__ . '/models/search_documents/ProjectSearchDocument.php',
                'ProjectElementSearchDocument' => __DIR__ . '/models/search_documents/ProjectElementSearchDocument.php',

                // Integrations
                'IntegrationInterface' => __DIR__ . '/models/integrations/IntegrationInterface.php',
                'DesktopAppIntegration' => __DIR__ . '/models/integrations/DesktopAppIntegration.php',
                'AbstractImporterIntegration' => __DIR__ . '/models/integrations/AbstractImporterIntegration.class.php',
                'BasecampImporterIntegration' => __DIR__ . '/models/integrations/BasecampImporterIntegration.php',
                'ClientPlusIntegration' => __DIR__ . '/models/integrations/ClientPlusIntegration.php',
                'TestLodgeIntegration' => __DIR__ . '/models/integrations/TestLodgeIntegration.php',
                'HubstaffIntegration' => __DIR__ . '/models/integrations/HubstaffIntegration.php',
                'TimeCampIntegration' => __DIR__ . '/models/integrations/TimeCampIntegration.php',
                'ThirdPartyIntegration' => __DIR__ . '/models/integrations/ThirdPartyIntegration.php',
                'TrelloImporterIntegration' => __DIR__ . '/models/integrations/TrelloImporterIntegration.php',
                'AsanaImporterIntegration' => __DIR__ . '/models/integrations/AsanaImporterIntegration.php',
                'CrispIntegration' => __DIR__ . '/models/integrations/CrispIntegration.php',
                'SlackIntegration' => __DIR__ . '/models/integrations/SlackIntegration.php',
                'WarehouseIntegration' => __DIR__ . '/models/integrations/WarehouseIntegration.php',
                'WarehouseIntegrationInterface' => __DIR__ . '/models/integrations/WarehouseIntegrationInterface.php',
                'GoogleDriveIntegration' => __DIR__ . '/models/integrations/GoogleDriveIntegration.php',
                'DropboxIntegration' => __DIR__ . '/models/integrations/DropboxIntegration.php',
                'ZapierIntegration' => __DIR__ . '/models/integrations/ZapierIntegration.php',
                'OneLoginIntegration' => __DIR__ . '/models/integrations/OneLoginIntegration.php',
                'WrikeImporterIntegration' => __DIR__ . '/models/integrations/WrikeImporterIntegration.php',
                'RealTimeIntegration' => __DIR__ . '/models/integrations/RealTimeIntegration.php',
                'PusherIntegration' => __DIR__ . '/models/integrations/PusherIntegration.php',
                'NewRelicIntegration' => __DIR__ . '/models/integrations/NewRelicIntegration.php',
                'RealTimeIntegrationResolver' => __DIR__ . '/models/integrations/resolver/RealTimeIntegrationResolver.php',
                'RealTimeIntegrationResolverInterface' => __DIR__ . '/models/integrations/resolver/RealTimeIntegrationResolverInterface.php',
                'SampleProjectsIntegration' => __DIR__ . '/models/integrations/SampleProjectsIntegration.php',

                // Attachments Archive
                'AttachmentsArchive' => __DIR__ . '/models/AttachmentsArchive.class.php',

                'IHiddenFromClients' => __DIR__ . '/models/IHiddenFromClients.php',
                'INewProjectElementNotificationOptOutConfig' => __DIR__ . '/models/INewProjectElementNotificationOptOutConfig.php',

                'LocalAttachment' => __DIR__ . '/models/attachments/LocalAttachment.class.php',
                'RemoteAttachment' => __DIR__ . '/models/attachments/RemoteAttachment.class.php',
                'WarehouseAttachment' => __DIR__ . '/models/attachments/WarehouseAttachment.class.php',
                'GoogleDriveAttachment' => __DIR__ . '/models/attachments/GoogleDriveAttachment.class.php',
                'DropboxAttachment' => __DIR__ . '/models/attachments/DropboxAttachment.class.php',

                'LocalUploadedFile' => __DIR__ . '/models/uploaded_files/LocalUploadedFile.class.php',
                'RemoteUploadedFile' => __DIR__ . '/models/uploaded_files/RemoteUploadedFile.class.php',
                'WarehouseUploadedFile' => __DIR__ . '/models/uploaded_files/WarehouseUploadedFile.class.php',
                'GoogleDriveUploadedFile' => __DIR__ . '/models/uploaded_files/GoogleDriveUploadedFile.class.php',
                'DropboxUploadedFile' => __DIR__ . '/models/uploaded_files/DropboxUploadedFile.class.php',

                // Payload transformator
                'SlackWebhookPayloadTransformator' => __DIR__ . '/models/webhooks/SlackWebhookPayloadTransformator.class.php',
                'ZapierWebhookPayloadTransformator' => __DIR__ . '/models/webhooks/ZapierWebhookPayloadTransformator.class.php',
                'PusherSocketPayloadTransformator' => __DIR__ . '/models/webhooks/PusherSocketPayloadTransformator.class.php',

                // Webhooks
                'SlackWebhook' => __DIR__ . '/models/webhooks/SlackWebhook.class.php',

                // Webhooks resolver
                'RealTimeUsersChannelsResolver' => __DIR__ . '/models/webhooks/resolver/RealTimeUsersChannelsResolver.php',
                'RealTimeUsersChannelsResolverInterface' => __DIR__ . '/models/webhooks/resolver/RealTimeUsersChannelsResolverInterface.php',

                'Versions' => __DIR__ . '/models/Versions.php',
                'LocalToWarehouseMover' => __DIR__ . '/models/LocalToWarehouseMover.php',

                'UserProfilePermissionsChecker' => __DIR__ . '/models/UserProfilePermissionsChecker.php',
                'OnboardingSurveyInterface' => __DIR__ . '/models/OnboardingSurvey/OnboardingSurveyInterface.php',
                'OnboardingSurvey' => __DIR__ . '/models/OnboardingSurvey/OnboardingSurvey.php',
                'SinceLastVisitServiceInterface' => __DIR__ . '/models/SinceLastVisitServiceInterface.php',
                'SinceLastVisitService' => __DIR__ . '/models/SinceLastVisitService.php',

                // Sockets
                'Socket' => __DIR__ . '/models/sockets/Socket.class.php',
                'Sockets' => __DIR__ . '/models/sockets/Sockets.class.php',
                'PusherSocket' => __DIR__ . '/models/sockets/PusherSocket.class.php',

                'SetupWizard' => __DIR__ . '/models/SetupWizard/SetupWizard.php',
                'SetupWizardInterface' => __DIR__ . '/models/SetupWizard/SetupWizardInterface.php',

                // CTA Notifications
                'FillOnboardingSurveyNotificationInterface' => __DIR__ . '/models/CTANotification/FillOnboardingSurveyNotificationInterface.php',
                'CTANotificationInterface' => __DIR__ . '/models/CTANotification/CTANotificationInterface.php',
                'FillOnboardingSurveyNotification' => __DIR__ . '/models/CTANotification/FillOnboardingSurveyNotification.php',
                'FillOnboardingSurveyNotificationStageResolver' => __DIR__ . '/models/CTANotification/FillOnboardingSurveyNotificationStageResolver.php',
                'CTANotifications' => __DIR__ . '/models/CTANotification/CTANotifications.php',

                // Crisp Notifications
                'CrispUserNotificationsResolver' => __DIR__ . '/models/CrispNotifications/CrispUserNotificationsResolver.php',
                'CrispNotificationForNewUser' => __DIR__ . '/models/CrispNotifications/CrispNotificationForNewUser.php',
                'CrispNotificationForExistingUser' => __DIR__ . '/models/CrispNotifications/CrispNotificationForExistingUser.php',
                'CrispNotificationInterface' => __DIR__ . '/models/CrispNotifications/CrispNotificationInterface.php',

                // Reactions
                'IReactions' => __DIR__ . '/models/reactions/IReactions.php',
                'IReactionsImplementation' => __DIR__ . '/models/reactions/IReactionsImplementation.php',
                'SmileReaction' => __DIR__ . '/models/reactions/SmileReaction.php',
                'ThinkingReaction' => __DIR__ . '/models/reactions/ThinkingReaction.php',
                'ThumbsUpReaction' => __DIR__ . '/models/reactions/ThumbsUpReaction.php',
                'ThumbsDownReaction' => __DIR__ . '/models/reactions/ThumbsDownReaction.php',
                'ApplauseReaction' => __DIR__ . '/models/reactions/ApplauseReaction.php',
                'HeartReaction' => __DIR__ . '/models/reactions/HeartReaction.php',
                'PartyReaction' => __DIR__ . '/models/reactions/PartyReaction.php',
            ]
        );
    }

    // ---------------------------------------------------
    //  Events and Routes
    // ---------------------------------------------------

    /**
     * Define module routes.
     */
    public function defineRoutes()
    {
        Router::mapResource('users', ['module' => self::INJECT_INTO], function ($collection, $single) {
            Router::map("$collection[name]_invite", "$collection[path]/invite", ['module' => $collection['module'], 'controller' => $collection['controller'], 'action' => ['POST' => 'invite']], $collection['requirements']);
            Router::map("$collection[name]_all", "$collection[path]/all", ['module' => $collection['module'], 'controller' => $collection['controller'], 'action' => ['GET' => 'all']], $collection['requirements']);
            Router::map("$collection[name]_archive", "$collection[path]/archive", ['module' => $collection['module'], 'controller' => $collection['controller'], 'action' => ['GET' => 'archive']], $collection['requirements']);
            Router::map("$collection[name]_check_email", "$collection[path]/check-email", ['module' => $collection['module'], 'controller' => $collection['controller'], 'action' => ['GET' => 'check_email']], $collection['requirements']);

            Router::map("$single[name]_archive", "$single[path]/archive", ['module' => $single['module'], 'controller' => $single['controller'], 'action' => ['PUT' => 'move_to_archive']], $single['requirements']);
            Router::map("$single[name]_reactivate", "$single[path]/reactivate", ['module' => $single['module'], 'controller' => $single['controller'], 'action' => ['PUT' => 'reactivate']], $single['requirements']);
            Router::map("$single[name]_change_password", "$single[path]/change-password", ['module' => $single['module'], 'controller' => $single['controller'], 'action' => ['PUT' => 'change_password']], $single['requirements']);
            Router::map("$single[name]_change_user_password", "$single[path]/change-user-password", ['module' => $single['module'], 'controller' => $single['controller'], 'action' => ['PUT' => 'change_user_password']], $single['requirements']);
            Router::map("$single[name]_change_user_profile", "$single[path]/change-user-profile", ['module' => $single['module'], 'controller' => $single['controller'], 'action' => ['PUT' => 'change_user_profile']], $single['requirements']);
            Router::map("$single[name]_resend_invitation", "$single[path]/resend-invitation", ['module' => $single['module'], 'controller' => $single['controller'], 'action' => ['PUT' => 'resend_invitation']], $single['requirements']);
            Router::map("$single[name]_get_invitation", "$single[path]/get-invitation", ['module' => $single['module'], 'controller' => $single['controller'], 'action' => ['GET' => 'get_invitation']], $single['requirements']);
            Router::map("$single[name]_get_accept_invitation_url", "$single[path]/get-invitation/accept-url", ['module' => $single['module'], 'controller' => $single['controller'], 'action' => ['GET' => 'get_accept_invitation_url']], $single['requirements']);
            Router::map("$single[name]_export", "$single[path]/export", ['module' => $single['module'], 'controller' => $single['controller'], 'action' => ['GET' => 'export']], $single['requirements']);
            Router::map("$single[name]_activities", "$single[path]/activities", ['module' => $single['module'], 'controller' => $single['controller'], 'action' => ['GET' => 'activities']], $single['requirements']);
            Router::map("$single[name]_clear_avatar", "$single[path]/clear-avatar", ['module' => $single['module'], 'controller' => $single['controller'], 'action' => ['DELETE' => 'clear_avatar']], $single['requirements']);
            Router::map("$single[name]_change_role", "$single[path]/change-role", ['module' => $single['module'], 'controller' => $single['controller'], 'action' => ['PUT' => 'change_role']], $single['requirements']);
            Router::map("$single[name]_profile_permissions", "$single[path]/profile-permissions", ['module' => $single['module'], 'controller' => $single['controller'], 'action' => ['GET' => 'profile_permissions']], $single['requirements']);
            Router::map("$single[name]_password_permissions", "$single[path]/password-permissions", ['module' => $single['module'], 'controller' => $single['controller'], 'action' => ['GET' => 'password_permissions']], $single['requirements']);

            Router::map("$single[name]_sessions", "$single[path]/sessions", ['module' => $single['module'], 'controller' => 'user_sessions', 'action' => ['GET' => 'index', 'DELETE' => 'remove']], $single['requirements']);

            Router::mapResource('api_subscriptions', ['collection_path' => "$single[path]/api-subscriptions", 'collection_requirements' => $single['requirements']]);

            Router::map("$single[name]_workspaces", "$single[path]/workspaces", ['module' => $single['module'], 'controller' => 'user_workspaces', 'action' => ['GET' => 'index']], $single['requirements']);
        });

        Router::map('user_session', 'user-session', ['controller' => 'user_session', 'action' => ['GET' => 'who_am_i', 'POST' => 'login', 'DELETE' => 'logout']]);
        Router::map('issue_token', 'issue-token', ['controller' => 'user_session', 'action' => ['POST' => 'issue_token']]);
        Router::map('issue_token_by_intent', 'issue-token-intent', ['controller' => 'user_session', 'action' => ['POST' => 'issue_token']]); // Alias to /issue-token, these API end-points used to behave differently.
        Router::map('accept_invitation', 'accept-invitation', ['controller' => 'user_session', 'action' => ['GET' => 'view_invitation', 'POST' => 'accept_invitation']]);

        Router::map('password_recovery_send_code', 'password-recovery/send-code', ['controller' => 'password_recovery', 'action' => ['POST' => 'send_code']]);
        Router::map('password_recovery_reset_password', 'password-recovery/reset-password', ['controller' => 'password_recovery', 'action' => ['POST' => 'reset_password']]);

        Router::map('socket_auth', 'socket/auth', ['controller' => 'socket_auth', 'action' => ['POST' => 'authenticate']]);

        Router::mapResource('companies', null, function ($collection, $single) {
            Router::map("$collection[name]_all", "$collection[path]/all", ['controller' => $collection['controller'], 'action' => ['GET' => 'all']], $collection['requirements']);
            Router::map("$collection[name]_archive", "$collection[path]/archive", ['controller' => $collection['controller'], 'action' => ['GET' => 'archive']], $collection['requirements']);
            Router::map("$collection[name]_notes", "$collection[path]/notes", ['controller' => $collection['controller'], 'action' => ['GET' => 'notes']], $collection['requirements']);
            Router::map("$single[name]_export", "$single[path]/export", ['controller' => $single['controller'], 'action' => ['GET' => 'export']], $single['requirements']);
            Router::map("$single[name]_projects", "$single[path]/projects", ['controller' => $single['controller'], 'action' => ['GET' => 'projects']], $single['requirements']);
            Router::map("$single[name]_project_names", "$single[path]/project-names", ['controller' => $single['controller'], 'action' => ['GET' => 'project_names']], $single['requirements']);
            Router::map("$single[name]_invoices", "$single[path]/invoices", ['controller' => $single['controller'], 'action' => ['GET' => 'invoices']], $single['requirements']);
        });

        Router::mapResource('teams', null, function ($collection, $single) {
            Router::map("$single[name]_members", "$single[path]/members", ['controller' => 'team_members', 'action' => ['GET' => 'index', 'POST' => 'add']]);
            Router::map("$single[name]_member", "$single[path]/members/:user_id", ['controller' => 'team_members', 'action' => ['DELETE' => 'delete']], array_merge($single['requirements'], ['user_id' => Router::MATCH_ID]));
        });

        // Projects
        Router::mapResource('projects', null, function ($collection, $single) {
            Router::map("$collection[name]_filter", "$collection[path]/filter", ['controller' => $collection['controller'], 'action' => ['GET' => 'filter']], $collection['requirements']);
            Router::map("$collection[name]_archive", "$collection[path]/archive", ['controller' => $collection['controller'], 'action' => ['GET' => 'archive']], $collection['requirements']);
            Router::map("$collection[name]_names", "$collection[path]/names", ['controller' => $collection['controller'], 'action' => ['GET' => 'names']], $collection['requirements']);
            Router::map("$collection[name]_with_tracking_enabled", "$collection[path]/with-tracking-enabled", ['controller' => $collection['controller'], 'action' => ['GET' => 'with_tracking_enabled']], $collection['requirements']);
            Router::map("$collection[name]_labels", "$collection[path]/labels", ['controller' => $collection['controller'], 'action' => ['GET' => 'labels']], $collection['requirements']);
            Router::map("$collection[name]_calendar_events", "$collection[path]/calendar-events", ['controller' => $collection['controller'], 'action' => ['GET' => 'calendar_events']], $collection['requirements']);
            Router::map("$collection[name]_categories", "$collection[path]/categories", ['controller' => $collection['controller'], 'action' => ['GET' => 'categories']], $collection['requirements']);
            Router::map("$collection[name]_categories", "$collection[path]/categories", ['controller' => $collection['controller'], 'action' => ['GET' => 'categories']], $collection['requirements']);

            Router::map("$single[name]_whats_new", "$single[path]/whats-new", ['controller' => $single['controller'], 'action' => ['GET' => 'whats_new']], $single['requirements']);
            Router::map("$single[name]_budget", "$single[path]/budget", ['controller' => $single['controller'], 'action' => ['GET' => 'budget']], $single['requirements']);
            Router::map("$single[name]_export", "$single[path]/export", ['controller' => $single['controller'], 'action' => ['GET' => 'export']], $single['requirements']);

            // People
            Router::map("$single[name]_members", "$single[path]/members", ['controller' => 'project_members', 'action' => ['GET' => 'index', 'POST' => 'add']]);
            Router::map("$single[name]_member", "$single[path]/members/:user_id", ['controller' => 'project_members', 'action' => ['PUT' => 'replace', 'DELETE' => 'delete']], array_merge($single['requirements'], ['user_id' => Router::MATCH_ID]));
            Router::map("$single[name]_revoke_client_access", "$single[path]/revoke-client-access", ['controller' => 'project_members', 'action' => ['PUT' => 'revoke_client_access']]);
            Router::map("$single[name]_responsibilities", "$single[path]/responsibilities", ['controller' => 'project_members', 'action' => ['GET' => 'responsibilities']]);

            // Subsections
            TasksModule::defineProjectTaskListRoutes($collection, $single);
            TasksModule::defineProjectTaskRoutes($collection, $single);
            TasksModule::defineProjectRecurringTaskRoutes($collection, $single);
            DiscussionsModule::defineProjectDiscussionRoutes($collection, $single);
            FilesModule::defineProjectFileRoutes($collection, $single);
            NotesModule::defineProjectNoteRoutes($collection, $single);
            TrackingModule::defineProjectTimeRecordRoutes($collection, $single);
            TrackingModule::defineProjectExpenseRoutes($collection, $single);
        });

        Router::map('project_labels', 'labels/project-labels', ['controller' => 'labels', 'action' => ['GET' => 'project_labels']]);
        Router::map('task_labels', 'labels/task-labels', ['controller' => 'labels', 'action' => ['GET' => 'task_labels']]);

        Router::mapResource('project_templates', [], function ($collection, $single) {
            Router::map("$single[name]_duplicate", "$single[path]/duplicate", ['controller' => 'project_templates', 'action' => ['POST' => 'duplicate']]);
            Router::map("$single[name]_reorder", "$single[path]/reorder", ['controller' => 'project_templates', 'action' => ['PUT' => 'reorder']]);

            Router::mapResource('project_template_elements', ['collection_path' => "$single[path]/elements", 'collection_requirements' => $collection['requirements']], function ($collection, $single) {
                Router::map("$collection[name]_batch", "$collection[path]/batch", ['action' => ['POST' => 'batch_add'], 'controller' => 'project_template_elements'], $collection['requirements']);
                Router::map("$single[name]_download", "$collection[path]/download", ['action' => ['GET' => 'download'], 'controller' => 'project_template_elements'], $collection['requirements']);
                Router::map("$collection[name]_reorder", "$collection[path]/reorder", ['action' => ['PUT' => 'reorder'], 'controller' => 'project_template_elements'], $collection['requirements']);
            });
        });

        Router::map('projects_with_people_permissions', '/projects/with-people-permissions', ['controller' => 'projects', 'action' => ['GET' => 'with_people_permissions']]);

        Router::map('user_projects', 'users/:user_id/projects', ['controller' => 'users', 'action' => ['GET' => 'projects', 'POST' => 'add_to_projects']], ['user_id' => Router::MATCH_ID]);
        Router::map('user_project_ids', 'users/:user_id/projects/ids', ['controller' => 'users', 'action' => ['GET' => 'project_ids']], ['user_id' => Router::MATCH_ID]);
        Router::map('feedback', 'feedback', ['controller' => 'feedback', 'action' => ['POST' => 'send']]);
        Router::map('feedback_check', 'feedback/check', ['controller' => 'feedback', 'action' => ['GET' => 'check']]);
        Router::map('new_features', 'new-features', ['controller' => 'new_features', 'action' => ['GET' => 'list_new_features']]);
        Router::map('maintenance_mode', 'maintenance-mode', ['controller' => 'maintenance_mode', 'action' => ['GET' => 'show_settings', 'PUT' => 'save_settings']]);
        Router::map('security', 'security', ['controller' => 'security', 'action' => ['GET' => 'show_settings', 'PUT' => 'save_settings']]);
        Router::map('announce', 'announce', ['action' => ['POST' => 'announce'], 'controller' => 'announcements'], ['parent_type' => Router::MATCH_SLUG, 'parent_id' => Router::MATCH_ID]);
        Router::map('versions', 'system/versions/old-versions', ['controller' => 'versions', 'action' => ['GET' => 'check_old_versions', 'DELETE' => 'delete_old_versions']]);

        // Basecamp integration controller
        Router::map('basecamp_check_credentials', 'integrations/basecamp-importer/check-credentials', ['controller' => 'basecamp_importer_integration', 'action' => ['POST' => 'check_credentials'], 'integration_type' => 'basecamp-importer']);
        Router::map('basecamp_schedule_import', 'integrations/basecamp-importer/schedule-import', ['controller' => 'basecamp_importer_integration', 'action' => ['POST' => 'schedule_import'], 'integration_type' => 'basecamp-importer']);
        Router::map('basecamp_start_over', 'integrations/basecamp-importer/start-over', ['controller' => 'basecamp_importer_integration', 'action' => ['POST' => 'start_over'], 'integration_type' => 'basecamp-importer']);
        Router::map('basecamp_check_status', 'integrations/basecamp-importer/check-status', ['controller' => 'basecamp_importer_integration', 'action' => ['GET' => 'check_status'], 'integration_type' => 'basecamp-importer']);
        Router::map('basecamp_invite_users', 'integrations/basecamp-importer/invite-users', ['controller' => 'basecamp_importer_integration', 'action' => ['POST' => 'invite_users'], 'integration_type' => 'basecamp-importer']);

        // Client+
        Router::map('client_plus', 'integrations/client-plus', ['controller' => 'client_plus_integration', 'action' => ['POST' => 'activate', 'DELETE' => 'deactivate'], 'integration_type' => 'client_plus']);

        // Trello integration controller
        Router::map('trello_request_url', 'integrations/trello-importer/request-url', ['controller' => 'trello_importer_integration', 'action' => ['GET' => 'get_request_url'], 'integration_type' => 'trello-importer']);
        Router::map('trello_authorize', 'integrations/trello-importer/authorize', ['controller' => 'trello_importer_integration', 'action' => ['PUT' => 'authorize'], 'integration_type' => 'trello-importer']);
        Router::map('trello_schedule_import', 'integrations/trello-importer/schedule-import', ['controller' => 'trello_importer_integration', 'action' => ['POST' => 'schedule_import'], 'integration_type' => 'trello-importer']);
        Router::map('trello_start_over', 'integrations/trello-importer/start-over', ['controller' => 'trello_importer_integration', 'action' => ['POST' => 'start_over'], 'integration_type' => 'trello-importer']);
        Router::map('trello_check_status', 'integrations/trello-importer/check-status', ['controller' => 'trello_importer_integration', 'action' => ['GET' => 'check_status'], 'integration_type' => 'trello-importer']);
        Router::map('trello_invite_users', 'integrations/trello-importer/invite-users', ['controller' => 'trello_importer_integration', 'action' => ['GET' => 'invite_users'], 'integration_type' => 'trello-importer']);

        // Asana integration controller
        Router::map('asana_request_url', 'integrations/asana-importer/request-url', ['controller' => 'asana_importer_integration', 'action' => ['GET' => 'get_request_url'], 'integration_type' => 'asana-importer']);
        Router::map('asana_authorize', 'integrations/asana-importer/authorize', ['controller' => 'asana_importer_integration', 'action' => ['PUT' => 'authorize'], 'integration_type' => 'asana-importer']);
        Router::map('asana_schedule_import', 'integrations/asana-importer/schedule-import', ['controller' => 'asana_importer_integration', 'action' => ['POST' => 'schedule_import'], 'integration_type' => 'asana-importer']);
        Router::map('asana_start_over', 'integrations/asana-importer/start-over', ['controller' => 'asana_importer_integration', 'action' => ['POST' => 'start_over'], 'integration_type' => 'asana-importer']);
        Router::map('asana_check_status', 'integrations/asana-importer/check-status', ['controller' => 'asana_importer_integration', 'action' => ['GET' => 'check_status'], 'integration_type' => 'asana-importer']);
        Router::map('asana_invite_users', 'integrations/asana-importer/invite-users', ['controller' => 'asana_importer_integration', 'action' => ['GET' => 'invite_users'], 'integration_type' => 'asana-importer']);

        // Sample projects controller
        Router::map('sample_projects', 'integrations/sample-projects', ['controller' => 'sample_projects_integration', 'action' => ['GET' => 'index', 'POST' => 'import'], 'integration_type' => 'sample-projects']);

        Router::map('slack_connect', 'integrations/slack/connect', ['controller' => 'slack_integration', 'action' => ['PUT' => 'connect'], 'integration_type' => 'slack']);
        Router::map('slack_notification_channel', 'integrations/slack/notification-channels/:notification_channel_id', ['controller' => 'slack_integration', 'action' => ['PUT' => 'edit', 'DELETE' => 'delete'], 'integration_type' => 'slack', 'notification_channel_id' => Router::MATCH_ID]);

        // Webhooks integration controller
        Router::map('webhooks_integration_ids', 'integrations/webhooks/:webhook_id', ['controller' => 'webhooks_integration', 'action' => ['PUT' => 'edit', 'DELETE' => 'delete'], 'integration_type' => 'webhooks', 'webhook_id' => Router::MATCH_ID]);
        Router::map('webhooks_integration', 'integrations/webhooks', ['controller' => 'webhooks_integration', 'action' => ['POST' => 'add'], 'integration_type' => 'webhooks']);

        // Calendar feeds
        Router::map('calendar_feeds', 'calendar-feeds', ['controller' => 'calendar_feeds', 'action' => ['GET' => 'index']]);
        Router::map('calendar_feeds_project', 'calendar-feeds/projects/:project_id', ['controller' => 'calendar_feeds', 'action' => ['GET' => 'project']], ['project_id' => Router::MATCH_ID]);
        Router::map('calendar_feeds_calendar', 'calendar-feeds/calendars/:calendar_id', ['controller' => 'calendar_feeds', 'action' => ['GET' => 'calendar']], ['calendar_id' => Router::MATCH_ID]);

        // Zapier integration controller
        Router::map('zapier_integration', 'integrations/zapier', ['controller' => 'zapier_integration', 'action' => ['GET' => 'get_data', 'POST' => 'enable', 'DELETE' => 'disable'], 'integration_type' => 'zapier']);
        Router::map('zapier_integration_payload_example', 'integrations/zapier/payload-examples/:event_type', ['controller' => 'zapier_integration', 'action' => ['GET' => 'payload_example'], 'integration_type' => 'zapier'], ['event_type' => Router::MATCH_SLUG]);

        // Zapier Webhooks REST Endpoint
        Router::mapResource('zapier_webhooks', ['collection_path' => '/integrations/zapier/webhooks']);

        // OneLogin integration
        Router::map('one_login_credentials', 'integrations/one-login/credentials', ['controller' => 'one_login_integration', 'action' => ['POST' => 'credentials'], 'integration_type' => 'one-login']);
        Router::map('one_login_enable', 'integrations/one-login/enable', ['controller' => 'one_login_integration', 'action' => ['GET' => 'enable'], 'integration_type' => 'one-login']);
        Router::map('one_login_disable', 'integrations/one-login/disable', ['controller' => 'one_login_integration', 'action' => ['GET' => 'disable'], 'integration_type' => 'one-login']);

        // Wrike integration controller
        Router::map('wrike_authorize', 'integrations/wrike-importer/authorize', ['controller' => 'wrike_importer_integration', 'action' => ['PUT' => 'authorize'], 'integration_type' => 'wrike-importer']);
        Router::map('wrike_schedule_import', 'integrations/wrike-importer/schedule-import', ['controller' => 'wrike_importer_integration', 'action' => ['POST' => 'schedule_import'], 'integration_type' => 'wrike-importer']);
        Router::map('wrike_start_over', 'integrations/wrike-importer/start-over', ['controller' => 'wrike_importer_integration', 'action' => ['POST' => 'start_over'], 'integration_type' => 'wrike-importer']);
        Router::map('wrike_check_status', 'integrations/wrike-importer/check-status', ['controller' => 'wrike_importer_integration', 'action' => ['GET' => 'check_status'], 'integration_type' => 'wrike-importer']);
        Router::map('wrike_invite_users', 'integrations/wrike-importer/invite-users', ['controller' => 'wrike_importer_integration', 'action' => ['GET' => 'invite_users'], 'integration_type' => 'wrike-importer']);

        Router::map('cta_notifications', 'cta-notifications/:notification_type', ['controller' => 'cta_notifications', 'action' => ['GET' => 'show']]);
        Router::map('cta_notifications_dismiss', 'cta-notifications/:notification_type/dismiss', ['controller' => 'cta_notifications', 'action' => ['POST' => 'dismiss']]);

        // Crisp Routes
        Router::map('crisp_enable', 'integrations/crisp/enable', ['controller' => 'crisp_integration', 'action' => ['POST' => 'enable_crisp'], 'integration_type' => 'crisp']);
        Router::map('crisp_disable', 'integrations/crisp/disable', ['controller' => 'crisp_integration', 'action' => ['POST' => 'disable_crisp'], 'integration_type' => 'crisp']);
        Router::map('crisp_notifications', 'integrations/crisp/notifications', ['controller' => 'crisp_integration', 'action' => ['GET' => 'notifications'], 'integration_type' => 'crisp']);
        Router::map('crisp_notification_enable', 'integrations/crisp/notification/:type/enable', ['controller' => 'crisp_integration', 'action' => ['POST' => 'enable_notification'], 'integration_type' => 'crisp']);
        Router::map('crisp_notification_disable', 'integrations/crisp/notification/:type/disable', ['controller' => 'crisp_integration', 'action' => ['POST' => 'disable_notification'], 'integration_type' => 'crisp']);
        Router::map('crisp_notification_dismiss', 'integrations/crisp/notification/:type/dismiss', ['controller' => 'crisp_integration', 'action' => ['POST' => 'dismiss_notification'], 'integration_type' => 'crisp']);
        Router::map('crisp_info_for_user', 'integrations/crisp/info-for-user', ['controller' => 'crisp_integration', 'action' => ['GET' => 'info_for_user'], 'integration_type' => 'crisp']);

        Router::map(
            'reactions',
            'reactions/:parent_type/:parent_id',
            [
                'action' => [
                        'POST' => 'add_reaction',
                        'DELETE' => 'remove_reaction',
                    ],
                'controller' => 'reactions',
                'module' => self::INJECT_INTO,
            ],
            [
                'parent_type' => Router::MATCH_SLUG,
                'parent_id' => Router::MATCH_ID,
            ]
        );

        Router::map(
            'reaction',
            'reactions/:reaction_id',
            [
                'module' => self::INJECT_INTO,
                'controller' => 'reactions',
                'action' => [],
            ],
            [
                'reaction_id' => Router::MATCH_ID,
            ]
        );
    }

    /**
     * Define sharing routes for given context.
     *
     * @param string $context
     * @param string $context_path
     * @param string $controller_name
     * @param string $module_name
     * @param array  $context_requirements
     */
    public function defineSharingRoutesFor(
        $context,
        $context_path,
        $controller_name,
        $module_name,
        $context_requirements = null
    )
    {
        Router::map(
            "{$context}_sharing_settings",
            "$context_path/sharing",
            [
                'controller' => $controller_name,
                'action' => "{$context}_sharing_settings",
                'module' => $module_name,
            ],
            $context_requirements
        );
    }

    /**
     * Define project template add routes for given context.
     *
     * @param string $context
     * @param string $context_path
     * @param string $controller_name
     * @param string $module_name
     * @param array  $context_requirements
     */
    public function defineTamplateRoutesFor(
        $context,
        $context_path,
        $controller_name,
        $module_name,
        $context_requirements = null
    )
    {
        Router::map(
            "{$context}_add",
            "$context_path/add",
            [
                'controller' => $controller_name,
                'action' => "{$context}_add",
                'module' => $module_name,
            ],
            $context_requirements
        );
        Router::map(
            "{$context}_edit",
            "$context_path/edit",
            [
                'controller' => $controller_name,
                'action' => "{$context}_edit",
                'module' => $module_name,
            ],
            $context_requirements
        );
    }

    /**
     * Define event handlers.
     */
    public function defineHandlers()
    {
        $this->listen('on_notification_inspector');

        $this->listen('on_rebuild_activity_logs');

        $this->listen('on_extra_stats');

        $this->listen('on_handle_public_subscribe');
        $this->listen('on_handle_public_unsubscribe');

        $this->listen('on_available_integrations');
        $this->listen('on_initial_settings');
        $this->listen('on_available_webhook_payload_transformators');

        $this->listen('on_visible_object_paths');
        $this->listen('on_trash_sections');
        $this->listen('on_search_filters');
        $this->listen('on_search_rebuild_index');
        $this->listen('on_user_access_search_filter');
        $this->listen('on_report_sections');
        $this->listen('on_reports');

        $this->listen('on_morning_mail');

        $this->listen('on_protected_config_options');
        $this->listen('on_history_field_renderers');

        $this->listen('on_company_created');
        $this->listen('on_moved_to_trash');
        $this->listen('on_restored_from_trash');

        $this->listen('on_daily_maintenance');
        $this->listen('on_reset_manager_states');
        $this->listen('on_user_invitation_accepted');

        $this->listen('on_resets_initial_settings_timestamp');

        $this->listen('on_smile_reaction_created', 'on_reaction_created');
        $this->listen('on_heart_reaction_created', 'on_reaction_created');
        $this->listen('on_thumbs_up_reaction_created', 'on_reaction_created');
        $this->listen('on_thumbs_down_reaction_created', 'on_reaction_created');
        $this->listen('on_thinking_reaction_created', 'on_reaction_created');
        $this->listen('on_applause_reaction_created', 'on_reaction_created');
        $this->listen('on_party_reaction_created', 'on_reaction_created');

        $this->listen('on_smile_reaction_deleted', 'on_reaction_deleted');
        $this->listen('on_heart_reaction_deleted', 'on_reaction_deleted');
        $this->listen('on_thumbs_up_reaction_deleted', 'on_reaction_deleted');
        $this->listen('on_thumbs_down_reaction_deleted', 'on_reaction_deleted');
        $this->listen('on_thinking_reaction_deleted', 'on_reaction_deleted');
        $this->listen('on_applause_reaction_deleted', 'on_reaction_deleted');
        $this->listen('on_party_reaction_deleted', 'on_reaction_deleted');
    }

    public function defineListeners(): array
    {
        return [

            // Listen for all data object life cycle events, capture ones that need to send a webhook, and prepare jobs
            // that will matching webhooks with the object payload.
            DataObjectLifeCycleEventInterface::class => AngieApplication::webhookDispatcher(),
        ];
    }
}
