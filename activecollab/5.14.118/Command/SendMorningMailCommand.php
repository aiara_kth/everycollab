<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

namespace ActiveCollab\Command;

use Angie\Command\Command;
use ConfigOptions;
use DateValue;
use MorningPaper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @package ActiveCollab\Command
 */
class SendMorningMailCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        parent::configure();

        $this->setDescription('Send morning mail.')
            ->addArgument('day', InputArgument::OPTIONAL, "Day at which the morning mail is supposed to be dispatched (e.g. 2015-09-29). Ommit to use today's date");
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $before_action_time = time();
        $output->writeln('<comment>Sending morning mail...</comment>');

        $day = $input->getArgument('day') ? DateValue::makeFromString($input->getArgument('day')) : DateValue::now();

        // Dispatch the emails
        MorningPaper::send($day);

        // Check if there are recent morning paper activty
        if ($last_mail_time = ConfigOptions::getValue('morning_paper_last_activity')) {
            if ($last_mail_time >= $before_action_time) {
                $output->writeln('<info>Morning paper email was dispatched successfully!</info>');
            }
        } else {
            $output->writeln('<error>Whops! Something went wrong while dispathing the Morning paper email!</error>');
        }
    }
}
