<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

namespace ActiveCollab\Command;

use Angie\Command\BillingCommand;
use FastSpringApiClient;
use FastSpringOrderRecorder;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RecordFastSpringOrderCommand extends BillingCommand
{
    protected function configure()
    {
        parent::configure();

        $this
            ->setDescription('Get order data from Fast Spring')
            ->addArgument('order_reference', InputOption::VALUE_REQUIRED, 'Order reference number')
            ->addOption('latest', '', InputOption::VALUE_NONE, 'Get Fast Spring details about order and save it in model');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($input->getOption('latest')) {
            $this->recordLatest();
        } else {
            if (!$input->getArgument('order_reference')) {
                throw new InvalidArgumentException('Order reference is required');
            }
        }
    }

    private function recordLatest()
    {
       (new FastSpringOrderRecorder(
            new FastSpringApiClient(
                FASTSPRING_PASSWORD,
                FASTSPRING_USERNAME,
                FASTSPRING_STORE_ID
            )
        ))->record(ON_DEMAND_FS_ORDER_REFERENCE);
    }
}
