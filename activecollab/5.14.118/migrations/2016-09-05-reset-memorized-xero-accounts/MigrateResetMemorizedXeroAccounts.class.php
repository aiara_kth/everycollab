<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * @package ActiveCollab.migrations
 */
class MigrateResetMemorizedXeroAccounts extends AngieModelMigration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        AngieApplication::memories()->forget('xero_accounts');
    }
}
