<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

class MigrateAddUserPasswordSetAtMemories extends AngieModelMigration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        if (AngieApplication::isOnDemand()) {

            $owner_id = $this->executeFirstCell('SELECT id FROM users WHERE type = ? AND is_archived = ? AND is_trashed = ? ORDER BY id LIMIT 0, 1', Owner::class, false, false);
            $user = $this->executeFirstRow('SELECT first_login_on, last_login_on, email FROM users WHERE `id` = ?', $owner_id);

            $first_login_at = $user['first_login_on'];

            $first_login_at = (new DateTimeValue($first_login_at))->getTimestamp();

            if ($memories = $this->executeFirstCell('SELECT `updated_on` FROM `memories` WHERE `key` = ? AND `value` LIKE ? ', 'password_set_in_wizard', '%b:1;%')) {
                $updated_on = (new DateTimeValue($memories))->getTimestamp();

                $this->execute('REPLACE INTO memories (`key`, `value`, `updated_on`) VALUES (?, ?, UTC_TIMESTAMP()), (?, ?, UTC_TIMESTAMP()), (?, ?, UTC_TIMESTAMP()), (?, ?, UTC_TIMESTAMP())',
                    'password_set_at', serialize($updated_on),
                    'granted_access_at', serialize($updated_on),
                    'owner_has_random_password', serialize(true),
                    'password_set_wizard_shown_at', serialize($first_login_at)
                );
            } elseif (!empty($first_login_at)) {
                if (AngieApplication::activeCollabId()->isUserPasswordGenerated($user['email'])) {
                    $this->execute('REPLACE INTO memories (`key`, `value`, `updated_on`) VALUES (?, ?, UTC_TIMESTAMP()), (?, ?, UTC_TIMESTAMP())',
                        'owner_has_random_password', serialize(true),
                        'password_set_wizard_shown_at', serialize($first_login_at)
                    );
                } else {
                    $this->execute('REPLACE INTO memories (`key`, `value`, `updated_on`) VALUES (?, ?, UTC_TIMESTAMP()), (?, ?, UTC_TIMESTAMP())',
                        'owner_has_random_password', serialize(false),
                        'granted_access_at', serialize($first_login_at)
                    );
                }
            }
        }
    }
}
