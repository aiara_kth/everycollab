<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

/**
 * Add default accounting app config option.
 *
 * @package activeCollab.modules.system
 */
class MigrateAddDefaultTimeReportConfigOption extends AngieModelMigration
{
    /**
     * Migrate up.
     */
    public function up()
    {
        $this->addConfigOption('time_report_mode', 'time_tracking');
    }
}
