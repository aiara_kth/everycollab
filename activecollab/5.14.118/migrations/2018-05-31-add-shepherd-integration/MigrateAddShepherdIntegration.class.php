<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */
use ActiveCollab\Module\OnDemand\Utils\ShepherdIntegration\ShepherdIntegration;
use ActiveCollab\Module\OnDemand\Utils\ShepherdIntegration\ShepherdWebhook;

/**
 * @package ActiveCollab.migrations
 */
class MigrateAddShepherdIntegration extends AngieModelMigration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        /** @var ShepherdIntegration $integration */
        $integration = Integrations::findFirstByType(ShepherdIntegration::class, false);

        if (AngieApplication::isOnDemand() && $integration && !$integration->isInUse()) {
            $integration->initialize();
        }
    }
}
