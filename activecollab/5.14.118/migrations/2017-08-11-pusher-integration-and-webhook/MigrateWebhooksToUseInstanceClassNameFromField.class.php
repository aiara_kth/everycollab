<?php

/*
 * This file is part of the Active Collab project.
 *
 * (c) A51 doo <info@activecollab.com>. All rights reserved.
 */

class MigrateWebhooksToUseInstanceClassNameFromField extends AngieModelMigration
{
    public function up()
    {
        $migrations = $this->useTableForAlter('webhooks');

        if (!$migrations->getColumn('type')) {
            $migrations->addColumn(DBTypeColumn::create('Webhook'), 'id');
        }

        $this->execute('UPDATE `webhooks` SET `type` = ?', 'Webhook');

        $this->doneUsingTables();
    }
}
