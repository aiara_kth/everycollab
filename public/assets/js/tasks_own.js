console.log('ajax start');
var checksum = $('#tasks_own_load_check');

var url = window.location.href;
var my_work_page = false;
if(url.indexOf('my-work') > - 1){
    my_work_page = true;
}

function c_replaceAll(str, search, replace) {
    return str.split(search).join(replace);
}


if(checksum.val() == 1 && my_work_page){

    $.ajax({
        url : '/backbroker/get_work_tasks_own.php',
        type: 'POST',
        data: {id: send_project_user_id},
        async: false,
        success: function(data){
            console.log(data);
            $('#work_tasks_own_wait').hide();
            if(data.trim() == null || data.trim() == ''){
                $('#work_tasks_own_none').show();
            }else{
                $('#work_tasks_own_place').append(data);
                $('#work_tasks_own_place').show();
            }
        },error: function(e){
            console.log(e);
        }

    });
    console.log('ajax end');

    $('.work_tasks_own_add_btn').click(function(){
        $('form[name=new_task_own_form]').show();
    });

    $('.work_tasks_own_add_close').click(function(){
        $('form[name=new_task_own_form]').hide();
    });


    function insert_task_own(){
        $.ajax({
            url: '/backbroker/tasks_own_handler.php',
            type: 'POST',
            data: {
                type: 'add',
                userid: send_project_user_id,
                name: $('#tasks_own_name').val(),
                body: $('#tasks_own_body').val()
            },success : function(data){
                var task_id = data.trim();
                if(task_id < 0 || task_id == 'fail'){
                    location.reload();
                }

                var check_count = $('.my_task_own_groupp').length;

                console.log(check_count);
                if(check_count < 1){
                    $('#work_tasks_own_none').hide();
                    $('#work_tasks_own_place').show();
                }
                var html = '    <div class="profile_page_section_group my_task_own_groupp" ng-repeat="my_task_own_groupp in my_tasks_own" data-id="' + task_id + '" id="task_own_total_' + task_id + '">' +
                    '        <div style="float: left; display:inline-block; height: 28px;">' +
                    '            <div style="    margin-top: 6px;">' +
                    '               <span class="icon icon_unchecked_grey size_14" id="task_own_checkbox_' + task_id + '" data-id="'+ task_id +'" data-name="' + $('#tasks_own_name').val() +'" onclick="task_own_check_proc(this)" ></span>' +
                    '               <span id="task_own_loading_' + task_id + '" class="loader size_16 grey" style=" display: none; ">' +
                    '                   <div class="rect1"></div>' +
                    '                   <div class="rect2"></div>' +
                    '                   <div class="rect3"></div>' +
                    '               </span>' +
                    '           </div>' +
                    '        </div>' +
                    '        <div style="width: 50px;"></div>' +
                    '        <div class="profile_page_section_group_content" style="margin-left: 10px; padding-left: 15px;height: 28px; padding-top: 3px;">' +
                    '            <div ng-repeat="task_list_group in project_group.task_lists" class="task_list_group">' +
                    '                <div class="task_list subtask_list" subtasks="task_list_group.subtasks">' +
                    '                    <div class="tasks" id="task_own_name_' + task_id + '" style="display: inline-block;">' + $('#tasks_own_name').val() +
                    '                    </div>' +
                    '                    <div class="tasks_delete" style="display: inline-block; float:right; margin-right: 20px;     margin-top: -6px;">' +
                    '                        <button type="button" class="tasks_delete_button" data-id="' + task_id +'" style="    height: 11px;" onclick="tasks_own_delete_proc(this)">×</button>' +
                    '                    </div>' +
                    '                    <span class="sort_tasks_own_btn reorder_handle icon icon_reorder_black ui-sortable-handle"></span>' +
                    '                </div>' +
                    '            </div>' +
                    '        </div>' +
                    '    </div>';
                $('#tasks_own_name').val('');
                $('#work_tasks_own_place').prepend(html);
            }, error: function() {
                alert('추가하기를 실패했습니다.');
                location.reload();
            }

        })
    }

    function task_own_check_proc(check){
        var id = $(check).attr('data-id');
        var name = $(check).attr('data-name');
        var check_val = 0;
        var check_word = '';

        $(check).hide();
        $('#task_own_loading_' + id).show();

        if($(check).hasClass('icon_unchecked_grey')){
            check_val = 1;
        }else{
            check_val = 0;
        }

        console.log(id);
        console.log(check_val);

        $.ajax({
            url: '/backbroker/tasks_own_handler.php',
            type: 'POST',
            data: {
                type: 'check',
                id: id,
                check : check_val
            },success : function(data){
                console.log(data);
                if(check_val == 1){
                    $(check).removeClass('icon_unchecked_grey').addClass('icon_check_black');
                    $('#task_own_name_'+id).empty().append('<del>' + name +'</del>');
                }else{
                    $(check).removeClass('icon_check_black').addClass('icon_unchecked_grey');
                    $('#task_own_name_'+id).empty().append(name);
                }
                setTimeout(function(){$('#task_own_loading_' + id).hide();$(check).show();}, 300);
                // location.reload();
            }, error: function() {
                alert('체크처리를 실패했습니다.');
                location.reload();
            }

        })
    }

    function tasks_own_delete_proc(input){
        var id = $(input).attr('data-id');

        console.log(id);

        $.ajax({
            url: '/backbroker/tasks_own_handler.php',
            type: 'POST',
            data: {
                type: 'delete',
                id: id,
            },success : function(data){
                console.log(data);
                // alert('삭제되었습니다.');
                $('#task_own_total_'+id).remove();
                var check_count = $('.my_task_own_groupp').length;

                if( check_count < 1){
                    $('#work_tasks_own_none').show();
                    $('#work_tasks_own_place').hide();
                }
            }, error: function() {
                alert('체크처리를 실패했습니다.');
                location.reload();
            }
        })

    }

    $('#work_tasks_own_place').sortable({
        handle: ".sort_tasks_own_btn",
        cancel: "",
        placeholder: "my_task_own_placeholder",
        update: function(event, ui) {
            var ids = c_replaceAll($(this).sortable('toArray').toString(),"task_own_total_","");
            console.log(ids);
            $.ajax({
                url: '/backbroker/tasks_own_handler.php',
                type: 'POST',
                data: {
                    type : "sort",
                    userid : send_project_user_id,
                    ids: ids,
                },success : function(data){
                    console.log(data);
                    console.log('tasks_own, sort ok');
                },error: function(e){
                    console.log(e);
                    console.log('tasks_own, sort error');
                }
            })
        }
    });

}else{
    console.log('ajax wait');
    checksum.val(1);
}


