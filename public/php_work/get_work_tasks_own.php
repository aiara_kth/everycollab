<?php
include_once 'broker_dbconn.php';
$conn = new broker_dbconn();
$id = $_REQUEST['id'];

$data = $conn->get_tasks_own($id);


foreach ($data as $tk){
    ?>
    <div class="profile_page_section_group my_task_own_groupp" ng-repeat="my_task_own_groupp in my_tasks_own" data-id="<?=$tk['id']?>" id="task_own_total_<?=$tk['id']?>">
        <div style="float: left; display:inline-block; height: 28px">
            <div style="    margin-top: 6px;">
                <span class="icon <?=$tk['is_checked'] == 1 ? 'icon_check_black' : 'icon_unchecked_grey'?> size_14" id="task_own_checkbox_<?=$tk['id']?>" data-id="<?=$tk['id']?>" data-name="<?=$tk['name']?>" onclick="task_own_check_proc(this)" ></span>
                <span id="task_own_loading_<?=$tk['id']?>" class="loader size_16 grey" style=" display: none; ">
                    <div class="rect1"></div>
                    <div class="rect2"></div>
                    <div class="rect3"></div>
                </span>
            </div>
        </div>
        <div style="width: 50px;"></div>
        <div class="profile_page_section_group_content" style="margin-left: 10px; padding-left: 15px; height: 28px; padding-top: 3px;">
            <div ng-repeat="task_list_group in project_group.task_lists" class="task_list_group">
                <div class="task_list subtask_list" subtasks="task_list_group.subtasks">
                    <div class="tasks" id="task_own_name_<?=$tk['id']?>" style="display: inline-block;">
                        <?php
                            if($tk['is_checked'] == 1){
                                echo '<del>'.$tk['name'].'</del>';
                            }else{
                                echo $tk['name'];
                            }
                        ?>
                    </div>
                    <div class="tasks_delete" style="display: inline-block; float:right; margin-right: 20px; margin-top: -6px;">
                        <button type="button" class="tasks_delete_button" data-id="<?=$tk['id']?>" style="    height: 11px;" onclick="tasks_own_delete_proc(this)">×</button>
                    </div>
                    <span class="sort_tasks_own_btn reorder_handle icon icon_reorder_black ui-sortable-handle"></span>
                </div>
            </div>
        </div>
    </div>
    <?php
}