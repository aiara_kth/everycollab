<?php
include_once 'broker_dbconn.php';
$conn = new broker_dbconn();
$type = $_REQUEST['type'];

switch ($type){
    case 'add' :
        $userid = $_REQUEST['userid'];
        $name = $_REQUEST['name'];
        $body = $_REQUEST['body'];
        $lastnumber = $conn->get_last_tasks_number($userid) + 1;

        if($conn->add_tasks_own($userid,$name,$body,$lastnumber)){
            $data = $conn->get_tasks_own($id);
//            $count = count($data);
            $ids = Array();
            foreach ($data as $dd){
                array_push($ids,$dd['id']);
            }

            $count = 1;
            foreach ($ids as $id){
                $conn->update_tasks_own_task_number($userid,$id,$count);
                $count++;
            }

            $last_id = $conn->get_last_tasks_id($userid);
            echo $last_id;
        }else{
            echo 'fail';
        }
        break;
    case 'update' :
        $userid = $_REQUEST['userid'];
        $name = $_REQUEST['name'];
        $body = $_REQUEST['body'];
        if($conn->update_tasks_own($userid,$name,$body)){
            echo 'ok!';
        }else{
            echo 'fail';
        }
        break;
    case 'delete' :
        $taskid = $_REQUEST['id'];
        $conn->delete_tasks_own($taskid);
        break;
    case 'check' :
        $taskid = $_REQUEST['id'];
        $check_val = $_REQUEST['check'];
        $conn->check_tasks_own($taskid, $check_val);
        break;
    case 'sort' :
        $userid = $_REQUEST['userid'];
        $ids = explode(',',$_REQUEST['ids']);

        $count = count($ids);
        foreach ($ids as $id){
            echo '<' . $id . '/' . $count . '>';
            $conn->update_tasks_own_task_number($userid,$id,$count);
            $count--;
        }

        break;
    case 'noti' :
        //TODO: To-Do 알람기능도 필요한지??
        break;
    default:
        break;
}