<?php
require_once  'broker_config.php';

class broker_dbconn {

    private $conn;
    private $host = DB_HOST;
    private $database = DB_NAME;
    private $username = DB_USER;
    private $password = DB_PASS;
    private $error_message;
    private $contents1 = Array();
    private $contents2 = Array();
    private $contents3 = Array();

    public function __construct()
    {
        $dsn = 'mysql:host=' . $this->host . ';port=3306;dbname=' . $this->database . ';charset=utf8';

        $options = array(
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        );

        try {
            $this->conn = new PDO($dsn, $this->username, $this->password, $options);
        } catch (PDOException $e) {
            $this->error_message = $e->getMessage();
        }
    }

    public function __destruct()
    {
        // TODO: Implement __destruct() method.
    }

    public function intl_output_error(){
        return $this->error_message;
    }


    public function test(){
        try{
            $stmt = $this->conn->prepare("select * from  access_logs limit 0,2");
            $stmt->execute();
            $this->contents1 = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $this->contents1;
        }catch (PDOException $e){
            echo $e->getMessage();
            return false;
        }
    }

    public function get_tasks_own($id){
        try{
            $stmt = $this->conn->prepare("select * from  tasks_own where (is_delete is null or is_delete != 1) and delegated_by_id = :user_id order by task_number DESC");
            $stmt->bindParam('user_id',$id);
            $stmt->execute();
            $this->contents1 = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $this->contents1;
        }catch (PDOException $e){
            echo $e->getMessage();
            return false;
        }
    }

    public function get_last_tasks_number($id){
        try{
            $stmt = $this->conn->prepare("select * from  tasks_own where (is_delete is null or is_delete != 1) and delegated_by_id = :user_id ORDER BY task_number DESC limit 1");
            $stmt->bindParam('user_id',$id);
            $stmt->execute();
            $this->contents1 = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return ($this->contents1[0]['task_number'] == null ? 1 : intval($this->contents1[0]['task_number']) + 1);
        }catch (PDOException $e){
            echo $e->getMessage();
            return false;
        }
    }

    public function get_last_tasks_id($id){
        try{
            $stmt = $this->conn->prepare("select id from  tasks_own where  delegated_by_id = :user_id order by id desc limit 1");
            $stmt->bindParam('user_id',$id);
            $stmt->execute();
            $this->contents1 = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $this->contents1[0]['id'];
        }catch (PDOException $e){
            echo $e->getMessage();
            return -1 ;
        }
    }


    public function add_tasks_own($id,$name,$body,$task_number){
        $dummy_num = 0;
        $date = date('Y-m-d H:i:s');
        $query = 'insert into tasks_own (id,project_id,task_number,task_list_id,assignee_id,delegated_by_id,created_from_recurring_task_id,name,body,is_important,created_on,job_type_id,estimate,position,is_hidden_from_clients,is_trashed,original_is_trashed,trashed_by_id,created_from_discussion_id) 
                                VALUES(DEFAULT ,:project_id,:task_number,:task_list_id,:assignee_id,:user_id,0,:name,:body,0,:date,0,0.00,0,0,0,0,0,0)';
        try{
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam('project_id',$id);
            $stmt->bindParam('task_number',$task_number);
            $stmt->bindParam('task_list_id',$dummy_num);
            $stmt->bindParam('assignee_id',$id);
            $stmt->bindParam('user_id',$id);
            $stmt->bindParam('name',$name);
            $stmt->bindParam('body',$body);
            $stmt->bindParam('date',$date);
            $stmt->execute();
            return true;
        }catch (PDOException $e){
            echo $e->getMessage();
            return false;
        }
    }

    public function update_tasks_own($id,$name,$body){
        $query = 'UPDATE tasks_own set name = :name, body = :body where id = :taskid';
        try{
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam('taskid',$id);
            $stmt->bindParam('name',$name);
            $stmt->bindParam('body',$body);
            $stmt->execute();
            return true;
        }catch (PDOException $e){
            echo $e->getMessage();
            return false;
        }
    }

    public function delete_tasks_own($id){
        $query = 'UPDATE tasks_own set is_delete = 1 where id = :taskid';
        try{
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam('taskid',$id);
            $stmt->execute();
            return true;
        }catch (PDOException $e){
            echo $e->getMessage();
            return false;
        }
    }

    public function check_tasks_own($id,$check_value){
        $query = 'UPDATE tasks_own set is_checked =:check_value where id = :taskid';
        try{
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam('taskid',$id);
            $stmt->bindParam('check_value',$check_value);
            $stmt->execute();
            return true;
        }catch (PDOException $e){
            echo $e->getMessage();
            return false;
        }
    }

    public function update_tasks_own_task_number($userid,$id,$key){
        $query = 'UPDATE tasks_own set task_number = :task_key where id = :task_id and delegated_by_id = :user_id ';
        try{
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam('user_id',$userid);
            $stmt->bindParam('task_id',$id);
            $stmt->bindParam('task_key',$key);
            $stmt->execute();
            return true;
        }catch (PDOException $e){
            echo $e->getMessage();
            return false;
        }
    }

    public function check_tasks_completed($id){
        $query = "SELECT name,completed_on from tasks where id = :task_id";
        try{
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam('task_id',$id);
            $stmt->execute();
            $this->contents1 = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $this->contents1[0];
        }catch (PDOException $e){
            echo $e->getMessage();
            return false;
        }

    }

}




